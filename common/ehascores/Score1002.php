<?php

namespace common\ehascores;

use common\models\Assessment;

class Score1002 extends BaseScore
{

    public function getTotalSelectP()
    {
        return 2;
    }

    public function getDefaultAssessStep()
    {
        return ['P1', 'P2'];
    }


    public function calculate()
    {
        $P = $this->getTotalSelectP();
        $totalP = $this->getPs($this->appFormID);
        $B = $totalP / $P;
        $percentB = ($B * $this->fullMarks6) / 100;

        $sopIdC1Codes = [1002700101, 1002700102];
        $sopIdC2Codes = [1002700201, 1002700202];
        $c1 = $this->getC($this->appFormID, $this->issueCode, $sopIdC1Codes);
        $c2 = $this->getC($this->appFormID, $this->issueCode, $sopIdC2Codes);
        $C = @($c2 / $c1) * 100;
        $percentC = $C * 100 / 100;
        $D = (($B + $C) / 200) * 100;
        $totalFullMarks = $this->fullMarks6 + $this->fullMarks7;
        $percentE = (($B + $C) / $totalFullMarks * 100);

        $lpa = $this->getLpa($this->localityCode, $this->year - 1);
        if ($lpa !== null) {
            $percentA = $lpa->component1to5_avgscore;
        } else {
            $percentA = 0;
        }
        $assess_result = $this->getCertificate($percentA, $percentB, $percentC, $percentE);
        $this->saveAssessmentConclusion([
            'be_year' => $this->year,
            'type' => $this->type,
            'issue_code' => $this->issueCode,
            'locality_code' => $this->localityCode,
            'component1_score' => isset($lpa->component1_score) ? $lpa->component1_score : null,
            'component2_score' => isset($lpa->component2_score) ? $lpa->component2_score : null,
            'component3_score' => isset($lpa->component3_score) ? $lpa->component3_score : null,
            'component4_score' => isset($lpa->component4_score) ? $lpa->component4_score : null,
            'component5_score' => isset($lpa->component5_score) ? $lpa->component5_score : null,
            'component1to5_avgscore' => isset($lpa->component1to5_avgscore) ? $lpa->component1to5_avgscore : null,
            'component6_score' => $percentB,
            'component7_score' => $percentC,
            'component6to7_avgscore' => ($percentB + $percentC) / 2,
            'component1to7_avgscore' => ($lpa->component1_score + $lpa->component2_score + $lpa->component3_score + $lpa->component4_score + $lpa->component5_score + $percentB + $percentC) / 7,
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
            'application_form_id' => $this->appFormID,
        ]);

        return [
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result
        ];
    }
}
