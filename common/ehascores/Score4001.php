<?php

namespace common\ehascores;


use common\models\Assessment;
use yii\helpers\VarDumper;
class Score4001 extends BaseScore
{
    public function calculate()
    {
        $B = $this->getPs($this->appFormID);
        $percentB = ($B * $this->fullMarks6) / 100;

        $hasA01 = $this->getC($this->appFormID, $this->issueCode, [4001700111]);
        $hasB01 = $this->getC($this->appFormID, $this->issueCode, [4001700112]);
        $total1 = $hasA01 > 0 ? ($hasB01 * 5) / $hasA01 : 0;

        $hasA04 = $this->getC($this->appFormID, $this->issueCode, [4001700121]);
        $hasB04 = $this->getC($this->appFormID, $this->issueCode, [4001700122]);
        $total2 = $hasA04 > 0 ? ($hasB04 * 15) / $hasA04 : 0;

        $sopIdC1Codes = [4001700130, 4001700201, 4001700202, 4001700203,4001700301,4001700302,4001700303];
        $all = $this->getC($this->appFormID, $this->issueCode, $sopIdC1Codes);

        $C = $total1 + $total2 + $all;

        $percentC = ($C * $this->fullMarks7) / 100;
        $D = $B + $C;
        $totalFullMarks = $this->fullMarks6 + $this->fullMarks7;
        $percentE = (($D * 100) / $totalFullMarks);

        $lpa = $this->getLpa($this->localityCode, $this->year - 1);
    
        if ($lpa !== null) {
            $percentA = $lpa->component1to5_avgscore;
        } else {
            $percentA = 0;
        }
        $assess_result = $this->getCertificate($percentA, $percentB, $percentC, $percentE);
        $this->saveAssessmentConclusion([
            'be_year' => $this->year,
            'type' => $this->type,
            'issue_code' => $this->issueCode,
            'locality_code' => $this->localityCode,
            'component1_score' => $lpa->component1_score,
            'component2_score' => $lpa->component2_score,
            'component3_score' => $lpa->component3_score,
            'component4_score' => $lpa->component4_score,
            'component5_score' => $lpa->component5_score,
            'component1to5_avgscore' => $lpa->component1to5_avgscore,
            'component6_score' => $percentB,
            'component7_score' => $percentC,
            'component6to7_avgscore' => ($percentB + $percentC) / 2,
            'component1to7_avgscore' => ($lpa->component1_score + $lpa->component2_score + $lpa->component3_score + $lpa->component4_score + $lpa->component5_score + $percentB + $percentC) / 7,
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
            'application_form_id' => $this->appFormID,
        ]);

        return [
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
        ];
    }
}
