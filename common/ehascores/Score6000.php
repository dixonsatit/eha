<?php

namespace common\ehascores;

class Score6000 extends BaseScore
{
    public function calculate()
    {
        $P = $this->getTotalSelectP();
        $B = $this->getPs($this->appFormID);
        $percentB = ($B * $this->fullMarks6) / 100;

        // กรณีทีกรอกค่า 1 จะเป็นฟอร์มกรณีที่ 1 ถ้า 2 จะเป็นกรณีที่ 2 ที่จะนำมาคำนวณ องค์ประกอบที่ 7
        $type = $this->getAssessmentByID($this->appFormID, $this->issueCode, 6000700000);
        if ($type !== null && $type->selfassess_score == 1) {
            $sopIdC1Codes = [6000710101, 6000710102, 6000710103];
            $C = $this->getC($this->appFormID, $this->issueCode, $sopIdC1Codes);
            $percentC = ($C * $this->fullMarks7) / 100;
        } else if ($type !== null && $type->selfassess_score == 2) {
            $sopIdC1Codes = [6000720210, 6000720220, 6000720230];
            $c1 = $this->getC($this->appFormID, $this->issueCode, $sopIdC1Codes);

            $ca = $this->getC($this->appFormID, $this->issueCode, [6000720241]);
            $cb = $this->getC($this->appFormID, $this->issueCode, [6000720242]);
            $cc = $this->getC($this->appFormID, $this->issueCode, [6000720243]);
            $c2 = $ca > 0 ? ((($cb + $cc)  / $ca) * 60) : 0;
            $C = $c2 + $c1;
            $percentC = ($C * $this->fullMarks7) / 100;
        } else {
            $C = 0;
            $percentC = 0;
        }

        $D = $B + $C;
        $totalFullMarks = $this->fullMarks6 + $this->fullMarks7;
        $percentE = (($D * 100) / $totalFullMarks);

        $lpa = $this->getLpa($this->localityCode, $this->year - 1);
        if ($lpa !== null) {
            $percentA = $lpa->component1to5_avgscore;
        } else {
            $percentA = 0;
        }
        $assess_result = $this->getCertificate($percentA, $percentB, $percentC, $percentE);
        $this->saveAssessmentConclusion([
            'be_year' => $this->year,
            'type' => $this->type,
            'issue_code' => $this->issueCode,
            'locality_code' => $this->localityCode,
            'component1_score' => $lpa->component1_score,
            'component2_score' => $lpa->component2_score,
            'component3_score' => $lpa->component3_score,
            'component4_score' => $lpa->component4_score,
            'component5_score' => $lpa->component5_score,
            'component1to5_avgscore' => $lpa->component1to5_avgscore,
            'component6_score' => $percentB,
            'component7_score' => $percentC,
            'component6to7_avgscore' => ($percentB + $percentC) / 2,
            'component1to7_avgscore' => ($lpa->component1_score + $lpa->component2_score + $lpa->component3_score + $lpa->component4_score + $lpa->component5_score + $percentB + $percentC) / 7,
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
            'application_form_id' => $this->appFormID,
        ]);

        return [
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
        ];
    }
}
