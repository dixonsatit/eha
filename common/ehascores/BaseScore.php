<?php

namespace common\ehascores;

use backend\models\LpaScore;
use common\models\Assessment;
use common\models\AssessmentConclusion;
use yii\base\Model;
use yii\helpers\VarDumper;

class BaseScore extends Model
{
    public $appFormID;
    public $localityCode;
    public $year;
    public $fullMarks6 = 100;
    public $fullMarks7 = 100;
    public $issueCode;
    public $paramsA;
    public $paramsB;
    public $paramsC;
    public $paramsD;
    public $paramsE;
    public $type = 'selfassess';
    public $assessStep = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['type', 'paramsA', 'paramsB', 'paramsC', 'paramsD', 'paramsE', 'appFormID'], 'integer'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'paramsA' => 'A',
            'paramsB' => 'B',
            'paramsC' => 'C',
            'paramsD' => 'D',
            'paramsE' => 'E',
        ];
    }

    public function getDefaultAssessStep()
    {
        return ['P1', 'P2', 'P3'];
    }

    public function getTotalSelectP()
    {
        return 3;
    }

    public function getAssessStep()
    {
        return count($this->assessStep) === 0 ? $this->getDefaultAssessStep() : $this->assessStep;
    }

    public function getLpa($locality_code, $year)
    {
        return LpaScore::find()->where([
            'be_year' => $year,
            'locality_code' => $locality_code,
        ])->one();
    }

    public function getPs($appFormID, $component_code = 6)
    {
        return Assessment::find()
            ->joinWith(['sop'])
            ->where(['application_form_id' => $appFormID])
            ->andWhere(['not', ['sop.parent_id' => null]])
            ->andWhere(['sop.component_code' => $component_code, 'issue_code' => $this->issueCode])
            ->sum($this->type == 'assessor' ? 'assessor_score' : 'selfassess_score');
    }

    public function getSopPartType($appFormID, $p='P1', $component_code = 6)
    {
        return Assessment::find()
            ->joinWith(['sop'])
            ->where(['application_form_id' => $appFormID])
            ->andWhere(['not', ['sop.parent_id' => null]])
            ->andWhere(['sop.component_code' => $component_code, 'issue_code' => $this->issueCode])
            ->andWhere(['sop.component_part' => $p])
            ->sum($this->type == 'assessor' ? 'assessor_score' : 'selfassess_score');
    }



    public function getC($appFormID, $issueCode, $sopId = [], $component_code = 7)
    {
        return Assessment::find()
            ->joinWith(['sop'])
            ->where(['application_form_id' => $appFormID])
            ->andWhere(['not', ['sop.parent_id' => null]])
            ->andWhere([
                'sop.component_code' => $component_code,
                'issue_code' => $issueCode,
                'sop.id' => $sopId,
            ])
            ->sum($this->type == 'assessor' ? 'assessor_score' : 'selfassess_score');
    }

    public function getAssessmentByID($appFormID, $issueCode, $sopId, $component_code = 7)
    {
        return Assessment::find()
            ->joinWith(['sop'])
            ->where(['application_form_id' => $appFormID])
            ->andWhere(['not', ['sop.parent_id' => null]])
            ->andWhere([
                'sop.component_code' => $component_code,
                'issue_code' => $issueCode,
                'sop.id' => $sopId,
            ])
            ->one();
    }

    public function getCertificate($a, $b, $c)
    {
        if ($a >= 80 && $b >= 80 && $c >= 80) {
            return '6';
        } else if ($a >= 60 && $b >= 60 && $c >= 60) {
            return '5';
        } else {
            return '3';
        }
    }

    public function calculate()
    {
        $P = $this->getTotalSelectP();
        $totalP = $this->getPs($this->appFormID);
        $B = $totalP / $P;
        $percentB = (100 / $this->fullMarks6) * $B;

        $sopIdC1Codes = [100117111, 100117112, 100117121];
        $sopIdC2Codes = [100117122, 100117131, 100117132];
        $c1 = $this->getC($this->appFormID, $this->issueCode, $sopIdC1Codes);
        $c2 = $this->getC($this->appFormID, $this->issueCode, $sopIdC2Codes);

        $C = ($c1 > 0 && $c2 > 0) ? $c2 / $c1 : 0;
        $percentC = $C * 100;
        $D = $B + $C;
        $totalFullMarks = $this->fullMarks6 + $this->fullMarks7;
        $percentE = (($D * 100) / $totalFullMarks);

        $lpa = $this->getLpa($this->localityCode, $this->year - 1);
        if ($lpa !== null) {
            $percentA = $lpa->component1to5_avgscore;
        } else {
            $percentA = 0;
        }
        $assess_result = $this->getCertificate($percentA, $percentB, $percentC, $percentE);
        $this->saveAssessmentConclusion([
            'be_year' => $this->year,
            'issue_code' => $this->issueCode,
            'locality_code' => $this->localityCode,
            'component1_score' => $lpa->component1_score,
            'component2_score' => $lpa->component2_score,
            'component3_score' => $lpa->component3_score,
            'component4_score' => $lpa->component4_score,
            'component5_score' => $lpa->component5_score,
            'component1to5_avgscore' => $lpa->component1to5_avgscore,
            'component6_score' => $percentB,
            'component7_score' => $percentC,
            'component6to7_avgscore' => ($percentB + $percentC) / 2,
            'component1to7_avgscore' => ($lpa->component1_score + $lpa->component2_score + $lpa->component3_score + $lpa->component4_score + $lpa->component5_score + $percentB + $percentC) / 7,
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result,
            'application_form_id' => $this->appFormID,
        ]);

        return [
            'b' => $B,
            'c' => $C,
            'd' => $D,
            'percentA' => $percentA,
            'percentB' => $percentB,
            'percentC' => $percentC,
            'percentE' => $percentE,
            'assess_result' => $assess_result
        ];
    }

    public function saveAssessmentConclusion($data)
    {
        $model = AssessmentConclusion::find()->where([
            'be_year' => $data['be_year'],
            'issue_code' => $data['issue_code'],
            'locality_code' => $data['locality_code'],
            'type' => $data['type']
        ])->one();
        if ($model === null) {
            $model = new AssessmentConclusion();
        }
         
        $model->attributes = $data;
        if($model->save()){
            return true;
        }else{
            VarDumper::dump($model->getErrors(), 10, true);
        };
    }
}
