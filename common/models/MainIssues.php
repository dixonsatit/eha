<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_issues".
 *
 * @property int $id เลขที่
 * @property string $main_issue_code รหัสประเด็นหลัก
 * @property string $main_issue_desc ประเด็นหลัก
 *
 * @property Issues[] $issues
 */
class MainIssues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_issues';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_issue_code'], 'required'],
            [['main_issue_code'], 'string', 'max' => 2],
            [['main_issue_desc'], 'string', 'max' => 255],
            [['main_issue_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'main_issue_code' => Yii::t('common', 'รหัสประเด็นหลัก'),
            'main_issue_desc' => Yii::t('common', 'ประเด็นหลัก'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssues()
    {
        return $this->hasMany(Issues::className(), ['main_issue_code' => 'main_issue_code']);
    }
}
