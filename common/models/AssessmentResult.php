<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "assessment_result".
 *
 * @property int $id เลขที่
 * @property string $result_code รหัสระดับผลการประเมิน
 * @property string $result_desc ระดับผลการประเมิน
 *
 * @property Assessment[] $assessments
 */
class AssessmentResult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assessment_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['result_code', 'result_desc'], 'required'],
            [['result_code'], 'string', 'max' => 1],
            [['result_desc'], 'string', 'max' => 100],
            [['result_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'result_code' => Yii::t('common', 'รหัสระดับผลการประเมิน'),
            'result_desc' => Yii::t('common', 'ระดับผลการประเมิน'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessments()
    {
        return $this->hasMany(Assessment::className(), ['assess_status' => 'result_code']);
    }
}
