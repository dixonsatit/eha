<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locality_type".
 *
 * @property int $id เลขที่
 * @property string $type_code รหัสประเภท อปท.
 * @property string $type_name ประเภท อปท.
 *
 * @property Locality[] $localities
 */
class LocalityType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locality_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_code', 'type_name'], 'required'],
            [['type_code'], 'string', 'max' => 1],
            [['type_name'], 'string', 'max' => 100],
            [['type_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'type_code' => Yii::t('common', 'รหัสประเภท อปท.'),
            'type_name' => Yii::t('common', 'ประเภท อปท.'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalities()
    {
        return $this->hasMany(Locality::className(), ['type_code' => 'type_code']);
    }
}
