<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "assessment_conclusion".
 *
 * @property int $id เลขที่
 * @property int $be_year ปีงบประมาณ (พ.ศ.)
 * @property string $issue_code
 * @property string $locality_code รหัส อปท.
 * @property string $component1_score คะแนนองค์ประกอบด้านที่ 1
 * @property string $component2_score คะแนนองค์ประกอบด้านที่ 2
 * @property string $component3_score คะแนนองค์ประกอบด้านที่ 3
 * @property string $component4_score คะแนนองค์ประกอบด้านที่ 4
 * @property string $component5_score คะแนนองค์ประกอบด้านที่ 5
 * @property string $component1to5_avgscore คะแนนเฉลี่ยองค์ประกอบด้านที่ 1-5
 * @property string $component6_score คะแนนองค์ประกอบด้านที่ 2
 * @property string $component7_score คะแนนองค์ประกอบด้านที่ 2
 * @property string $component6to7_avgscore คะแนนเฉลี่ยองค์ประกอบด้านที่ 6-7
 * @property string $component1to7_avgscore คะแนนเฉลี่ยองค์ประกอบด้านที่ 1-7
 * @property string $assess_result ระดับผลการประเมิน
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $percentA
 * @property string $percentB
 * @property string $percentC
 * @property string $percentE
 * @property int $assessed_by_uid1 User-Id ผู้ตรวจประเมิน
 * @property string $assessed_by_fullname1 ชื่อผู้ตรวจประเมิน
 * @property int $assessed_by_uid2 User-Id ผู้ตรวจประเมิน
 * @property string $assessed_by_fullname2 ชื่อผู้ตรวจประเมิน
 * @property int $assessed_by_uid3 User-Id ผู้ตรวจประเมิน
 * @property string $assessed_by_fullname3 ชื่อผู้ตรวจประเมิน
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property Locality $localityCode
 * @property AssessmentResult $assessResult
 */
class AssessmentConclusion extends \yii\db\ActiveRecord
{
  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assessment_conclusion';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::class,
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['be_year', 'locality_code', 'issue_code'], 'required'],
            [['be_year', 'assessed_by_uid1', 'assessed_by_uid2', 'assessed_by_uid3', 'created_by', 'updated_by'], 'integer'],
            [['component1_score', 'component2_score', 'component3_score', 'component4_score', 'component5_score', 'component1to5_avgscore', 'component6_score', 'component7_score', 'component6to7_avgscore', 'component1to7_avgscore', 'b', 'c', 'd', 'percentA', 'percentB', 'percentC', 'percentE', 'application_form_id'], 'number'],
            [['created_at', 'updated_at', 'type'], 'safe'],
            [['issue_code'], 'string', 'max' => 7],
            [['locality_code', 'approve'], 'string', 'max' => 8],
            [['assess_result'], 'string', 'max' => 1],
            [['assessed_by_fullname1', 'assessed_by_fullname2', 'assessed_by_fullname3'], 'string', 'max' => 100],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
            [['be_year', 'issue_code', 'locality_code', 'type'], 'unique', 'targetAttribute' => ['be_year', 'issue_code', 'locality_code', 'type']],
            [['locality_code'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['locality_code' => 'locality_code']],
            [['assess_result'], 'exist', 'skipOnError' => true, 'targetClass' => AssessmentResult::className(), 'targetAttribute' => ['assess_result' => 'result_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'be_year' => 'Be Year',
            'issue_code' => 'Issue Code',
            'locality_code' => 'Locality Code',
            'component1_score' => 'Component1 Score',
            'component2_score' => 'Component2 Score',
            'component3_score' => 'Component3 Score',
            'component4_score' => 'Component4 Score',
            'component5_score' => 'Component5 Score',
            'component1to5_avgscore' => 'Component1to5 Avgscore',
            'component6_score' => 'Component6 Score',
            'component7_score' => 'Component7 Score',
            'component6to7_avgscore' => 'Component6to7 Avgscore',
            'component1to7_avgscore' => 'Component1to7 Avgscore',
            'assess_result' => 'Assess Result',
            'b' => 'B',
            'c' => 'C',
            'd' => 'D',
            'percentA' => 'Percent A',
            'percentB' => 'Percent B',
            'percentC' => 'Percent C',
            'percentE' => 'Percent E',
            'assessed_by_uid1' => 'Assessed By Uid1',
            'assessed_by_fullname1' => 'Assessed By Fullname1',
            'assessed_by_uid2' => 'Assessed By Uid2',
            'assessed_by_fullname2' => 'Assessed By Fullname2',
            'assessed_by_uid3' => 'Assessed By Uid3',
            'assessed_by_fullname3' => 'Assessed By Fullname3',
            'created_by_ip' => 'Created By Ip',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by_ip' => 'Updated By Ip',
            'approve' => 'ยืนยัน',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalityCode()
    {
        return $this->hasOne(Locality::className(), ['locality_code' => 'locality_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessResult()
    {
        return $this->hasOne(AssessmentResult::className(), ['result_code' => 'assess_result']);
    }

    public function getApplicationForm()
    {
        return $this->hasOne(ApplicationForm::className(), ['id' => 'application_form_id']);
    }

    public function getIssueCode()
    {
        return $this->hasOne(Issues::className(), ['issue_code' => 'issue_code']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\AssessmentConclusionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\AssessmentConclusionQuery(get_called_class());
    }

    public static function getScores($issue_code)
    {
        $selfScore = AssessmentConclusion::find()->joinWith('assessResult')->where(['application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])
            ->andWhere(['assessment_conclusion.issue_code' => $issue_code])
            ->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])
            ->andWhere(['locality_code' => Yii::$app->session['applicationForm']['locality_code']])->orderBy(['id' => SORT_DESC])->one();
        if ($selfScore != null) {
            return $selfScore;
        } else {
            return false;
        }
    }
}
