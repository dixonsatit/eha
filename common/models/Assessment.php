<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "assessment".
 *
 * @property int $id เลขที่การประเมิน
 * @property string $locality_code รหัส อปท.
 * @property int $sop_id กระบวนการ
 * @property int $selfassess_score คะแนนประเมินตนเอง
 * @property string $selfassess_date วันที่ประเมินตนเอง
 * @property int $assessor_score คะแนนตรวจประเมิน
 * @property string $assessor_date วันที่ตรวจประเมิน
 * @property string $assessor_notes บันทึกการตรวจประเมิน
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property Sop $sop
 * @property User $createdBy
 * @property User $updatedBy
 */
class Assessment extends \yii\db\ActiveRecord
{
//    public $sop_id;
    public $sop_parent_id;
    public $sop_issue_code;
    public $sop_assess_step;
    public $sop_assess_step_detail;
    public $assess_step_evidence;
    public $sop_assess_score;

    public $issue_code;
    public $issue_desc;

    public $main_issue_code;
    public $main_issue_desc;

    public $attachments;

    public $sopObj;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assessment';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),

            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

            'created_by_ip' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_by_ip'
                ],
                'value' => Yii::$app->request->userIP
            ],

            'updated_by_ip' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'created_by_ip'
                ],
                'value' => Yii::$app->request->userIP
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'assessmentAttachments',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute' => 'order',
                'typeAttribute' => 'type',
                'sizeAttribute' => 'size',
                'nameAttribute' => 'name',
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['locality_code', 'sop_id', 'application_form_id'], 'required'],
            [['sop_id', 'selfassess_score', 'assessor_score', 'created_by', 'updated_by', 'application_form_id'], 'integer'],
            [['selfassess_date', 'assessor_date', 'created_at', 'updated_at'], 'safe'],
            [['locality_code'], 'string', 'max' => 8],
            [['assessor_notes'], 'string', 'max' => 1024],
            [['created_by_ip'], 'string', 'max' => 50],
            [['updated_by_ip'], 'string', 'max' => 255],
            [['locality_code', 'sop_id', 'application_form_id'], 'unique', 'targetAttribute' => ['locality_code', 'sop_id', 'application_form_id']],
            [['sop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sop::className(), 'targetAttribute' => ['sop_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['sop_assess_step_detail', 'sop_assess_step'], 'string'],
            [['sop_parent_id', 'sop_issue_code', 'sop_assess_score'], 'integer'],
            [['issue_code', 'issue_desc'], 'string'],
            [['main_issue_code', 'main_issue_desc'], 'string'],
            ['selfassess_score', 'integer', 'min' => 0],
            [['attachments', 'thumbnail'], 'safe'],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่การประเมิน'),
            'locality_code' => Yii::t('common', 'รหัส อปท.'),
            'sop_id' => Yii::t('common', 'กระบวนการ'),
            'selfassess_score' => Yii::t('common', 'คะแนนประเมินตนเอง'),
            'selfassess_date' => Yii::t('common', 'วันที่ประเมินตนเอง'),
            'assessor_score' => Yii::t('common', 'คะแนนตรวจประเมิน'),
            'assessor_date' => Yii::t('common', 'วันที่ตรวจประเมิน'),
            'assessor_notes' => Yii::t('common', 'บันทึกการตรวจประเมิน'),
            'created_by_ip' => Yii::t('common', 'สร้างโดย ip'),
            'created_by' => Yii::t('common', 'สร้างโดย'),
            'created_at' => Yii::t('common', 'สร้างเมื่อ'),
            'updated_by_ip' => Yii::t('common', 'แก้ไขโดย ip'),
            'updated_by' => Yii::t('common', 'แก้ไขโดย'),
            'updated_at' => Yii::t('common', 'แก้ไขเมื่อ'),
            'attachments' => 'แนบไฟล์'
        ];
    }

    public static function getCountAttach($assessment_id = null)
    {

        $attach = AssessmentAttachment::find()->where(['assessment_id' => $assessment_id])->count();
        if ($attach > 0) {
            return $attach;
        } else {
            return 0;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSop()
    {
        return $this->hasOne(Sop::className(), ['id' => 'sop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getApplicationForm()
    {

        return $this->hasOne(ApplicationForm::className(), ['id' => 'application_form_id']);
    }

    public function getAssessmentAttachments()
    {
        return $this->hasMany(AssessmentAttachment::className(), ['assessment_id' => 'id']);
    }

    public function getAssessmentConclusion()
    {
        return $this->hasMany(AssessmentConclusion::className(), ['application_form_id' => 'id']);
    }


    /* 1.เมื่อ อปท. ส่ง "ขอรับการประเมิน" จะมี อีเมล์ แจ้งการสมัครขอรับการประเมิน
    *** เฉพาะของ อปท. ในเขตรับผิดชอบของ สสจ. นั้นเท่านั้น ****/
    public static function MailNotificationAssessmentSendSummit()
    {
            $managers = User::getRoleUsers('manager', Yii::$app->user->identity->userProfile->office->provinceCode->region_code, '');
            $messages = [];
            foreach ($managers as $user) {
                $locality = Locality::find()->where(['locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one()->locality_name;
                $messages[] = Yii::$app->mailer->compose()
                    ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                    ->setSubject('แจ้งเตือน อปท. ส่งขอรับการประเมิน')
                    ->setTextBody('แจ้งเตือน อปท. ส่งขอรับการประเมิน' . " " . $locality)
                    ->setTo($user['email']);
            }
            Yii::$app->mailer->sendMultiple($messages);
    }

    /* 1.เมื่อล็อกอินเข้ามา จะมี Web Notification แจ้งการสมัครขอรับการประเมินเข้ามาใหม่ 7 วันล่าสุด
    *** เฉพาะของ อปท. ในเขตรับผิดชอบของ สสจ. นั้นเท่านั้น *** */
    public static function WebNotificationAssessmentSendSummit()
    {
       if (Yii::$app->user->can('administrator')) {

               $query = "SELECT a.be_year FROM assessment_conclusion a
                 WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                 AND a.approve = 'Sent'";

               $command = Yii::$app->db->createCommand($query);
               $result = $command->queryAll();
               $count = count($result);

               if ($result != null) {
                   return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
               } else {
                   return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
               }
       }

        if (Yii::$app->user->can('director')) {

            $province_code = Yii::$app->user->identity->userProfile->office->province_code;
            $office_region = isset(Yii::$app->user->identity->userProfile->office->office_region) ? Yii::$app->user->identity->userProfile->office->office_region : '';

            if ($province_code != null || $office_region != null) {

                $query = "SELECT a.be_year FROM assessment_conclusion a
                  LEFT JOIN user_profile p on a.created_by = p.user_id
                  LEFT JOIN locality l on p.locality_code = l.locality_code
				  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND a.approve = 'Sent'
                  AND l.province_code = '$province_code' OR f.office_region = '$office_region'
                  GROUP BY a.be_year";

                $command = Yii::$app->db->createCommand($query);
                $result = $command->queryAll();
                $count = count($result);

                if ($result != null) {
                    return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
                } else {
                    return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
                }
            }
        }

        if (Yii::$app->user->can('manager')) {

            $province_code = Yii::$app->user->identity->userProfile->office->province_code;
            $office_region = isset(Yii::$app->user->identity->userProfile->office->office_region) ? Yii::$app->user->identity->userProfile->office->office_region : '';

            if ($province_code != null || $office_region != null) {

                $query = "SELECT a.be_year FROM assessment_conclusion a
                  LEFT JOIN user_profile p on a.created_by = p.user_id
                  LEFT JOIN locality l on p.locality_code = l.locality_code
				  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND a.approve = 'Sent'
                  AND l.province_code = '$province_code' OR f.office_region = '$office_region'
                  GROUP BY a.be_year";

                $command = Yii::$app->db->createCommand($query);
                $result = $command->queryAll();
                $count = count($result);

                if ($result != null) {
                    return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
                } else {
                    return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
                }
            }
        }
    }


    /*เมื่อ อปท. ส่ง "ขอรับการประเมิน" จะมี อีเมล์ แจ้งการสมัครขอรับการประเมิน
    *** ผู้ดูแลระบบ ****/
    public static function MailNotificationAssessmentSendSummitAdmin()
    {
        $administrator = User::getRoleUsersAdmin('administrator');

        $messages = [];
        foreach ($administrator as $user) {
            $messages[] = Yii::$app->mailer->compose()
                ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                ->setSubject('แจ้งเตือน อปท. ส่งขอรับการประเมิน')
                ->setTextBody('แจ้งเตือน อปท. ส่งขอรับการประเมิน' . " " . Yii::$app->session['applicationForm']['locality_name'])
                ->setTo($user['email']);
        }
        Yii::$app->mailer->sendMultiple($messages);

    }


    /*เมื่อล็อกอินเข้ามา จะมี Web Notification แจ้งการสมัครขอรับการประเมินเข้ามาใหม่ 7 วันล่าสุด *ทั้งหมด*/
    public static function WebNotificationAssessmentSendSummitAdmin()
    {
        $query = "SELECT a.selfassess_date FROM assessment a
                  LEFT JOIN user_profile p on a.created_by = p.user_id
                  LEFT JOIN locality l on p.locality_code = l.locality_code
				  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.assessor_date) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND a.assess_status = 2
                  GROUP BY a.selfassess_date";

        $command = Yii::$app->db->createCommand($query);
        $result = $command->queryAll();
        $count = count($result);

        if ($result != null) {
            return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
        } else {
            return 'แจ้งเตือน อปท. ส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
        }
    }


    /*แจ้งเตือน webnotification ผลการการประเมิน 7 วันล่าสุด ไปยัง สสจ.(manager) ที่รับผิดชอบ*/
    public static function WebNotificationAssessmentSendResult()
    {
        $province_code = Yii::$app->user->identity->userProfile->office->province_code;

        if ($province_code != null) {

            $query = "SELECT * FROM assessment_conclusion a
                  LEFT JOIN locality l on a.locality_code = l.locality_code
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND l.province_code = $province_code
                  AND a.type = 'selfassess' AND a.approve = 'Sent'";

            $command = Yii::$app->db->createCommand($query);
            $result = $command->queryAll();
            $count = count($result);

            if ($result != null) {
                return 'แจ้งเตือน มีการส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
            } else {
                return 'แจ้งเตือน มีการส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
            }

        }
    }

    /*แจ้งเตือน Email ผลการการประเมิน ไปยัง สสจ.(manager) ที่รับผิดชอบ*/
    public static function MailNotificationAssessmentSendResult()
    {
        $manager = User::getRoleUsers('manager', isset(Yii::$app->user->identity->userProfile->locality->provinceCode->region_code) ? Yii::$app->user->identity->userProfile->locality->provinceCode->region_code : '', '');

        $messages = [];
        foreach ($manager as $user) {
            $messages[] = Yii::$app->mailer->compose()
                ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                ->setSubject('แจ้งเตือน มีผลการประเมิน')
                ->setTextBody('แจ้งเตือน มีผลการประเมิน' . " " . Yii::$app->session['applicationForm']['locality_name'])
                ->setTo($user['email']);
        }
        Yii::$app->mailer->sendMultiple($messages);

    }


    /*แจ้งเตือน Webnotification ผลการการประเมิน 7 วันล่าสุด ไปยัง เขต(director) ที่รับผิดชอบ*/
    public static function WebNotificationAssessmentSendResultDirector()
    {
        $province_code = isset(Yii::$app->user->identity->userProfile->office->province_code) ? Yii::$app->user->identity->userProfile->office->province_code : '';
        if ($province_code != null) {

            $query = "SELECT * FROM assessment_conclusion a
                  LEFT JOIN locality l on a.locality_code = l.locality_code
				  LEFT JOIN user_profile p on l.locality_code = p.locality_code
                  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND l.province_code = $province_code
				  AND a.type = 'selfassess' AND a.approve = 'Sent'";

            $command = Yii::$app->db->createCommand($query);
            $result = $command->queryAll();
            $count = count($result);

            if ($result != null) {
                return 'แจ้งเตือน มีการส่งขอรับการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
            } else {
                return 'แจ้งเตือน มีการส่งขอรับการประเมิน 7 วันล่าสุด 0 รายการ';
            }
        }
    }

    /*แจ้งเตือน Email ผลการการประเมิน ไปยัง เขต(director) ที่รับผิดชอบ*/
    public static function MailNotificationAssessmentSendResultDirector()
    {
        $director = User::getRoleUsers('director', '', isset(Yii::$app->user->identity->userProfile->office->provinceCode->region_code) ? Yii::$app->user->identity->userProfile->office->provinceCode->region_code : '');

        $messages = [];
        foreach ($director as $user) {
            $messages[] = Yii::$app->mailer->compose()
                ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                ->setSubject('แจ้งเตือน มีผลการประเมิน')
                ->setTextBody('แจ้งเตือน มีผลการประเมิน' . " " . Yii::$app->session['applicationForm']['locality_name'])
                ->setTo($user['email']);
        }
        Yii::$app->mailer->sendMultiple($messages);

    }


    /*แจ้งเตือน Webnotification ผลการการประเมิน 7 วันล่าสุด ไปยัง ผู้ดูแลระบบของสำนักสุขาภิบาลอาหารและน้ำ(admin)*/
    public static function WebNotificationAssessmentSendResultAdmin()
    {
        if (Yii::$app->user->can('Administrator')) {
            $query = "SELECT * FROM assessment_conclusion a
                      WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
				      AND a.type = 'assessor'";

            $command = Yii::$app->db->createCommand($query);
            $result = $command->queryAll();
            $count = count($result);

            if ($result != null) {
                return 'แจ้งเตือน มีผลการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
            } else {
                return 'แจ้งเตือน มีผลการประเมิน 7 วันล่าสุด 0 รายการ';
            }
        } else {
            $query = "SELECT * FROM assessment_conclusion a
                  LEFT JOIN locality l on a.locality_code = l.locality_code
				  LEFT JOIN user_profile p on l.locality_code = p.locality_code
                  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
				  AND a.type = 'assessor'";

            $command = Yii::$app->db->createCommand($query);
            $result = $command->queryAll();
            $count = count($result);

            if ($result != null) {
                return 'แจ้งเตือน มีผลการประเมิน 7 วันล่าสุด ' . $count . ' รายการ';
            } else {
                return 'แจ้งเตือน มีผลการประเมิน 7 วันล่าสุด 0 รายการ';
            }
        }
    }

    /*แจ้งเตือน Email ผลการการประเมิน ไปยัง ผู้ดูแลระบบของสำนักสุขาภิบาลอาหารและน้ำ(admin)*/
    public static function MailNotificationAssessmentSendResultAdmin()
    {
        $administrator = User::getRoleUsersAdmin('administrator');

        $messages = [];
        foreach ($administrator as $user) {
            $messages[] = Yii::$app->mailer->compose()
                ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                ->setSubject('แจ้งเตือน มีผลการประเมิน')
                ->setTextBody('แจ้งเตือน มีผลการประเมิน' . " " . Yii::$app->session['applicationForm']['locality_name'])
                ->setTo($user['email']);
        }
        Yii::$app->mailer->sendMultiple($messages);

    }


    /*แจ้งเตือน Webnotification ผลการการประเมิน 7 วันล่าสุด ไปยัง อปท(user)*/
    public static function WebNotificationAssessmentSendResultUser()
    {
        $locality = Yii::$app->user->identity->userProfile->locality_code;

        if ($locality != null) {

            $query = "SELECT * FROM assessment_conclusion a
                  LEFT JOIN locality l on a.locality_code = l.locality_code
				  LEFT JOIN user_profile p on l.locality_code = p.locality_code
                  LEFT JOIN office f on p.office_id = f.office_id
                  WHERE DATE(a.created_at) BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()
                  AND a.locality_code = '{$locality}'
				  AND a.type = 'assessor';";

            $command = Yii::$app->db->createCommand($query);
            $result = $command->queryAll();
            $count = count($result);

            if ($result != null) {
                return 'แจ้งเตือน มีผลการประเมินใหม ่' . $count . ' รายการ';
            } else {
                return 'แจ้งเตือน มีผลการประเมินใหม่ 0 รายการ';
            }
        }
    }


    /*แจ้งเตือน Email ผลการการประเมิน ไปยัง อปท.(user)*/
    public static function MailNotificationAssessmentSendResultLocality()
    {
        ;
        $users = User::getRoleUserLocality('user', Yii::$app->session['applicationForm']['locality_code']);

        $messages = [];
        foreach ($users as $user) {
            $locality = Locality::find()->where(['locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();
            if ($locality != null) {
                $messages[] = Yii::$app->mailer->compose()
                    ->setFrom(['ehasmartinfo@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
                    ->setSubject('แจ้งเตือน มีผลการประเมิน')
                    ->setTextBody('แจ้งเตือน มีผลการประเมิน' . " " . Yii::$app->session['applicationForm']['locality_name'])
                    ->setTo($user['email']);
            }
        }
        Yii::$app->mailer->sendMultiple($messages);

    }


}
