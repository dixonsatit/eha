<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "application_issues".
 *
 * @property int $id เลขที่
 * @property int $application_id เลขที่ใบสมัคร
 * @property string $issue_code รหัสประเด็นงาน
 * @property string|null $created_by_ip สร้างโดย ip
 * @property int|null $created_by สร้างโดย
 * @property string|null $created_at สร้างเมื่อ
 * @property string|null $updated_by_ip แก้ไขโดย ip
 * @property int|null $updated_by แก้ไขโดย
 * @property string|null $updated_at แก้ไขเมื่อ
 *
 * @property ApplicationForm $application
 * @property Issues $issueCode
 */
class ApplicationIssues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_issues';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'issue_code'], 'required'],
            [['application_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['issue_code'], 'string', 'max' => 7],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
            [['application_id', 'issue_code'], 'unique', 'targetAttribute' => ['application_id', 'issue_code']],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplicationForm::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['issue_code'], 'exist', 'skipOnError' => true, 'targetClass' => Issues::className(), 'targetAttribute' => ['issue_code' => 'issue_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'issue_code' => 'Issue Code',
            'created_by_ip' => 'Created By Ip',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by_ip' => 'Updated By Ip',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(ApplicationForm::className(), ['id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueCode()
    {
        return $this->hasOne(Issues::className(), ['issue_code' => 'issue_code']);
    }
}
