<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "locality".
 *
 * @property int $id เลขที่
 * @property string $locality_code รหัส อปท.
 * @property string $locality_name ชื่อ อปท.
 * @property string $locality_shortname
 * @property string $administrator ชื่อผู้บริหาร
 * @property string $administrative_title ตำแหน่งผู้บริหาร
 * @property string $type_code รหัสประเภทหน่วยงาน
 * @property string $address ที่อยู่
 * @property string $subdistrict_code รหัสตำบล
 * @property string $district_code รหัสอำเภอ
 * @property string $province_code รหัสจังหวัด
 * @property string $postal_code รหัสไปรษณีย์
 * @property string $active_status สถานะการใช้งาน
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property AssessmentConclusion[] $assessmentConclusions
 * @property LocalityType $typeCode
 * @property Province $provinceCode
 * @property District $districtCode
 */
class Locality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locality';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['locality_code'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['locality_code'], 'string', 'max' => 8],
            [['locality_name', 'administrator', 'administrative_title'], 'string', 'max' => 255],
            [['locality_shortname'], 'string', 'max' => 100],
            [['type_code'], 'string', 'max' => 10],
            [['address'], 'string', 'max' => 512],
            [['subdistrict_code'], 'string', 'max' => 6],
            [['district_code'], 'string', 'max' => 4],
            [['province_code'], 'string', 'max' => 2],
            [['postal_code'], 'string', 'max' => 5],
            [['active_status'], 'string', 'max' => 1],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
            [['locality_code'], 'unique'],
            [['type_code'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityType::className(), 'targetAttribute' => ['type_code' => 'type_code']],
            [['province_code'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_code' => 'province_code']],
            [['district_code'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_code' => 'district_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'locality_code' => Yii::t('common', 'รหัส อปท.'),
            'locality_name' => Yii::t('common', 'ชื่อ อปท.'),
            'locality_shortname' => Yii::t('common', 'Locality Shortname'),
            'administrator' => Yii::t('common', 'ชื่อผู้บริหาร'),
            'administrative_title' => Yii::t('common', 'ตำแหน่งผู้บริหาร'),
            'type_code' => Yii::t('common', 'รหัสประเภทหน่วยงาน'),
            'address' => Yii::t('common', 'ที่อยู่'),
            'subdistrict_code' => Yii::t('common', 'รหัสตำบล'),
            'district_code' => Yii::t('common', 'รหัสอำเภอ'),
            'province_code' => Yii::t('common', 'รหัสจังหวัด'),
            'postal_code' => Yii::t('common', 'รหัสไปรษณีย์'),
            'active_status' => Yii::t('common', 'สถานะการใช้งาน'),
            'created_by_ip' => Yii::t('common', 'สร้างโดย ip'),
            'created_by' => Yii::t('common', 'สร้างโดย'),
            'created_at' => Yii::t('common', 'สร้างเมื่อ'),
            'updated_by_ip' => Yii::t('common', 'แก้ไขโดย ip'),
            'updated_by' => Yii::t('common', 'แก้ไขโดย'),
            'updated_at' => Yii::t('common', 'แก้ไขเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessmentConclusions()
    {
        return $this->hasMany(AssessmentConclusion::className(), ['locality_code' => 'locality_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeCode()
    {
        return $this->hasOne(LocalityType::className(), ['type_code' => 'type_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceCode()
    {
        return @$this->hasOne(Province::className(), ['province_code' => 'province_code']);
    }

    public function getUserProfile(){
        return $this->hasMany(UserProfile::className(), ['locality_code' => 'locality_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictCode()
    {
        return $this->hasOne(District::className(), ['district_code' => 'district_code']);
    }

    public function getSubdistrictCode()
    {
        return $this->hasOne(Subdistrict::className(), ['subdistrict_code' => 'subdistrict_code']);
    }

    public function getApplicationForm(){
        return $this->hasMany(ApplicationForm::className(), ['locality_code' => 'locality_code']);
    }

}
