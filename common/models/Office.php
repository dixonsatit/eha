<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property int $id เลขที่
 * @property string $office_id รหัสหน่วยงาน
 * @property string $office_name ชื่อหน่วยงาน
 * @property string $office_shortname ชื่อหน่วยงาน (ย่อ)
 * @property string $office_region รหัสเขตสุขภาพ 2 หลัก
 * @property string $office_type
 * @property string $administrator ชื่อผู้บริหาร
 * @property string $administrative_title ตำแหน่งผู้บริหาร
 * @property string $address ที่อยู่
 * @property string $subdistrict_code รหัสตำบล
 * @property string $district_code รหัสอำเภอ
 * @property string $province_code รหัสจังหวัด
 * @property string $postal_code รหัสไปรษณีย์
 * @property string $active_status สถานะการใช้งาน
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property Subdistrict $subdistrictCode
 * @property District $districtCode
 * @property Province $provinceCode
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['office_id'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['office_id'], 'string', 'max' => 20],
            [['office_name'], 'string', 'max' => 100],
            [['office_shortname'], 'string', 'max' => 30],
            [['office_region', 'office_type', 'province_code'], 'string', 'max' => 2],
            [['administrator', 'administrative_title'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 512],
            [['subdistrict_code'], 'string', 'max' => 6],
            [['district_code'], 'string', 'max' => 4],
            [['postal_code'], 'string', 'max' => 5],
            [['active_status'], 'string', 'max' => 1],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
            [['subdistrict_code'], 'exist', 'skipOnError' => true, 'targetClass' => Subdistrict::className(), 'targetAttribute' => ['subdistrict_code' => 'subdistrict_code']],
            [['district_code'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_code' => 'district_code']],
            [['province_code'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_code' => 'province_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'office_id' => Yii::t('common', 'รหัสหน่วยงาน'),
            'office_name' => Yii::t('common', 'ชื่อหน่วยงาน'),
            'office_shortname' => Yii::t('common', 'ชื่อหน่วยงาน (ย่อ)'),
            'office_region' => Yii::t('common', 'รหัสเขตสุขภาพ 2 หลัก'),
            'office_type' => Yii::t('common', 'Office Type'),
            'administrator' => Yii::t('common', 'ชื่อผู้บริหาร'),
            'administrative_title' => Yii::t('common', 'ตำแหน่งผู้บริหาร'),
            'address' => Yii::t('common', 'ที่อยู่'),
            'subdistrict_code' => Yii::t('common', 'รหัสตำบล'),
            'district_code' => Yii::t('common', 'รหัสอำเภอ'),
            'province_code' => Yii::t('common', 'รหัสจังหวัด'),
            'postal_code' => Yii::t('common', 'รหัสไปรษณีย์'),
            'active_status' => Yii::t('common', 'สถานะการใช้งาน'),
            'created_by_ip' => Yii::t('common', 'สร้างโดย ip'),
            'created_by' => Yii::t('common', 'สร้างโดย'),
            'created_at' => Yii::t('common', 'สร้างเมื่อ'),
            'updated_by_ip' => Yii::t('common', 'แก้ไขโดย ip'),
            'updated_by' => Yii::t('common', 'แก้ไขโดย'),
            'updated_at' => Yii::t('common', 'แก้ไขเมื่อ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistrictCode()
    {
        return $this->hasOne(Subdistrict::className(), ['subdistrict_code' => 'subdistrict_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictCode()
    {
        return $this->hasOne(District::className(), ['district_code' => 'district_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceCode()
    {
        return $this->hasOne(Province::className(), ['province_code' => 'province_code']);
    }
}
