<?php

namespace common\models;

use backend\models\LpaScore;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * This is the model class for table "application_form".
 *
 * @property int $id เลขที่ใบสมัคร
 * @property int $be_year ปี
 * @property string $locality_code รหัส อปท.
 * @property string $contact_person ชื่อผู้ประสานงาน
 * @property string $contact_title ตำแหน่ง
 * @property string $contact_phone โทรศัพท์/โทรสาร
 * @property string $contact_mobile มือถือ
 * @property string $contact_email อีเมล์
 * @property string $apply_person ผูู้ที่ได้รับมอบหมาย
 * @property string $apply_title ตำแหน่งผูู้ที่ได้รับมอบหมาย
 * @property string $apply_issues ประเด็นย่อยที่สมัครขอรับการประเมิน
 * @property string $apply_date วันที่สมัคร
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property ApplicationIssues[] $applicationIssues
 * @property Issues[] $issueCodes
 */
class ApplicationForm extends \yii\db\ActiveRecord
{

    public $localityType;
    public $localityName;
    public $subdictrict;
    public $dictrict;
    public $province;
    public $tel;
    public $directorName;
    public $position;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_form';
    }

    public function behaviors()
    {
        return [

            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],

            'apply_date' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'apply_date'
                ],
                'value' => new Expression('NOW()'),
            ],

            'created_by_ip' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_by_ip'
                ],
                'value' => Yii::$app->request->userIP
            ],

            'updated_by_ip' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'created_by_ip'
                ],
                'value' => Yii::$app->request->userIP
            ],

            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['be_year', 'locality_code'], 'required'],
            [['be_year', 'created_by', 'updated_by'], 'integer'],
            [['apply_issues'], 'string'],
            [['apply_date', 'created_at', 'updated_at'], 'safe'],
            [['locality_code'], 'string', 'max' => 8],
            [['contact_person', 'contact_title', 'contact_phone', 'contact_mobile', 'contact_email', 'apply_person', 'apply_title'], 'string', 'max' => 100],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
             ['be_year', 'unique', 'targetAttribute' => ['be_year', 'locality_code'], 'message' => 'ประเด็นการประเมินของท่าน' . '{attribute} {value}' . ' มีการสร้างแล้ว'],
            [['localityType', 'localityName', 'subdictrict', 'dictrict', 'province', 'tel', 'directorName', 'position'], 'string'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่ใบสมัคร'),
            'be_year' => Yii::t('common', 'ปี'),
            'locality_code' => Yii::t('common', 'รหัส อปท.'),
            'contact_person' => Yii::t('common', 'ชื่อผู้รับผิดชอบ/ผู้ประสานงาน'),
            'contact_title' => Yii::t('common', 'ตำแหน่ง'),
            'contact_phone' => Yii::t('common', 'โทรศัพท์/โทรสาร'),
            'contact_mobile' => Yii::t('common', 'มือถือ'),
            'contact_email' => Yii::t('common', 'อีเมล์'),
            'apply_person' => Yii::t('common', 'ชื่อ-สกุล ผู้บริหารหน่วยงาน'),
            'apply_title' => Yii::t('common', 'ตำแหน่ง'),
            'apply_issues' => Yii::t('common', 'ประเด็นย่อยที่สมัครขอรับการประเมิน'),
            'apply_date' => Yii::t('common', 'วันที่สมัคร'),
            'created_by_ip' => Yii::t('common', 'สร้างโดย ip'),
            'created_by' => Yii::t('common', 'สร้างโดย'),
            'created_at' => Yii::t('common', 'สร้างเมื่อ'),
            'updated_by_ip' => Yii::t('common', 'แก้ไขโดย ip'),
            'updated_by' => Yii::t('common', 'แก้ไขโดย'),
            'updated_at' => Yii::t('common', 'แก้ไขเมื่อ'),

            'localityType' => Yii::t('common', 'ประเภทหน่วยงาน'),
            'localityName' => Yii::t('common', 'ชื่อหน่วยงาน'),
            'subdictrict' => Yii::t('common', 'ตำบล'),
            'dictrict' => Yii::t('common', 'อำเภอ'),
            'province' => Yii::t('common', 'จังหวัด'),
            'tel' => Yii::t('common', 'โทร'),
            'directorName' => Yii::t('common', 'ชื่อ-สกุลผู้บริหารหน่วยงาน'),
            'position' => Yii::t('common', 'ตำแหน่ง')
        ];
    }

    public static function chkApplicationForm()
    {

        $check = ApplicationForm::find()->where(['locality_code' => Yii::$app->user->identity->userProfile->locality_code]);
        if (!empty($check)) {
            return 'update';
        } else {
            return 'create';
        }
    }

    public function getAuthor()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationIssues()
    {
        return $this->hasMany(ApplicationIssues::className(), ['application_id' => 'id']);
    }

    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['locality_code' => 'locality_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueCodes()
    {
        //        return $this->hasMany(Issues::className(), ['issue_code' => 'apply_issues'])->viaTable('application_issues', ['application_id' => 'id']);
        return $this->hasMany(Issues::className(), ['issue_code' => 'apply_issues']);
    }

    public function getAssessmentResult()
    {
        return $this->hasOne(AssessmentResult::className(), ['result_code' => 'status']);
    }


    public function getAssessmentConclusion()
    {
        return $this->hasMany(AssessmentConclusion::className(), ['application_form_id' => 'id']);
    }


    public function my_required($attribute_name, $params)
    {
        if (empty($this->file) && empty($this->$attribute_name)) {

            $this->addError($attribute_name, Yii::t('locality_code', 'เลือกประด็นขอรับการประเมินของปี' . Date('Y') + 543 . ' ได้รับการบันทึกไปแล้ว ถ้าต้องการแก้ไข คลิก'));
            $this->addError($attribute_name, Yii::t('be_year', 'เลือกประด็นขอรับการประเมินของปี' . Date('Y') + 543 . ' ได้รับการบันทึกไปแล้ว ถ้าต้องการแก้ไข คลิก'));

            return false;
        }
        return true;
    }

    public static function  getLastYearLpa()
    {
        return LpaScore::find()->max('be_year');
    }

    public static function fiscalYear($date)
    {
        list($year, $month, $day) = explode("-", $date);
        $cday = mktime(0, 0, 0, $month, $day, $year);
        $d1 = mktime(0, 0, 0, 10, 1, $year);
        $d2 = mktime(0, 0, 0, 1, 1, $year + 1);
        if ($cday >= $d1 && $cday < $d2) {
            $year++;
        }
        
        return $year+543;
    }
}
