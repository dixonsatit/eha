<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "issues".
 *
 * @property int $id เลขที่
 * @property string $issue_code รหัสประเด็นงาน
 * @property string $issue_name ชื่อประเด็นงาน
 * @property string $issue_desc ประเด็นงาน
 * @property string $main_issue_code รหัสประเด็นหลัก
 *
 * @property ApplicationIssues[] $applicationIssues
 * @property ApplicationForm[] $applications
 * @property MainIssues $mainIssueCode
 * @property Sop[] $sops
 */
class Issues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issues';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['issue_code', 'main_issue_code'], 'required'],
            [['issue_code'], 'string', 'max' => 7],
            [['issue_name'], 'string', 'max' => 8],
            [['issue_desc'], 'string', 'max' => 255],
            [['main_issue_code'], 'string', 'max' => 1],
            [['issue_code'], 'unique'],
            [['main_issue_code'], 'exist', 'skipOnError' => true, 'targetClass' => MainIssues::className(), 'targetAttribute' => ['main_issue_code' => 'main_issue_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'issue_code' => Yii::t('common', 'รหัสประเด็นงาน'),
            'issue_name' => Yii::t('common', 'ชื่อประเด็นงาน'),
            'issue_desc' => Yii::t('common', 'ประเด็นงาน'),
            'main_issue_code' => Yii::t('common', 'รหัสประเด็นหลัก'),
        ];
    }

    public static function getIssueByCode($issues){

        if(empty($issues)){
            echo 555;
            die();
            return null;
        }

        $arrayIssues = Json::decode($issues);

        $arIssues = [];

        foreach($arrayIssues as $arrayIssue){
            $arIssues[] = $arrayIssue;
        }

        return $arIssues;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationIssues()
    {
        return $this->hasMany(ApplicationIssues::className(), ['issue_code' => 'issue_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(ApplicationForm::className(), ['id' => 'application_id'])->viaTable('application_issues', ['issue_code' => 'issue_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainIssueCode()
    {
        return $this->hasOne(MainIssues::className(), ['main_issue_code' => 'main_issue_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSops()
    {
        return $this->hasMany(Sop::className(), ['issue_code' => 'issue_code']);
    }
}
