<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "district".
 *
 * @property int $id หมายเลข
 * @property string $district_code รหัสอำเภอ
 * @property string $district_name อำเภอ
 * @property string $province_code จังหวัด
 *
 * @property Province $provinceCode
 * @property Locality[] $localities
 * @property Office[] $offices
 * @property Subdistrict[] $subdistricts
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_code', 'province_code'], 'required'],
            [['district_code'], 'string', 'max' => 4],
            [['district_name'], 'string', 'max' => 255],
            [['province_code'], 'string', 'max' => 2],
            [['district_code'], 'unique'],
            [['province_code'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_code' => 'province_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'หมายเลข'),
            'district_code' => Yii::t('common', 'รหัสอำเภอ'),
            'district_name' => Yii::t('common', 'อำเภอ'),
            'province_code' => Yii::t('common', 'จังหวัด'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinceCode()
    {
        return $this->hasOne(Province::className(), ['province_code' => 'province_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalities()
    {
        return $this->hasMany(Locality::className(), ['district_code' => 'district_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['district_code' => 'district_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistricts()
    {
        return $this->hasMany(Subdistrict::className(), ['district_code' => 'district_code']);
    }
}
