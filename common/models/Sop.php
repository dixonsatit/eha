<?php

namespace common\models;

use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "sop".
 *
 * @property int $id เลขที่กระบวนการ
 * @property int $parent_id เลขที่กระบวนการหลัก
 * @property string $component_code
 * @property int $be_year ปีงบประมาณ (พ.ศ.)
 * @property string $issue_code รหัสการรับรอง EHA
 * @property string $assess_step ขั้นตอนการประเมิน
 * @property string $assess_step_detail
 * @property int $assess_score คะแนนการประเมิน
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 *
 * @property Assessment[] $assessments
 * @property Locality[] $localityCodes
 * @property Issues $issueCode
 * @property AssessmentComponents $componentCode
 */
class Sop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'be_year', 'assess_score', 'created_by', 'updated_by'], 'integer'],
            [['be_year', 'issue_code', 'assess_step'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['component_code'], 'string', 'max' => 1],
            [['issue_code'], 'string', 'max' => 7],
            [['assess_step'], 'string', 'max' => 7],
            [['assess_step_detail', 'assess_step_evidence', 'assess_type_value'], 'string', 'max' => 1024],
            [['created_by_ip'], 'string', 'max' => 50],
            [['updated_by_ip'], 'string', 'max' => 255],
            [['be_year', 'issue_code', 'assess_step'], 'unique', 'targetAttribute' => ['be_year', 'issue_code', 'assess_step']],
            [['issue_code'], 'exist', 'skipOnError' => true, 'targetClass' => Issues::className(), 'targetAttribute' => ['issue_code' => 'issue_code']],
            [['component_code'], 'exist', 'skipOnError' => true, 'targetClass' => AssessmentComponents::className(), 'targetAttribute' => ['component_code' => 'component_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่กระบวนการ'),
            'parent_id' => Yii::t('common', 'เลขที่กระบวนการหลัก'),
            'component_code' => Yii::t('common', 'Component Code'),
            'be_year' => Yii::t('common', 'ปีงบประมาณ (พ.ศ.)'),
            'issue_code' => Yii::t('common', 'รหัสการรับรอง EHA'),
            'assess_step' => Yii::t('common', 'ขั้นตอนการประเมิน'),
            'assess_step_detail' => Yii::t('common', 'Assess Step Detail'),
            'assess_score' => Yii::t('common', 'คะแนนการประเมิน'),
            'created_by_ip' => Yii::t('common', 'สร้างโดย ip'),
            'created_by' => Yii::t('common', 'สร้างโดย'),
            'created_at' => Yii::t('common', 'สร้างเมื่อ'),
            'updated_by_ip' => Yii::t('common', 'แก้ไขโดย ip'),
            'updated_by' => Yii::t('common', 'แก้ไขโดย'),
            'updated_at' => Yii::t('common', 'แก้ไขเมื่อ'),
        ];
    }

    public static function getTypeEha($eha = null)
    {
        if ($eha == 'EHA1001') {
            return Html::encode('(สถานที่จำหน่ายอาหาร, สถานที่สะสมอาหาร: มินิมาร์ท, ซูเปอร์มาร์เก็ต, ร้านขายของชำ)');
        } else if ($eha == 'EHA1002') {
            return Html::encode('(ตลาดประเภทที่ 1 : ตลาดสด, ตลาดประเภทที่ 2 : ตลาดนัด)');
        } else if ($eha == 'EHA1003') {
            return Html::encode('(แบบตั้งประจำที่, แบบเร่ขาย)');
        } else if ($eha == 'EHA2001') {
            return Html::encode('(ผลิตโดยองค์กรปกครองส่วนท้องถิ่น (อปท.))');
        } else if ($eha == 'EHA2002') {
            return Html::encode(('ผลิตโดยหน่วยงานอื่น'));
        } else if ($eha == 'EHA2003') {
            return Html::encode('');
        } else if ($eha == 'EHA3001') {
            return Html::encode('');
        } else if ($eha == 'EHA3002') {
            return Html::encode('');
        } else if ($eha == 'EHA4001') {
            return Html::encode('');
        } else if ($eha == 'EHA4002') {
            return Html::encode('');
        } else if ($eha == 'EHA4003') {
            return Html::encode('');
        } else if ($eha == 'EHA5000') {
            return Html::encode('');
        } else if ($eha == 'EHA6000') {
            return Html::encode('');
        } else if ($eha == 'EHA7000') {
            return Html::encode('');
        } else if ($eha == 'EHA8000') {
            return Html::encode('');
        } else if ($eha == 'EHA9001') {
            return Html::encode('');
        } else if ($eha == 'EHA9002') {
            return Html::encode('');
        } else if ($eha == 'EHA9003') {
            return Html::encode('');
        } else if ($eha == 'EHA9004') {
            return Html::encode('');
        } else if ($eha == 'EHA9005') {
            return Html::encode('');
        } else {
            return '';
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessments()
    {
        return $this->hasMany(Assessment::className(), ['sop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalityCodes()
    {
        return $this->hasMany(Locality::className(), ['locality_code' => 'locality_code'])->viaTable('assessment', ['sop_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIssueCode()
    {
        return $this->hasOne(Issues::className(), ['issue_code' => 'issue_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponentCode()
    {
        return $this->hasOne(AssessmentComponents::className(), ['component_code' => 'component_code']);
    }
}
