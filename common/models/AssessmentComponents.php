<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "assessment_components".
 *
 * @property int $id เลขที่
 * @property string $component_code รหัสองค์ประกอบการประเมิน
 * @property string $component_name องค์ประกอบการประเมิน
 *
 * @property Sop[] $sops
 */
class AssessmentComponents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assessment_components';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['component_code', 'component_name'], 'required'],
            [['component_code'], 'string', 'max' => 1],
            [['component_name'], 'string', 'max' => 100],
            [['component_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'เลขที่'),
            'component_code' => Yii::t('common', 'รหัสองค์ประกอบการประเมิน'),
            'component_name' => Yii::t('common', 'องค์ประกอบการประเมิน'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSops()
    {
        return $this->hasMany(Sop::className(), ['component_code' => 'component_code']);
    }
}
