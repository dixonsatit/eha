<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "region".
 *
 * @property int $id หมายเลข
 * @property string $region_code รหัสเขตสุขภาพ
 * @property string $region_shortname เขตสุขภาพ (ย่อ)
 * @property string $region_name เขตสุขภาพ
 *
 * @property Province[] $provinces
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_code'], 'required'],
            [['region_code'], 'string', 'max' => 2],
            [['region_shortname'], 'string', 'max' => 50],
            [['region_name'], 'string', 'max' => 150],
            [['region_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'หมายเลข'),
            'region_code' => Yii::t('common', 'รหัสเขตสุขภาพ'),
            'region_shortname' => Yii::t('common', 'เขตสุขภาพ (ย่อ)'),
            'region_name' => Yii::t('common', 'เขตสุขภาพ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinces()
    {
        return $this->hasMany(Province::className(), ['region_code' => 'region_code']);
    }
}
