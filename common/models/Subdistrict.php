<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "subdistrict".
 *
 * @property int $id หมายเลข
 * @property string $subdistrict_code รหัสตำบล
 * @property string $subdistrict_name ตำบล
 * @property string $district_code อำเภอ
 *
 * @property Office[] $offices
 * @property District $districtCode
 */
class Subdistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subdistrict';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdistrict_code', 'district_code'], 'required'],
            [['subdistrict_code'], 'string', 'max' => 6],
            [['subdistrict_name'], 'string', 'max' => 255],
            [['district_code'], 'string', 'max' => 4],
            [['subdistrict_code'], 'unique'],
            [['district_code'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_code' => 'district_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'หมายเลข'),
            'subdistrict_code' => Yii::t('common', 'รหัสตำบล'),
            'subdistrict_name' => Yii::t('common', 'ตำบล'),
            'district_code' => Yii::t('common', 'อำเภอ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['subdistrict_code' => 'subdistrict_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrictCode()
    {
        return $this->hasOne(District::className(), ['district_code' => 'district_code']);
    }
}
