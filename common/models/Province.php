<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property int $id หมายเลข
 * @property string $province_code รหัสจังหวัด
 * @property string $province_name จังหวัด
 * @property string $region_code เขตสุขภาพ
 * @property string $province_lat พิกัดละติจูด
 * @property string $province_long พิกัดลองจิจูด
 *
 * @property District[] $districts
 * @property Locality[] $localities
 * @property Office[] $offices
 * @property Region $regionCode
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_code'], 'required'],
            [['province_code', 'region_code'], 'string', 'max' => 2],
            [['province_name', 'province_lat', 'province_long'], 'string', 'max' => 255],
            [['province_code'], 'unique'],
            [['region_code'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_code' => 'region_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'หมายเลข'),
            'province_code' => Yii::t('common', 'รหัสจังหวัด'),
            'province_name' => Yii::t('common', 'จังหวัด'),
            'region_code' => Yii::t('common', 'เขตสุขภาพ'),
            'province_lat' => Yii::t('common', 'พิกัดละติจูด'),
            'province_long' => Yii::t('common', 'พิกัดลองจิจูด'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['province_code' => 'province_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalities()
    {
        return $this->hasMany(Locality::className(), ['province_code' => 'province_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffices()
    {
        return $this->hasMany(Office::className(), ['province_code' => 'province_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegionCode()
    {
        return $this->hasOne(Region::className(), ['region_code' => 'region_code']);
    }
}
