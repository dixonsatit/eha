<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "assessment_attachment".
 *
 * @property int $id
 * @property int $article_id
 * @property string $path
 * @property string $base_url
 * @property string $type
 * @property int $size
 * @property string $name
 * @property int $created_at
 * @property int $order
 *
 * @property Article $article
 */
class AssessmentAttachment extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assessment_attachment';
    }

    public static function primaryKey()
    {
        return ['id'];
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assessment_id', 'path'], 'required'],
            [['assessment_id', 'size', 'created_at', 'order'], 'integer'],
            [['base_url', 'path', 'type', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'attachments' => Yii::t('common', 'ไฟล์'),
            'assessment_id' => Yii::t('common', 'Assessment ID'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'type' => Yii::t('common', 'Type'),
            'size' => Yii::t('common', 'Size'),
            'name' => Yii::t('common', 'Name'),
            'created_at' => Yii::t('common', 'Created At'),
            'order' => Yii::t('common', 'Order'),
            'files' => Yii::t('common', 'ไฟล์')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssessment()
    {
        return $this->hasOne(Assessment::className(), ['id' => 'assessment_id']);
    }


    public function getUrl()
    {
        return $this->base_url . '/' . $this->path;
    }

}
