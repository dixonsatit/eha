<?php

namespace common\models;

use backend\modules\rbac\models\RbacAuthAssignment;
use backend\modules\rbac\models\RbacAuthItem;
use common\commands\AddToTimelineCommand;
use common\models\query\UserQuery;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $email
 * @property string $auth_key
 * @property string $access_token
 * @property string $oauth_client
 * @property string $oauth_client_user_id
 * @property string $publicIdentity
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $logged_at
 * @property string $password write-only password
 *
 * @property \common\models\UserProfile $userProfile
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;

    const ROLE_USER = 'user';
    const ROLE_MANAGER = 'manager';
    const ROLE_ADMINISTRATOR = 'administrator';

    const EVENT_AFTER_SIGNUP = 'afterSignup';
    const EVENT_AFTER_LOGIN = 'afterLogin';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->active()
            ->andWhere(['id' => $id])
            ->one();
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->active()
            ->andWhere(['access_token' => $token])
            ->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return User|array|null
     */
    public static function findByUsername($username)
    {
        return static::find()
            ->active()
            ->andWhere(['username' => $username])
            ->one();
    }

    /**
     * Finds user by username or email
     *
     * @param string $login
     * @return User|array|null
     */
    public static function findByLogin($login)
    {
        return static::find()
            ->active()
            ->andWhere(['or', ['username' => $login], ['email' => $login]])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'auth_key' => [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'auth_key'
                ],
                'value' => Yii::$app->getSecurity()->generateRandomString()
            ],
            'access_token' => [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'access_token'
                ],
                'value' => function () {
                    return Yii::$app->getSecurity()->generateRandomString(40);
                }
            ]
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge(
            parent::scenarios(),
            [
                'oauth_create' => [
                    'oauth_client', 'oauth_client_user_id', 'email', 'username', '!status'
                ]
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_NOT_ACTIVE],
            ['status', 'in', 'range' => array_keys(self::statuses())],
            [['username'], 'filter', 'filter' => '\yii\helpers\Html::encode']
        ];
    }

    /**
     * Returns user statuses list
     * @return array|mixed
     */
    public static function statuses()
    {
        return [
            self::STATUS_NOT_ACTIVE => Yii::t('common', 'Not Active'),
            self::STATUS_ACTIVE => Yii::t('common', 'Active'),
            self::STATUS_DELETED => Yii::t('common', 'Deleted')
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Username'),
            'email' => Yii::t('common', 'E-mail'),
            'status' => Yii::t('common', 'Status'),
            'access_token' => Yii::t('common', 'API access token'),
            'created_at' => Yii::t('common', 'Created at'),
            'updated_at' => Yii::t('common', 'Updated at'),
            'logged_at' => Yii::t('common', 'Last login'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::class, ['user_id' => 'id']);
    }

    public function getRole()
    {
        return $this->hasOne(RbacAuthAssignment::class, ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Creates user profile and application event
     * @param array $profileData
     */
    public function afterSignup(array $profileData = [])
    {
        $this->refresh();
        Yii::$app->commandBus->handle(new AddToTimelineCommand([
            'category' => 'user',
            'event' => 'signup',
            'data' => [
                'public_identity' => $this->getPublicIdentity(),
                'user_id' => $this->getId(),
                'created_at' => $this->created_at
            ]
        ]));

        $profile = new UserProfile();
        $profile->load($profileData, '');
        $this->link('userProfile', $profile);
        $this->trigger(self::EVENT_AFTER_SIGNUP);

        // Default role
        $auth = Yii::$app->authManager;
        if(!empty($profileData['office_id'])){
            $auth->assign($auth->getRole(User::ROLE_MANAGER), $this->getId());
        }
        if(!empty($profileData['locality_code'])){
            $auth->assign($auth->getRole(User::ROLE_USER), $this->getId());
        }
    }

    /**
     * @return string
     */
    public function getPublicIdentity()
    {
        if ($this->userProfile && $this->userProfile->getFullName()) {
            return $this->userProfile->getFullName();
        }
        if ($this->username) {
            return $this->username;
        }
        return $this->email;
    }

    public static function getRoleUserLocality($role_name, $locality)
    {
        $connection = \Yii::$app->db;
        $connection->open();

        $sql = "SELECT u.id, u.username, u.email FROM rbac_auth_assignment a
                INNER JOIN user u on a.user_id = u.id
                LEFT JOIN user_profile p on p.user_id = u.id
                LEFT JOIN locality l on p.locality_code = p.locality_code
                LEFT JOIN office f on p.office_id = f.office_id
                WHERE a.item_name = '{$role_name}'
                AND p.locality_code = '{$locality}'
                GROUP BY u.id, u.username, u.email
                ";
        $command = $connection->createCommand($sql);

        $users = $command->queryAll();
        $connection->close();

        return $users;
    }

    public static function getRoleUsers($role_name, $province_code = null, $office_region = null)
    {
        $connection = \Yii::$app->db;
        $connection->open();

        $sql = "SELECT u.id, u.username, u.email, p.locality_code, pvl.province_name, pvo.province_name FROM user u
        LEFT JOIN user_profile p ON u.id = p.user_id
        LEFT JOIN locality l ON p.locality_code = l.locality_code
        LEFT JOIN office o ON p.office_id = o.office_id
        LEFT JOIN province pvl ON l.province_code = pvl.province_code
        LEFT JOIN province pvo ON o.province_code = pvo.province_code
        INNER JOIN rbac_auth_assignment a ON u.id = a.user_id
        WHERE (pvl.region_code = '{$office_region}' OR pvo.region_code = '{$office_region}') AND (a.item_name = '{$role_name}')
        GROUP BY u.id, u.username, u.email, p.locality_code, pvl.province_name, pvo.province_name";

        // $sql = "SELECT u.id, u.username, u.email FROM rbac_auth_assignment a
        //         INNER JOIN user u on a.user_id = u.id
        //         LEFT JOIN user_profile p on p.user_id = u.id
        //         LEFT JOIN locality l on p.locality_code = p.locality_code
        //         LEFT JOIN office f on p.office_id = f.office_id
        //         WHERE a.item_name = '{$role_name}'
        //         AND (l.province_code = '{$province_code}' OR f.office_region = '{$office_region}')
        //         GROUP BY u.id, u.username, u.email
        //         ";

        $command = $connection->createCommand($sql);

        $users = $command->queryAll();
        $connection->close();

        return $users;
    }

    public static function getRoleUsersAdmin($role_name)
    {
        $connection = \Yii::$app->db;
        $connection->open();

        $sql = "SELECT u.id, u.username, u.email FROM rbac_auth_assignment a
                INNER JOIN user u on a.user_id = u.id
                LEFT JOIN user_profile p on p.user_id = u.id
                WHERE a.item_name = '{$role_name}'
                GROUP BY u.id, u.username, u.email
                ";
        $command = $connection->createCommand($sql);

        $users = $command->queryAll();
        $connection->close();

        return $users;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
