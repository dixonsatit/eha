<?php

namespace common\components\custom;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use yii\base\Model;
use Yii;
use yii\httpclient\JsonParser;
use yii\httpclient\Client;

/**
 * Class LocalFlysystemProvider
 * @author Eugene Terentev <eugene@terentev.net>
 */
class Mail extends Model
{

    public function getMailer($email)
    {
        // detect if the email or domain is using Gmail to send emails
        if (Yii::$app->params['forwardGmail']) {
            // detect @gmail.com domain first
            if (str_ends_with($email, "@gmail.com")) {
                return Yii::$app->mailerGmail;
            }

            // extract domain name
            $parts = explode('@', $email);
            $domain = array_pop($parts);

            // check DNS using local server requests to DNS
            // if it fails query Google DNS service API (might have limits)
            if (getmxrr($domain, $mx_records)) {
                foreach ($mx_records as $record) {
                    if (stripos($record, "google.com") !== false || stripos($record, "googlemail.com") !== false) {
                        return Yii::$app->mailerGmail;
                    }
                }

                // return default mailer (if there were records detected but NOT google)
                return Yii::$app->mailer;
            }

            // make DNS request
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('GET')
                ->setUrl('https://dns.google.com/resolve')
                ->setData(['name' => $domain, 'type' => 'MX'])
                ->setOptions([
                    'timeout' => 5, // set timeout to 5 seconds for the case server is not responding
                ])
                ->send();

            if ($response->isOk) {
                $parser = new JsonParser();

                $data = $parser->parse($response);

                if ($data && array_key_exists("Answer", $data)) {
                    foreach ($data["Answer"] as $key => $value) {
                        if (array_key_exists("name", $value) && array_key_exists("data", $value)) {
                            if (stripos($value["name"], $domain) !== false) {
                                if (stripos($value["data"], "google.com") !== false || stripos($value["data"], "googlemail.com") !== false) {
                                    return Yii::$app->mailerGmail;
                                }
                            }
                        }
                    }
                }
            }
        }

        // return default mailer
        return Yii::$app->mailer;
    }

    public function sendEmail()
    {
        try {
            // find all active subscribers
            $message = Yii::$app->Custom->getMailer($this->email)->compose();

            $message->setTo([$this->email => $this->name]);
            $message->setFrom([\Yii::$app->params['supportEmail'] => "Bartosz Wójcik"]);
            $message->setSubject($this->subject);
            $message->setTextBody($this->body);

            $headers = $message->getSwiftMessage()->getHeaders();

            // message ID header (hide admin panel)
            $msgId = $headers->get('Message-ID');
            $msgId->setId(md5(time()) . '@pelock.com');

            $result = $message->send();

            return $result;
        } catch (\Exception $e) {
        }
    }
}
