<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 26/12/18 AD
 * Time: 09:28
 */

namespace common\widgets;


use common\models\ApplicationForm;
use yii\base\Widget;
use yii\helpers\Html;

class ApplicationFormDetail extends Widget {

    public $message;
    public $be_year;
    public $location;

    public function init(){
        parent::init();
        $appForm = ApplicationForm::find()->joinWith('locality')->where(['application_form.be_year' => $this->be_year])->andWhere(['application_form.locality_code' => \Yii::$app->user->identity->userProfile->locality_code])->one();
        if(!empty($appForm)){
            $this->be_year = $appForm->be_year;
            $this->location = "[".$appForm->locality_code."] " . $appForm->locality->locality_name;
            $this->message = 'การประเมินประจำปี:'.$this->be_year .'<br>'. $this->location;
        }else{
            return false;
        }
    }

    public function run(){
        return $this->render('@common/widgets/views/_applicationFormDetail', ['message' => $this->message]);
    }

} 