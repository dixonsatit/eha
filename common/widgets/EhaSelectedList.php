<?php

namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\VarDumper;

/**
 * Widget for display list of links to related models
 */
class EhaSelectedList extends Widget
{
    /**
     * @var \yii\db\ActiveRecord[] Related models
     */
    public $models = [];

    /**
     * @var string Base to build text content of the link.
     * You should specify attribute name. In case of dynamic generation ('getFullName()') you should specify just 'fullName'.
     */
    public $linkContentBase = 'issue_desc';

    /**
     * @var string Route to build url to related model
     */
    public $viewRoute;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!$this->models) {
            return null;
        }

        $items = [];
        foreach ($this->models as $model) {
            $items[] = Html::a($model->{$this->linkContentBase}, [$this->viewRoute, 'apply_issues' => $model->issue_code]);
        }

        return Html::ul($items, [
            'class' => 'list-unstyled',
            'encode' => false,
        ]);
    }
}