<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lpa_score_import`.
 */
class m190108_162054_create_lpa_score_import_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lpa_score_import', [
            'id' => $this->primaryKey(),
            'year' => $this->string(4)->notNull(),
            'file_path' => $this->string(255)->notNull(),
            'file_base_url' => $this->string(255)->notNull(),
            'total_record' => $this->integer(),
            'status' => $this->integer(1)->defaultValue(0),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lpa_score_import');
    }
}
