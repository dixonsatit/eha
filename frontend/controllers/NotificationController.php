<?php

namespace frontend\controllers;

use common\models\Assessment;
use common\models\AssessmentConclusion;
use common\models\Locality;
use common\models\Sop;
use Yii;
use common\models\ApplicationForm;
use frontend\models\search\ApplicationFormSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MainIssues;
use common\models\Issues;
use kartik\mpdf\Pdf;
use Da\QrCode\QrCode;

/**
 * ApplicationFormController implements the CRUD actions for ApplicationForm model.
 */
class NotificationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'print-certificated'],
                        'allow' => true,
                    ],

                    [
                        'actions' => [
                            ''
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/default/index']);
                        },

                    ],
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    public function actionNot1()
    {
    }
}
