<?php

namespace frontend\controllers;

use common\models\Issues;
use Yii;
use common\models\AssessmentConclusion;
use frontend\models\search\ApplicationFormSearch;
use frontend\models\search\AssessmentConclusionSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Session;

/**
 * AssessmentConclusionController implements the CRUD actions for AssessmentConclusion model.
 */
class AssessmentConclusionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all AssessmentConclusion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AssessmentConclusionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPassed()
    {

        // VarDumper::dump(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id, 10, true));
        // die();

        if (Yii::$app->user->can('administrator')) { //สำนักงานปกครองส่วนท้องถิ่น (อปท)
            $searchModel = new ApplicationFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        } else if (Yii::$app->user->can('director')) { //ศูนย์อนามัย เขต
            $searchModel = new ApplicationFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['province.region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code]);
        } else if (Yii::$app->user->can('manager')) { //สำนักงานสาธารณสุขจังหวัด (สสจ)
            $searchModel = new ApplicationFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['province.province_code' => Yii::$app->user->identity->userProfile->office->province_code]);
        } else if (Yii::$app->user->can('user')) { //สำนักงานปกครองส่วนท้องถิ่น (อปท)
            $searchModel = new ApplicationFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['province.province_code' => Yii::$app->user->identity->userProfile->office->province_code]);
        }

        return $this->render('passed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExpandConclusion()
    {
        if (isset($_POST['expandRowKey']['be_year']) && isset($_POST['expandRowKey']['locality_code'])) {
            $models = AssessmentConclusion::find()->joinWith('assessResult')->joinWith('issueCode')->where(['be_year' => $_POST['expandRowKey']['be_year']])->andWhere(['locality_code' => $_POST['expandRowKey']['locality_code']])->andWhere(['approve' => 'Complete'])->all();
            return $this->renderPartial('_expandConclusion.php', ['models' => $models]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }
    /**
     * Displays a single AssessmentConclusion model.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $locality_code
     * @param string $type
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($be_year, $issue_code, $locality_code, $type)
    {
        return $this->render('view', [
            'model' => $this->findModel($be_year, $issue_code, $locality_code, $type),
        ]);
    }

    /**
     * Creates a new AssessmentConclusion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AssessmentConclusion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'locality_code' => $model->locality_code, 'type' => $model->type]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AssessmentConclusion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $locality_code
     * @param string $type
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'locality_code' => $model->locality_code, 'type' => $model->type]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionApprove($issue_code, $locality_code)
    {
        $model = \common\models\AssessmentConclusion::find()->where(['issue_code' => $issue_code])->andWhere(['locality_code' => $locality_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'assessor'])->one();

        if ($model->load(Yii::$app->request->post())) {
            $model->approve = "Complete";
            $model->save();
            $email = Yii::$app->session['applicationForm']['email'];
            $issue = Issues::find()->where(['issue_code' => $model->issue_code])->one();
            // Yii::$app->mailer->compose()
            //     ->setFrom(['eha@anamai.mail.go.th' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
            //     ->setTo(!empty($email) ? $email : 'example@mail.com')
            //     ->setSubject('แจ้งผลการประเมิน')
            //     ->setHtmlBody('ผมการประเมิน' . $issue->issue_desc . '<h4>' . $model->assessResult->result_desc . '</h4>')
            //     ->send();

            Yii::$app->getSession()->setFlash('alert', [
                'body' => 'แจ้งผลประเมินเรียบร้อยแล้ว',
                'options' => ['class' => 'alert alert-success', 'role' => 'alert']
            ]);

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->renderAjax('_approve', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AssessmentConclusion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $locality_code
     * @param string $type
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($be_year, $issue_code, $locality_code, $type)
    {
        $this->findModel($be_year, $issue_code, $locality_code, $type)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AssessmentConclusion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $locality_code
     * @param string $type
     * @return AssessmentConclusion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AssessmentConclusion::findOne(['id' => $id,])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
