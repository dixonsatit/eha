<?php

namespace frontend\controllers;

use Yii;

class GisController extends \yii\web\Controller
{
    public function actionIndex($year = null)
    {
        $year = $year==null ? date('Y') +543 : $year; 
        $makers = [];
        $conn = Yii::$app->db;
        $makers = $conn->cache(function ($db) use ($year) {
            $sql = "select g.lat,g.lon, l.`locality_name`, c.`be_year`, c.`locality_code`, c.`assess_result`, r.`result_desc` , c.`issue_code`,
            if(c.`assess_result` > 3,r.`result_desc`,'ไม่ผ่าน') as 'result'
            from `assessment_conclusion` as c
            inner join `application_form` as a on a.id = c.`application_form_id`
            inner join `locality` as l on l.`locality_code` = c.`locality_code`
            inner join `assessment_result` as r on r.id = c.`assess_result`
            inner join (select * from geojson 
            where geojson.areatype = 4 
            and geojson.areacode != ''
            group by geojson.areacode ) as g on g.`areacode` = l.`subdistrict_code`
            where 
                g.areatype = 4 
            and areacode != ''
            and a.`be_year` = :year
            group by c.`locality_code`";
            return $db->createCommand($sql)
            ->bindValues([':year'=> $year])
            ->queryAll();
        });

        return $this->render('index', [
            'makers' => $makers,
            'geo' => json_encode($this->geo())
        ]);
    }

    public function geo() 
    {
        $conn = Yii::$app->db;
        $sql = 'select *, region.region_name from geojson 
                inner join region on region.region_code = geojson.areacode
                where areatype = 1';
        $cmd = $conn->createCommand($sql);
        $mData = $cmd->queryAll();
        $gd = [];
        foreach ($mData as $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => [
                    "name"=> $m['region_name']
                ],
                "geometry" => json_decode($m['geojson']),
            ];
        }
        return $gd;
    }
}
