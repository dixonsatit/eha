<?php

namespace frontend\controllers;

use common\models\AssessmentConclusion;
use Yii;
use BaconQrCode\Common\Mode;
use common\models\ApplicationForm;
use common\models\Assessment;
use common\models\AssessmentAttachment;
use common\models\Issues;
use common\models\Locality;
use common\models\Sop;
use common\models\User;
use Exception;
use yii\web\Response;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Transaction;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AssessmentController implements the CRUD actions for Assessment model.
 */
class AssessmentController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Assessment models.
     * @return mixed
     */
    public function actionIndex($application_form_id)
    {
        $assessment = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->where(['assessment.application_form_id' => $application_form_id])->andWhere(['assessment.locality_code' => Yii::$app->user->identity->userProfile->locality_code])->all();

        $dataProvider = ArrayDataProvider::className([
            'allModels' => $assessment,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'assessment' => $assessment,
        ]);
    }

    public function actionListIssues($application_form_id)
    {
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        }

        if (isset($application_form_id)) {
            $applicationForm = ApplicationForm::find()->joinWith('locality')->where(['application_form.id' => $application_form_id])->one();
            if ($applicationForm != null) {
                $apply_issues = Json::decode($applicationForm->apply_issues);
                $apply_issues = is_array($apply_issues) ? $apply_issues : [];
                Yii::$app->session['applicationForm'] = [
                    'applicationForm' => $applicationForm,
                    'application_form_id' => $applicationForm->id,
                    'be_year' => $applicationForm->be_year,
                    'apply_issues' => $apply_issues,
                    'locality_code' => $applicationForm->locality_code,
                    'status' => $applicationForm->status,
                    'locality_name' => $applicationForm->locality->locality_name,
                ];
            } else {
                Yii::$app->session['applicationForm'] = [
                    'application_form_id' => '',
                    'be_year' => '',
                    'apply_issues' => [],
                    'locality_code' => '',
                    'status' => 0,
                    'locality_name' => '',
                ];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $query = Issues::find()->where(['IN', 'issues.issue_code', Yii::$app->session['applicationForm']['apply_issues']]);
        // VarDumper::dump($applicationForm, 10, true);
        // die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->render('list-issues', [
            'dataProvider' => $dataProvider,
            'applicationForm' => $applicationForm,
            'application_form_id' => Yii::$app->session['applicationForm']['application_form_id'],
        ]);
    }


    //รายการประเด็นส่งขอรับการประเมิน
    public function actionListSendIssues($application_form_id)
    {
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        }

        if (isset($application_form_id)) {
            $applicationForm = ApplicationForm::find()->joinWith('locality')->where(['application_form.id' => $application_form_id])->one();
            if ($applicationForm != null) {
                $apply_issues = Json::decode($applicationForm->apply_issues);
                $apply_issues = is_array($apply_issues) ? $apply_issues : [];
                Yii::$app->session['applicationForm'] = [
                    'applicationForm' => $applicationForm,
                    'application_form_id' => $applicationForm->id,
                    'be_year' => $applicationForm->be_year,
                    'apply_issues' => $apply_issues,
                    'locality_code' => $applicationForm->locality_code,
                    'status' => $applicationForm->status,
                    'locality_name' => $applicationForm->locality->locality_name,
                ];
            } else {
                Yii::$app->session['applicationForm'] = [
                    'application_form_id' => '',
                    'be_year' => '',
                    'apply_issues' => [],
                    'locality_code' => '',
                    'status' => 0,
                    'locality_name' => '',
                ];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $query = Issues::find()->where(['IN', 'issues.issue_code', Yii::$app->session['applicationForm']['apply_issues']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->render('list-send-issues', [
            'dataProvider' => $dataProvider,
            'application_form_id' => Yii::$app->session['applicationForm']['application_form_id'],
        ]);
    }

    public function actionSendAssessment($issue_code)
    {

        $query = Issues::find()->where(['IN', 'issues.issue_code', Yii::$app->session['applicationForm']['apply_issues']]);

        $assessments = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->where(['sop.issue_code' => $issue_code])->andWhere(['assessment.application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->all();

        $assessmentConclusion = AssessmentConclusion::find()->where(['issue_code' => $issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->one();

        if ($assessmentConclusion != null) {
            $assessmentConclusion->approve = 'Sent';
            $assessmentConclusion->assess_result = 2;

            $assessmentConclusion->save(false);

            $admins = User::getRoleUsers('administrator', Yii::$app->user->identity->userProfile->locality->provinceCode->region_code);
            $managers = User::getRoleUsers('manager', Yii::$app->user->identity->userProfile->locality->provinceCode->region_code);

            $users = array_merge($admins, $managers);

            $messages = [];
            // foreach ($users as $user) {
            $locality = Locality::find()->where(['locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one()->locality_name;
            //     $messages[] = Yii::$app->mailer->compose()
            //         ->setFrom(['eha@anamai.mail.go.th' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
            //         ->setSubject('มีการส่งขอรับการประเมิน เข้ามาใหม่')
            //         ->setTextBody('มีการส่งขอรับการประเมิน เข้ามาใหม่' . " " . $locality)
            //         ->setTo($user['email']);
            // }
            // Yii::$app->mailer->sendMultiple($messages);

            Yii::$app->getSession()->setFlash('alert', [
                'body' => 'บันทึกข้อมูลเรียบร้อยแล้ว',
                'options' => ['class' => 'alert alert-success', 'role' => 'alert']
            ]);

            return $this->redirect(Yii::$app->request->referrer);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->render('list-issues', [
            'dataProvider' => $dataProvider,
            'application_form_id' => Yii::$app->session['applicationForm']['application_form_id'],
        ]);
    }


    /**
     * Displays a single Assessment model.
     * @param string $locality_code
     * @param integer $sop_id
     * @return mixed
     */
    public function actionView($locality_code, $sop_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($locality_code, $sop_id),
        ]);
    }

    /**
     * Creates a new Assessment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($sop_id)
    {
        $assessment = new Assessment();

        if ($assessment->load(Yii::$app->request->post()) && $assessment->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $assessment,
        ]);
    }

    public function actionCreateByIssue($application_form_id, $issue_code)
    {

        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        }

        if (isset($application_form_id)) {
            $applicationForm = ApplicationForm::find()->joinWith('locality')->where(['application_form.id' => $application_form_id])->one();
            if ($applicationForm != null) {
                $apply_issues = Json::decode($applicationForm->apply_issues);
                $apply_issues = is_array($apply_issues) ? $apply_issues : [];
                Yii::$app->session['applicationForm'] = [
                    'applicationForm' => $applicationForm,
                    'application_form_id' => $applicationForm->id,
                    'be_year' => $applicationForm->be_year,
                    'apply_issues' => $apply_issues,
                    'locality_code' => $applicationForm->locality_code,
                    'status' => $applicationForm->status,
                    'locality_name' => $applicationForm->locality->locality_name,
                ];
            } else {
                Yii::$app->session['applicationForm'] = [
                    'application_form_id' => '',
                    'be_year' => '',
                    'apply_issues' => [],
                    'locality_code' => '',
                    'status' => 0,
                    'locality_name' => '',
                ];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Assessment();
        $models = [new Assessment()];

        $mainSops = Sop::find()->joinWith('issueCode')->joinWith('issueCode.mainIssueCode')->where(['issues.issue_code' => $issue_code])->andWhere(['sop.be_year' => $applicationForm->be_year - 1])->orderBy(['sop.id' => SORT_ASC])->all();

        foreach ($mainSops as $key => $mainSop) {
            $model = new Assessment([
                'application_form_id' =>  $applicationForm->id,
                'main_issue_code' => $mainSop['issueCode']['main_issue_code'],
                'main_issue_desc' => $mainSop['issueCode']['mainIssueCode']['main_issue_desc'],
                'issue_code' => $mainSop['issue_code'],
                'issue_desc' => $mainSop['issueCode']['issue_desc'],
                'sop_id' => $mainSop['id'],
                'sop_assess_step_detail' => $mainSop['assess_step_detail'],
                'assess_step_evidence' => $mainSop['assess_step_evidence'],
                'selfassess_score' => 0,
                'selfassess_date' => '',
                'assessor_score' => 0,
                'assessor_date' => '',
                'assessor_notes' => '',
                'locality_code' => Yii::$app->user->identity->userProfile->locality_code,
                'sop_parent_id' => $mainSop['parent_id'],
                'sop_issue_code' => (int)$mainSop['issue_code'],
                'sop_assess_step' => $mainSop['assess_step'],
                'sop_assess_step_detail' => $mainSop['assess_step_detail'],
                'sop_assess_score' => $mainSop['assess_score'],
            ]);
            $models[] = $model;
            $model->save();
        }

        return $this->redirect(['update-by-issue', 'issue_code' => $issue_code, 'application_form_id' => $application_form_id]);
    }

    public function actionUpdateByIssue($issue_code)
    {
        $model = ApplicationForm::find()->where(['id' => Yii::$app->session['applicationForm']['application_form_id']])->one();
        $mainSops = Sop::find()->joinWith('issueCode')->joinWith('issueCode.mainIssueCode')->where(['issues.issue_code' => $issue_code])->orderBy(['sop.id' => SORT_ASC])->all();

        $assessments = Assessment::getDb()->cache(function ($db) use ($issue_code) {
            return Assessment::find()
            ->joinWith('sop')
            ->joinWith('sop.issueCode')
            ->joinWith('sop.issueCode.mainIssueCode')
            ->where(['assessment.application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])
            ->andWhere(['sop.issue_code' => $issue_code])
            ->orderBy(['sop.id' => SORT_ASC])
            ->all(); // ดึงข้อมูล Assessment
        });

        foreach ($assessments as $key => $assessment) {
            $models[] = new Assessment([
                'sopObj' => $assessment['sop'],
                'main_issue_code' => $assessment['sop']['issueCode']['main_issue_code'],
                'main_issue_desc' => $assessment['sop']['issueCode']['mainIssueCode']['main_issue_desc'],
                'issue_code' => $assessment['sop']['issue_code'],
                'issue_desc' => $assessment['sop']['issueCode']['issue_desc'],
                'id' => $assessment['id'],
                'sop_id' => $assessment['sop']['id'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'assess_step_evidence' => $assessment['sop']['assess_step_evidence'],
                'selfassess_score' => $assessment['selfassess_score'] != null ? $assessment['selfassess_score'] : 0,
                'selfassess_date' => $assessment['selfassess_date'],
                'assessor_score' => $assessment['assessor_score'] != null ? $assessment['assessor_score'] : 0,
                'assessor_date' => $assessment['assessor_date'],
                'assessor_notes' => $assessment['assessor_notes'],
                'locality_code' => Yii::$app->user->identity->userProfile->locality_code,
                'sop_parent_id' => $assessment['sop']['parent_id'],
                'sop_issue_code' => $assessment['sop']['issue_code'],
                'sop_assess_step' => $assessment['sop']['assess_step'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'sop_assess_score' => $assessment['sop']['assess_score'],
            ]);
        }

        if (Model::loadMultiple($assessments, Yii::$app->request->post())) {
            foreach ($assessments as $assessment) {
                $assessment->save(false);
                unset($assessment);
            }

            if ($issue_code) {
                $this->saveScoresByIssue($model->id, $model->be_year, 'selfassess', $model->locality_code, $issue_code);
            }

            Yii::$app->getSession()->setFlash('alert', [
                'body' => 'บันทึกข้อมูลเรียบร้อยแล้ว',
                'options' => ['class' => 'alert alert-success', 'role' => 'alert']
            ]);

            return $this->redirect(['view-by-issue', 'issue_code' => $issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]);
        }

        return $this->render('create-by-issue', [
            'model' => $model,
            'models' => $models,
            'mainSops' => $mainSops,
        ]);
    }

    public function actionViewByIssue($issue_code, $application_form_id)
    {

        $assessment = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->andWhere(['sop.issue_code' => $issue_code])->andWhere(['assessment.application_form_id' => $application_form_id])->orderBy(['sop.id' => SORT_ASC])->all();
        $applicationForm = ApplicationForm::find()->where(['id' => $application_form_id])->one();

        $dataProvider = ArrayDataProvider::className([
            'allModels' => $assessment,
            'pagination' => false,
        ]);

        return $this->render('view-by-issue', [
            'dataProvider' => $dataProvider,
            'assessment' => $assessment,
            'applicationForm' => $applicationForm
        ]);
    }

    public function actionUpdate($application_form_id)
    {
        $appForm = ApplicationForm::find()->joinWith('locality')->joinWith('locality.typeCode')->where(['application_form.id' => $application_form_id])->andWhere(['application_form.locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();
        $model = ApplicationForm::find()->where(['id' => $application_form_id])->one();
        $mainSops = Sop::find()->joinWith('issueCode')->joinWith('issueCode.mainIssueCode')->where(['issues.id' => $appForm->id])->all();

        $assessments = Assessment::getDb()->cache(function ($db) use ($application_form_id) {
            return Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->where(['assessment.application_form_id' => $application_form_id])->all(); // ดึงข้อมูล Assessment
        });

        foreach ($assessments as $key => $assessment) {
            $models[] = new Assessment([
                'main_issue_code' => $assessment['sop']['issueCode']['main_issue_code'],
                'main_issue_desc' => $assessment['sop']['issueCode']['mainIssueCode']['main_issue_desc'],
                'issue_code' => $assessment['sop']['issue_code'],
                'issue_desc' => $assessment['sop']['issueCode']['issue_desc'],
                'id' => $assessment['id'],
                'sop_id' => $assessment['sop']['id'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'selfassess_score' => $assessment['selfassess_score'],
                'selfassess_date' => $assessment['selfassess_date'],
                'assessor_score' => $assessment['assessor_score'] != null ? $assessment['assessor_score'] : 0,
                'assessor_date' => $assessment['assessor_date'],
                'assessor_notes' => $assessment['assessor_notes'],
                'locality_code' => Yii::$app->user->identity->userProfile->locality_code,
                'sop_parent_id' => $assessment['sop']['parent_id'],
                'sop_issue_code' => $assessment['sop']['issue_code'],
                'sop_assess_step' => $assessment['sop']['assess_step'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'sop_assess_score' => $assessment['sop']['assess_score'],
            ]);
        }

        if (Model::loadMultiple($assessments, Yii::$app->request->post())) {
            foreach ($assessments as $assessment) {
                $assessment->save(false);
                unset($assessment);
            }

            $issues = json_decode($model->apply_issues);
            if (is_array($issues)) {
                $this->saveScores($model->id, $model->be_year, 'selfassess', $model->locality_code, $issues);
            }

            return $this->redirect(['index', 'application_form_id' => $application_form_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'models' => $models,
            'appForm' => $appForm,
        ]);
    }

    public function actionUploadById($id)
    {
        $model = Assessment::findOne($id);
        $query = AssessmentAttachment::find()->where(['assessment_id' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'status' => 'success',
                'total' => count($model->assessmentAttachments)
            ];
        }

        return $this->renderAjax('uploads-by-id', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDownloadFile($id)
    {

        $this->layout = '@flareui/views/layouts/modal.php';

        $id = Assessment::findOne($id);
        $assessment = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->where(['assessment.sop_id' => $id])->one();
        $query = AssessmentAttachment::find()->where(['assessment_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($assessment->load(Yii::$app->request->post()) && $assessment->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->redirect(['view-by-issue', 'issue_code' => $assessment->sop->issueCode->issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]);
        }

        return $this->render('download-file', [
            'model' => $id,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionComment($id)
    {
        $assessment = Assessment::find()->where(['id' => $id])->one();
        if ($assessment) {
            return $this->renderAjax('_comment-form', [
                'model' => $assessment,
            ]);
        } else {
            return false;
        }
    }

    public function saveScores($application_form_id, $year, $type, $localityCode, $issues = [])
    {

        foreach ($issues as $issue) {
            if ($issue === 'EHA1001') {
                $eha = new \common\ehascores\Score1001();
            } else if ($issue === 'EHA1002') {
                $eha = new \common\ehascores\Score1002();
            } else if ($issue === 'EHA1003') {
                $eha = new \common\ehascores\Score1003();
            } else if ($issue === 'EHA2001') {
                $eha = new \common\ehascores\Score2001();
            } else if ($issue === 'EHA2002') {
                $eha = new \common\ehascores\Score2002();
            } else if ($issue === 'EHA2003') {
                $eha = new \common\ehascores\Score2003();
            } else if ($issue === 'EHA3001') {
                $eha = new \common\ehascores\Score3001();
            } else if ($issue === 'EHA3002') {
                $eha = new \common\ehascores\Score3002();
            } else if ($issue === 'EHA4001') {
                $eha = new \common\ehascores\Score4001();
            } else if ($issue === 'EHA4002') {
                $eha = new \common\ehascores\Score4002();
            } else if ($issue === 'EHA5000') {
                $eha = new \common\ehascores\Score5000();
            } else if ($issue === 'EHA6000') {
                $eha = new \common\ehascores\Score6000();
            } else if ($issue === 'EHA7000') {
                $eha = new \common\ehascores\Score7000();
            } else if ($issue === 'EHA8000') {
                $eha = new \common\ehascores\Score8000();
            } else if ($issue === 'EHA9001') {
                $eha = new \common\ehascores\Score9001();
            } else if ($issue === 'EHA9002') {
                $eha = new \common\ehascores\Score9002();
            } else if ($issue === 'EHA9003') {
                $eha = new \common\ehascores\Score9003();
            } else if ($issue === 'EHA9004') {
                $eha = new \common\ehascores\Score9004();
            } else if ($issue === 'EHA9005') {
                $eha = new \common\ehascores\Score9005();
            }

            $eha->appFormID = $application_form_id;
            $eha->issueCode = $issue;
            $eha->year = $year;
            $eha->type = $type;
            $eha->localityCode = $localityCode;
            $eha->calculate();
            unset($eha);
        }
    }

    public function saveScoresByIssue($application_form_id, $year, $type, $localityCode, $issue)
    {
        if ($issue === 'EHA1001') {
            $eha = new \common\ehascores\Score1001();
        } else if ($issue === 'EHA1002') {
            $eha = new \common\ehascores\Score1002();
        } else if ($issue === 'EHA1003') {
            $eha = new \common\ehascores\Score1003();
        } else if ($issue === 'EHA2001') {
            $eha = new \common\ehascores\Score2001();
        } else if ($issue === 'EHA2002') {
            $eha = new \common\ehascores\Score2002();
        } else if ($issue === 'EHA2003') {
            $eha = new \common\ehascores\Score2003();
        } else if ($issue === 'EHA3001') {
            $eha = new \common\ehascores\Score3001();
        } else if ($issue === 'EHA3002') {
            $eha = new \common\ehascores\Score3002();
        } else if ($issue === 'EHA4001') {
            $eha = new \common\ehascores\Score4001();
        } else if ($issue === 'EHA4002') {
            $eha = new \common\ehascores\Score4002();
        } else if ($issue === 'EHA4003') {
            $eha = new \common\ehascores\Score4003();
        } else if ($issue === 'EHA5000') {
            $eha = new \common\ehascores\Score5000();
        } else if ($issue === 'EHA6000') {
            $eha = new \common\ehascores\Score6000();
        } else if ($issue === 'EHA7000') {
            $eha = new \common\ehascores\Score7000();
        } else if ($issue === 'EHA8000') {
            $eha = new \common\ehascores\Score8000();
        } else if ($issue === 'EHA9001') {
            $eha = new \common\ehascores\Score9001();
        } else if ($issue === 'EHA9002') {
            $eha = new \common\ehascores\Score9002();
        } else if ($issue === 'EHA9003') {
            $eha = new \common\ehascores\Score9003();
        } else if ($issue === 'EHA9004') {
            $eha = new \common\ehascores\Score9004();
        } else if ($issue === 'EHA9005') {
            $eha = new \common\ehascores\Score9005();
        }

        $eha->appFormID = $application_form_id;
        $eha->issueCode = $issue;
        $eha->year = $year;
        $eha->type = $type;
        $eha->localityCode = $localityCode;
        $eha->calculate();
        unset($eha);
    }

    /**
     * Deletes an existing Assessment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $locality_code
     * @param integer $sop_id
     * @return mixed
     */
    public function actionDelete($locality_code, $sop_id)
    {
        $this->findModel($locality_code, $sop_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Assessment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $locality_code
     * @param integer $sop_id
     * @return Assessment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assessment::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
