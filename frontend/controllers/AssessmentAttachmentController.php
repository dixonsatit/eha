<?php

namespace frontend\controllers;

use common\models\Assessment;
use Yii;
use common\models\AssessmentAttachment;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * AssessmentAttachmentController implements the CRUD actions for AssessmentAttachment model.
 */
class AssessmentAttachmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'upload' => [
                'class' => 'trntv\filekit\actions\UploadAction',
                //'deleteRoute' => 'my-custom-delete', // my custom delete action for deleting just uploaded files(not yet saved)
                //'fileStorage' => 'myfileStorage', // my custom fileStorage from configuration
                'multiple' => true,
                'disableCsrf' => true,
                'responseFormat' => Response::FORMAT_JSON,
                'responsePathParam' => 'path',
                'responseBaseUrlParam' => 'base_url',
                'responseUrlParam' => 'url',
                'responseDeleteUrlParam' => 'delete_url',
                'responseMimeTypeParam' => 'type',
                'responseNameParam' => 'name',
                'responseSizeParam' => 'size',
                'deleteRoute' => 'delete',
                'fileStorage' => 'fileStorage', // Yii::$app->get('fileStorage')
                'fileStorageParam' => 'fileStorage', // ?fileStorage=someStorageComponent
                'sessionKey' => '_uploadedFiles',
                'allowChangeFilestorage' => false,
                'validationRules' => [
                ],
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    // do something (resize, add watermark etc)
                }
            ],

            'view' => [
                'class' => 'trntv\filekit\actions\ViewAction',
            ],

            'delete' => [
                'class' => 'trntv\filekit\actions\DeleteAction',
                //'fileStorage' => 'fileStorageMy', // my custom fileStorage from configuration(such as in the upload action)
            ],
        ];
    }

    /**
     * Lists all AssessmentAttachment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AssessmentAttachment::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AssessmentAttachment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AssessmentAttachment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $this->layout = '@flareui/views/layouts/blank.php';

        $assessment = Assessment::find()->joinWith('sop')->where(['assessment.id' => $id])->one();
        $attachment = AssessmentAttachment::find()->where(['assessment_id' => $id])->count();

        $model = new AssessmentAttachment();

        if (Model::loadMultiple(Yii::$app->request->post()) && $model['files'] != null) {

            foreach ($model['files'] as $key => $file) {
                    $model->assessment_id = $id;
                    $model->path = $file['path'];
                    $model->name = $file['name'];
                    $model->size = $file['size'];
                    $model->type = $file['type'];
                    $model->order = $file['order'];
                    $model->base_url = $file['base_url'];
                    $model->save();
            }

            return $this->redirect(['index']);

        }

        if ($attachment > 0) {
            return $this->redirect(['update',
                'model' => $model,
                'assessment' => $assessment,
                'id' => $id,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'assessment' => $assessment,
            'id' => $id,
        ]);
    }

    /**
     * Updates an existing AssessmentAttachment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $assessment = Assessment::find()->joinWith('assessmentAttachment')->joinWith('sop')->where(['assessment_attachment.assessment_id' => $id])->all();
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
        return $this->render('update', [
            'assessment' => $assessment,
        ]);
    }

    /**
     * Deletes an existing AssessmentAttachment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AssessmentAttachment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AssessmentAttachment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AssessmentAttachment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
