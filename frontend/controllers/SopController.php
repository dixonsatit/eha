<?php

namespace frontend\controllers;

use Yii;
use common\models\Sop;
use frontend\models\search\SopSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SopController implements the CRUD actions for Sop model.
 */
class SopController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sop models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sop model.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $assess_step
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($be_year, $issue_code, $assess_step)
    {
        return $this->render('view', [
            'model' => $this->findModel($be_year, $issue_code, $assess_step),
        ]);
    }

    /**
     * Creates a new Sop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sop();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $assess_step
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($be_year, $issue_code, $assess_step)
    {
        $model = $this->findModel($be_year, $issue_code, $assess_step);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $assess_step
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($be_year, $issue_code, $assess_step)
    {
        $this->findModel($be_year, $issue_code, $assess_step)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Sop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $be_year
     * @param string $issue_code
     * @param string $assess_step
     * @return Sop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($be_year, $issue_code, $assess_step)
    {
        if (($model = Sop::findOne(['be_year' => $be_year, 'issue_code' => $issue_code, 'assess_step' => $assess_step])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
