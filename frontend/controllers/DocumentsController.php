<?php

namespace frontend\controllers;

use backend\models\ConfigReport;
use common\models\Assessment;
use common\models\AssessmentConclusion;
use common\models\Locality;
use common\models\Sop;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Yii;
use common\models\ApplicationForm;
use frontend\models\search\ApplicationFormSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MainIssues;
use common\models\Issues;
use kartik\mpdf\Pdf;
use Da\QrCode\QrCode;

/**
 * ApplicationFormController implements the CRUD actions for ApplicationForm model.
 */
class DocumentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'print-certificated'],
                        'allow' => true,
                    ],

                    [
                        'actions' => [
                            'print-diploma', 'diploma', 'list-issues', 'print-diploma-by-code', 'print-appform',
                            'print-assessment',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/default/index']);
                        },

                    ],
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    public function actionDiploma()
    {

        $query = ApplicationForm::find()->where(['locality_code' => \Yii::$app->user->identity->userProfile->locality_code])->orderBy(['id' => SORT_DESC])->one();
        if ($query != null) {
            $issues = Issues::find()->joinWith('mainIssueCode')->andWhere(['in', 'issues.issue_code', Json::decode($query->apply_issues)])->all();
            return $this->render('diploma', [
                'issues' => $issues
            ]);
        }
    }


    public function actionPrintDiploma()
    {
        $locality = Locality::find()->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => \Yii::$app->user->identity->userProfile->locality_code])->one();
        $applicationForm = ApplicationForm::find()->where(['locality_code' => \Yii::$app->user->identity->userProfile->locality_code])->orderBy(['id' => SORT_DESC])->one();
        $issues = Issues::find()->joinWith('mainIssueCode')->andWhere(['in', 'issues.issue_code', Json::decode($applicationForm->apply_issues)])->all();

        $content = $this->renderPartial('diploma-pdf', [
            'locality' => $locality,
            'issues' => $issues

        ]);
        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            //'cssFile' => '@frontend/web/css/pdf.css',
            'cssInline' => '
                        body {
                            font-family: "Garuda";
                            font-size: 16px;
                            line-height: 1.42857143;
                            color: #333;
                            background-color: #fff;
                            }
                            p{
                                font-size: 14px;
                            }
                            table {
                                    border-collapse: collapse;
                                    width: 100%;
                                }
                            table, th, td {
                                    border: 1px solid black;
                                    padding: 5px;
                                }
                            @media all
                                {
                                .page{page-break-inside:avoid;}
                            @media all
                                {
                                   .page-break { display:none; }
                                   .page-break-on{ display:none; }
                                }
                            @media print
                                {
                                    .page-break { display:block;height:1px; page-break-before:always; }
                                    .page-break-on{ display:block;height:1px; page-break-inside:avoid; }
                                }
                            ',
            'options' => [
                'title' => 'สรุปผลตรวจประเมินรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม',
                'subject' => 'สรุปผลตรวจประเมินรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม',
                'keywords' => 'สรุปผลตรวจประเมินรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม, EHA, Environmental Health Accreditation',
            ],

            'methods' => [
                'SetHeader' => false,
                'SetFooter' => false,
            ]
        ]);

        return $pdf->render();
    }

    public function actionPrintCertificated()
    {
        $locality = Locality::find()->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();
        $countIssue = Issues::find()->count();
        $cerForm = ConfigReport::findOne(1);
        $assessmentConclusions = AssessmentConclusion::find()->joinWith('issueCode')->where(['assessment_conclusion.be_year' => Yii::$app->session['applicationForm']['be_year']])
            ->andWhere(['assessment_conclusion.locality_code' => Yii::$app->session['applicationForm']['locality_code']])
            ->andWhere(['assessment_conclusion.type' => 'assessor'])
            ->andWhere(['assessment_conclusion.approve' => 'Complete'])
          //->andWhere(['>=', 'assessment_conclusion.assess_result', '5'])
            ->orderBy(['issues.issue_code' => 'ASC'])
            ->all();

        $countConclusion = count($assessmentConclusions);

        $rowEmpty = $countIssue - $countConclusion;

        $qrCode = (new QrCode(Url::base(true) . Url::to(['/documents/print-certificated', []])))
            ->setSize(200)
            ->setMargin(5)
            ->useForegroundColor(51, 153, 255);

        $qrCode->writeFile(Yii::getAlias('@frontend') . '/web/img/code.png');

        if ($assessmentConclusions != null) {

            $content = $this->renderPartial('certificated', [
                'locality' => $locality,
                'rowEmpty' => $rowEmpty,
                'assessmentConclusions' => $assessmentConclusions,
                'cerForm' => !empty($cerForm) ? $cerForm : []
            ]);
        } else {

            $content = $this->renderPartial('certificated', [
                'locality' => $locality,
                'rowEmpty' => $rowEmpty,
                'cerForm' => !empty($cerForm) ? $cerForm : [],
                'assessmentConclusions' => $assessmentConclusions,
                'failAssessment' => 'ยังไม่มีประเด็นที่ผ่านการประเมิน'
            ]);
        }

        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        $pdf->cssInline = '  
        body {
            font-family: "dsnptm";
            font-size: 16px;
            line-height: 1.42857143px;
            color: #333;
            background-color: #fff;
        }
        h2{
            line-height: 1.42857143px;
        }
        .table_bordered {
            border: 1px solid #000;
            margin-bottom: 10px;
        }
        
        .table_bordered td {
            border: 0.5px solid #000;
            padding: 3px;
        }
        
        @page {
            background-image: url("img/eha-bg-a4.png");
            background-image-resize: 6
        }                  
        ';

        return $pdf->render();
    }

    public function actionPrintDiplomaByCode($issue_code)
    {
        $locality = Locality::find()->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();

        $assessmentCon = AssessmentConclusion::getScores($issue_code);

        if ($assessmentCon) {

            $content = $this->renderPartial('diploma-pdf', [
                'locality' => $locality,
                //'issues' => $issues,
                'assessmentCon' => $assessmentCon

            ]);

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = '
            body {
               font-size: 16px;
               line-height: 1.42857143;
               color: #333;
               background-color: #fff;
             }
              p{
                font-size: 14px;
             }
              table {
                border-collapse: collapse;
                width: 100%;
              }
              table, th, td {
                border: 1px solid black;
                padding: 5px;
                }
                @media all
                {
                .page{page-break-inside:avoid;}
                @media all
                 {
                    .page-break { display:none; }
                    .page-break-on{ display:none; }
                 }
                @media print
                 {
                    .page-break { display:block;height:1px; page-break-before:always; }
                    .page-break-on{ display:block;height:1px; page-break-inside:avoid; }
                 }
                                
        ';
            return $pdf->render();
        }
    }

    public function actionPrintAssessment($application_form_id)
    {
        $applicationForm = $this->findApplicationFormModel($application_form_id);
        $content = $this->renderPartial('assessment', [
            'applicationForm' => $applicationForm,
        ]);

        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        $pdf->cssInline = '
        body {
               font-size: 16px;
               line-height: 1.42857143;
               color: #333;
                background-color: #fff;
             }
              p{
                font-size: 14px;
             }
              table {
                border-collapse: collapse;
                width: 100%;
              }
              table, th, td {
                border: 1px solid black;
                padding: 5px;
                }
                @media all
                {
                .page{page-break-inside:avoid;}
                @media all
                 {
                    .page-break { display:none; }
                    .page-break-on{ display:none; }
                 }
                @media print
                 {
                    .page-break { display:block;height:1px; page-break-before:always; }
                    .page-break-on{ display:block;height:1px; page-break-inside:avoid; }
                 }
                                
        ';
        return $pdf->render();
    }

    public function actionListIssues()
    {
        //$applicationForm = ApplicationForm::find()->where(['locality_code' => \Yii::$app->user->identity->userProfile->locality_code])->orderBy(['id' => SORT_DESC])->one();
        //$issues = Issues::find()->joinWith('mainIssueCode')->andWhere(['in', 'issues.issue_code', Json::decode($applicationForm->apply_issues)]);
        $assessmentCon = AssessmentConclusion::find()->joinWith('applicationForm')->joinWith('issueCode')->joinWith('assessResult')->where(['application_form.locality_code' => \Yii::$app->user->identity->userProfile->locality_code, 'assessment_conclusion.type' => 'assessor']);

        $dataProvider = new ActiveDataProvider([
            'query' => $assessmentCon
        ]);

        return $this->render('list-issues', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionPrintAppform($be_year, $locality_code)
    {

        $model = ApplicationForm::findOne(['be_year' => $be_year, 'locality_code' => $locality_code]);
        $mainIssues = MainIssues::find()->all();
        $issues = Issues::find()->with('mainIssueCode')->all();
        $localInfo = Locality::find()->joinWith('typeCode')->joinWith('subdistrictCode')->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();

        $arIssue = [];

        foreach ($mainIssues as $key => $mainissue) {
            $arIssue[] = [
                'mainIssue_id' => $mainissue['id'],
                'mainIssue_desc' => $mainissue['main_issue_desc'],
                'issues' => []
            ];

            foreach ($issues as $issue) {
                if ($mainissue['main_issue_code'] == $issue['main_issue_code']) {
                    $arIssue[$key]['issues'][] = [
                        'issue_code' => $issue['issue_code'],
                        'issue_desc' => $issue['issue_desc'],
                    ];
                }
            }
        }


        if ($model !== null) {
            $selection = Json::decode($model->apply_issues);

            //localityInfo
            $model->localityType = !empty($localInfo->typeCode->type_name) ? $localInfo->typeCode->type_name : '';
            $model->localityName = !empty($localInfo->locality_name) ? $localInfo->locality_name : '';
            $model->subdictrict = !empty($localInfo->subdistrictCode->subdistrict_name) ? $localInfo->subdistrictCode->subdistrict_name : '';
            $model->dictrict = !empty($localInfo->districtCode->district_name) ? $localInfo->districtCode->district_name : '';
            $model->province = !empty($localInfo->provinceCode->province_name) ? $localInfo->provinceCode->province_name : '';
            $model->tel = !empty(Yii::$app->user->identity->userProfile->phone) ? Yii::$app->user->identity->userProfile->phone : '';
            $model->directorName = !empty($localInfo->typeCode->administrator) ? $localInfo->typeCode->administrator : '';
            $model->position = !empty($localInfo->typeCode->office) ? $localInfo->typeCode->office : '';


            $model->be_year = Date('Y') + 543;
            $model->locality_code = Yii::$app->user->identity->userProfile->locality_code;
            $model->contact_person = Yii::$app->user->identity->getPublicIdentity();
            $model->contact_phone = Yii::$app->user->identity->userProfile->phone;
            $model->contact_mobile = Yii::$app->user->identity->userProfile->mobile;
            $model->contact_email = Yii::$app->user->identity->email;

            $info = [
                'localityType' => $model->localityType,
                'localityName' => $model->localityName,
                'subdictrict' => $model->subdictrict,
                'dictrict' => $model->dictrict,
                'province' => $model->province,
                'tel' => $model->tel,
                'directorName' => $model->directorName,
                'position' => $model->position,

                'be_year' => $model->be_year,
                'locality_code' => $model->locality_code,
                'contact_person' => $model->contact_person,
                'contact_phone' => $model->contact_phone,
                'contact_mobile' => $model->contact_mobile,
                'contact_email' => $model->contact_email,
                'created_at' => Yii::$app->formatter->asDate($model->created_at, 'dd MMMM'),
                'year' => date("Y", strtotime($model->be_year) + 543),
                'apply_person' => $model->apply_person != null ? $model->apply_person : '',
                'apply_title' => $model->apply_title != null ? $model->apply_title : ' ',
                'contact_title' => $model->contact_title != null ? $model->contact_title : ''

            ];

            $dataProvider = new ActiveDataProvider([
                'query' => $issues,
            ]);

            $content = $this->renderPartial('appform-pdf', [
                'model' => $model,
                'results' => $arIssue, //$Issues->createCommand()->queryAll(),
                'localInfo' => $localInfo,
                'selection' => $selection,
                'dataProvider' => $dataProvider,
                'info' => $info
            ]);

            $pdf = Yii::$app->pdf;
            $pdf->content = $content;
            $pdf->cssInline = '
            body {
               font-size: 16px;
               line-height: 1.42857143;
               color: #333;
                background-color: #fff;
                }
                p{
                font-size: 13px;
                }
                table {
                border-collapse: collapse;
                width: 100%;
                }
                table, th, td {
                border: 1px solid black;
                padding: 5px;
                }
                @media all
                {
                .page{page-break-inside:avoid;}
                @media all
                 {
                    .page-break { display:none; }
                    .page-break-on{ display:none; }
                 }
                @media print
                 {
                    .page-break { display:block;height:1px; page-break-before:always; }
                    .page-break-on{ display:block;height:1px; page-break-inside:avoid; }
                 }
                                
        ';
            return $pdf->render();
        }
    }

    protected function findApplicationFormModel($id)
    {
        if (($model = ApplicationForm::find()->where(['id' => $id])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }
}
