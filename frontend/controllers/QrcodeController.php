<?php

namespace frontend\controllers;

use frontend\models\QrcodeForm;
use Yii;

class QrcodeController extends \yii\web\Controller
{
    public function actions()
    {
        return [
            'qr' => [
                'class' => \Da\QrCode\Action\QrCodeAction::className(),
                'text' => 'Qrcode',
                'param' => 'data',
                'component' => 'qr', // if configured in our app as `qr`
            ],
        ];
    }
    public function actionIndex()
    {
        $model = new QrcodeForm();
        $data = null;
        if ($model->load(Yii::$app->request->post())) {
            $data = $model->createQrcode($model->data);
            return $this->render('preview', [
                'model' => $model,
                'data' => $data,
            ]);
        } else {
            return $this->render('index', [
                'model' => $model,
                'data' => $data,
            ]);
        }

    }

}
