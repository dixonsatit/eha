<?php

namespace frontend\controllers;

use common\models\Assessment;
use common\models\Locality;
use common\models\Sop;
use common\models\User;
use Yii;
use common\models\ApplicationForm;
use common\models\AssessmentConclusion;
use frontend\models\search\ApplicationFormSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MainIssues;
use common\models\Issues;
use yii\db\Expression;

/**
 * ApplicationFormController implements the CRUD actions for ApplicationForm model.
 */
class ApplicationFormController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'view', 'index', 'submit-app-form', 'list-application-form', 'index',
                            'list-send-app-form', 'delete-assessment'
                        ],
                        'allow' => true,
                        'roles' => ['user', 'administrator'],
                        'denyCallback' => function () {
                            return Yii::$app->controller->redirect(['/user/default/index']);
                        }
                    ],
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    public function actionIndex()
    {
        $applicationForm = ApplicationForm::find()->where(['locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();
        return $this->render('index', [
            'applicationForm' => $applicationForm,
        ]);
    }

    /**
     * Lists all ApplicationForm models.
     * @return mixed
     */
    public function actionListApplicationForm()
    {
        unset(Yii::$app->session['applicationForm']);

        $searchModel = new ApplicationFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality_code]);

        return $this->render('list-application-form', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApplicationForm model.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($be_year, $locality_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($be_year, $locality_code),
        ]);
    }


    /**
     * Creates a new ApplicationForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new ApplicationForm();
        $localInfo = Locality::find()->joinWith('typeCode')->joinWith('subdistrictCode')->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();
        $Issues = Issues::find()->with('mainIssueCode');

        $model->be_year = ApplicationForm::fiscalYear(date('Y-m-d'));
   
        //localityInfo
        $model->localityType = !empty($localInfo->typeCode->type_name) ? $localInfo->typeCode->type_name : '';
        $model->localityName = !empty($localInfo->locality_name) ? $localInfo->locality_name : '';
        $model->subdictrict = !empty($localInfo->subdistrictCode->subdistrict_name) ? $localInfo->subdistrictCode->subdistrict_name : '';
        $model->dictrict = !empty($localInfo->districtCode->district_name) ? $localInfo->districtCode->district_name : '';
        $model->province = !empty($localInfo->provinceCode->province_name) ? $localInfo->provinceCode->province_name : '';
        $model->tel = !empty(Yii::$app->user->identity->userProfile->phone) ? Yii::$app->user->identity->userProfile->phone : '';
        $model->directorName = !empty($localInfo->typeCode->administrator) ? $localInfo->typeCode->administrator : '';
        $model->position = !empty($localInfo->typeCode->office) ? $localInfo->typeCode->office : '';

        $model->locality_code = Yii::$app->user->identity->userProfile->locality_code;
        $model->contact_person = Yii::$app->user->identity->getPublicIdentity();
        $model->contact_phone = Yii::$app->user->identity->userProfile->phone;
        $model->contact_mobile = Yii::$app->user->identity->userProfile->mobile;
        $model->contact_email = Yii::$app->user->identity->email;


        if ($model->load(Yii::$app->request->post())) {

            if (Yii::$app->request->post('selection')) {
                $selects = Yii::$app->request->post('selection');
                $model->apply_issues = Json::encode($selects);
                $model->status = 1;
                if ($model->save()) {

                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'เลือกประเด็นการขอรับการประเมินเรียบร้อยแล้ว',
                        'options' => ['class' => 'alert alert-success', 'role' => 'alert']
                    ]);

                    return $this->redirect(['list-application-form']);
                }
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $Issues,
        ]);

        return $this->render('create', [
            'model' => $model,
            'localInfo' => $localInfo,
            'selection' => [],
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Updates an existing ApplicationForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($be_year, $locality_code)
    {

        $model = ApplicationForm::find()->where(['be_year' => $be_year, 'locality_code' => $locality_code])->one();

        $Issues = Issues::find()->with('mainIssueCode');
        $localInfo = Locality::find()->joinWith('typeCode')->joinWith('subdistrictCode')->joinWith('districtCode')->joinWith('provinceCode')->where(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();

        if ($model !== null) {
            $selection = isset($model->apply_issues) ? Json::decode($model->apply_issues) : [];

            //localityInfo
            $model->localityType = !empty($localInfo->typeCode->type_name) ? $localInfo->typeCode->type_name : '';
            $model->localityName = !empty($localInfo->locality_name) ? $localInfo->locality_name : '';
            $model->subdictrict = !empty($localInfo->subdistrictCode->subdistrict_name) ? $localInfo->subdistrictCode->subdistrict_name : '';
            $model->dictrict = !empty($localInfo->districtCode->district_name) ? $localInfo->districtCode->district_name : '';
            $model->province = !empty($localInfo->provinceCode->province_name) ? $localInfo->provinceCode->province_name : '';
            $model->tel = !empty(Yii::$app->user->identity->userProfile->phone) ? Yii::$app->user->identity->userProfile->phone : '';
            $model->directorName = !empty($localInfo->typeCode->administrator) ? $localInfo->typeCode->administrator : '';
            $model->position = !empty($localInfo->typeCode->office) ? $localInfo->typeCode->office : '';

            $dataProvider = new ActiveDataProvider([
                'query' => $Issues,
            ]);

            if ($model === null) {
                $selection = isset($model->apply_issues) ? Json::decode($model->apply_issues) : [];

                //localityInfo
                $model->localityType = !empty($localInfo->typeCode->type_name) ? $localInfo->typeCode->type_name : '';
                $model->localityName = !empty($localInfo->locality_name) ? $localInfo->locality_name : '';
                $model->subdictrict = !empty($localInfo->subdistrictCode->subdistrict_name) ? $localInfo->subdistrictCode->subdistrict_name : '';
                $model->dictrict = !empty($localInfo->districtCode->district_name) ? $localInfo->districtCode->district_name : '';
                $model->province = !empty($localInfo->provinceCode->province_name) ? $localInfo->provinceCode->province_name : '';
                $model->tel = !empty(Yii::$app->user->identity->userProfile->phone) ? Yii::$app->user->identity->userProfile->phone : '';
                $model->directorName = !empty($localInfo->typeCode->administrator) ? $localInfo->typeCode->administrator : '';
                $model->position = !empty($localInfo->typeCode->office) ? $localInfo->typeCode->office : '';

                $dataProvider = new ActiveDataProvider([
                    'query' => $Issues,
                ]);
            }

            if ($model->load(Yii::$app->request->post())) {

                $selects = Yii::$app->request->post('selection');

                $arDels = [];
                $ar1 = Json::decode($model->apply_issues);
                $ar2 = Yii::$app->request->post('selection');

                foreach ($ar1 as $item) {
                    if (in_array($item, $ar2)) {
                    } else {
                        $arDels[] = $item;
                        $findSop = Sop::find()->where(['issue_code' => $item])->andWhere(['be_year' => $be_year])->andWhere(['not', ['parent_id' => null]])->all();
                        if ($findSop) {
                            foreach ($findSop as $so) {
                                $findAss = Assessment::find()->where(['application_form_id' => $model->id])->andWhere(['sop_id' => $so->id])->one();
                    
                                if ($findAss) {
                                    $findAss->delete();
                                }
                            }
                        }

                        $findAssCon = AssessmentConclusion::find()->where(['be_year' => $be_year])->andWhere(['locality_code' => $locality_code])->andWhere(['issue_code' => $item])->one();

                        if ($findAssCon) {
                            $findAssCon->deleteAll();
                        }
                    }
                }

                $model->apply_issues = Json::encode($selects);

                if ($model->save()) {

                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'แก้ไขประเด็นการขอรับการประเมินเรียบร้อยแล้ว',
                        'options' => ['class' => 'alert alert-success', 'role' => 'alert']
                    ]);

                    return $this->redirect(['list-application-form']);
                }
            }

            return $this->render('update', [
                'model' => $model,
                'localInfo' => $localInfo,
                'dataProvider' => $dataProvider,
                'selection' => $selection
            ]);
        }
    }

    /**
     * Deletes an existing ApplicationForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($be_year, $locality_code)
    {
        $this->findModel($be_year, $locality_code)->delete();

        return $this->redirect(['index']);
    }


    public function actionSubmitAppForm($id)
    {
        $model = ApplicationForm::find()->where(['id' => $id])->one();
        if ($model) {
            $model->status = 2;
            if ($model->save()) {
                $issues = json_decode($model->apply_issues);
                $this->saveScores($model->id, $model->be_year, 'selfassess', $model->locality_code, $issues);
                return $this->redirect(['index']);
            }
        } else {
            return false;
        }
    }

    public function actionListSendAppForm()
    {

        unset(Yii::$app->session['applicationForm']);

        $searchModel = new ApplicationFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality_code]);

        return $this->render('list-send-app-form', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    //    public function actionDeleteAssessment()
    //    {
    //        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //        if (Yii::$app->request->isAjax) {
    //            $be_year = Yii::$app->request->post('be_year');
    //            $locality_code = Yii::$app->request->post('locality_code');
    //            $issue_code = Yii::$app->request->post('issue_code');
    //            $applicationForm = ApplicationForm::findOne(['be_year' => $be_year, 'locality_code' => $locality_code]);
    //            if ($applicationForm != null) {
    //                $sops = Sop::find()->where(['issue_code' => $issue_code])->all();
    //                if ($sops != null) {
    //                    $listSops = [];
    //                    foreach ($sops as $sop) {
    //                        $listSops[] = $sop->id;
    //                    }
    //
    //                    $listSopss = implode(',', $listSops);
    //                    Assessment::deleteAll(
    //                            "locality_code = :locality_code AND application_form_id = :application_form_id AND sop_id IN ($listSopss)",
    //                        [
    //                            ':locality_code' => $locality_code,
    //                            ':application_form_id' => $applicationForm->id,
    //                            ':issue_code' => $issue_code,
    //                            ':listSops' => $listSops,
    //                        ]);
    //                }
    //            }
    //        }
    //    }

    /**
     * Finds the ApplicationForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $be_year
     * @param string $locality_code
     * @return ApplicationForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($be_year, $locality_code)
    {
        if (($model = ApplicationForm::findOne(['be_year' => $be_year, 'locality_code' => $locality_code])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }

    public function saveScores($application_form_id, $year, $type, $localityCode, $issues = [])
    {

        foreach ($issues as $issue) {
            if ($issue === 'EHA1001') {
                $eha = new \common\ehascores\Score1001();
            } else if ($issue === 'EHA1002') {
                $eha = new \common\ehascores\Score1002();
            } else if ($issue === 'EHA1003') {
                $eha = new \common\ehascores\Score1003();
            } else if ($issue === 'EHA2001') {
                $eha = new \common\ehascores\Score2001();
            } else if ($issue === 'EHA2002') {
                $eha = new \common\ehascores\Score2002();
            } else if ($issue === 'EHA2003') {
                $eha = new \common\ehascores\Score2003();
            } else if ($issue === 'EHA3001') {
                $eha = new \common\ehascores\Score3001();
            } else if ($issue === 'EHA3002') {
                $eha = new \common\ehascores\Score3002();
            } else if ($issue === 'EHA4001') {
                $eha = new \common\ehascores\Score4001();
            } else if ($issue === 'EHA4002') {
                $eha = new \common\ehascores\Score4002();
            } else if ($issue === 'EHA5000') {
                $eha = new \common\ehascores\Score5000();
            } else if ($issue === 'EHA6000') {
                $eha = new \common\ehascores\Score6000();
            } else if ($issue === 'EHA7000') {
                $eha = new \common\ehascores\Score7000();
            } else if ($issue === 'EHA8000') {
                $eha = new \common\ehascores\Score8000();
            } else if ($issue === 'EHA9001') {
                $eha = new \common\ehascores\Score9001();
            } else if ($issue === 'EHA9002') {
                $eha = new \common\ehascores\Score9002();
            } else if ($issue === 'EHA9003') {
                $eha = new \common\ehascores\Score9003();
            } else if ($issue === 'EHA9004') {
                $eha = new \common\ehascores\Score9004();
            } else if ($issue === 'EHA9005') {
                $eha = new \common\ehascores\Score9005();
            }

            $eha->appFormID = $application_form_id;
            $eha->issueCode = $issue;
            $eha->year = $year;
            $eha->type = $type;
            $eha->localityCode = $localityCode;
            $eha->calculate();
            unset($eha);
        }
    }
}
