<?php

namespace frontend\controllers;

use backend\models\search\LocalitySearch;
use Codeception\Application;
use common\models\ApplicationForm;
use common\models\Assessment;
use common\models\AssessmentConclusion;
use common\models\Issues;
use common\models\Locality;
use common\models\Sop;
use frontend\models\search\ApplicationFormSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LocalityController implements the CRUD actions for Locality model.
 */
class AssessorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                    [
                        'actions' => ['locality-approve', 'locality-all', 'index', 'create', 'view', 'update', 'delete', 'list-application-form', 'list-issues', 'update-by-issue', 'view-by-issue', 'send-improve', 'send-mail', 'send-notification', 'approve', 'comment'],
                        'allow' => true,
                        'roles' => ['manager', 'director', 'administrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Locality models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocalitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLocalityApprove()
    {
        $curDate = date('Y-m-d');
        $fiscalYear = ApplicationForm::fiscalYear($curDate);

        $searchModel = new ApplicationFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->user->can('director')) {
            $dataProvider->query->andWhere(['province.region_code' => Yii::$app->user->identity->userProfile->office->provinceCode->region_code])->andWhere(['be_year' => $fiscalYear]);
        }

        if (Yii::$app->user->can('manager')) {
            $dataProvider->query->andWhere(['province.province_code' => Yii::$app->user->identity->userProfile->office->province_code])->andWhere(['be_year' => $fiscalYear]);
        }
        
        $dataProvider->pagination->pageSize=100;

        return $this->render('locality-approve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLocalityAll()
    {
        $searchModel = new LocalitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('locality-all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListApplicationForm($locality_code)
    {
        $searchModel = new ApplicationFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['application_form.locality_code' => $locality_code]);

        return $this->render('list-application-form', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($application_form_id)
    {
        $appForm = ApplicationForm::find()->joinWith('locality')->joinWith('locality.typeCode')->where(['application_form.id' => $application_form_id])->one();
        $model = ApplicationForm::find()->where(['id' => $application_form_id])->one();

        $assessments = Assessment::getDb()->cache(function ($db) use ($application_form_id) {
            return Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->where(['assessment.application_form_id' => $application_form_id])->all(); // ดึงข้อมูล Assessment
        });

        foreach ($assessments as $key => $assessment) {
            $models[] = new Assessment([
                'main_issue_code' => $assessment['sop']['issueCode']['main_issue_code'],
                'main_issue_desc' => $assessment['sop']['issueCode']['mainIssueCode']['main_issue_desc'],
                'issue_code' => $assessment['sop']['issue_code'],
                'issue_desc' => $assessment['sop']['issueCode']['issue_desc'],
                'id' => $assessment['id'],
                'sop_id' => $assessment['sop']['id'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'assess_step_evidence' => $assessment['sop']['assess_step_evidence'],
                'selfassess_score' => $assessment['selfassess_score'],
                'selfassess_date' => $assessment['selfassess_date'],
                'assessor_score' => $assessment['assessor_score'],
                'assessor_date' => $assessment['assessor_date'],
                'assessor_notes' => $assessment['assessor_notes'],
                'locality_code' => Yii::$app->user->identity->userProfile->locality_code,
                'sop_parent_id' => $assessment['sop']['parent_id'],
                'sop_issue_code' => $assessment['sop']['issue_code'],
                'sop_assess_step' => $assessment['sop']['assess_step'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'sop_assess_score' => $assessment['sop']['assess_score'],
            ]);
        }

        if (Model::loadMultiple($assessments, Yii::$app->request->post())) {
            foreach ($assessments as $assessment) {
                $assessment->save(false);
                unset($assessment);
            }

            $issues = json_decode($model->apply_issues);
            $this->saveScores($model->id, $model->be_year, 'assessor', $model->locality_code, $issues);
            return $this->redirect(['index', 'application_form_id' => $application_form_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'models' => $models,
            'appForm' => $appForm,
        ]);
    }

    public function actionListIssues($application_form_id)
    {
        if (session_id() == '' || !isset($_SESSION)) {
            session_start();
        }

        if (isset($application_form_id)) {
            $applicationForm = ApplicationForm::find()->joinWith('locality')->where(['application_form.id' => $application_form_id])->one();
            if ($applicationForm != null) {
                $apply_issues = Json::decode($applicationForm->apply_issues);
                $apply_issues = is_array($apply_issues) ? $apply_issues : [];
                Yii::$app->session['applicationForm'] = [
                    'applicationForm' => $applicationForm,
                    'application_form_id' => $applicationForm->id,
                    'be_year' => $applicationForm->be_year,
                    'apply_issues' => $apply_issues,
                    'locality_code' => $applicationForm->locality_code,
                    'status' => $applicationForm->status,
                    'locality_name' => $applicationForm->locality->locality_name,
                    'email' => $applicationForm->contact_email
                ];
            } else {
                Yii::$app->session['applicationForm'] = [
                    'application_form_id' => '',
                    'be_year' => '',
                    'apply_issues' => [],
                    'locality_code' => '',
                    'status' => 0,
                    'locality_name' => '',
                    'email' => ''
                ];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $query = Issues::find()->where(['IN', 'issues.issue_code', Yii::$app->session['applicationForm']['apply_issues']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->render('list-issues', [
            'applicationForm' => $applicationForm,
            'dataProvider' => $dataProvider,
            'application_form_id' => Yii::$app->session['applicationForm']['application_form_id'],
        ]);
    }

    public function actionUpdateByIssue($issue_code)
    {
        $models = [];
        $model = ApplicationForm::find()->where(['id' => Yii::$app->session['applicationForm']['application_form_id']])->one();
        $mainSops = Sop::find()->joinWith('issueCode')->joinWith('issueCode.mainIssueCode')->where(['issues.issue_code' => $issue_code])->orderBy(['sop.id' => SORT_ASC])->all();

        $assessments = Assessment::getDb()->cache(function ($db) use ($issue_code) {
            return Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->where(['assessment.application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->andWhere(['sop.issue_code' => $issue_code])->orderBy(['sop.id' => SORT_ASC])->all(); // ดึงข้อมูล Assessment
        });

        if (Model::loadMultiple($assessments, Yii::$app->request->post())) {
            foreach ($assessments as $assessment) {
                $assessment->assessor_date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
                $assessment->save(false);
                unset($assessment);
            }

            if ($issue_code) {
                $this->saveScoresByIssue($model->id, $model->be_year, 'assessor', $model->locality_code, $issue_code);

                //แจ้งผลไปยังผู้อปท(user)
                //Assessment::MailNotificationAssessmentSendResultLocality();

                //แจ้งผลไปยังเขต(director)
                //Assessment::MailNotificationAssessmentSendResultDirector();

                //แจ้งผลไปยังสสจ.(manager)
                //Assessment::MailNotificationAssessmentSendResult();

                //แจ้งผลไปยังผู้ดูแลระบบ(administrator)
                //Assessment::MailNotificationAssessmentSendResultAdmin();

            }
            return $this->redirect(['view-by-issue', 'issue_code' => $issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]);
        }

        return $this->render('update-by-issue', [
            'model' => $model,
            'models' => $assessments,
            'mainSops' => $mainSops,
        ]);
    }

    public function actionUpdateByIssueOld($issue_code)
    {
        $models = [new Assessment()];
        $model = ApplicationForm::find()->where(['id' => Yii::$app->session['applicationForm']['application_form_id']])->one();
        $mainSops = Sop::find()->joinWith('issueCode')->joinWith('issueCode.mainIssueCode')->where(['issues.issue_code' => $issue_code])->orderBy(['sop.id' => SORT_ASC])->all();

        $assessments = Assessment::getDb()->cache(function ($db) use ($issue_code) {
            return Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->where(['assessment.application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->andWhere(['sop.issue_code' => $issue_code])->orderBy(['sop.id' => SORT_ASC])->all(); // ดึงข้อมูล Assessment
        });

        foreach ($assessments as $key => $assessment) {
            $models[] = new Assessment([
                'main_issue_code' => $assessment['sop']['issueCode']['main_issue_code'],
                'main_issue_desc' => $assessment['sop']['issueCode']['mainIssueCode']['main_issue_desc'],
                'issue_code' => $assessment['sop']['issue_code'],
                'issue_desc' => $assessment['sop']['issueCode']['issue_desc'],
                'id' => $assessment['id'],
                'sop_id' => $assessment['sop']['id'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'assess_step_evidence' => $assessment['sop']['assess_step_evidence'],
                'selfassess_score' => $assessment['selfassess_score'],
                'selfassess_date' => $assessment['selfassess_date'],
                'assessor_score' => $assessment['assessor_score'],
                'assessor_date' => $assessment['assessor_date'],
                'assessor_notes' => $assessment['assessor_notes'],
                'locality_code' => Yii::$app->session['applicationForm']['application_form_id'],
                'sop_parent_id' => $assessment['sop']['parent_id'],
                'sop_issue_code' => $assessment['sop']['issue_code'],
                'sop_assess_step' => $assessment['sop']['assess_step'],
                'sop_assess_step_detail' => $assessment['sop']['assess_step_detail'],
                'sop_assess_score' => $assessment['sop']['assess_score'],
            ]);
        }

        if (Model::loadMultiple($assessments, Yii::$app->request->post())) {
            foreach ($assessments as $assessment) {
                $assessment->assessor_date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
                $assessment->save(false);
                unset($assessment);
            }

            if ($issue_code) {
                $this->saveScoresByIssue($model->id, $model->be_year, 'assessor', $model->locality_code, $issue_code);

                //แจ้งผลไปยังผู้อปท(user)
                //Assessment::MailNotificationAssessmentSendResultLocality();

                //แจ้งผลไปยังเขต(director)
                //Assessment::MailNotificationAssessmentSendResultDirector();

                //แจ้งผลไปยังสสจ.(manager)
                //Assessment::MailNotificationAssessmentSendResult();

                //แจ้งผลไปยังผู้ดูแลระบบ(administrator)
                //Assessment::MailNotificationAssessmentSendResultAdmin();


            }
            return $this->redirect(['view-by-issue', 'issue_code' => $issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]);
        }

        return $this->render('update-by-issue', [
            'model' => $model,
            'models' => $models,
            'mainSops' => $mainSops,
        ]);
    }

    public function actionViewByIssue($issue_code, $application_form_id)
    {

        $assessment = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode')->andWhere(['sop.issue_code' => $issue_code])->andWhere(['assessment.application_form_id' => $application_form_id])->orderBy(['sop.id' => SORT_ASC])->all();
        $applicationForm = ApplicationForm::find()->where(['id' => $application_form_id])->one();

        $dataProvider = ArrayDataProvider::className([
            'allModels' => $assessment,
            'pagination' => false,
        ]);

        return $this->render('view-by-issue', [
            'dataProvider' => $dataProvider,
            'assessment' => $assessment,
            'applicationForm' => $applicationForm
        ]);
    }

    public function actionSendImprove($issue_code)
    {

        $query = Issues::find()->where(['IN', 'issues.issue_code', Yii::$app->session['applicationForm']['apply_issues']]);

        $assessments = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->where(['sop.issue_code' => $issue_code])->andWhere(['assessment.application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->all();

        $assessmentConclusion = AssessmentConclusion::find()->where(['issue_code' => $issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->one();
        
        if ($assessmentConclusion != null) {

            $assessmentConclusion->approve = 'No';
            $assessmentConclusion->assess_result = '4';

            $assessmentConclusion->save(false);

            $issue = Issues::find()->where(['issue_code' => $issue_code])->one();
            $email = Yii::$app->session['applicationForm']['email'];
            // Yii::$app->mailer->compose()
            //     ->setFrom(['eha@anamai.mail.go.th' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
            //     ->setTo(!empty($email) ? $email : 'example@mail.com')
            //     ->setSubject('ส่งกลับเพื่อแก้ไขการประเมิน')
            //     ->setHtmlBody('<h2>ส่งกลับเพื่อแก้ไขการประเมิน' . " " . $issue->issue_desc.'</h2>')
            //     ->send();

            Yii::$app->getSession()->setFlash('alert', [
                'body' => 'ส่งกลับไปแก้ไขเรียบร้อยแล้ว',
                'options' => ['class' => 'alert alert-success', 'role' => 'alert']
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,

            ],
        ]);

        return $this->redirect(Yii::$app->request->referrer);

        //        return $this->render('list-issues', [
        //            'dataProvider' => $dataProvider,
        //            'application_form_id' => Yii::$app->session['applicationForm']['application_form_id'],
        //        ]);
    }

    /**
     * Displays a single Locality model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Locality model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->locality_code]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Locality model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Locality model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Locality the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locality::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'The requested page does not exist.'));
    }

    public function saveScores($application_form_id, $year, $type, $localityCode, $issues = [])
    {

        foreach ($issues as $issue) {
            if ($issue === 'EHA1001') {
                $eha = new \common\ehascores\Score1001();
            } else if ($issue === 'EHA1002') {
                $eha = new \common\ehascores\Score1002();
            } else if ($issue === 'EHA1003') {
                $eha = new \common\ehascores\Score1003();
            } else if ($issue === 'EHA2001') {
                $eha = new \common\ehascores\Score2001();
            } else if ($issue === 'EHA2002') {
                $eha = new \common\ehascores\Score2002();
            } else if ($issue === 'EHA2003') {
                $eha = new \common\ehascores\Score2003();
            } else if ($issue === 'EHA3001') {
                $eha = new \common\ehascores\Score3001();
            } else if ($issue === 'EHA3002') {
                $eha = new \common\ehascores\Score3002();
            } else if ($issue === 'EHA4001') {
                $eha = new \common\ehascores\Score4001();
            } else if ($issue === 'EHA4002') {
                $eha = new \common\ehascores\Score4002();
            } else if ($issue === 'EHA4003') {
                $eha = new \common\ehascores\Score4003();
            } else if ($issue === 'EHA5000') {
                $eha = new \common\ehascores\Score5000();
            } else if ($issue === 'EHA6000') {
                $eha = new \common\ehascores\Score6000();
            } else if ($issue === 'EHA7000') {
                $eha = new \common\ehascores\Score7000();
            } else if ($issue === 'EHA8000') {
                $eha = new \common\ehascores\Score8000();
            } else if ($issue === 'EHA9001') {
                $eha = new \common\ehascores\Score9001();
            } else if ($issue === 'EHA9002') {
                $eha = new \common\ehascores\Score9002();
            } else if ($issue === 'EHA9003') {
                $eha = new \common\ehascores\Score9003();
            } else if ($issue === 'EHA9004') {
                $eha = new \common\ehascores\Score9004();
            } else if ($issue === 'EHA9005') {
                $eha = new \common\ehascores\Score9005();
            }

            $eha->appFormID = $application_form_id;
            $eha->issueCode = $issue;
            $eha->year = $year;
            $eha->type = $type;
            $eha->localityCode = $localityCode;
            $eha->calculate();
            unset($eha);
        }
    }

    public function saveScoresByIssue($application_form_id, $year, $type, $localityCode, $issue)
    {
        if ($issue === 'EHA1001') {
            $eha = new \common\ehascores\Score1001();
        } else if ($issue === 'EHA1002') {
            $eha = new \common\ehascores\Score1002();
        } else if ($issue === 'EHA1003') {
            $eha = new \common\ehascores\Score1003();
        } else if ($issue === 'EHA2001') {
            $eha = new \common\ehascores\Score2001();
        } else if ($issue === 'EHA2002') {
            $eha = new \common\ehascores\Score2002();
        } else if ($issue === 'EHA2003') {
            $eha = new \common\ehascores\Score2003();
        } else if ($issue === 'EHA3001') {
            $eha = new \common\ehascores\Score3001();
        } else if ($issue === 'EHA3002') {
            $eha = new \common\ehascores\Score3002();
        } else if ($issue === 'EHA4001') {
            $eha = new \common\ehascores\Score4001();
        } else if ($issue === 'EHA4002') {
            $eha = new \common\ehascores\Score4002();
        } else if ($issue === 'EHA4003') {
            $eha = new \common\ehascores\Score4003();
        } else if ($issue === 'EHA5000') {
            $eha = new \common\ehascores\Score5000();
        } else if ($issue === 'EHA6000') {
            $eha = new \common\ehascores\Score6000();
        } else if ($issue === 'EHA7000') {
            $eha = new \common\ehascores\Score7000();
        } else if ($issue === 'EHA8000') {
            $eha = new \common\ehascores\Score8000();
        } else if ($issue === 'EHA9001') {
            $eha = new \common\ehascores\Score9001();
        } else if ($issue === 'EHA9002') {
            $eha = new \common\ehascores\Score9002();
        } else if ($issue === 'EHA9003') {
            $eha = new \common\ehascores\Score9003();
        } else if ($issue === 'EHA9004') {
            $eha = new \common\ehascores\Score9004();
        } else if ($issue === 'EHA9005') {
            $eha = new \common\ehascores\Score9005();
        }

        $eha->appFormID = $application_form_id;
        $eha->issueCode = $issue;
        $eha->year = $year;
        $eha->type = $type;
        $eha->localityCode = $localityCode;
        $eha->calculate();
        unset($eha);
    }


    public function actionSendNotification($issue_code, $status)
    {
        $email = Yii::$app->session['applicationForm']['email'];
        $issue = Issues::find()->where(['issue_code' => $issue_code])->one();
        // Yii::$app->mailer->compose()
        //     ->setFrom(['ehainfosmart@gmail.com' => 'ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม EHA'])
        //     ->setTo(!empty($email) ? $email : 'example@mail.com')
        //     ->setSubject('สถานะการประเมิน')
        //     ->setTextBody('ผมการประเมิน' . " " . $issue->issue_desc, '<br>' . $status)
        //     ->send();

        Yii::$app->getSession()->setFlash('alert', [
            'body' => 'แจ้งผลประเมินเรียบร้อยแล้ว',
            'options' => ['class' => 'alert alert-success', 'role' => 'alert']
        ]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionComment($id){
        $assessment = Assessment::find()->where(['id' => $id])->one();
        if($assessment){
            return $this->renderAjax('_comment-form', [
                'model' => $assessment,
            ]);
        }else{
            return false;
        }
       
    }
}
