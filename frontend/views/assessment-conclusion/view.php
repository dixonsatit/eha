<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentConclusion */

$this->title = $model->be_year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessment Conclusions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="assessment-conclusion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'locality_code' => $model->locality_code, 'type' => $model->type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'locality_code' => $model->locality_code, 'type' => $model->type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'be_year',
            'issue_code',
            'locality_code',
            'type',
            'component1_score',
            'component2_score',
            'component3_score',
            'component4_score',
            'component5_score',
            'component1to5_avgscore',
            'component6_score',
            'component7_score',
            'component6to7_avgscore',
            'component1to7_avgscore',
            'assess_result',
            'b',
            'c',
            'd',
            'percentA',
            'percentB',
            'percentC',
            'percentE',
            'assessed_by_uid1',
            'assessed_by_fullname1',
            'assessed_by_uid2',
            'assessed_by_fullname2',
            'assessed_by_uid3',
            'assessed_by_fullname3',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'certificate',
            'approve',
            'application_form_id',
        ],
    ]) ?>

</div>
