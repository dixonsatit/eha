<?php

use common\models\ApplicationForm;
use common\models\District;
use common\models\Locality;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'อทป.ที่เข้าร่วมประเมินในเขตพื้นที่');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-conclusion-index">

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'locality_code',
                        'vAlign' => 'middle',
                        'width' => '180px',
                        'value' => function ($data) {
                            return $data->locality->locality_name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'size' => Select2::LARGE,
                            'data' => yii\helpers\ArrayHelper::map(Locality::find()->all(), 'locality_code', 'locality_name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['/user/default/local-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term};}')
                                ],

                                'escapeMarkup' => new JsExpression('function(markup) { return markup;}'),
                                'templateResult' => new JsExpression('function(local){ return local.text;}'),
                                'templateSelection' => new JsExpression('function(local) {return local.text;}'),
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => '-- อปท. --'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'district',
                        'label' => 'อำเภอ',
                        'vAlign' => 'middle',
                        'width' => '180px',
                        'value' => function ($data) {
                            return $data->locality->districtCode->district_name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'size' => Select2::LARGE,
                            'data' => yii\helpers\ArrayHelper::map(District::find()->all(), 'district_code', 'district_name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['/user/default/district-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term};}')
                                ],

                                'escapeMarkup' => new JsExpression('function(markup) { return markup;}'),
                                'templateResult' => new JsExpression('function(district){ return district.text;}'),
                                'templateSelection' => new JsExpression('function(district) {return district.text;}'),
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => '-- อำเภอ --'],
                        'format' => 'raw'
                    ],

                    // [
                    //     'attribute' => 'district',
                    //     'label' => 'อำเภอ',
                    //     'value' => function ($data) {
                    //         return $data->locality->districtCode->district_name;
                    //     },
                    //     'filter' => Select2::widget([
                    //         'name' => 'AssessmentConclusionSearch[district]',
                    //         'model' => $searchModel,
                    //         'value' => $searchModel->locality->districtCode->district,
                    //         'data' => ArrayHelper::map(\common\models\District::find()->joinWith('provinceCode')->where(['province.region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code])->all(), 'district_code', 'district_name'),
                    //         'size' => Select2::LARGE,
                    //         'options' => [
                    //             'placeholder' => '-- อำเภอ --',
                    //             'style' => 'width: 100%;'
                    //         ],
                    //         'pluginOptions' => [
                    //             'allowClear' => true
                    //         ],
                    //         // 'addon' => [
                    //         //     'prepend' => [
                    //         //         'content' => FAS::icon('open')
                    //         //     ]
                    //         // ],
                    //     ]),
                    // ],

                    // [
                    //     'label' => 'จังหวัด',
                    //     'attribute' => 'province',
                    //     'filter' => \yii\helpers\ArrayHelper::map(\common\models\Province::find()->where(['region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code])->all(), 'province_code', 'province_name'),
                    //     'value' => function ($data) {
                    //         return $data->localityCode->provinceCode->province_name;
                    //     }
                    // ],

                    [
                        'attribute' => 'province',
                        'label' => 'จังหวัด',
                        'value' => function ($data) {
                            return $data->locality->provinceCode->province_name;
                        },
                        'filter' => Select2::widget([
                            'name' => 'AssessmentConclusionSearch[province]',
                            'model' => $searchModel,
                            'value' => $searchModel->province,
                            //'data' => ArrayHelper::map(\common\models\Province::find()->where(['region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code])->all(), 'province_code', 'province_name'),
                            'data' => ArrayHelper::map(\common\models\Province::find()->asArray()->all(), 'province_code', 'province_name'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- จังหวัด --',
                                'style' => 'width: 100%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],
                    [
                        'attribute' => 'be_year',
                        // 'filter'=>ArrayHelper::map(ApplicationForm::find()->groupBy('be_year')->asArray()->all(), 'be_year', 'be_year'),
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'be_year',
                            'value' => $searchModel->be_year,
                            'data' => ArrayHelper::map(ApplicationForm::find()->groupBy('be_year')->asArray()->all(), 'be_year', 'be_year'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- ปีงบประมาณ --',
                                'style' => 'width: 10%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detailUrl' => Url::to(['expand-conclusion']),
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true
                    ],
                    // [
                    //     'label' => 'ระดับการปนะเมิน',
                    //     'attribute' => 'assess_result',
                    //     'filter' => \yii\helpers\ArrayHelper::map(\common\models\AssessmentResult::find()->all(), 'result_code', 'result_desc'),
                    //     'value' => function ($data) {
                    //         return $data->assessmentConclusion->assessResult->result_desc;
                    //     }
                    // ],
                ],
            ]); ?>
        </div>
    </div>
</div>