<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentConclusion */

$this->title = Yii::t('common', 'Update Assessment Conclusion: {name}', [
    'name' => $model->be_year,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessment Conclusions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->be_year, 'url' => ['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'locality_code' => $model->locality_code, 'type' => $model->type]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="assessment-conclusion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
