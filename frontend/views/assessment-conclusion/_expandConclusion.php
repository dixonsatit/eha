<?php

use yii\helpers\VarDumper;
?>

<?php

?>

<table style="width: 100%;">
    <thead>
        <tr>
            <td>รหัส</td>
            <td>ประเด็นงาน</td>
            <td>คะแนน</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $item) : ?>
            <tr>
                <td><?php echo $item->issue_code; ?></td>
                <td><?php echo $item->issueCode->issue_desc; ?></td>
                <td><?php echo $item->assessResult->result_desc;?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>