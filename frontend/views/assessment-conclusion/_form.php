<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentConclusion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="assessment-conclusion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'be_year')->textInput() ?>

    <?= $form->field($model, 'issue_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'locality_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'selfassess' => 'Selfassess', 'assessor' => 'Assessor', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'component1_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component2_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component3_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component4_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component5_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component1to5_avgscore')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component6_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component7_score')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component6to7_avgscore')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'component1to7_avgscore')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assess_result')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percentA')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percentB')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percentC')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percentE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assessed_by_uid1')->textInput() ?>

    <?= $form->field($model, 'assessed_by_fullname1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assessed_by_uid2')->textInput() ?>

    <?= $form->field($model, 'assessed_by_fullname2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assessed_by_uid3')->textInput() ?>

    <?= $form->field($model, 'assessed_by_fullname3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_by_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'approve')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'application_form_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
