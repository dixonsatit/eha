<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentConclusion */

$this->title = Yii::t('common', 'Create Assessment Conclusion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessment Conclusions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-conclusion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
