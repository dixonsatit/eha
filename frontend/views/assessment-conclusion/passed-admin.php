<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'อทป.ที่ผ่านการประเมินในเขตพื้นที่');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-conclusion-index">

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'locality_code',
                        'label' => 'อปท.',
                        'value' => function ($data) {
                            return $data->localityCode->locality_name;
                        },
                        'filter' => Select2::widget([
                            'name' => 'AssessmentConclusionSearch[locality_code]',
                            'model' => $searchModel,
                            'value' => $searchModel->localityCode->district,
                            'data' => ArrayHelper::map(\common\models\Locality::find()->joinWith('provinceCode')->all(), 'locality_code', 'locality_name'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- อปท. --',
                                'style' => 'width: 100%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],

                    // [
                    //     'label' => 'อำเภอ',
                    //     'attribute' => 'district',
                    //     'filter' => \yii\helpers\ArrayHelper::map(\common\models\District::find()->joinWith('provinceCode')->where(['province.region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code])->all(), 'district_code', 'district_name'),
                    //     'value' => function ($data) {
                    //         return $data->localityCode->districtCode->district_name;
                    //     }

                    // ],

                    [
                        'attribute' => 'district',
                        'label' => 'อำเภอ',
                        'value' => function ($data) {
                            return $data->localityCode->districtCode->district_name;
                        },
                        'filter' => Select2::widget([
                            'name' => 'AssessmentConclusionSearch[district]',
                            'model' => $searchModel,
                            'value' => $searchModel->localityCode->districtCode->district,
                            'data' => ArrayHelper::map(\common\models\District::find()->joinWith('provinceCode')->all(), 'district_code', 'district_name'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- อำเภอ --',
                                'style' => 'width: 100%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],

                    // [
                    //     'label' => 'จังหวัด',
                    //     'attribute' => 'province',
                    //     'filter' => \yii\helpers\ArrayHelper::map(\common\models\Province::find()->where(['region_code' => Yii::$app->user->identity->userProfile->locality->provinceCode->region_code])->all(), 'province_code', 'province_name'),
                    //     'value' => function ($data) {
                    //         return $data->localityCode->provinceCode->province_name;
                    //     }
                    // ],

                    [
                        'attribute' => 'province',
                        'label' => 'จังหวัด',
                        'value' => function ($data) {
                            return $data->hospitalName->name;
                        },
                        'filter' => Select2::widget([
                            'name' => 'AssessmentConclusionSearch[province]',
                            'model' => $searchModel,
                            'value' => $searchModel->province,
                            'data' => ArrayHelper::map(\common\models\Province::find()->all(), 'province_code', 'province_name'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- จังหวัด --',
                                'style' => 'width: 100%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],
                    [
                        'label' => 'ระดับการปนะเมิน',
                        'attribute' => 'assess_result',
                        'filter' => \yii\helpers\ArrayHelper::map(\common\models\AssessmentResult::find()->all(), 'result_code', 'result_desc'),
                        'value' => function ($data) {
                            return $data->assessResult->result_desc;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>