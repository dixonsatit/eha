<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentConclusion */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="assessment-conclusion-form">

        <?php $form = ActiveForm::begin([
            'id' => 'approve-form'
        ]); ?>

        <div class="row">
            <div class="col-md-12 form-group">
                <label class="control-label" for="assessmentconclusion-approve">หน่วยงาน <?php echo $model->locality_code?></label>
                <?php echo Html::textInput('txt-locality', \common\models\Locality::find()->where(['locality_code' => $model->locality_code])->one()->locality_name, ['class' => 'form-control', 'readonly' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 form-group">
                <label class="control-label" for="assessmentconclusion-approve">ประเด็น</label>
                <?php echo Html::textarea('txt_issue', '[' . $model->issue_code . '] ' . \common\models\Issues::find()->where(['issue_code' => $model->issue_code])->one()->issue_desc, ['class' => 'form-control', 'readonly' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 form-group">
                <label class="control-label" for="assessmentconclusion-approve">ผลลัพธ์</label>
                <?php echo Html::textInput('txt-result', $model->assessResult->result_desc, ['class' => 'form-control', 'readonly' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 form-group">
                <label class="control-label" for="assessmentconclusion-approve">ข้อตกลง</label>
                <?php echo Html::textarea('txt_issue', 'การส่งผลการประเมินจะไม่สามารถทำการแก้ไขผลการประเมินได้อีก ยืนยันคลิกที่ด้านล่าง', ['class' => 'form-control', 'readonly' => true]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 form-group">
                <?= $form->field($model, 'approve')->checkbox(['Yes' => 'ยืนยัน'], ['maxlength' => true]) ?>
            </div>
        </div>

        <div class="form-group">
            <hr>
            <?= Html::submitButton(Yii::t('common', '<i class="glyphicon glyphicon-save"></i> บันทึกข้อมูล'), ['class' => 'btn btn-success', 'disabled' => true]) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$js = <<< JS

    $('#app-form').on(function (e) {
    if (e.which == 13) {
            e.preventDefault();
        }
    })

    $('#assessmentconclusion-approve').on('change', function() {
        if ($(this).prop('checked')) {
            $(':input[type="submit"]').prop('disabled', false);
        }
        else {
           $(':input[type="submit"]').prop('disabled', true);
        }
    });

JS;
$this->registerJS($js);
?>