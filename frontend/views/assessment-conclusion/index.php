<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Assessment Conclusions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-conclusion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Create Assessment Conclusion'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'be_year',
            'issue_code',
            'locality_code',
            'type',
            //'component1_score',
            //'component2_score',
            //'component3_score',
            //'component4_score',
            //'component5_score',
            //'component1to5_avgscore',
            //'component6_score',
            //'component7_score',
            //'component6to7_avgscore',
            //'component1to7_avgscore',
            //'assess_result',
            //'b',
            //'c',
            //'d',
            //'percentA',
            //'percentB',
            //'percentC',
            //'percentE',
            //'assessed_by_uid1',
            //'assessed_by_fullname1',
            //'assessed_by_uid2',
            //'assessed_by_fullname2',
            //'assessed_by_uid3',
            //'assessed_by_fullname3',
            //'created_by_ip',
            //'created_by',
            //'created_at',
            //'updated_by_ip',
            //'updated_by',
            //'certificate',
            //'approve',
            //'application_form_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
