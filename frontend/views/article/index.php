<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel frontend\models\search\ArticleSearch */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\VarDumper;

$this->title = Yii::t('frontend', 'Articles');
$this->params['breadcrumbs'][] = $category->title;
?>

<!-- <span class="glyphicon glyphicon-search" data-toggle="collapse" data-target="#search-form"></span>-->

<?php if ($category->slug == 'documents') : ?>
    <div class="collapse" id="search-form">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'options' => ['class' => 'form-inline']
        ]) ?>
        <div>
            <?php echo $form->field($searchModel, 'title')->label(false)->error(false) ?>
            <?php echo Html::submitButton(Yii::t('frontend', 'Search'), ['class' => 'btn btn-default']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
    <div class="row">
        <?php
        echo \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'layout'       => '{items}{pager}',
            'itemOptions'  => [
                'class' => "col-md-4",
            ],
            'itemView'     => '_item',
            'options'      => ['class' => 'grid-list-view'],
            'afterItem' => function ($model, $key, $index, $widget) {
                // the needed solution
                if ($index == 12 || $index == 12) {
                    return '</div>';
                }
            }
        ]);
        ?>
    </div>

<?php elseif ($category->slug == 'questions') : ?>
    <div class="row">
        <div class="col-lg-12">
            <!-- <div class="card px-3"> -->
            <!-- <div class="card-body"> -->
            <div class="list-wrapper">

                <ul class="d-flex flex-column-reverse todo-list">
                    <?php echo \yii\widgets\ListView::widget([
                        'dataProvider' => $dataProvider,
                        'layout'       => '{items}{pager}',
                        'pager' => [
                            'hideOnSinglePage' => true,
                        ],
                        'itemView' => '_item'
                    ]) ?>
                </ul>
                <!-- </div> -->
                <!-- </div> -->
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="card">
        <div class="card-body">
            <div class="row">

            </div>
        </div>
    </div>
<?php endif; ?>
