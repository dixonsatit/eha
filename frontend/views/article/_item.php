<?php

/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if ($model->category->slug == 'documents') : ?>
    <a href="<?php echo Url::to(['view', 'slug' => $model->slug]); ?>">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <?php if ($model->thumbnail_path) : ?>
                            <?php echo Html::img(
                                !empty($model->thumbnail_path) ? Yii::$app->glide->createSignedUrl([
                                    'glide/index',
                                    'path' => $model->thumbnail_path,
                                    'h' => 100,
                                    // 'fit' => 'crop'
                                ], true) : $this->assetManager->getAssetUrl($bundle, 'images/gallery/01.jpg'),
                                ['class' => 'article-thumb img-rounded']
                            ) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-12" style="text-align: center; padding-top: 10px;">
                        <h5 class="card-title"> <?php echo  $model->title ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <br>

<?php elseif ($model->category->slug == 'questions') : ?>
    <a href="<?php echo Url::to(['view', 'slug' => $model->slug]); ?>">
        <div id="accordion-1" class="accordion">
            <div class="card">
                <div class="card-header" id="<?php echo $model->id; ?>">
                    <h5 class="mb-0">
                        <a data-toggle="collapse" data-target="<?php echo '#' . $model->slug; ?>" aria-expanded="false" aria-controls="<?php echo $model->slug; ?>" class="collapsed">
                            <?php echo $model->title; ?>
                        </a>
                    </h5>
                </div>
                <div id="<?php echo $model->slug; ?>" class="collapse" aria-labelledby="<?php echo $model->id; ?>" data-parent="#accordion-<?php echo $model->id; ?>" style="">
                    <div class="card-body">
                        <?php echo $model->body; ?>
                    </div>
                </div>
            </div>
        </div>
    </a>

<?php else : ?>
    <div class="col-12 results">
        <div class="pt-4 border-bottom">
            <a class="d-block h4 mb-0" href="<?php echo Url::to(['view', 'slug' => $model->slug]); ?>"><?php echo $model->title; ?></a>
            <!-- <a class="page-url text-primary" href="#">https://www.bootstrapdash.com/</a> -->
            <p class="page-description mt-1 w-75 text-muted">
                <?php echo \yii\helpers\StringHelper::truncate($model->body, 150, '...', null, true) ?>
            </p>
        </div>
    </div>
<?php endif; ?>
