<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2mod\comments\models\CommentModel */
/* @var $encryptedEntity string */
/* @var $formId string comment form id */
?>
<div class="comment-form-container">
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => $formId,
            'class' => 'comment-box',
        ],
        'action' => Url::to(['/comment/default/create', 'entity' => $encryptedEntity]),
        'validateOnChange' => false,
        'validateOnBlur' => false,
    ]); ?>

    <?php echo $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textarea(['placeholder' => Yii::t('yii2mod.comments', 'ข้อคิดเห็น/ข้อเสนอแนะ(ถ้ามี...)'), 'rows' => 4, 'data' => ['comment' => 'content']]); ?>

    <?php
    //  echo $form->field($commentModel, 'content')->widget(
    //     \yii\imperavi\Widget::class,
    //     [
    //         'plugins' => ['fullscreen', 'fontcolor', 'video'],
    //         'options' => [
    //             'placeholder' => Yii::t('yii2mod.comments', 'ข้อคิดเห็น/ข้อเสนอแนะ(ถ้ามี...)'),
    //             'template' => '{input}{error}',
    //             'minHeight' => 200,
    //             'maxHeight' => 200,
    //             'buttonSource' => true,
    //             'convertDivs' => false,
    //             'removeEmptyTags' => true,
    //             'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
    //             'data' => ['comment' => 'content']
    //         ],
    //     ]
    // )->label(false); 
    ?>

    <?php echo $form->field($commentModel, 'parentId', ['template' => '{input}'])->hiddenInput(['data' => ['comment' => 'parent-id']]); ?>
    <div class="comment-box-partial">
        <div class="button-container show">
            <?php echo Html::a(Yii::t('yii2mod.comments', 'Click here to cancel reply.'), '#', ['id' => 'cancel-reply', 'class' => 'pull-right', 'data' => ['action' => 'cancel-reply']]); ?>
            <br>
            <?php echo Html::button('กลับหน้าประเมิน', ['class' => 'btn btn-primary btn-xs', 'style' => 'float:left;', 'onClick' => 'window.location="' . Yii::$app->request->referrer . '"']) . " " . Html::submitButton(Yii::t('yii2mod.comments', 'บันทึกข้อเสนอแนะ'), ['class' => 'btn btn-primary comment-submit']);?>
        </div>
    </div>
    <?php $form->end(); ?>
    <div class="clearfix"></div>
</div>