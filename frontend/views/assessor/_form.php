<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForm */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="assessment-form">

    <div class="row" style="color: blue;">
        <div class="col-md-12"><h4><?php echo 'การประเมินประจำปี: ' . $appForm->be_year; ?></h4></div>
        <div class="col-md-12"><h4><?php echo $appForm->locality->typeCode->type_name . ': ['.$appForm->locality_code.']'.$appForm->locality->locality_name; ?></h4></div>
    </div>
    <hr>
    <?php $form = ActiveForm::begin(); ?>
    <table class="table table-bordered table-hover table-striped" style="width: 100%;">
        <thead>
        <tr>
            <td class="block">ประเด็นหลัก</td>
            <td>ประเด็นย่อย</td>
            <td>กระบวนการ</td>
            <td>คะแนนเต็ม</td>
            <td>คะแนนประเมินตนเอง</td>
            <td>ไฟล์เอกสาร</td>
            <td>คะแนนผู้ประเมิน</td>
        <tr>
        </thead>
        <tbody>
        <?php foreach ($models AS $key => $item) : ?>
            <?php if ($item['sop_id'] !== null): ?>
                <tr>
                    <td class="block">
                        <?php echo $item['sop_parent_id'] == null ? $item['main_issue_code'] . ": " . $item['main_issue_desc'] : ''; ?>
                        <?php echo $form->field($item, "[$key]id")->hiddenInput(['readonly' => true, 'class' => 'txt_id'])->label(false); ?>
                    </td>
                    <td><?php echo $item['sop_parent_id'] == null ? $item['issue_code'] . ": " . $item['issue_desc'] : ''; ?></td>
                    <td>
                        <?php echo $item['sop_assess_step_detail']; ?>
                        <?php echo $form->field($item, "[$key]sop_id")->hiddenInput(['readonly' => true, 'class' => 'txt_sop_id'])->label(false); ?>
                    </td>
                    <td><?php echo $item['sop_assess_score']; ?></td>
                    <td><?php echo $item['sop_parent_id'] != null ? $form->field($item, "[$key]selfassess_score")->textInput(['maxlength' => true, 'class' => 'form-control input-sm txt_no', 'readonly' => true])->label(false) : ''; ?></td>
                   <td>
                        <?php if ($item['id'] != ''): ?>
                            <?php echo $item['sop_parent_id'] != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลดไฟล์'.'('.\common\models\Assessment::getCountAttach($item['id']).')', ['assessment/download-file', 'id' => $item['id']], ['class' => 'btn-upload btn btn-success btn-xs']) : ''; ?>
                        <?php endif; ?>
                    </td>
                    <td><?php echo $item['sop_parent_id'] != null ? $form->field($item, "[$key]assessor_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item['sop_assess_score'], 'class' => 'form-control input-sm txt_no', 'type' => 'number', 'disabled' => $appForm->status > 2 ? 'disabled' : false])->label(false) : ''; ?></td>

                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <hr>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'บันทึกการประเมิน' : 'บันทึกการประเมิน', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary', 'disabled' => $appForm->status > 2 ? 'disabled' : false]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>



<style type="text/css">
    td {
        border: 1px solid #000;
    }

    tr td:last-child {
        width: 1%;
        white-space: nowrap;
    }
</style>

<?php
$js = <<< JS

    $('#app-form').on(function (e) {
    if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('.txt_no').on('change', function(e){
        var max = parseInt($(this).attr("max"));
        var score = parseInt($(this).val());

        if(score > max){
            alert('จำนวนคะแนนต้องไม่มากกว่า คะแนน SOP');
            $(this).val(0);
        }
    });

    $('.btn-upload').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

    //$(this).closest('form').trigger('submit');
JS;
$this->registerJS($js);
?>