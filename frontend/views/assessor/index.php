<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'รายชื่อผู้เขารับการประเมิน');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="locality-index">

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

//            'id',
                    'locality_code',
                    'locality_name',
                    'administrator',
                    //'administrative_title',
                    //'type_code',
                    //'address',
                    //'subdistrict_code',
                    //'district_code',
                    //'province_code',
                    //'postal_code',
                    //'active_status',
                    //'created_by_ip',
                    //'created_by',
                    //'created_at',
                    //'updated_by_ip',
                    //'updated_by',
                    //'updated_at',
                    [
                        'label' => 'รับการประเมิน',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a('รับการประเมิน', ['/assessor/list-application-form', 'locality_code' => $data->locality_code], ['class' => 'btn btn-primary btn-block']);
                        }
                    ]
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>