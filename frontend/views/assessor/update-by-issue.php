<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii2mod\comments\models\CommentModel;
use yii\bootstrap\Modal;


$this->title = Yii::t('common', 'ประเมินรายประเด็น');
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['/application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['/application-form/list-application-form']];

$this->params['breadcrumbs'][] = $this->title;
foreach ($models as $first) {
    break;
}
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="assessment-form">

            <?= \common\widgets\ApplicationFormDetail::widget(['be_year' => Yii::$app->session['applicationForm']['be_year']]); ?>

            <?php $form = ActiveForm::begin([
                'id' => 'app-form',
            ]); ?>

            <br>
            <h4 class="text-primary">
                <?php if (isset($first->sop->issueCode->issue_code) && isset($first->sop->issueCode->issue_desc)) : ?>
                    <?php echo $first->sop->issueCode->issue_code . ": " . $first->sop->issueCode->issue_desc; ?>
                <?php endif; ?>
            </h4>
            <hr>
            <table class="table table-bordered table-hover " style="width: 100%;">
                <thead>
                    <tr>
                        <td>รหัส</td>
                        <td>กระบวนการ</td>
                        <td>หลักฐาน</td>
                        <td style="text-align: center;">คะแนนเต็ม</td>
                        <td style="width: 50px; text-align: center;">คะแนนประเมินตนเอง</td>
                        <td style="width: 80px; text-align: center;">คะแนนประเมิน</td>
                        <td style="text-align: center;">แนบไฟล์</td>
                        <td style="text-align: center;">ข้อเสนอแนะ</td>
                    <tr>
                </thead>
                <tbody>
                    <?php foreach ($models as $key => $item) : ?>
                        <?php if ($item['sop_id'] !== null) : ?>
                            <tr style="<?php echo $item->sop->parent_id ? '' : 'background-color:#f1f0f0;' ?>">
                                <td>
                                    <?php echo $item['sop_id']; ?>
                                    <?php echo $form->field($item, "[$key]id")->hiddenInput(['readonly' => true, 'class' => 'txt_id'])->label(false); ?>
                                </td>
                                <td colspan="<?php echo $item->sop->parent_id ? 0 : 2 ?>">
                                    <?php echo $item->sop->assess_step_detail; ?>
                                </td>
                                <?php if ($item->sop->parent_id) : ?>
                                    <td>
                                        <?php echo $item->sop->assess_step_evidence; ?>
                                    </td>
                                <?php endif; ?>
                                <td style="text-align: center;">
                                    <?php echo !$item->sop->parent_id ? '<span class="badge">' . $item->sop->assess_score . '</span>' : $item->sop->assess_score; ?>
                                </td>
                                <td style="text-align: center;">
                                    <?php echo $item->sop->parent_id != null ? '<div style="height:30px; line-height:20px; font-weight:bold; border: 1px solid #cacaca; background-color:#f3f3f3; display:block;" class="badge badge-outline-primary">' . $item->selfassess_score . '</div>' : '';
                                            //$form->field($item, "[$key]selfassess_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item['sop_assess_score'], 'class' => 'form-control input-sm txt_no', 'type' => 'number', 'readonly' => true])->label(false) : ''; 
                                            ?>
                                </td>
                                <td>
                                    <?php echo $item->sop->parent_id != null ? $form->field($item, "[$key]assessor_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item->sop->assess_score, 'class' => 'form-control input-sm txt_no', 'type' => 'number'])->label(false) : ''; ?>
                                </td>
                                <td>
                                    <?php echo $item->sop->parent_id != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลด' . '(' . \common\models\Assessment::getCountAttach($item['id']) . ')', ['assessment/upload-by-id', 'id' => $item['id']], ['class' => 'btn-upload btn btn-success btn-xs']) : ''; ?>
                                </td>
                                <td>
                                    <?php echo $item->sop->parent_id != null ? Html::a('<i class="icon-doc"></i> ข้อเสนอแนะ' . '(' . CommentModel::find()->where(['entityId' => $item['id']])->count() . ')', ['/assessor/comment', 'id' => $item['id']], ['class' => 'btn btn-primary btn-xs btn-comment', 'data-pjax' => '0']) : ''; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>

                </tbody>
            </table>
            <br>
            <hr>

            <div class="form-group">
                <?php echo Html::submitButton('<i class="glyphicon glyphicon-save"></i> บันทึกข้อมูล', ['class' =>  'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

<?php
Modal::begin([
    'header' => 'ข้อเสนอแนะ',
    'id' => 'modal-comment',
    'size' => 'modal-lg',
]);
echo "<div id='modalComment'></div>";
Modal::end();
?>



<style type="text/css">
    td {
        border: 1px solid #000;
    }

    tr td:last-child {
        width: 1%;
        white-space: nowrap;
    }

    .modal-content {
        margin-top: 200px;
        background-color: white;
    }
</style>

<?php
$js = <<< JS

    $('#app-form').on(function (e) {
    if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('.txt_no').on('change', function(e){
        var max = parseInt($(this).attr("max"));
        var score = parseInt($(this).val());

        if(score > max){
            alert('จำนวนคะแนนต้องไม่มากกว่า คะแนน SOP');
            $(this).val(0);
        }
    });

    $('.btn-upload').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

    $('.btn-comment').click(function(e){
        e.preventDefault();
        $.get($(this).attr('href'), function(data) {
          $('#modal-comment').modal('show').find('#modalComment').html(data)
       });
       return false;
    })

JS;
$this->registerJS($js);
?>