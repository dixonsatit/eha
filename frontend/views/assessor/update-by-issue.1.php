<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Modal;


$this->title = Yii::t('common', 'ประเมินรายประเด็น');
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['/application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['/application-form/list-application-form']];

$this->params['breadcrumbs'][] = $this->title;
foreach ($models as $first){
    if ($first['sop_id'] !== null)
    {
        break;
    }
}
?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>

            <p class="card-description">

            </p>

            <div class="assessment-form">

                <?= \common\widgets\ApplicationFormDetail::widget(['be_year' => Yii::$app->session['applicationForm']['be_year']]); ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'app-form',
                ]); ?>

                <br>
                <h4 class="text-primary">
                    <?php echo $first['sop_parent_id'] == null ? $first['issue_code'] . ": " . $first['issue_desc'] : ''; ?>
                </h4>
                <hr>
                <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <td>รหัส</td>
                        <td>กระบวนการ</td>
                        <td>หลักฐาน</td>
                        <td style="text-align: center;">คะแนนเต็ม</td>
                        <td style="width: 100px; text-align: center;">คะแนนประเมินตนเอง</td>
                        <td style="width: 100px; text-align: center;">คะแนนประเมิน</td>
                        <td style="text-align: center;">แนบไฟล์</td>
                    <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($models as $key => $item) : ?>
                        <?php if ($item['sop_id'] !== null): ?>
                            <tr>
                                <td>
                                    <?php echo $item['sop_id']; ?>
                                    <?php echo $form->field($item, "[$key]id")->hiddenInput(['readonly' => true, 'class' => 'txt_id'])->label(false); ?>
                                </td>
                                <td>
                                    <?php echo $item['sop_assess_step_detail']; ?>
                                    <?php echo $form->field($item, "[$key]sop_id")->hiddenInput(['readonly' => true, 'class' => 'txt_sop_id'])->label(false); ?>
                                </td>
                                <td>
                                    <?php echo $item['assess_step_evidence']; ?>
                                </td>
                                <td style="text-align: center;"><?php echo $item['sop_assess_score']; ?></td>
                                <td><?php echo $item['sop_parent_id'] != null ? $form->field($item, "[$key]selfassess_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item['sop_assess_score'], 'class' => 'form-control input-sm txt_no', 'type' => 'number', 'readonly' => true])->label(false) : ''; ?></td>
                                <td style="width: 100px;"><?php echo $item['sop_parent_id'] != null ? $form->field($item, "[$key]assessor_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item['sop_assess_score'], 'class' => 'form-control input-sm txt_no', 'type' => 'number'])->label(false) : ''; ?></td>
                                <td>
                                    <?php if ($item['id'] != ''): ?>
                                        <?php echo $item['sop_parent_id'] != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลด'.'('.\common\models\Assessment::getCountAttach($item['id']).')', ['assessment/upload-by-id', 'id' => $item['id']], ['class' => 'btn-upload btn btn-success btn-xs']) : ''; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    </tbody>
                </table>
                <br>
                <hr>

                <div class="form-group">
                    <?php echo Html::submitButton('บันทึกการประเมิน', ['class' =>  'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>



    <style type="text/css">
        td {
            border: 1px solid #000;
        }

        tr td:last-child {
            width: 1%;
            white-space: nowrap;
        }
        .modal-content{
            margin-top: 200px;
            background-color: white;
        }
    </style>

<?php
$js = <<< JS

    $('#app-form').on(function (e) {
    if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('.txt_no').on('change', function(e){
        var max = parseInt($(this).attr("max"));
        var score = parseInt($(this).val());

        if(score > max){
            alert('จำนวนคะแนนต้องไม่มากกว่า คะแนน SOP');
            $(this).val(0);
        }
    });

    $('.btn-upload').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

    //$(this).closest('form').trigger('submit');
JS;
$this->registerJS($js);
?>