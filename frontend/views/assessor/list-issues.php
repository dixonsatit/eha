<?php

use backend\models\LpaScore;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประเมินรายการประเด็น ( ' . Yii::$app->session['applicationForm']['locality_name'] . ' )';
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['/assessor/list-application-form', 'locality_code' => Yii::$app->session['applicationForm']['locality_code']]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>
        <h5>ประจำปีงบประมาณ <?php echo $applicationForm->be_year; ?></h5>

        <div class="row">
            <div class="col-md-12">
                <?php echo Html::a('<i class="icon-printer"></i> พิมพ์ใบรับรอง', ['/documents/print-certificated'], ['class' => 'btn btn-xs btn-success  pull-right', 'target' => '_blank']); ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'summary' => '',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'issue_code',
                            'label' => 'รหัส'
                        ],
                        [
                            'attribute' => 'issue_desc'
                        ],
                        [
                            'header' => 'องค์ประกอบ<br> 1-5',
                            'value' => function ($data){
                                $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                                return $score !== false ? $score->component1to5_avgscore : 0;
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'header' => 'องค์ประกอบ<br> 6',
                            'format' => 'raw',
                            'value' => function ($data) {
                                $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                                return $score !== false ? $score->percentB : 0;
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'text-align: center;'],
                        ],
                        [
                            'header' => 'องค์ประกอบ<br> 7',
                            'value' => function ($data) {
                                $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                                return $score !== false ? $score->percentC : 0;
                            },
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'contentOptions' => ['style' => 'text-align: center;'],
                        ],

                        [
                            'label' => 'ประเมิน',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function ($data) {

                                $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->one();

                                if ($assessmentConclusion != null && $assessmentConclusion->approve == 'Sent') {
                                    return Html::a('<i class="icon-note"></i> ประเมินตามประเด็น', ['update-by-issue', 'issue_code' => $data->issue_code], ['class' => 'btn btn-xs btn-block btn-primary btn-block']);
                                }
                                if ($assessmentConclusion != null && $assessmentConclusion->approve == 'Complete') {
                                    return Html::button('<i class="icon-note"></i> ประเมินตามประเด็น', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                                } else {
                                    return Html::button('<i class="icon-note"></i> ประเมินตามประเด็น', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                                }
                            },

                        ],
                        [
                            'label' => 'ส่งกลับ',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function ($data) {
                                $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->one();
                                if ($assessmentConclusion != null && $assessmentConclusion->approve == 'Sent') {
                                    return Html::a('<i class="icon-loop"></i> ส่งกลับไปแก้ไข', ['send-improve', 'issue_code' => $data->issue_code], ['class' => 'btn btn-xs btn-block btn-warning btn-block']);
                                }
                                if ($assessmentConclusion != null && $assessmentConclusion->approve == 'No') {
                                    return Html::button('<i class="icon-loop"></i> ส่งกลับไปแก้ไข', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                                } else {
                                    return Html::button('<i class="icon-loop"></i> ส่งกลับไปแก้ไข', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                                }
                            },

                        ],

//        [
//            'label' => 'รายละเอียด',
//            'format' => 'html',
//            'headerOptions' => ['style' => 'text-align: center;'],
//            'value' => function ($data) {
//                return Html::a('รายละเอียด', ['view-by-issue', 'issue_code' => $data->issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']], ['class' => 'btn btn-xs btn-block btn-info btn-block']);
//            }
//        ],


                        [
                            'label' => 'แจ้งผลประเมิน',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function ($data) {
                                $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->andWhere(['approve' => 'sent'])->one();
                                if ($assessmentConclusion != null) {
                                    return Html::a('<i class="icon-paper-plane"></i> แจ้งผลประเมิน', ['/assessment-conclusion/approve', 'issue_code' => $data->issue_code, 'locality_code' => Yii::$app->session['applicationForm']['locality_code']], ['class' => 'btn btn-xs btn-block btn-success btn-block btn-result']);
                                } else {
                                    return Html::button('<i class="icon-paper-plane"></i> แจ้งผลประเมิน', ['class' => 'btn btn-block btn-xs btn-default', 'disabled' => true]);
                                }
                            }
                        ],

                        [
                            'label' => 'สรุปผล',
                            'format' => 'raw',
                            'headerOptions' => ['style' => 'text-align: center;'],
                            'value' => function ($data) {
                                $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'assessor'])->andWhere(['approve' => 'Complete'])->one();

                                if ($assessmentConclusion != null) {
                                    return Html::a('<i class="icon-printer"></i> พิมพ์สรุปผลตรวจ', ['/documents/print-diploma-by-code', 'issue_code' => $data->issue_code, 'locality_code' => Yii::$app->session['applicationForm']['locality_code']], ['class' => 'btn btn-xs btn-block btn-success btn-block', 'target' => '_blank']);
                                } else {
                                    return Html::button('<i class="icon-printer"></i> พิมพ์สรุปผลตรวจ', ['class' => 'btn btn-block btn-xs btn-default', 'disabled' => true]);
                                }
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    td {
        border: 1px solid #000;
    }

    tr td:last-child {
        width: 1%;
        white-space: nowrap;
    }

    .modal-content {
        margin-top: 200px;
        background-color: white;
    }
</style>

<?php
Modal::begin([
    'header' => '<h4 style="display:box;">แจ้งผลการประเมิน</h4>',
    'id' => 'result-modal',
    'size' => 'modal-lg',
    'clientOptions' => [
        'backdrop' => true,
        'keyboard' => false,
    ]
]);

?>
<div class="clearfix"></div>
<?php
Modal::end();
?>

<?php
$js = <<< JS

    $('.btn-result').click(function (){
        $('#result-modal').find('.modal-body').html("<h5 class='text-center'>Loading.....<h5>");
        $('#result-modal').modal('show');
        $.get($(this).attr('href'), function(data) {
            $('#result-modal').find('.modal-body').html(data);
        });
       return false;
    });

JS;
$this->registerJS($js);
?>


