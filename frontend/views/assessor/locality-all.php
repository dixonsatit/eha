<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'ประเมินโดยคณะกรรมการ');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="locality-index">

            <ul class="nav nav-tabs tab-basic" role="tablist">
                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['locality-approve']) ?>" class="nav-link" id="home-tab" data-toggle="tab" href="#whoweare" role="tab"
                       aria-controls="whoweare"
                       aria-selected="false">รายชื่อผู้เข้ารับการประเมิน</a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['locality-all']) ?>" class="nav-link active show" id="profile-tab" data-toggle="tab" href="#ourgoal" role="tab"
                       aria-controls="ourgoal" aria-selected="true">รายชื่อทั้งหมด</a>
                </li>
            </ul>
            <div class="tab-content tab-content-basic">
                <div class="tab-pane" id="whoweare" role="tabpanel" aria-labelledby="home-tab">

                </div>
                <div class="tab-pane fade active show" id="ourgoal" role="tabpanel" aria-labelledby="profile-tab">
                    <?php Pjax::begin(); ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

//            'id',
                            'locality_code',
                            'locality_name',
                            [
                                'label' => 'จังหวัด',
                                'format' => 'html',
                                'attribute' => 'province_code',
                                'filter'=>\yii\helpers\ArrayHelper::map(\common\models\Province::find()->asArray()->all(), 'province_code', 'province_name'),
                                'value' => function($data){
                                    return $data->provinceCode->province_name;
                                }

                            ],
                            'administrator',
                            //'administrative_title',
                            //'type_code',
                            //'address',
                            //'subdistrict_code',
                            //'district_code',
                            //'province_code',
                            //'postal_code',
                            //'active_status',
                            //'created_by_ip',
                            //'created_by',
                            //'created_at',
                            //'updated_by_ip',
                            //'updated_by',
                            //'updated_at',
                            [
                                'label' => 'รับการประเมิน',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return Html::a('รับการประเมิน', ['/assessor/list-application-form', 'locality_code' => $data->locality_code], ['class' => 'btn btn-primary btn-block']);
                                }
                            ]
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
