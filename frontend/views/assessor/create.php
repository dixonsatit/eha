<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Assessment */

$this->title = Yii::t('common', '{modelClass}', [
    'modelClass' => 'ประเมินด้วยผู้ประเมิน',
]);
//$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessments'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->locality_code, 'url' => ['view', 'locality_code' => $model->locality_code, 'sop_id' => $model->sop_id]];
//$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
//

$this->params['breadcrumbs'][] = ['label' => 'ประเมินโดยคณะกรรมการ', 'url' => ['assessor/locality-approve']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['assessor/list-application-form']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="assessment-update">

            <?php echo $this->render('_form', [
                'models' => $models,
                'model' => $model,
                'appForm' => $appForm,
            ]) ?>
        </div>
    </div>
</div>
