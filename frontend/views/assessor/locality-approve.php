<?php

use common\models\ApplicationForm;
use yii\widgets\Pjax;
use common\models\District;
use common\models\Locality;
use common\models\Province;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'ประเมินโดยคณะกรรมการ');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title">รายชื่อ อปท. เข้ารับการประเมิน</h4>
        <div class="locality-index">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'locality_code',
                        'label' => 'อปท.',
                        'vAlign' => 'middle',
                        'width' => '180px',
                        'value' => function ($data) {
                            return $data->locality->locality_name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'size' => Select2::LARGE,
                            'data' => yii\helpers\ArrayHelper::map(Locality::find()->all(), 'locality_code', 'locality_name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['/user/default/local-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term};}')
                                ],

                                'escapeMarkup' => new JsExpression('function(markup) { return markup;}'),
                                'templateResult' => new JsExpression('function(local){ return local.text;}'),
                                'templateSelection' => new JsExpression('function(local) {return local.text;}'),
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => '-- อปท. --'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'district',
                        'label' => 'อำเภอ',
                        'vAlign' => 'middle',
                        'width' => '180px',
                        'value' => function ($data) {
                            return $data->locality->districtCode->district_name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'size' => Select2::LARGE,
                            'data' => yii\helpers\ArrayHelper::map(District::find()->all(), 'district_code', 'district_name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => Url::to(['/user/default/district-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term};}')
                                ],

                                'escapeMarkup' => new JsExpression('function(markup) { return markup;}'),
                                'templateResult' => new JsExpression('function(district){ return district.text;}'),
                                'templateSelection' => new JsExpression('function(district) {return district.text;}'),
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => '-- อำเภอ --'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'province',
                        'label' => 'จังหวัด',
                        'value' => function ($data) {
                            return $data->locality->provinceCode->province_name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filterWidgetOptions' => [
                            'size' => Select2::LARGE,
                            'data' => yii\helpers\ArrayHelper::map(Province::find()->all(), 'province_code', 'province_name'),
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => '-- จังหวัด --'],
                        'format' => 'raw'
                    ],

                    [
                        'attribute' => 'be_year',
                        // 'filter'=>ArrayHelper::map(ApplicationForm::find()->groupBy('be_year')->asArray()->all(), 'be_year', 'be_year'),
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'be_year',
                            'value' => $searchModel->be_year,
                            'data' => ArrayHelper::map(ApplicationForm::find()->asArray()->all(), 'be_year', 'be_year'),
                            'size' => Select2::LARGE,
                            'options' => [
                                'placeholder' => '-- ปีงบประมาณ --',
                                'style' => 'width: 10%;'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            // 'addon' => [
                            //     'prepend' => [
                            //         'content' => FAS::icon('open')
                            //     ]
                            // ],
                        ]),
                    ],

                    // [
                    //     'attribute' => 'province',
                    //     'label' => 'จังหวัด',
                    //     'value' => function ($data) {
                    //         return $data->provinceCode->province_name;
                    //     },
                    //     'filter' => Select2::widget([
                    //         'name' => 'LocalitySearch[province]',
                    //         'model' => $searchModel,
                    //         'value' => $searchModel->province_code,
                    //         'data' =>  \yii\helpers\ArrayHelper::map(\common\models\Province::find()->asArray()->all(), 'province_code', 'province_name'),
                    //         // 'data' => function () {
                    //         //     if (Yii::$app->user->can('webmaster')) {
                    //         //         return \yii\helpers\ArrayHelper::map(\common\models\Province::find()->asArray()->all(), 'province_code', 'province_name');
                    //         //     }
                    //         //     if (Yii::$app->user->can('manager')) {
                    //         //         return \yii\helpers\ArrayHelper::map(\common\models\Province::find()->where(['province_code' => isset(Yii::$app->user->identity->userProfile->office->province_code) ? Yii::$app->user->identity->userProfile->office->province_code : ''])->asArray()->all(), 'province_code', 'province_name');
                    //         //     }
                    //         // },
                    //         'size' => Select2::LARGE,
                    //         'options' => [
                    //             'placeholder' => '-- จังหวัด --',
                    //             'style' => 'width: 100%;'
                    //         ],
                    //         'pluginOptions' => [
                    //             'allowClear' => true
                    //         ],
                    //         // 'addon' => [
                    //         //     'prepend' => [
                    //         //         'content' => FAS::icon('open')
                    //         //     ]
                    //         // ],
                    //     ]),
                    // ],

                    //'administrative_title',
                    //'type_code',
                    //'address',
                    //'subdistrict_code',
                    //'district_code',
                    //'province_code',
                    //'postal_code',
                    //'active_status',
                    //'created_by_ip',
                    //'created_by',
                    //'created_at',
                    //'updated_by_ip',
                    //'updated_by',
                    //'updated_at',
                    [
                        'label' => 'รับการประเมิน',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a('<i class="icon-list"></i> รับการประเมิน', ['/assessor/list-issues', 'application_form_id' => $data->id], ['class' => 'btn btn-primary btn-block btn-xs']);
                            // return Html::a('<i class="icon-list"></i> รับการประเมิน', ['/assessor/list-application-form', 'locality_code' => $data->locality_code], ['class' => 'btn btn-primary btn-block btn-xs']);
                        }
                    ],
                ]
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>