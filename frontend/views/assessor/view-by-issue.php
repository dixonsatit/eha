<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii2mod\comments\models\CommentModel;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'รายละเอียดการประเมิน') . '('. $applicationForm->locality->locality_name .')';
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['/application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['/assessor/list-issues', 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]];
$this->params['breadcrumbs'][] = $this->title;
foreach ($assessment as $index=>$first){
    if ($first['sop_id'] !== null)
    {
        break;
    }
}
?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>

            <div class="assessment-index">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <h4 class="text-primary">
                    <?php echo $first['sop']['parent_id'] == null ? $first['sop']['issueCode']['issue_code'] . ": " . $first['sop']['issueCode']['issue_desc'] : ''; ?>
                </h4>
                <hr>

                <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <td class="block">รหัส</td>
                        <!-- <td>ประเด็นย่อย</td> -->
                        <td>กระบวนการ</td>
                         <td>หลักฐาน</td>
                        <td>คะแนนเต็ม</td>
                        <td>คะแนนประเมินตนเอง</td>
                        <td>คะแนนตรวจประเมิน</td>
                        <td style="text-align: center;">ไฟล์</td>
                    <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($assessment AS $key => $item) : ?>
                        <?php if ($item['sop_id'] !== null): ?>
                            <tr style="<?php echo $item->sop->parent_id ? '' : 'background-color:#f1f0f0;'?>">
                                <td class="block">
                                    <?php echo $item['sop']['id']; //$item['sop']['parent_id'] == null ? $item['sop']['issueCode']['mainIssueCode']['id'] . ": " . $item['sop']['issueCode']['mainIssueCode']['main_issue_desc'] : ''; ?>
                                </td>
                                <!-- <td><?php //echo $item['sop']['parent_id'] == null ? $item['sop']['issueCode']['issue_code'] . ": " . $item['sop']['issueCode']['issue_desc'] : ''; ?></td> -->
                                <td colspan="<?php echo $item->sop->parent_id ? 0 : 2?>"> 
                                    <?php echo $item['sop']['assess_step_detail']; ?>
                                </td>
                                <?php if($item->sop->parent_id): ?>
                                    <td>
                                        <?php echo $item['sop']['assess_step_evidence']; ?>
                                    </td>
                                  <?php endif; ?>
                                <td style="width: 80px; text-align: center;"><?php echo !$item->sop->parent_id ? '<span class="badge">'.$item->sop->assess_score.'</span>':$item->sop->assess_score; ?></td>
                                <td style="width: 140px; text-align: center;"><?php echo $item->sop->parent_id? $item['selfassess_score']: null; ?></td>
                                <td style="width: 130px; text-align: center;"><?php echo $item->sop->parent_id? $item['assessor_score'] : null; ?></td>
                                <td style="text-align: center;"><?php echo $item['sop']['parent_id'] != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลด'.'('.\common\models\Assessment::getCountAttach($item['id']).')', ['assessment/download-file', 'id' => $item['id']], ['class' => 'btn-download btn btn-success btn-xs']) : ''; ?></td>
                                <td>
                                    <?php echo $item->sop->parent_id != null ? Html::a('<i class="icon-doc"></i> ข้อเสนอแนะ' . '(' . CommentModel::find()->where(['entityId' => $item['id']])->count() . ')', ['/assessor/comment', 'id' => $item['id']], ['class' => 'btn btn-primary btn-xs btn-comment', 'data-pjax' => '0']) : ''; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                                    <hr>
                <div class="row">
                <div class="col-md-12">
                <?php echo Html::a('กลับหน้ารับการประเมิน', ['/assessor/list-issues', 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']], ['class' => 'btn btn-primary btn-sm'] )?>
                </div>
                </div>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

<?php
Modal::begin([
    'header' => 'ข้อเสนอแนะ',
    'id' => 'modal-comment',
    'size' => 'modal-lg',
]);
echo "<div id='modalComment'></div>";
Modal::end();
?>

<style type="text/css">

    .modal-content{
        margin-top: 200px;
        background-color: white;
    }

</style>

<?php
$js = <<< JS

    $('.btn-download').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

    $('.btn-comment').click(function(e){
        e.preventDefault();
        $.get($(this).attr('href'), function(data) {
          $('#modal-comment').modal('show').find('#modalComment').html(data)
       });
       return false;
    });

JS;
$this->registerJS($js);
?>

