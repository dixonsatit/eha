<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ApplicationFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'รายการประเมิน');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมินโดยคณะกรรมการ'), 'url' => ['/assessor/locality-approve']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title . ' ' . $dataProvider->models[0]->locality->locality_name; ?></h4>
        
        <p class="card-description">

        </p>

        <div class="application-form-index">
            <?php Pjax::begin();?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?=GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'columns' => [

//                    [
//                        'class' => 'kartik\grid\ExpandRowColumn',
//                        'width' => '50px',
//                        'value' => function ($model, $key, $index, $column) {
//                            return GridView::ROW_EXPANDED;
//                        },
//                        'detail' => function ($data, $key, $index, $column) {
//
//                            $apply_issues = \yii\helpers\Json::decode($data->apply_issues);
//                            $apply_issues = is_array($apply_issues) ? $apply_issues : [];
//                            $query = \common\models\Issues::find()->where(['IN', 'issues.issue_code', $apply_issues]);
//
//                            $dataProvider = new \yii\data\ActiveDataProvider([
//                                'query' => $query,
//                                'pagination' => [
//                                    'pageSize' => 20,
//
//                                ],
//                            ]);
//                            return Yii::$app->controller->renderPartial('/assessor/list-issues', [
//                                'dataProvider' => $dataProvider,
//                                'application_form_id' => $data->id
//                            ]);
//                        },
//                        'headerOptions' => ['class' => 'kartik-sheet-style'],
//                        'expandOneOnly' => true,
//                    ],

                    'be_year',
                    [
                        'label' => 'อปท.',
                        'value' => function ($data) {
                            return  '['.$data->locality_code .'] '.$data->locality->locality_name;
                        },
                    ],

//                    'contact_person',
                    //                    'contact_title',

                    //'contact_mobile',
                    //'contact_email:email',
                    //'apply_person',
                    //'apply_title',
                    //'apply_issues:ntext',
                    //'apply_date',
                    //'created_by_ip',
                    //'created_by',
//                    [
//                        'label' => 'วันที่สร้าง',
//                        'attribute' => 'created_at',
//                        'value' => function($data){
//                            return Yii::$app->formatter->asDate($data->created_at, 'dd MMMM Y');
//                        }
//                    ],
                    //'updated_by_ip',
                    //'updated_by',
                    //'updated_at',
//                    [
//                        'label' => 'สถานะ',
//                        'format' => 'raw',
//                        'value' => function ($data) {
//                            $model = \common\models\AssessmentResult::findOne($data->status);
//                            return $model !== null ? $model->result_desc : 'เตรียมข้อมูล';
//                        },
//                    ],

                    [
                        'label' => 'รายการ',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->status > 0) {
                                return Html::a('รายการประเด็น', ['assessor/list-issues', 'application_form_id' => $data->id], [
                                    'class' => 'btn btn-info btn-fw btn-xs',
                                    'data-pjax' => '0',
                                    //'disabled' => $data->status == 2 ? false : 'disabled'
                                    ]);

//                                . ' ' . Html::a('แสดงการประเมิน', ['assessment/index', 'be_year' => $data->be_year, 'application_form_id' => $data->id], [
//                                    'class' => 'btn btn-success btn-fw btn-sm',
//                                    'data-pjax' => '0',
//                                ]);
                            }
                        },
                    ],
                ],
            ]);?>
            <?php Pjax::end();?>
        </div>
    </div>
</div>

