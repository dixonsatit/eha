<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 13/1/19 AD
 * Time: 14:29
 */
use common\models\Sop;
?>
<?php //echo \yii\helpers\Html::img(Yii::getAlias('@frontend').'/web/img/eha-bg.png', ['width' => '200px'])?>

<?php if (isset($assessmentCon)): ?>
    <div style="line-height: 12px;" class="page-break-on">
        <div style="text-align: center;">
            <h3>สรุปผลตรวจประเมินรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม</h3>
            <h4>องค์กรปกครองส่วนท้องถิ่น (Environmental Health Accreditation: EHA)</h4>
            <h4>ประจำปี <?php echo $assessmentCon->be_year; ?></h4>
        </div>

        <br>
        <br>

        <div style="width: 100%; padding: 10px; border: 1px solid #000000;">

            <p>ประเด็นงานที่ <?php echo $assessmentCon->issueCode->issue_desc; ?></p>

            <p><?php echo Sop::getTypeEha($assessmentCon->issueCode->issue_code); ?></p>

            <p>รหัสการรับรอง <?php echo $assessmentCon->issueCode->issue_name; ?></p>
        </div>
        <br>
        <br>

        <div>
            <p>ชื่อ อปท. ....<?php echo $locality->locality_name; ?>
                ....อำเภอ....<?php echo $locality->districtCode->district_name; ?>
                ....จังหวัด....<?php echo $locality->provinceCode->province_name; ?>....</p>
        </div>
        <br>
        <br>

        <div>
            <table>
                <thead>
                <tr>
                    <td><h5>หัวข้อประเมิน</h5></td>
                    <td><h5>ร้อยละคะแนนที่ได้</h5></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <h5>องค์ประกอบที่ 1 - 5</h5>

                        <p>ใช้ผลการประเมินมาตรฐานการปฏิบัติราชการขององค์กรปกครองส่วนท้องถิ่น</p>

                        <p>(LPA) (คะแนนเฉลี่ยรวม)</p>
                    </td>
                    <td style="text-align: right;">
                        <h4><?php echo $assessmentCon->percentA != null ? number_format($assessmentCon->percentA, 2) : '-' ?></h4>
                        (A)
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <br>
        <br>

        <div>
            <table style="width: 100%; font-size: 14px;">
                <thead style="text-align: center;">
                <tr>
                    <td rowspan="2">หัวข้อประเมิน</td>
                    <td rowspan="2">คะแนนเต็ม</td>
                    <td colspan="2">คะแนนที่ได้</td>
                </tr>
                <tr>
                    <td>คะแนน</td>
                    <td>ร้อยละ</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>องค์ประกอบที่ 6 การจัดการกระบวนการ</td>
                    <td style="text-align: center;">100</td>
                    <td style="text-align: right;"><?php echo $assessmentCon->b != null ? number_format($assessmentCon->b, 1) : '-' ?>
                        (B)
                    </td>
                    <td style="text-align: right;"><?php echo $assessmentCon->percentB != null ? number_format($assessmentCon->percentB, 1) : '-' ?></td>
                </tr>
                <tr>
                    <td>องค์ประกอบที่ 7 การวัดผลลัพธ์</td>
                    <td style="text-align: center;">100</td>
                    <td style="text-align: right;"><?php echo $assessmentCon->c != null ? number_format($assessmentCon->c, 1) : '-' ?>
                        (C)
                    </td>
                    <td style="text-align: right;"><?php echo $assessmentCon->percentC != null ? number_format($assessmentCon->percentC, 1) : '-' ?></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td>คะแนนรวม</td>
                    <td style="text-align: center;">200</td>
                    <td style="text-align: right;"><?php echo $assessmentCon->d != null ? number_format($assessmentCon->d, 1) : '-' ?>
                        (D)
                    </td>
                    <td style="text-align: right;"><?php echo $assessmentCon->percentE != null ? number_format($assessmentCon->percentE, 1) : '-' ?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>

        <br>
        <br>
        <br>
        <br>

        <div style="border: 1px solid #000000; width: 100%; padding: 5px; line-height: 1.42857143;">
            <p>หมายเหตุ: <span>1.ร้านละของคะแนนรวม = (D)/200x100</span><span>; (D)=(B)+(C)</span></p>

            <p>2.ผ่านการประเมินระดับพื่นฐาน ต้องมีร้อยละของคะแนนแต่ละข้อ (A),(B),(C) และคะแนน(D) ไม่น้อยกว่าร้อยละ60</p>

            <p>3.ผ่านการประเมินระดับเกีรยติบัตร ต้องมีร้อยละของคะแนนแต่ะละข้อ (A),(B),(C) และคะแนน (D)
                ไม่น้อยกว่าร้อยละ80</p>
        </div>
        <br>
        <br>
        <br>
        <br>

        <div>
            <table style="width: 100%; padding: 10px;">
                <tbody>
                <tr>
                    <td style="line-height: 30px; text-align: center;">

                        <?php echo $assessmentCon->assessResult->result_desc; ?>

                    </td>
                    <td style="text-align: center;">
                        <p>ลงชื่อ...............................................</p>

                        <p>&nbsp; &nbsp; (..................................................)</p>

                        <p>ตำแหน่ง.........................................</p>

                        <p>(ผู้รับการประเมิน)</p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <br>
        <br>


        <div>
            <h5>คณะผู้ตรวจประเมิน</h5>

            <p>ลงชื่อ........................................</p>

            <p>ลงชื่อ........................................</p>

            <p>ลงชื่อ........................................</p>
        </div>
    </div>
<?php endif; ?>