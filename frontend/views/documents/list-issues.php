<?php

use common\grid\EnumColumn;
use common\models\Article;
use common\models\ArticleCategory;
use trntv\yii\datetime\DateTimeWidget;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * @var $this         yii\web\View
 * @var $searchModel  backend\modules\content\models\search\ArticleSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Yii::t('frontend', 'สรุปผลการประเมิน (จากคณะกรรมการ)');
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['application-form/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => [
                    'class' => 'grid-view table-responsive',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'label' => 'รหัส',
                        'value' => function ($data) {
                            return $data->issueCode->issue_name;
                        }
                    ],
                    [
                        'label' => 'รายการ',
                        'value' => function ($data) {
                            return $data->issueCode->issue_desc;
                        }
                    ],
                    [
                        'label' => 'สถานะ',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a($data->assessResult->result_desc, ['print-diploma-by-code', 'issue_code' => $data->issue_code], ['class' => 'btn btn-success btn-block btn-sm', 'target' => '_blank']);
                        }
                    ]
                ],
            ]); ?>
        </div>
    </div>
</div>