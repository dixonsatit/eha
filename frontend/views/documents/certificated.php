<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 13/1/19 AD
 * Time: 14:29
 */
?>

<div>
    <div style="text-align: center;">

        <?php echo \yii\helpers\Html::img(Yii::getAlias('@frontend') . '/web/img/logo-1.png', ['width' => '100px', 'style' => 'margin-left: 35%; float: left; position: block;']) ?>
        <?php echo \yii\helpers\Html::img(Yii::getAlias('@frontend') . '/web/img/logo-2.png', ['width' => '100px', 'style' => 'margin-right: 30%; float: left; position: block;']) ?>
    </div>

    <br>
    <br>

    <div style="text-align: center;">

        <h1><?php echo isset($cerForm->headline1) ? $cerForm->headline1 : ''?></h1>

        <br>

        <h2><?php echo $locality->locality_name; ?></h2>

        <h2>อำเภอ <?php echo $locality->districtCode->district_name; ?></h2>

        <h2>จังหวัด <?php echo $locality->provinceCode->province_name; ?></h2>

    </div>

    <br>

    <div style="text-align: center;">

        <h2>ได้รับการรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม</h2>

        <h2 style="font-family: Niramit; font-size: 16px;">(Environmental Health Accreditation : EHA)</h2>

        <br>

        <table width="100%">
            <tbody>ด้าน:

            <?php if (isset($failAssessment)): ?>
                <tr style="width: 100%;">
                    <td style="text-align: center; font-family: Niramit;"><?php echo $failAssessment; ?></td>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </tr>
            <?php endif; ?>

            <?php if (!isset($failAssessment)): ?>

            <?php foreach ($assessmentConclusions as $key => $assessmentConclusion): ?>
                <tr>
                    <td style="font-size: 22px; color: #000000;"><?php echo $key == 0 ? 'ด้าน&nbsp;&nbsp;' : '' ?></td>
                    <td style="font-size: 22px; color: red;">
                        <?php echo $assessmentConclusion->issueCode->issue_desc; ?>
                    </td>
                    <td style="text-align: right; font-family: Niramit; font-size: 15px; color: red;">
                        <?php echo $assessmentConclusion->issue_code . ' : ' . $assessmentConclusion->be_year; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php for ($x = 8; $x <= $rowEmpty; $x++): ?>
                <tr>
                    <td style="font-size: 22px; color: red;">&nbsp;</td>
                    <td style="text-align: right; font-family: Niramit; font-size: 15px; color: red;">&nbsp;</td>
                </tr>
            <?php endfor; ?>

            <?php endif; ?>

            </tbody>
        </table>
        <br>
        <br>

        <?php
        $year = date("Y", strtotime(date('Y-m-d'))) + 543; ?>
        <h3>ออกให้ ณ วันที่ <?php echo Yii::$app->formatter->asDate('now', 'd MMMM ') . $year; ?></h3>
    </div>

    <br>
    <br>
    <br>
    <br>

    <table width="100%">
        <thead>
        <tr style="text-align: center; font-size: 14px;">
            <td style="text-align: center; width: 33%"><h4>(<?php echo isset($cerForm->footer3) ? $cerForm->footer3 : ''?>)<br>อธิบดีกรมอนามัย</h4>
            </td>
            <td style="text-align: center; width: 33%;">
                <!-- <?php //echo \yii\helpers\Html::img(Yii::getAlias('@frontend') . '/web/img/code.png', ['width' => '70']) ?> -->
            </td>
            <td style="text-align: center; width: 33%;"><h4>(<?php echo isset($cerForm->footer4) ? $cerForm->footer4 : ''?>)<br>อธิบดีกรมส่งเสริมการปกครองท้องถิ่น</h4></td>
        </tr>
        </thead>
    </table>

</div>
