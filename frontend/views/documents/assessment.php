<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 13/1/19 AD
 * Time: 14:29
 */
$locality = $applicationForm->locality;
?>
<?php //echo \yii\helpers\Html::img(Yii::getAlias('@frontend').'/web/img/eha-bg.png', ['width' => '200px'])?>


    <div style="line-height: 12px;" class="page-break-on">
        <div style="text-align: center;">
            <h3>สรุปผลตรวจประเมินรับรองคุณภาพระบบบริการอนามัยสิ่งแวดล้อม</h3>
            <h4>องค์กรปกครองส่วนท้องถิ่น(Environmental Health Accreditation: EHA) 
            ประจำปี <?php echo $applicationForm->be_year; ?></h4>
            <p>(เอกสารที่ทีมประเมินฯ นำไปลงคะแนนในแบบประเมินประสิทธิภาพขององค์กรปกครองส่วนท้องถิ่น</p>
            <p> ( Local Performance Assessment : LPA) ประจำปี <?php echo $applicationForm->be_year; ?> ด้านที่ 4 ด้านการบริการสาธารณะ 
หัวข้อประเมินที่ ข้อ 2.2.6 และ 2.2.7)
</p>
        </div>

        <br>
        <br>

       <div>
            <p>ชื่อ อปท. <strong><u><?php echo $locality->locality_name; ?></u></strong>
                อำเภอ <strong><u><?php echo $locality->districtCode->district_name; ?></u></strong>
                จังหวัด <strong><u><?php echo $locality->provinceCode->province_name; ?></u></strong></p>
        </div>

        <br>
        <br>
        <p>ข้อ 2.2.6 ผ่านการประเมินคุณภาพระบบบริการอนามัยสิ่งแวดล้อม ด้านการจัดการสุขาภิบาลอาหาร และ ด้านการจัดการคุณภาพน้ำบริโภค
</p>
        <div>
            <table>
                <thead>
                <tr>
                    <td><h5>เกณฑ์การประเมิน</h5></td>
                    <td><h5>เกณฑ์ที่ให้คะแนน</h5></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                         ผ่านการประเมินระดับเกียรติบัตร 1 – 2 ด้าน
                    </td>
                    <td style="text-align: right;">
                        5 คะแนน
                    </td>
                </tr>
                <tr>
                    <td>
                         ผ่านการประเมินระดับพื้นฐาน 1 – 2 ด้าน
                    </td>
                    <td style="text-align: right;">
                        3 คะแนน
                    </td>
                </tr>
                 <tr>
                    <td>
                         มีการสมัครเข้าร่วม อย่างน้อย 1 ด้าน และมีผลการประเมินตนเอง และมีผลการประเมินจากคณะกรรมการ (ไม่ผ่าน)
                    </td>
                    <td style="text-align: right;">
                        1 คะแนน
                    </td>
                </tr>
                 <tr>
                    <td>
                         ไม่สมัครเข้าร่วมการพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม (ทั้ง 2 ด้าน)
                    </td>
                    <td style="text-align: right;">
                        0 คะแนน
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <br>
        <br>
        <p>ข้อ 2.2.7 ผ่านการประเมินคุณภาพระบบบริการอนามัยสิ่งแวดล้อม ด้านการจัดการสิ่งปฏิกูล  และ ด้านการจัดการมูลฝอย
</p>
        <div>
           <table>
                <thead>
                <tr>
                    <td><h5>เกณฑ์การประเมิน</h5></td>
                    <td><h5>เกณฑ์ที่ให้คะแนน</h5></td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                         ผ่านการประเมินระดับเกียรติบัตร 1 – 2 ด้าน
                    </td>
                    <td style="text-align: right;">
                        5 คะแนน
                    </td>
                </tr>
                <tr>
                    <td>
                         ผ่านการประเมินระดับพื้นฐาน 1 – 2 ด้าน
                    </td>
                    <td style="text-align: right;">
                        3 คะแนน
                    </td>
                </tr>
                 <tr>
                    <td>
                         มีการสมัครเข้าร่วม อย่างน้อย 1 ด้าน และมีผลการประเมินตนเอง และมีผลการประเมินจากคณะกรรมการ (ไม่ผ่าน)
                    </td>
                    <td style="text-align: right;">
                        1 คะแนน
                    </td>
                </tr>
                 <tr>
                    <td>
                         ไม่สมัครเข้าร่วมการพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม (ทั้ง 2 ด้าน)
                    </td>
                    <td style="text-align: right;">
                        0 คะแนน
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <br>
        <br>
        <br>
        <br>

        <div>
            <table style="width: 100%; padding: 10px; border:none;">
                <tbody>
                <tr >
                    <td style="line-height: 200px; text-align: center;border:none;">

                        <?php echo isset($assessmentCon->assessResult->result_desc) ? $assessmentCon->assessResult->result_desc : ''; ?>

                    </td>
                    <td style="text-align: right; border:none;" >
                        <p>ลงชื่อ..........................................ประธานผู้ตรวจประเมิน</p>
                        <p>&nbsp; &nbsp; (..................................................)</p>
                        <p>ลงชื่อ...............................................คณะผู้ตรวจประเมิน</p>
                        <p>&nbsp; &nbsp; (..................................................)</p>
                        <p>ลงชื่อ...............................................คณะผู้ตรวจประเมิน</p>
                        <p>&nbsp; &nbsp; (..................................................)</p>
                        
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <br>
        <br>


        <div style="text-align:center;">
            <h5>คณะผู้ตรวจประเมิน</h5>

            <p>ลงชื่อ........................................ผู้รับการประเมิน</p>
            <p>&nbsp; &nbsp; (..................................................)</p>
            <p>ปลัด/รองปลัด/ผอ.สำนัก/กอง..................................................</p>
        </div>
    </div>
