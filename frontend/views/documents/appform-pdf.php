<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 7/2/19 AD
 * Time: 08:32
 */
use kartik\grid\GridView;
use yii\helpers\Html;

?>

<div class="page">
    <table style="border: 0px;">
        <tr style="border: 0px;">
            <td style="border: 0px; border: 0px;"></td>
            <td style="text-align: center; border: 0px;">
                <h2>ใบสมัคร</h2>
            </td>
            <td style="border: 0px;"></td>
        </tr>
    </table>
    <h4 style="text-align: center;">การพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม องค์ปกครองท้องถิ่น<br>Environmental Health
        Accreditation : EHA</h4>

    <p>
        ประเภทหน่วยงาน (เทศบาล นคร/เมือง/ตำบล/ หรือ อบต. หรื่อ อื่นๆ (ระบุ) <?php echo $info['localityType']; ?><br>
        ชื่อหน่วยงาน <?php echo $info['localityName']; ?> ตำบล <?php echo $info['subdictrict']; ?><br>
        อำเภอ <?php echo $info['dictrict']; ?> จังหวัด <?php echo $info['province']; ?> โทร <?php echo $info['tel'] !== '' ? $info['tel'] : '......................................' ?>
        ชื่อ-สกุล ผู้บริหารหน่วยงาน <?php echo $info['apply_person'] !== '' ? $info['apply_person'] : '.....................................'; ?> ตำแหน่ง <?php echo $info['apply_title'] == '' ? $info['apply_title'] : '.....................................'; ?>
    </p>

    <p style="text-indent: 2.5em;">
        ยินดีสมัครเข้าร่วมการพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม องค์กรปกครองส่วนท้องถิ่น
        ของกรมอนามัยกระทนวงสาธารณสุข ดังนี้ (หน่วยงานสามารถสมัครเข้าร่วมการพัฒนาคุณภาพ ได้มากกว่า 1 ด้าน)
    </p>
    <table style="border: 0px;">
        <tbody>
            <?php foreach ($results as $key => $result) : ?>
                <tr style="border: 0px;">
                    <td style="border: 0px; vertical-align: top;">
                        <?php echo $result['mainIssue_id'] . '.' . $result['mainIssue_desc']; ?>
                    </td>

                    <td style="text-align: left; border: 0px;">

                        <table style="border: 0px;">
                            <tbody>
                                <?php foreach ($result['issues'] as $item) : ?>
                                    <tr style="border: 0px;">
                                        <td width="50px" style="border: 0px; padding: 1px;">
                                            <?php foreach ($selection as $s => $select) : ?>
                                                <?php if ($select === $item['issue_code']):?>
                                                <?php echo '[ √ ]';?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </td>
                                        <td style="border: 0px; text-align: left; padding: 1px;">
                                            <?php echo $item['issue_code'] . ' ' . $item['issue_desc'];?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <br>

    <p>
        ชื่อ-สุกล เจ้าหน้าที่ผู้รับผิดชอบ/ประสานงาน <?php echo $info['contact_person']; ?><br>
        ตำแหน่ง <?php echo $info['contact_title']; ?>
        โทรศัพท์/โทรสาร <?php echo $info['contact_phone']; ?>
        มือถือ <?php echo $info['contact_mobile']; ?> E-mail: <?php echo $info['contact_email'];; ?>
    </p>

    <table style="border: 0px;">
        <tbody style="border: 0px;">
<<<<<<< HEAD
        <tr>
            <td style="vertical-align: text-top; border: 0px; width: 40%;">
                <p style="font-size: 12px; border: 0px;">
                    กรุณาส่งใบสมัคร ทางโทรสารได้ที่ สำนักงานสาธารณสุขจังหวัด, สำนักงานสาธารณสุขอำเภอร์<br>
                    สามารถตรวจสอบเบอร์โทรศูนย์อนามัยที่ 1-12 ได้ที่เว็บไซต์: http://eha.anamai.moph.go.th<br>
                    หรือสอบถามข้อมูลเพิ่มเติมได้ที่ สำนักสุขาภิบาลอาหารและน้ำ กรมอนามัย โทรศัพท์: 0 2590 4188} 0 2590
                    4184
                </p>
            </td>
            <td style="text-align: center; border: 0px;">
                <p>
                    ลงชื่อ............................................................................... <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.............................................................................)<br>
                    ตำแหน่ง นายกเทศมนตรี.................................................<br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(หรือผู้ที่ได้รับมอบหมาย)<br>
                    วันที่สมัคร <?php echo $info['created_at'] . ' ' . $info['year']; ?>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
</div>
=======
            <tr>
                <td style="vertical-align: text-top; border: 0px; width: 40%;">
                    <p style="font-size: 12px; border: 0px;">
                        กรุณาส่งใบสมัคร ทางโทรสารได้ที่ สำนักงานสาธารณสุขจังหวัด, สำนักงานสาธารณสุขอำเภอร์<br>
                        สามารถตรวจสอบเบอร์โทรศูนย์อนามัยที่ 1-12 ได้ที่เว็บไซต์: http://eha.anamai.moph.go.th<br>
                        หรือสอบถามข้อมูลเพิ่มเติมได้ที่ สำนักสุขาภิบาลอาหารและน้ำ กรมอนามัย โทรศัพท์: 0 2590 4188} 0 2590
                        4184
                    </p>
                </td>
                <td style="text-align: center; border: 0px;">
                    <p>
                        ลงชื่อ............................................................................... <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.............................................................................)<br>
                        ตำแหน่ง นายกเทศมนตรี.................................................<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(หรือผู้ที่ได้รับมอบหมาย)<br>
                        วันที่สมัคร <?php echo $info['created_at'] . ' ' . $info['year']; ?>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
>>>>>>> 71cc3b3d1d3302b500a5d5724ff6c114537e40de
