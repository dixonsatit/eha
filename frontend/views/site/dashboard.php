
<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 8/11/18 AD
 * Time: 11:32
 */

$this->title = 'Dashboard';
//$this->params['breadcrumbs'][] = ['label' => '', 'url' => ['']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">

	<div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/gis/index']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4 class="mb-0 font-weight-medium">แผนที่พิกัด</h4>
                           
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

	<div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/dashboard/applied/index']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4 class="mb-0 font-weight-medium">ร้อยละของ อปท. ที่สมัครเข้ารับการประเมิน EHA</h4>
                           
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>	

</div>


<div class="row">

	<div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/dashboard/basic-accredit/index']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4 class="mb-0 font-weight-medium"> ร้อยละของ อปท. ที่ผ่านเกณฑ์การประเมิน EHA ระดับพื้นฐาน</h4>
                           
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

	<div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/dashboard/certified-accredit/index']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4 class="mb-0 font-weight-medium"> ร้อยละของ อปท. ที่ผ่านเกณฑ์การประเมิน EHA ระดับเกียรติบัตร</h4>
                           
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>	

</div>