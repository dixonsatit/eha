<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
?>
<style>
.small-logo {
    display: block!important;
    float: left;
}
</style>
<div id="header">
    <div class="color-line">
    </div>
    <?= $this->render('//layouts/_logo');?>
    <!--<div id="logo" class="light-version">
        <span>
          <?=Yii::$app->id == 'app-backend'? 'ADMIN' : ''?>  <?= strtoupper(Yii::$app->params['app.brandLabel'])?>
        </span>
    </div>-->
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="small-logo">
            <span class="text-primary" style="font-size:18px;">ระบบสารสนเทศสนับสนุนด้านการส่งเสริมสุขภาพและอนามัยสิ่งแวดล้อม</span>
        </div>

        <div class="mobile-menu">
            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">
                <i class="fa fa-chevron-down"></i>
            </button>

            <div class="collapse mobile-navbar" id="mobile-collapse">
                <?=Nav::widget([
                    'encodeLabels'=>false,
                    'options'=>[
                      'class'=>'nav navbar-nav'
                    ],
                    'items' => [
                        [
                          'label' => '<i class="pe-7s-users"></i> Account',
                          'url' => ['/user/settings/profile'],
                          'visible'=> !Yii::$app->user->isGuest
                        ],
                        [
                          'label' => '<i class="pe-7s-upload pe-rotate-90"></i> Logout',
                          'url' => ['/logout'],
                           'linkOptions' => ['data-method' => 'post']
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

        <div class="navbar-right">
            <?=Nav::widget([
                'encodeLabels'=>false,
                'options'=> [
                   'class'=>'nav navbar-nav no-borders'
                ],
                'items' => [
                    [
                      'label' => '<i class="pe-7s-key"></i>',
                      'url' => ['/login'],
                      'visible'=> Yii::$app->user->isGuest
                    ],
                    [
                      'label' => '<i class="pe-7s-users"></i>',
                      'url' => ['/user/settings/profile'],
                       'visible'=> !Yii::$app->user->isGuest
                    ],
                    [
                      'label' => '<i class="pe-7s-upload pe-rotate-90"></i>',
                      'url' => ['/logout'],
                      'linkOptions' => ['data-method' => 'post'],
                       'visible'=> !Yii::$app->user->isGuest
                    ],
                ],
            ]);
            ?>
        </div>
    </nav>
</div>
