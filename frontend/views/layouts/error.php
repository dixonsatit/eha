<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginContent('@common/themes/homer/views/layouts/_body.php',[
  'page'=>'error'
]); ?>
<div class="color-line"></div>

<div class="error-container text-center">

    <?=$content?>
    <div class="row">
        <div class="col-md-12 text-center">
             Copyright  © <?=(date('Y')+543)?> <?=Yii::$app->params['app.footerLabel'] ?>
        </div>
    </div>
</div>

<br><br>

<?php $this->endContent(); ?>
