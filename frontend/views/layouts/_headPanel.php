<?php

use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DomainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="normalheader transition animated fadeIn small-header">
    <div class="hpanel">
        <div class="panel-body">
            <!--<a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>-->

            <?=$this->render('//layouts/_breadcrumb');?>

            <h2 class="font-light m-b-xs">
                <?= Html::encode($this->title) ?>
            </h2>
            <?php if(isset($this->params['subtitle'])):?>
              <small><?= $this->params['subtitle']?></small>
            <?php endif;?>
        </div>
    </div>
</div>
