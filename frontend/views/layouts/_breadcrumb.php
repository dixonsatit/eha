<?php

use yii\helpers\Url;
use yii\helpers\Html;
use  yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DomainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<!-- Original div id="hbreadcrumb" class="pull-right"-->
<div id="breadcrumb">
<?= Breadcrumbs::widget([
    'options'=>[
      'class'=>'breadcrumb'
    ],
    'itemTemplate' => "<li>{link}</li>\n", // template for all links
	'homeLink' => [ 
		  'label' => Yii::t('yii', 'หน้าหลัก'),
		  'url' => Yii::$app->homeUrl,
	],
	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>
</div>
