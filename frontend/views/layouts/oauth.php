<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginContent('@common/themes/homer/views/layouts/_body.php',[
  'page' => 'login'
]); ?>
<div class="color-line"></div>
<!--
<div class="back-link">
    <a href="<?=Url::to(['/site/index'])?>" class="btn btn-primary">Back to Dashboard</a>
</div> -->


    <?=$content?>
    <div class="row">
        <div class="col-md-12 text-center">
              Copyright  © <?=(date('Y')+543)?> <?=Yii::$app->params['app.footerLabel'] ?>
        </div>
    </div>

<br><br>

<?php $this->endContent(); ?>
