<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginContent('@common/themes/homer/views/layouts/_body.php',[
  'page'=>'error'
]); ?>
<div class="color-line"></div>

<div class="error-container text-center">

  <div class="text-center m-b-md" style="margin-bottom:0;">
      <?=Html::img(Url::base(true).'/images/maintenaince.svg',[
        'style'=>'width:150px;'
      ])?>
  </div>

    <?=$content?>
    <div class="row">
        <div class="col-md-12 text-center">
            <?=date('Y')?> Copyright   <strong><?=Yii::$app->params['app.brandLabel'] ?></strong>
        </div>
    </div>
</div>

<br><br>

<?php $this->endContent(); ?>
