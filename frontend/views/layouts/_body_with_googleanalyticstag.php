<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\themes\homer\HomerAsset;
$bundle = HomerAsset::register($this);

$bodyOptions = (isset($page) && in_array($page,['login','error'])) ? ['class'=>'blank'] : [];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="shortcut icon" type="image/ico" href="<?=Url::base()?>/icons/Icon-Small.png" />
    <link rel="apple-touch-icon" href="<?=Url::base()?>/icons/Icon-60@2x.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?=Url::base()?>/icons/Icon-60@3x.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?=Url::base()?>/icons/Icon-76.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="<?=Url::base()?>/icons/Icon-76@2x.png" />
    <link rel="apple-touch-icon" sizes="58x58" href="<?=Url::base()?>/icons/Icon-Small@2x.png" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="DOH">
</head>

<?=Html::beginTag('body',$bodyOptions); ?>

    <?php $this->beginBody() ?>

    <?=$content;?>

    <?php
    // use kartik\social\GoogleAnalytics;
    
    // // If any parameters are not passed, the widget will use settings 
    // // from the social module wherever possible
    // echo GoogleAnalytics::widget();
    
    // // Basic widget usage
    // echo GoogleAnalytics::widget([
    //     // 'id' => 'TRACKING_ID', 
    //     // 'domain' => 'TRACKING_DOMAIN',
    //     'id' => 'UA-120155659-1', 
    //     'domain' => 'http://dashboard.anamai.moph.go.th',
    // ]);
    
    // // Advanced widget usage
    // echo GoogleAnalytics::widget([
    //     // 'id' => 'TRACKING_ID', 
    //     // 'domain' => 'TRACKING_DOMAIN',
    //     'id' => 'UA-120155659-1', 
    //     'domain' => 'http://dashboard.anamai.moph.go.th',
    //     'noscript' => 'Analytics cannot be run on this browser since Javascript is not enabled.'
    // ]);
    ?>

    <?php $this->endBody() ?>

<?=Html::endtag('body'); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120155659-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  // Enable pageview tracking
  gtag('config', 'UA-120155659-1');

  // Disable pageview tracking
  // The default behavior of this snippet is to send a pageview hit to Google Analytics. 
  // This is the desired behavior in most cases; pageviews are automatically tracked 
  // once you add the snippet to each page on your site. However, if you don’t want the snippet 
  // to send a pageview hit to Google Analytics, set the send_page_view parameter to false:
  // gtag('config', 'UA-120155659-1', { 'send_page_view': false });
</script>

<!-- cybercog/yii2-google-analytics - Yii2 Google Analytics Tracking -->
<script>
// https://github.com/cybercog/yii2-google-analytics

// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
// })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
// ga('create', 'UA-120155659-1', 'auto');
// ga('send', 'pageview');

// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
// 	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
// 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
// ga('create', 'UA-120155659-1', "auto");
// ga('send', 'pageview');
// ga('set', 'anonymizeIp', true);

</script>

</html>
<?php $this->endPage() ?>
