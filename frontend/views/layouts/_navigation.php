<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
  $menus  = [
    'app-sso'=>[
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-th-large"></i> Developers </span>', 'url' => ['/apps/apps/index'],'visible'=>Yii::$app->user->can('Developer')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-th-large"></i> Scopes </span>', 'url' => ['/apps/oauth-scope/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-th-large"></i> Jwt </span>', 'url' => ['/apps/oauth-jwt/index'],'visible'=>Yii::$app->user->can('Developer')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-th-large"></i> PublicKeys </span>', 'url' => ['/apps/oauth-public-keys/index'],'visible'=>Yii::$app->user->can('Developer')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list"></i> Users </span>', 'url' => ['/user/admin/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-user"></i> Account </span>', 'url' => ['/user/settings/profile']],
    ],
    'app-frontend'=>[
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-dashboard"></i> Dashboard</span>', 'url' => ['/site/index']],        
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัดระดับกระทรวง</span>', 'url' => ['/ministry']],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัดตรวจราชการ</span>', 'url' => ['/inspection']],          
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัด PA อธิบดี</span>', 'url' => ['/pa']],           
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัดระดับกรม</span>', 'url' => ['/department']],             
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัดเฝ้าระวังฯ</span>', 'url' => ['/surveillance']],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ตัวชี้วัด PMQA</span>', 'url' => ['/pmqa']],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> ข้อมูลสำคัญอื่นๆ</span>', 'url' => ['/important']],        
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list-alt"></i> รายการข้อมูลทั้งหมด</span>', 'url' => ['/kpilist'],'visible' => Yii::$app->user->can('KPI-DATA-USER')],
        //['label' => '<span class="nav-label"><i class="fa fa-medkit"></i> ระบบบันทีกข้อมูล</span>', 'url' => ['/kpis/data/default/index'],'visible' => Yii::$app->user->can('KPI-DATA-USER')],
        ['label' => '<span class="nav-label"><i class="fa fa-medkit"></i> ระบบบันทีกข้อมูล</span>', 'url' => ['/kpis/data/default/index'],'visible' => Yii::$app->user->can('User')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-edit"></i> ระบบวิเคราะห์สถานการณ์ข้อมูลสุขภาพ</span>', 'url' => ['/analysis/data/index'],'visible' => !Yii::$app->user->isGuest],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-tasks"></i> HDC API</span>', 'url' => ['/kpis/hdc/kpi-hdc/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-stats"></i> Specical Report</span>', 'url' => ['/specialreport/index'],'visible'=> !Yii::$app->user->isGuest],
        //['label' => '<span class="nav-label"><i class="glyphicon glyphicon-stats"></i> Self-Assetment Report</span>', 'url' => ['/sar/sar-report/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-cog"></i> ตั้งค่าข้อมูล</span>', 'url' => ['/kpis/default/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-cog"></i> นำเข้า excel </span>', 'url' => ['/xls/xls-import/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-list"></i> บัญชีผู้ใช้งาน </span>', 'url' => ['/user/admin/index'],'visible'=>Yii::$app->user->can('Admin')],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-user"></i>  ข้อมูลส่วนตัว </span>', 'url' => ['/user/settings/profile'],'visible'=> !Yii::$app->user->isGuest],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-user"></i>  ผู้ประสานงานตัวชี้วัด </span>', 'url' => ['/datacoordinator'],'visible'=> Yii::$app->user->isGuest],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-user"></i>  ทีมพัฒนาระบบ </span>', 'url' => ['/team'],'visible'=> Yii::$app->user->isGuest],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-log-in"></i>  เข้าสู่ระบบ </span>', 'url' => ['/login'],'visible'=> Yii::$app->user->isGuest],
        ['label' => '<span class="nav-label"><i class="glyphicon glyphicon-log-in"></i>  ออกจากระบบ </span>', 'linkOptions' => ['data-method' => 'post'], 'url' => ['/logout'],'visible'=> !Yii::$app->user->isGuest],
    ],
    'app-backend'=>[
        ['label' => '<span class="nav-label">Dashboard </span>', 'url' => ['/dashboard/index']],
        ['label' => '<span class="nav-label">Users</span>', 'url' => ['/user/admin/index']]
    ]
  ];
 ?>

<style>
.img-and-label-wrapper {
  display : inline-block;
}

.img-and-label-wrapper label{
  display: block;
  text-align: center;
}
</style>

<aside id="menu">
    <div id="navigation">
        <div class="profile-picture">

            <?php
            // Dune 2018-09-19
            $file_webpath = Yii::getAlias('@web').'/photos'; 
            $file_realpath = Yii::getAlias('@frontend').'/photos'; 

            // Html::img(Yii::$app->user->identity->profile->getAvatarUrl(150));

            if(isset(Yii::$app->user->identity->profile->gravatar_email)){
                $email = Yii::$app->user->identity->profile->gravatar_email;
            } else {
                $email = Yii::$app->user->isGuest ? null : Yii::$app->user->identity->email;
            }
            //
            if(trim(Yii::$app->user->identity->profile->photo)!="" && is_file($file_realpath."/".Yii::$app->user->identity->profile->photo) && file_exists($file_realpath."/".Yii::$app->user->identity->profile->photo)){
                $photo = $file_webpath."/".Yii::$app->user->identity->profile->photo;
            } else {
                //$photo = Yii::$app->user->isGuest ? $file_webpath."/"."noimage.png" : $file_webpath."/".Yii::$app->user->identity->photo;
                $photo = Yii::$app->user->isGuest ? $file_webpath."/"."noimage.png" : $file_webpath."/".Yii::$app->user->identity->photo;
            }
            ?>

            <?php 
            //Html::a(Yii::$app->gravatar->Avatar($email,[
            //'s'=>'75',
            //'imgOptions'=>[
            //  'class'=>'img-circle m-b',
            //  'alt'=>'Profile'  
            //]
            //]),['/user/settings/profile'])
            ?>

            <?=Html::a(Html::img($photo, [
                    'alt' => 'Profile', 
                    'width' => '85', 'height' => '85', 
                    'data-toggle' => 'tooltip', 
                    'data-placement' => 'left', 
                    'title' => Yii::$app->user->identity->username, 
                    'style' => 'cursor:default; display: block; margin-left: auto; margin-right: auto;',
                    'class'=>'img-circle m-b img-responsive CattoBorderRadius'
                ]),
                ['/user/settings/profile'])
              ;
            ?>
            <div class="stats-label text-color">
                <span class="font-extra-bold font-uppercase"><?=isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username: null;?></span>
            </div>
        </div>

        <?=Nav::widget([
            'id'=>'side-menu',
            'encodeLabels'=>false,
            'items' =>  $menus[Yii::$app->id]
        ]);
        ?>
    </div>
</aside>
