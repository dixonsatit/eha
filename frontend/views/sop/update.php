<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sop */

$this->title = Yii::t('common', 'Update Sop: ' . $model->be_year, [
    'nameAttribute' => '' . $model->be_year,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Sops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->be_year, 'url' => ['view', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="sop-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
