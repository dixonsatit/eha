<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Sop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'component_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'be_year')->textInput() ?>

    <?= $form->field($model, 'issue_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assess_step')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assess_step_detail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assess_score')->textInput() ?>

    <?= $form->field($model, 'created_by_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_by_ip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
