<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sop */

$this->title = $model->be_year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Sops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_id',
            'component_code',
            'be_year',
            'issue_code',
            'assess_step',
            'assess_step_detail',
            'assess_score',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
