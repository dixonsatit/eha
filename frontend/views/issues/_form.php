<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Issues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="issues-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'issue_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'issue_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_issue_code')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
