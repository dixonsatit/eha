<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Issues */

$this->title = Yii::t('common', 'Update Issues: ' . $model->issue_code, [
    'nameAttribute' => '' . $model->issue_code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Issues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->issue_code, 'url' => ['view', 'id' => $model->issue_code]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="issues-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
