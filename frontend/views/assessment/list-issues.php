<?php

use backend\models\LpaScore;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการประเด็น ' . Yii::$app->session['applicationForm']['locality_name'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมิน EHA'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ประเมินตนเอง', 'url' => ['application-form/list-application-form']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="application-form-index">

            <?= GridView::widget([
                'summary' => '',
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'issue_code',
                        'label' => 'รหัส'
                    ],
                    [
                        'attribute' => 'issue_desc',
                        'contentOptions' => ['style' => 'width: 100px;'],

                    ],
                    [
                        'header' => 'องค์ประกอบ <br> 1-5',
                        'format' => 'raw',
                        'value' => function ($data) use ($applicationForm){
                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            // $score = LpaScore::find()->where(['be_year' => $applicationForm->be_year - 1])->andWhere(['locality_code' => $applicationForm->locality_code])->one();
                            return $score !== false ? $score->component1to5_avgscore : 0;
                        },
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'header' => 'องค์ประกอบ<br> 6',
                        'value' => function ($data) {
                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $score !== false ? $score->percentB : 0;
                        },
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'header' => 'องค์ประกอบ<br> 7',
                        'value' => function ($data) {
                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $score !== false ? $score->percentC : 0;
                        },
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'label' => 'สถานะ',
                        'value' => function ($data) {
                            $result = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $result !== false ? $result->assessResult->result_desc : '';
                        },
                        'headerOptions' => ['style' => 'width: 150px; text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],

                    ],

                    [
                        'label' => 'ประเมิน',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'value' => function ($data) use ($application_form_id) {
                            $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->andWhere(['locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();
                            if ($assessmentConclusion == null) {
                                return Html::a('<i class="icon-note"></i> ประเมินตามประเด็น', ['/assessment/create-by-issue', 'application_form_id' => $application_form_id, 'issue_code' => $data->issue_code], ['class' => 'btn btn-xs btn-block btn-primary btn-block', 'data-pjax' => 0,]);
                            }
                            if ($assessmentConclusion != null && $assessmentConclusion->approve == 'No') {
                                return Html::a('<i class="icon-note"></i> ประเมินตามประเด็น', ['/assessment/update-by-issue', 'application_form_id' => $application_form_id, 'issue_code' => $data->issue_code], ['class' => 'btn btn-xs btn-block btn-primary btn-block', 'data-pjax' => 0,]);
                            }
                            if ($assessmentConclusion != null && $assessmentConclusion->approve == 'Sent') {
                                return Html::button('<i class="icon-note"></i> ประเมินตามประเด็น', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                            }
                        },

                    ],

                    [
                        'label' => 'ส่งประเมิน',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'value' => function ($data) {
                            $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => Yii::$app->session['applicationForm']['be_year']])->andWhere(['type' => 'selfassess'])->andWhere(['type' => 'selfassess'])->andWhere(['locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();
                            if ($assessmentConclusion != null && $assessmentConclusion->approve == 'No') {
                                return Html::a('<i class="icon-loop"></i> ส่งรับการประเมิน', ['/assessment/send-assessment', 'issue_code' => $data->issue_code], [
                                    'class' => 'btn btn-xs btn-block btn-warning btn-block',
                                    'data' => [
                                        'confirm' => 'การส่งประเมินจะไม่สามารถแก้ไขการประเมินได้ คุณต้องการส่ง ใช่ หรือ ไม่',
                                    ],
                                ]);
                            }
                            if ($assessmentConclusion != null && $assessmentConclusion->approve == 'Sent') {
                                return Html::button('<i class="icon-loop"></i> ส่งรับกรประเมินแล้ว', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                            } else {
                                return Html::button('<i class="icon-loop"></i> ยังไม่ประเมินตนเอง', ['class' => 'btn btn-xs btn-block btn-default btn-block', 'disabled' => true]);
                            }
                        },

                    ],

                    //                    [
                    //                        'label' => 'รายละเอียด',
                    //                        'format' => 'html',
                    //                        'headerOptions' => ['style' => 'text-align: center;'],
                    //                        'value' => function ($data) {
                    //                            return Html::a('รายละเอียด', ['/assessment/view-by-issue', 'issue_code' => $data->issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']], ['class' => 'btn btn-xs btn-block btn-success btn-block']);
                    //                        }
                    //                    ],
                    [
                        'label' => 'พิมพ์',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'value' => function ($data) use ($applicationForm) {
                            $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['type' => 'assessor'])->andWhere(['locality_code' => Yii::$app->session['applicationForm']['locality_code']])->one();
                            if ($assessmentConclusion != null) {
                                return Html::a('<i class="icon-printer"></i> พิมพ์สรุปผลตรวจ', ['/documents/print-diploma-by-code', 'issue_code' => $data->issue_code], ['target' => '_blank', 'class' => 'btn btn-xs btn-block btn-success btn-block']);
                            } else {
                                return Html::button('<i class="icon-printer"></i> พิมพ์สรุปผลตรวจ', ['class' => 'btn btn-xs btn-default btn-block', 'disabled' => true]);
                            }
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>