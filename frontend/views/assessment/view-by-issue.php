<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'รายละเอียดการประเมิน') . ' ('. $applicationForm->locality->locality_name .')';
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['/application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['/assessment/list-issues', 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]];
$this->params['breadcrumbs'][] = $this->title;
foreach ($assessment as $index => $first) {
    if ($first['sop_id'] !== null) {
        break;
    }
}
?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>

            <div class="assessment-index">

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <h4 class="text-primary">
                    <?php echo $first['sop']['parent_id'] == null ? $first['sop']['issueCode']['issue_code'] . ": " . $first['sop']['issueCode']['issue_desc'] : ''; ?>
                </h4>
                <hr>
                <table class="table table-bordered table-hover" style="width: 100%;">
                    <thead>
                    <tr>
                        <td class="block">รหัส</td>
                        <!-- <td>ประเด็นย่อย</td> -->
                        <td>กระบวนการ</td>
                        <td>หลักฐาน</td>
                        <td>คะแนนเต็ม</td>
                        <td>คะแนนประเมินตนเอง</td>
                        <td style="width:130px; text-align: center;">คะแนนตรวจประเมิน</td>
                        <td style="text-align: center;">ไฟล์</td>
                    <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($assessment as $key => $item) : ?>
                        <?php if ($item['sop_id'] !== null): ?>
                        
                             <tr style="<?php echo $item['sop']['parent_id'] ? 0 : 'background-color:#f1f0f0;'?>">

                                <td class="block">
                                    <?php echo $item['sop']['id']; //echo $item['sop']['parent_id'] == null ? $item['sop']['issueCode']['mainIssueCode']['id'] . ": " . $item['sop']['issueCode']['mainIssueCode']['main_issue_desc'] : ''; ?>
                                </td>
                                <!-- <td><?php //echo $item['sop']['parent_id'] == null ? $item['sop']['issueCode']['issue_code'] . ": " . $item['sop']['issueCode']['issue_desc'] : ''; ?></td> -->
                                <td colspan="<?php echo $item['sop']['parent_id'] ? 0 : 2?>">
                                    <?php echo $item['sop']['assess_step_detail']; ?>
                                </td>

                                 <?php if($item['sop']['parent_id']): ?>
                                <td>
                                    <?php echo $item['sop']['assess_step_evidence']; ?>
                                </td>
                                <?php endif; ?>

                                <td style="width: 80px; text-align: center;"> <?php echo !$item['sop']['parent_id'] ? '<span class="badge">'.$item['sop']['assess_score'].'</span>' : $item['sop']['assess_score'] ; ?></td>
                                <td style="width: 140px; text-align: center;"><?php echo $item['selfassess_score']; ?></td>
                                <td style="width: 130px; text-align: center;"><?php echo $item['assessor_score']; ?></td>
                                <td style="text-align: center;"><?php echo $item['sop']['parent_id'] != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลด' . '(' . \common\models\Assessment::getCountAttach($item['id']) . ')', ['assessment/download-file', 'id' => $item['id']], ['class' => 'btn-download btn btn-success btn-xs']) : ''; ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <?php $chkAssessment = \common\models\Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->where(['application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->andWhere(['sop.issue_code' => $first['sop']['issueCode']['issue_code']])->andWhere(['assessment.assess_status' => 2])->count(); ?>
                <hr>
                <?php if ($chkAssessment > 0) : ?>
                    <?php echo Html::button('ส่งขอรับการประเมินแล้ว', ['class' => 'btn btn-primary btn-md', 'disabled' => true]) ?>
                <?php endif; ?>
                <?php if ($chkAssessment == 0 || $chkAssessment == null) : ?>
                    <?php echo Html::a('<i class=" icon-pencil"></i> แก้ไขการประเมิน', ['assessment/update-by-issue', 'issue_code' => $first['sop']['issueCode']['issue_code']], ['class' => 'btn btn-primary btn-md']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

    <style type="text/css">
        .modal-content {
            margin-top: 200px;
            background-color: white;
        }
    </style>

<?php
$js = <<< JS

    $('.btn-download').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

JS;
$this->registerJS($js);
?>