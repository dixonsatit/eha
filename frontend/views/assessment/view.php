<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Assessment */

$this->title = $model->locality_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-view">

    <p>
        <?php echo Html::a(Yii::t('common', 'Update'), ['update', 'locality_code' => $model->locality_code, 'sop_id' => $model->sop_id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('common', 'Delete'), ['delete', 'locality_code' => $model->locality_code, 'sop_id' => $model->sop_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'locality_code',
            'sop_id',
            'selfassess_score',
            'selfassess_date',
            'assessor_score',
            'assessor_date',
            'assessor_notes',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
