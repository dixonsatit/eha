<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Assessment */

$this->title = Yii::t('common', '{modelClass}', [
    'modelClass' => 'การประเมินตนเอง/ประเมินด้วยผู้ประเมิน',
]);
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['application-form/app-form']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="assessment-create">

            <?php echo $this->render('_form', [
                'model' => $model,
            ]); ?>

        </div>
    </div>
</div>
