<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii2mod\comments\models\CommentModel;
use yii\bootstrap\Modal;


$this->title = Yii::t('common', 'ประเมินตามประเด็น');
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['/application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเด็น', 'url' => ['/assessment/list-issues', 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]];

$this->params['breadcrumbs'][] = $this->title;
foreach ($models as $first) {
    if ($first['sop_id'] !== null) {
        break;
    }
}
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>


        <div class="assessment-form">
            <?= \common\widgets\ApplicationFormDetail::widget(['be_year' => Yii::$app->session['applicationForm']['be_year']]); ?>

            <?php $form = ActiveForm::begin([
                'id' => 'app-form',
            ]); ?>
            <br>
            <h4 class="text-primary">
                <?php echo $first['sop_parent_id'] == null ? $first['issue_code'] . ": " . $first['issue_desc'] : ''; ?>
            </h4>
            <hr>
            <table class="table table-bordered table-hover" style="width: 100%;">
                <thead>
                    <tr>
                        <td>รหัส</td>
                        <!-- <td>ประเด็นย่อย</td> -->
                        <td>กระบวนการ</td>
                        <td>หลักฐาน</td>
                        <td style="width: 100px; text-align: center;">คะแนนเต็ม</td>
                        <td style="width: 150px; text-align: center">คะแนนประเมินตนเอง</td>
                        <td style="text-align: center">แนบไฟล์</td>
                        <td style="text-align: center;">ข้อเสนอแนะ</td>
                    <tr>
                </thead>
                <tbody>
                    <?php
                    $activeP = null;
                    foreach ($models as $key => $item) : ?>
                        <?php $currentP = substr($item['sopObj']->assess_step, 0, 3);

                        if ($currentP == $activeP) {
                        } else {
                            $activeP = $currentP;
                            if ($activeP == 'P60') {
                                $title = 'P1';
                            } else if ($activeP == 'P61') {
                                $title = 'P1';
                            } else if ($activeP == 'P62') {
                                $title = 'P2';
                            } else if ($activeP == 'P63') {
                                $title = 'P3';
                            } else if ($activeP == 'P70') {
                                $title = 'องค์ประกอบที่ 7';
                            }
                            echo ($activeP == 'P61' || $activeP == 'P60') ? '<tr><th style="background-color: #eafffc;" colspan="7">องค์ประกอบที่ 6</th></tr>' : '';
                            echo '<tr><th style="background-color: #eafffc;" colspan="7">' . $title . '</th></tr>';
                        }
                        ?>

                        <?php if ($item['sop_id'] !== null) : ?>
                            <tr style="<?php echo $item['sopObj']->parent_id ? 0 : 'background-color:#f1f0f0;' ?>">
                                <td>
                                    <?php echo $item['sop_id']; ?>
                                    <?php echo $form->field($item, "[$key]id")->hiddenInput(['readonly' => true, 'class' => 'txt_id'])->label(false); ?>
                                </td>


                                <td colspan="<?php echo $item['sopObj']->parent_id ? 0 : 2 ?>">
                                    <?php echo $item['sop_assess_step_detail']; ?>
                                    <?php echo $form->field($item, "[$key]sop_id")->hiddenInput(['readonly' => true, 'class' => 'txt_sop_id'])->label(false); ?>
                                </td>

                                <?php if ($item['sopObj']->parent_id) : ?>
                                    <td>
                                        <?php echo $item['assess_step_evidence']; ?>
                                    </td>
                                <?php endif; ?>
                                <td style="text-align: center;"> <?php echo !$item['sop']['parent_id'] ? '<span class="badge">' . $item['sop_assess_score'] . '</span>' : $item['sop_assess_score']; ?></td>
                                <td><?php echo $item['sop_parent_id'] != null ? $form->field($item, "[$key]selfassess_score")->textInput(['maxlength' => true, 'min' => 0, 'max' => $item['sop_assess_score'], 'class' => 'form-control input-sm txt_no', 'type' => 'number', 'readonly' => Yii::$app->session['applicationForm']['status'] >= 2 ? true : false])->label(false) : ''; ?></td>
                                <td>
                                    <?php if ($item['id'] != '') : ?>
                                        <?php echo $item['sop_parent_id'] != null ? Html::a('<i class="icon-doc"></i> แนบไฟล์' . '(' . \common\models\Assessment::getCountAttach($item['id']) . ')', ['assessment/upload-by-id', 'id' => $item['id']], ['class' => 'btn-upload btn btn-success btn-xs', 'dta-pjax' => 0]) : ''; ?>
                                    <?php endif; ?>
                                    <?php if ($item['id'] == '') : ?>
                                        <?php echo $item['sop_parent_id'] != null ? Html::a('<i class="icon-doc"></i> แนบไฟล์' . '(' . \common\models\Assessment::getCountAttach($item['id']) . ')', ['assessment/create'], ['class' => 'btn-upload btn btn-success btn-xs', 'dta-pjax' => 0]) : ''; ?>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php echo $item->sop->parent_id != null ? Html::a('<i class="icon-doc"></i> ข้อเสนอแนะ' . '(' . CommentModel::find()->where(['entityId' => $item['id']])->count() . ')', ['/assessment/comment', 'id' => $item['id']], ['class' => 'btn btn-primary btn-xs btn-comment', 'data-pjax' => '0']) : ''; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <br>
            <hr>

            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? '<i class="glyphicon glyphicon-save"></i> บันทึกข้อมูล' : '<i class="glyphicon glyphicon-save"></i> บันทึกข้อมูล', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary', 'disabled' => Yii::$app->session['applicationForm']['status'] >= 2 ? 'disabled' : false]) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h4 style="display:box;">แนบไฟล์</h4>',
    'id' => 'attachment-modal',
    'size' => 'modal-lg',
    'clientOptions' => [
        'backdrop' => true,
        'keyboard' => false,
    ]
]);

?>
<div class="clearfix"></div>
<?php
Modal::end();
?>

<?php
Modal::begin([
    'header' => 'ข้อเสนอแนะ',
    'id' => 'modal-comment',
    'size' => 'modal-lg',
]);
echo "<div id='modalComment'></div>";
Modal::end();
?>

<style type="text/css">
    td {
        border: 1px solid #000;
    }

    tr td:last-child {
        width: 1%;
        white-space: nowrap;
    }

    .modal-content {
        margin-top: 200px;
        background-color: white;
    }
</style>

<?php
$js = <<< JS

    $('#app-form').on(function (e) {
    if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('.txt_no').on('change', function(e){
        var max = parseInt($(this).attr("max"));
        var score = parseInt($(this).val());

        if(score > max){
            alert('จำนวนคะแนนต้องไม่มากกว่า คะแนน SOP');
            $(this).val(0);
        }
    });

    $('.btn-upload').click(function (){
        $('#attachment-modal').find('.modal-body').html("<h5 class='text-center'>Loading.....<h5>");
        $('#attachment-modal').modal('show');
        $.get($(this).attr('href'), function(data) {
            $('#attachment-modal').find('.modal-body').html(data);
        });
       return false;
    });

    $('.btn-comment').click(function(e){
        e.preventDefault();
        $.get($(this).attr('href'), function(data) {
          $('#modal-comment').modal('show').find('#modalComment').html(data)
       });
       return false;
    })

    //$(this).closest('form').trigger('submit');
JS;
$this->registerJS($js);
?>