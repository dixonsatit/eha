<?php

use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this       yii\web\View
 * @var $model      common\models\Article
 * @var $categories common\models\ArticleCategory[]
 */

?>
<h4 class="card-title"><?php echo 'เอกสารอ้างอิงการประเมิน'; ?></h4>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
]) ?>

<?php echo $form->field($model, 'id')->hiddenInput(['readonly' => true])->label(false); ?>

<?php echo '3001600101'?>

<?php
$sop = \common\models\Sop::find()->joinWith('issueCode')->where(['sop.id' => $model->sop_id])->one();
?>

<h4>ประเด็น <?php echo $sop != null ? $sop->assess_step_evidence : ''; ?></h4>

<br>

<?php echo $form->field($model, 'attachments')->widget(
    Upload::className(),
    [
        'url' => ['/file/storage/upload'],
        'sortable' => true,
        'maxFileSize' => 10000000, // 10 MiB
        'maxNumberOfFiles' => 10,
    ]);
?>

<?= \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'base_url',
            'label' => 'ชื่อไฟล์',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::a($data->name, $data->base_url . '/' . $data->path, ['target' => '_blank']);
            }
        ],
        [
            'attribute' => 'type',
            'label' => 'ประเภทไฟล์'
        ],
        [
            'attribute' => 'size',
            'label' => 'ขนาดไฟล์',
            'value' => function ($data) {
                return Yii::$app->formatter->asShortSize($data->size, 2);
            }
        ],
    ],
]); ?>

<br>

<hr>
<div class="form-group">
    <?php echo Html::submitButton(
        $model->isNewRecord ? Yii::t('backend', 'บันทึกข้อมูล') : Yii::t('backend', 'บันทึกข้อมูล'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    <?php echo Html::button('ปิดหน้านี้', ["data-dismiss"=>"modal", 'class' => 'btn btn-light']);?>
</div>

<?php ActiveForm::end() ?>
