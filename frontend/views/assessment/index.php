<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\AssessmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'ข้อมูลการประเมิน');
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการประเมิน', 'url' => ['application-form/app-form']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <div class="assessment-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                <thead>
                <tr>
                    <td class="block">ประเด็นหลัก</td>
                    <td>ประเด็นย่อย</td>
                    <td>ประเด็นงาน</td>
                    <td>คะแนนเต็ม</td>
                    <td>คะแนนประเมินตนเอง</td>
                    <td>คะแนนตรวจประเมิน</td>
                    <td>แนบไฟล์</td>
                <tr>
                </thead>
                <tbody>
                <?php foreach ($assessment AS $key => $item) : ?>
                    <?php if ($item['sop_id'] !== null): ?>
                        <tr>
                            <td class="block">
                                <?php echo $item['sop']['parent_id'] == null ? $item['sop']['issueCode']['mainIssueCode']['id'] . ": " . $item['sop']['issueCode']['mainIssueCode']['main_issue_desc'] : ''; ?>
                            </td>
                            <td><?php echo $item['sop']['parent_id'] == null ? $item['sop']['issueCode']['issue_code'] . ": " . $item['sop']['issueCode']['issue_desc'] : ''; ?></td>
                            <td>
                                <?php echo $item['sop']['assess_step_detail']; ?>
                            </td>
                            <td style="width: 80px; text-align: center;"><?php echo $item['sop']['assess_score']; ?></td>
                            <td style="width: 140px; text-align: center;"><?php echo $item['selfassess_score']; ?></td>
                            <td style="width: 130px; text-align: center;"><?php echo $item['assessor_score']; ?></td>
                            <td style="text-align: center;"><?php echo $item['sop']['parent_id'] != null ? Html::a('<i class="icon-doc"></i> ดาวน์โหลด'.'('.\common\models\Assessment::getCountAttach($item['id']).')', ['assessment/download-file', 'id' => $item['id']], ['class' => 'btn-download btn btn-success btn-xs']) : ''; ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div class="row"></div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => 'อัพโหลดไฟล์',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

<?php
$js = <<< JS

    $('.btn-download').click(function (){
          $.get($(this).attr('href'), function(data) {
          $('#modal').modal('show').find('#modalContent').html(data)
       });
       return false;
    });

JS;
$this->registerJS($js);
?>