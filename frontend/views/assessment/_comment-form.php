<?php

use common\models\Sop;
use yii\helpers\VarDumper;
?>

<?php $sop = Sop::find()->where(['id' => $model->sop_id])->one()?>
<h4>ข้อเสนอแนะ: <?php echo !empty($sop->assess_step_evidence) ?  $sop->assess_step_evidence : ''?></h4>
<?php echo \yii2mod\comments\widgets\Comment::widget([
    'model' => $model,
    'commentView' => '@frontend/views/comments/index',
    'dataProviderConfig' => [
        'pagination' => [
            'pageSize' => 10
        ],
    ]
]); ?>