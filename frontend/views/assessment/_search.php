<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\search\AssessmentSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="assessment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'locality_code') ?>

    <?php echo $form->field($model, 'sop_id') ?>

    <?php echo $form->field($model, 'selfassess_score') ?>

    <?php echo $form->field($model, 'selfassess_date') ?>

    <?php // echo $form->field($model, 'assessor_score') ?>

    <?php // echo $form->field($model, 'assessor_date') ?>

    <?php // echo $form->field($model, 'assessor_notes') ?>

    <?php // echo $form->field($model, 'created_by_ip') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_by_ip') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('common', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
