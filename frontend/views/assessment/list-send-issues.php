<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\LocalitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->session['applicationForm']['locality_name'];
$this->params['breadcrumbs'][] = ['label' => 'ประเมิน EHA', 'url' => ['application-form/index']];
$this->params['breadcrumbs'][] = ['label' => 'รายการส่งประเมิน', 'url' => ['/application-form/list-application-form', 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']]];

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="locality-index">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'issue_code',
                        'label' => 'รหัส'
                    ],
                    [
                        'attribute' => 'issue_desc'
                    ],
                    [
                        'label' => 'คะแนน A (%)',
                        'headerOptions' => ['style' => 'text-align: center;'],

                        'value' => function ($data) {

                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $score !== false ? $score->percentA : 0;
                        },
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'label' => 'คะแนน B (%)',
                        'headerOptions' => ['style' => 'text-align: center;'],

                        'value' => function ($data) {
                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $score !== false ? $score->percentB : 0;
                        },
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'label' => 'คะแนน C (%)',
                        'headerOptions' => ['style' => 'text-align: center;'],

                        'value' => function ($data) {
                            $score = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $score !== false ? $score->percentC : 0;
                        },
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'contentOptions' => ['style' => 'text-align: center;'],
                    ],
                    [
                        'label' => 'สถานะ',
                        'value' => function ($data) {
                            $result = \common\models\AssessmentConclusion::getScores($data->issue_code);
                            return $result !== false ? $result->assessResult->result_desc : '';
                        },
                        'headerOptions' => ['style' => 'text-align: center;'],
                    ],

                    [
                        'label' => 'ประเมิน',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'value' => function ($data) {
                            $chkAssessment = \common\models\Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->where(['application_form_id' => Yii::$app->session['applicationForm']['application_form_id']])->andWhere(['sop.issue_code' => $data->issue_code])->andWhere(['assessment.assess_status' => 2])->count();
                            if ($chkAssessment > 0) {
                                return Html::button('ส่งขอรับการประเมิน', ['class' => 'btn btn-xs btn-default btn-block', 'disabled' => true]);
                            } else {
                                return Html::a('ส่งขอรับการประเมิน', ['send-assessment', 'issue_code' => $data->issue_code], [
                                    'class' => 'btn btn-xs btn-block btn-primary btn-block',
                                    'data-confirm' => 'การส่งขอรับการประเมินจะไม่สามารถแก้ไขได้ จนกว่าจะได้รับอนุญาตให้แก้ไข คุณต้องการส่ง ใช่ หรือ ไม่',
                                    'data-pjax' => 0,
                                    'data-method' => 'post',
                                ]);
                            }
                        },

                    ],

                    [
                        'label' => 'รายละเอียด',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'text-align: center;'],
                        'value' => function ($data) {
                            return Html::a('รายละเอียด', ['view-by-issue', 'issue_code' => $data->issue_code, 'application_form_id' => Yii::$app->session['applicationForm']['application_form_id']], ['class' => 'btn btn-xs btn-block btn-success btn-block']);
                        }
                    ],
                    [
                        'label' => 'พิมพ์',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align_center;'],
                        'value' => function ($data) {
                            return Html::a('พิมพ์สรุปผลตรวจ', ['/documents/print-diploma-by-code', 'issue_code' => $data->issue_code], ['target' => '_blank', 'class' => 'btn btn-xs btn-block btn-info btn-block']);
                        }
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>