<?php

use backend\models\LpaScore;
use common\models\ApplicationForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForm */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="application-form-form">

    <?php $form = ActiveForm::begin([
        'encodeErrorSummary' => false,
    ]); ?>

    <!-- <div class="row">
        <div class="col-md-10"> -->
            <?= $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
        <!-- </div>
        <div class="col-md-2"> -->
            <?php //echo Html::a('<i class="icon-pencil"></i> แก้ไขประเด็น',  \yii\helpers\Url::to(['/application-form/update', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code]), ['class' => 'btn btn-warning  btn-block']); ?>
        <!-- </div>
    </div> -->

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'localityType')->textInput(['readonly' => true]); ?>
            <?= $form->field($model, 'be_year', ['template' => "{label}\n{input}\n{hint}"])->hiddenInput(['readonly' => true, 'id' => 'be_year'])->label(false) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'localityName')->textInput(['readonly' => true]); ?>
            <?= $form->field($model, 'locality_code', ['template' => "{label}\n{input}\n{hint}"])->hiddenInput(['readonly' => true, 'id' => 'locality_code'])->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'subdictrict')->textInput(['readonly' => true]); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'dictrict')->textInput(['readonly' => true]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'province')->textInput(['readonly' => true]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tel')->textInput(['readonly' => true]); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->field($model, 'be_year')->textInput(['readonly' => true])->label('LPA Score ปีงบประมาณ'); ?>
            <?php //echo $form->field($model, 'be_year')->dropDownList(ApplicationForm::createRangeYear(date('Y', strtotime('+1 year'))+543))->label('ปีงบประมาณ') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'apply_person')->textInput([]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'apply_title')->textInput([]); ?>
        </div>
    </div>
    <br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
            [
                'attribute' => 'mainIssueCode.main_issue_code',
                'group' => true,  // enable grouping
            ],
            [
                'attribute' => 'mainIssueCode.main_issue_desc',
                'group' => true,  // enable grouping
            ],
            [
                'class' => '\kartik\grid\CheckboxColumn',
                'header' => Html::checkBox('selection_all', false, [
                    'class' => 'select-on-check-all',
                    'label' => 'เลือกทั้งหมด',
                ]),
                'rowSelectedClass' => GridView::TYPE_INFO,
                'checkboxOptions' => function ($data, $key, $index, $column) use ($selection, $model) {
                    $assessmentConclusion = \common\models\AssessmentConclusion::find()->where(['issue_code' => $data->issue_code])->andWhere(['be_year' => $model->be_year])->andWhere(['type' => 'selfassess'])->andWhere(['approve' => 'Sent'])->andWhere(['locality_code' => Yii::$app->user->identity->userProfile->locality_code])->one();
                    $bool = $selection != null ? in_array($data->issue_code, $selection) : false;
                    return [
                        'value' => $data->issue_code,
                        'checked' => $bool,
                        // 'disabled' => $assessmentConclusion != null ? 'disabled' : false,
                    ];
                },

            ],
            [
                'attribute' => 'issue_desc',
                'value' => function ($data) {
                    return $data->issue_name . " " . $data->issue_desc;
                }
            ],
        ],
    ]); ?>

    <br>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'contact_title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'contact_mobile')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <p>
            <?php echo Html::submitButton('<i class="glyphicon glyphicon-save"></i> บันทึกข้อมูล', ['class' => 'btn btn-primary', $model->status <= 1 ? 'disabled' == false : 'disabled' => true]); ?>
            <?php echo $model->isNewRecord ? Html::button('<i class="glyphicon glyphicon-print"></i> พิมพ์ใบสมัคร', ['class' => 'btn btn-default', 'disabled' => true]) : Html::a('<i class="glyphicon glyphicon-print"></i> พิมพ์ใบสมัคร', ['/documents/print-appform', 'be_year' => $model->be_year, 'locality_code' => Yii::$app->user->identity->userProfile->locality_code], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
        </p>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<div id="url-get-unactive" data-ungetactive="<?php echo \yii\helpers\Url::to(['application-form/delete-assessment']) ?>"></div>


<?php
$js = <<< JS


    // $('.kv-row-checkbox').change(function() {
    //     if(this.checked) {
    //          $(this).prop("checked", true);
    //     }else{
    //         var issue_code = $(this).val();
    //         var be_year = $('#be_year').val();
    //         var locality_code = $('#locality_code').val();
    //         var url = $('#url-get-unactive').data('ungetactive');
    //         if (confirm('ผมประเมินของประเด็นนี้จะถูกลบทั้งหมด คุณต้องการลบข้อมูล ใช่ หรือ ไม่')) {
    //         $.ajax({
    //             url: url,
    //             type: 'POST',
    //             dataType: 'JSON',
    //             data: {issue_code: issue_code, be_year: be_year, locality_code: locality_code}
    //         }).done(function (data){
    //             $(this).prop("checked", false);
    //             });
    //         }
    //         else{
    //          $(this).prop("checked", true);
    //         }
    //     }
    // });

JS;
$this->registerJS($js);
?>