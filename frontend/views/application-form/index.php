<?php
/**
 * Created by PhpStorm.
 * User: pil2ate
 * Date: 8/11/18 AD
 * Time: 11:32
 */

$this->title = 'การประเมิน EHA';
//$this->params['breadcrumbs'][] = ['label' => '', 'url' => ['']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/user/sign-in/signup']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4>ขั้นตอนที่ 1</h4>

                            <h3 class="mb-0 font-weight-medium">ลงทะเบียนเข้าใช้งาน</h3>
                            <small class="text-gray">ดำเนินการแล้ว</small>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/user/default/index']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="icon-in-bg bg-success text-white rounded-circle">
                            <i class="icon-check font-weight-bold"></i>
                        </div>
                        <div class="ml-4">
                            <h4>ขั้นตอนที่ 2</h4>

                            <h3 class="mb-0 font-weight-medium">ระบุหน่วยงานและข้อมูลการติดต่อ</h3>
                            <small class="text-gray">ดำเนินการแล้ว</small>

                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

</div>


<div class="row">
    <!-- <div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php //echo \yii\helpers\Url::to(['/application-form/create']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <?php //if (isset($applicationForm->apply_issues) && $applicationForm->apply_issues != null): ?>
                            <div class="icon-in-bg bg-success text-white rounded-circle">
                                <i class="icon-check font-weight-bold"></i>
                            </div>
                        <?php //endif; ?>
                        <?php //if (empty($applicationForm->apply_issues)): ?>
                            <div class="icon-in-bg bg-warning text-white rounded-circle">
                                <i class="icon-close font-weight-bold"></i>
                            </div>
                        <?php //endif; ?>
                        <div class="ml-4">
                            <h4>ขั้นตอนที่ 3</h4>

                            <h3 class="mb-0 font-weight-medium">เลือกประเด็นขอรับการประเมิน</h3>
                            <?php //if (isset($applicationForm->apply_issues) && $applicationForm->apply_issues != null): ?>
                                <small class="text-gray"><?php //echo 'ดำเนินการแล้ว'; ?></small>
                            <?php //endif; ?>
                            <?php //if (empty($applicationForm->apply_issues)): ?>
                                <small class="text-gray"><?php //echo 'ยังไม่ดำเนินการ' ?></small>
                            <?php //endif; ?>

                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div> -->

    <div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/application-form/list-application-form']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <?php if (isset($applicationForm)): ?>
                            <div class="icon-in-bg bg-success text-white rounded-circle">
                                <i class="icon-check font-weight-bold"></i>
                            </div>
                        <?php endif; ?>
                        <?php if (!isset($applicationForm)): ?>
                            <div class="icon-in-bg bg-warning text-white rounded-circle">
                                <i class="icon-close font-weight-bold"></i>
                            </div>
                        <?php endif; ?>
                        <div class="ml-4">
                            <h4>ขั้นตอนที่ 3</h4>

                            <h3 class="mb-0 font-weight-medium">เลือกประเด็นขอรับการประเมิน</h3>
                            <?php if ($applicationForm != null): ?>
                                <small class="text-gray"><?php echo 'ดำเนินการแล้ว'; ?></small>
                            <?php endif; ?>
                            <?php if ($applicationForm == null): ?>
                                <small class="text-gray"><?php echo 'ยังไม่ดำเนินการ' ?></small>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-md-6 grid-margin">
        <div class="card">
            <a href="<?php echo \yii\helpers\Url::to(['/documents/list-issues']) ?>"
               style="text-decoration: none; color: #000000; disable">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <?php $ckeckConclusion = \common\models\AssessmentConclusion::find()->where(['locality_code' => Yii::$app->user->identity->userProfile->locality_code])->andWhere(['type' => 'assessor'])->all() ?>
                        <?php if (isset($ckeckConclusion) && $ckeckConclusion != null): ?>
                            <div class="icon-in-bg bg-success text-white rounded-circle">
                                <i class="icon-check font-weight-bold"></i>
                            </div>
                        <?php endif; ?>
                        <?php if ($ckeckConclusion == null): ?>
                            <div class="icon-in-bg bg-warning text-white rounded-circle">
                                <i class="icon-close font-weight-bold"></i>
                            </div>
                        <?php endif; ?>
                        <div class="ml-4">
                            <h4>ขั้นตอนที่ 4</h4>

                            <h3 class="mb-0 font-weight-medium">สรุปผลการประเมิน (จากคณะกรรมการ)</h3>
                            <?php if (isset($ckeckConclusion) && $ckeckConclusion != null): ?>
                                <small class="text-gray"><?php echo 'ดำเนินการแล้ว'; ?></small>
                            <?php endif; ?>
                            <?php if ($ckeckConclusion == null): ?>
                                <small class="text-gray"><?php echo 'ยังไม่ดำเนินการ' ?></small>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

</div>








