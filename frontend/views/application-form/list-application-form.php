<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ApplicationFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'ประเมินตนเอง');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมิน EHA'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-form-index">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>

            <?= \common\widgets\ApplicationFormDetail::widget(); ?>

            <div class="row">
                <div class="col-md-12">
                    <p><?php echo Html::a('<i class="icon-plus"></i> สมัครเพิ่มประเด็น',  \yii\helpers\Url::to(['/application-form/create']), ['class' => 'btn btn-primary btn-md pull-right']) ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'pjax' => false,
                        'columns' => [
                            //                    [
                            //                        'class' => 'kartik\grid\ExpandRowColumn',
                            //                        'width' => '50px',
                            //                        'value' => function ($model, $key, $index, $column) {
                            //                            return GridView::ROW_EXPANDED;
                            //                        },
                            //                        'detail' => function ($data, $key, $index, $column) {
                            //
                            //                            $apply_issues = \yii\helpers\Json::decode($data->apply_issues);
                            //                            $apply_issues = is_array($apply_issues) ? $apply_issues : [];
                            //                            $query = \common\models\Issues::find()->where(['IN', 'issues.issue_code', $apply_issues]);
                            //
                            //                            $dataProvider = new \yii\data\ActiveDataProvider([
                            //                                'query' => $query,
                            //                                'pagination' => [
                            //                                    'pageSize' => 20,
                            //
                            //                                ],
                            //                            ]);
                            //                            return Yii::$app->controller->renderPartial('/assessment/list-issues', [
                            //                                'dataProvider' => $dataProvider,
                            //                                'application_form_id' => $data->id
                            //                            ]);
                            //                        },
                            //                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                            //                        'expandOneOnly' => true,
                            //                    ],
                            [
                                'attribute' => 'be_year',
                                'label' => 'ปีงบประมาณ',
                            ],
                            [
                                'label' => 'ชื่อ อปท.',
                                'value' => function ($data) {
                                    return '[' . $data->locality_code . ' ] ' . $data->locality->locality_name;
                                },
                            ],
                            [
                                // 'label' => 'รายการประเด็น',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return Html::a('<i class="icon-list"></i> ประเมินตนเองรายการประเด็น', ['/assessment/list-issues', 'application_form_id' => $data->id], ['class' => 'btn btn-block btn-xs btn-primary']);
                                }
                            ],

                            [
                                // 'label' => 'แก้ไข',
                                'format' => 'raw',
                                'options' => ['style' => 'width:140px;'],
                                'value' => function ($data) {
                                    return Html::a('<i class="icon-pencil"></i> แก้ไขประเด็น',  \yii\helpers\Url::to(['/application-form/update', 'be_year' => $data->be_year, 'locality_code' => $data->locality_code]), ['class' => 'btn btn-warning btn-xs btn-block']);
                                }
                            ],

                            [
                                // 'label' => 'พิมพ์ใบสรุป',
                                'format' => 'raw',
                                'options' => ['style' => 'width:150px;'],
                                'value' => function ($data) {
                                    return Html::a('<i class="icon-printer"></i> พิมพ์ใบสรุป', ['/documents/print-assessment', 'application_form_id' => $data->id], ['class' => 'btn btn-xs btn-block btn-success btn-block', 'target' => '_blank']);
                                }
                            ],
                            //                    [
                            //                        'label' => 'คะแนนประเมินตนเอง',
                            //                        'value' => function($data){
                            //                            return '0';
                            //                        }
                            //                    ],
                            //                    [
                            //                        'label' => 'คะแนนประเมินจากกรรมการ',
                            //                        'value' => function($data){
                            //                            return '0';
                            //                        }
                            //                    ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>