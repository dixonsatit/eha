<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForm */

$this->title = 'เลขที่ใบสมัคร ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Application Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="application-form-create">

            <div class="application-form-view">

                <?php if ($model->status == 0): ?>

                    <p>
                        <?= Html::a(Yii::t('common', 'Update'), ['update', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code], ['class' => 'btn btn-primary']) ?>
                        <?php
//                        echo Html::a(Yii::t('common', 'Delete'), ['delete', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code], [
//                            'class' => 'btn btn-danger',
//                            'data' => [
//                                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
//                                'method' => 'post',
//                            ],
//                        ])
                        ?>
                    </p>

                <?php endif; ?>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'be_year',
                        'locality_code',
                        'locality.locality_name',
                        'contact_person',
                        'contact_title',
                        'contact_phone',
                        'contact_mobile',
                        'contact_email:email',
                        'apply_person',
                        'apply_title',
//                        'apply_issues:ntext',
//                        'issueCodes.issue_desc',
//                        [
//                            'attribute' => 'issueCodes',
//                            'label' => 'ประเด็นย่อยที่สมัครขอรับการประเมิน',
//                            'format' => 'raw',
//                            'value' => \common\widgets\EhaSelectedList::widget([
//                                'models' => $model->issueCodes,
//                                'viewRoute' => '/issues/view',
//                            ]),
//                        ],
                        [
                            'label' => 'ประเด็นที่สมัคร',
                            'format' => 'html',
                            'value' => function ($data) {
                                $arrDatas = \yii\helpers\Json::decode($data->apply_issues);
                                $issueText = '';
                                foreach ($arrDatas as $arrData) {
                                    $issue = \common\models\Issues::find()->where(['issue_code' => $arrData])->one();
                                    $issueText .= '<li>' . $issue->issue_desc . "</li>";
                                }
                                return '<ul>' . $issueText . '</ul>';
                            }
                        ],
                        'apply_date',
                        [
                            'label' => 'สถานะ',
                            'value' => function($data){
                                return isset($data->assessmentResult->result_desc) ? $data->assessmentResult->result_desc : 'แก้ไขประเด็นได้';
                            }
                        ],
                        'created_by_ip',
                        [
                            'label' => 'บันทึกโดย',
                            'value' => function ($data) {
                                return $data->author->username;
                            }
                        ],
                        'created_at',
                        'updated_by_ip',
                        [
                            'label' => 'แก้ไขโดย',
                            'value' => function ($data) {
                                return $data->updater->username;
                            }
                        ],
                        'updated_at',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>
