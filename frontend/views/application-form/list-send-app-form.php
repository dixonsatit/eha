<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\search\ApplicationFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'รายการประเมิน');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมิน EHA'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <?= \common\widgets\ApplicationFormDetail::widget(); ?>


        <p class="card-description">

        </p>

        <div class="application-form-index">
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,

                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'width' => '50px',
                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_EXPANDED;
                        },
                        'detail' => function ($data, $key, $index, $column) {
                            //return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
                            if ($data->status == 1) {
                                $assessment = \common\models\Assessment::find()->where(['application_form_id' => $data->id])->one();
                                return Html::a('ส่งขอรับการประเมิน', ['assessment/list-send-issues', 'application_form_id' => $data->id], [
                                    'class' => 'btn btn-primary btn-fw btn-xs',
                                    'data-pjax' => '0',

                                ]);
//                                . ' ' . Html::a('ส่งขอรับการประเมิน', ['application-form/submit-app-form', 'id' => $data->id], [
//                                    'data-confirm' => Yii::t('backend', 'หลังการส่งขอรับการประเมินจะไม่สามารถแก้ไขประเด็นฯได้ <br> ต้องการยืนยันการส่งขอรับการประเมินหรือไม่?'),
//                                    'class' => 'btn btn-warning btn-fw btn-sx',
//                                    'data-pjax' => '0',
//                                ]) . " " . Html::a('เรียกดู', ['assessment/index', 'be_year' => $data->be_year, 'application_form_id' => $data->id], [
//                                    'class' => 'btn btn-success btn-fw btn-sx',
//                                    'data-pjax' => '0',
//                                ])
//                                . ' ' .
//                                Html::a('สรุปผลตรวจ', ['documents/print-diploma'], ['class' => 'btn btn-success btn-fw btn-sm', 'target' => '_blank']);

//                                . ' ' .
//                                Html::a('<i class="fa fa-pencil"></i> แก้ไข', ['update', 'be_year' => $data->be_year, 'locality_code' => Yii::$app->user->identity->userProfile->locality_code, 'application_form_id' => $data->id], [
//                                    'data-id' => $data->id,
//                                    'class' => 'btn btn-success btn-fw  btn-sx pull-right',
//                                    'data-pjax' => '0',
//
//                                ])
//                                . ' ' .
//                                Html::a('<i class="fa fa-pencil"></i> ลบ', $data->status == 1 ? ['delete', 'be_year' => $data->be_year, 'locality_code' => Yii::$app->user->identity->userProfile->locality_code] : ['#'], [
//                                    'data-method' => 'post',
//                                    'data-confirm' => $data->status == 1 ? Yii::t('backend', 'คุณแน่ใจหรือไม่ที่จะลบรายการนี้?') : false,
//                                    'class' => 'btn btn-danger btn-fw  btn-sx pull-right',
//                                    'disabled' => $data->status == 1 ? false : true,
//                                    'data-pjax' => '0',
//                                ]);
                            }
                        },
                        'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => true,
                    ],
                    'be_year',
                    'locality_code',
                    [
                        'label' => 'อปท.',
                        'value' => function ($data) {
                            return $data->locality->locality_name;
                        },
                    ],
//                    [
//                        'label' => 'คะแนนประเมินตนเอง',
//                        'value' => function($data){
//                            return '0';
//                        }
//                    ],
//                    [
//                        'label' => 'คะแนนประเมินจากกรรมการ',
//                        'value' => function($data){
//                            return '0';
//                        }
//                    ],
                    [
                        'label' => 'สถานะ',
                        'value' => function($data){
                            return $data->assessmentResult->result_desc;
                        }
                    ]

                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

