<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForm */
$this->title = 'เลือกประเด็นขอรับการประเมิน';
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมิน EHA'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('common', 'เลือกประด็นขอรับการประเมิน');
?>
<div class="card">
    <div class="card-body">
        <h4 class="card-title">
        <?php echo $this->title; ?>
        <span>การพัฒนาคุณภาพระบบบริการอนามัยสิ่งแฝวดล้อม องค์การปกครองส่วนท้องถิ่น<span>
        </h4>

        <p class="card-description">

        </p>

        <br>

        <div class="application-form-create">

            <?= $this->render('_form', [
                'model' => $model,
                'localInfo' => $localInfo,
                'dataProvider' => $dataProvider,
                'selection' => !empty($selection) ? $selection : [],
            ]) ?>

        </div>
    </div>
</div>
