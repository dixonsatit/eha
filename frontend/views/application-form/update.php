<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApplicationForm */

$this->title = Yii::t('common', 'แก้ไขประเด็นขอรับการประเมิน ' . $model->be_year, [
    'nameAttribute' => '' . $model->be_year,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'ประเมิน EHA'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-body">
        <h4 class="card-title"><?php echo $this->title; ?></h4>

        <p class="card-description">

        </p>

        <div class="application-form-update">

            <?= $this->render('_update', [
                'model' => $model,
                'localInfo' => $localInfo,
                'dataProvider' => $dataProvider,
                'selection' => $selection,

            ]) ?>

        </div>

    </div>
</div>
