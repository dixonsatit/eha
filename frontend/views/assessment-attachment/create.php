<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentAttachment */

$this->title = Yii::t('common', 'เพิ่มไฟล์');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Assessment Attachments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-attachment-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
        'assessment' => $assessment
    ]) ?>

</div>
