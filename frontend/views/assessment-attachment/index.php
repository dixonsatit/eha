<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('common', 'Assessment Attachments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assessment-attachment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Create Assessment Attachment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'assessment_id',
            'path',
            'base_url:url',
            'type',
            //'size',
            //'name',
            //'created_at',
            //'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
