<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssessmentAttachment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="assessment-attachment-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-12">

            <?php //echo Html::textInput('txt_sop_name', '[' . $sop->issue_code . "] " . $sop->assess_step_detail, ['class' => 'form-control', 'readonly' => true]); ?>

        </div>
    </div>

    <div class="col-md-12">
        <br>

        <?php echo $form->field($model, 'files')->widget(
            '\trntv\filekit\widget\Upload',
            [
                'url' => ['upload'],
                'sortable' => true,
                'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
                'maxNumberOfFiles' => 5,
                'clientOptions' => [],
            ]
        );?>
    </div>

    <div class="col-md-12">

        <div class="form-group">
            <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
