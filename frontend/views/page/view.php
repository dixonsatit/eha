<?php

/**
 * @var $this \yii\web\View
 * @var $model \common\models\Page
 */
$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="card">
        <div class="card-body">
            <?php echo $model->body ?>
        </div>
    </div>
</div>