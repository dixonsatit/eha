<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?> 
<h1>สร้าง Qrcode</h1>
<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'qrcode-form']); ?>
            <?php echo $form->field($model, 'data')->textArea(['rows' => 6]) ?>
            <div class="form-group">
                <?php echo Html::submitButton('สร้าง', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

