<?php
use yii\helpers\Html;
?>
<h1>สร้าง Qrcode</h1>
<br>

<div class="row">
	<div class="col-md-4">
	<div class="card">
	<div class="card-body text-center ">
		<?php echo Html::a(Html::img($data, ['img img-thumbnail']), ['qr', 'data'=>$model->data]); ?>
		<?php echo Html::a('Download', ['qr', 'data'=>$model->data]); ?>
	</div>
	</div>
	</div>
</div>



