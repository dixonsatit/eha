<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MainIssues */

$this->title = Yii::t('common', 'Create Main Issues');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Main Issues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-issues-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
