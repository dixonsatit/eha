<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MainIssues */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-issues-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'main_issue_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_issue_desc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
