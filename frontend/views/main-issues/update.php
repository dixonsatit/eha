<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainIssues */

$this->title = Yii::t('common', 'Update Main Issues: ' . $model->main_issue_code, [
    'nameAttribute' => '' . $model->main_issue_code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Main Issues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->main_issue_code, 'url' => ['view', 'id' => $model->main_issue_code]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="main-issues-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
