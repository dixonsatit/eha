<?php

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js');
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css');
$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.Default.css');

$makers = new yii\web\JsExpression(json_encode($makers));
$js = <<<JS
    var data = $makers;
    //Token
    var mapboxAccessToken = "pk.eyJ1Ijoicml0dGhpbmV0IiwiYSI6ImNpenNscmEwMjAzbDUzMm80MjdkcGVwYWcifQ.qA2XQ8qf9KnNlEuFIDqshA";

    var markerCollection = L.markerClusterGroup();

    //เซ็ตตำแหน่งแผนที่(ประเทศไทย)
    var map = L.map('map',{
        
    }).setView([13.736717, 100.523186], 5.5);


    //กำหนดขนาดตัวอักษร
    map.createPane('labels');
    map.getPane('labels').style.zIndex = 650;
    map.getPane('labels').style.pointerEvents = 'none';

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png?access_token='+ mapboxAccessToken, {
		maxZoom: 17,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
	    }).addTo(map);

        var LeafIcon = L.Icon.extend({
            options: {
                iconSize:     [30, 30]
            }
        });

       var redIcon = new LeafIcon({iconUrl: 'http://yii2-starter-kit.localhost/images/01.png'});
       var orangeIcon = new LeafIcon({iconUrl: 'http://yii2-starter-kit.localhost/images/02.png'});
       var greenIcon  = new LeafIcon({iconUrl: 'http://yii2-starter-kit.localhost/images/03.png'});

    //    var marker1 = L.marker([13.98893001, 100.57293266],{icon: greenIcon}).addTo(map).bindPopup("I am a green leaf.");
    //    var marker2 = L.marker([13.557631, 100.592536],{icon: redIcon}).addTo(map).bindPopup("I am a green leaf.");
    //    var marker3 = L.marker([13.513991, 100.683587],{icon: orangeIcon}).addTo(map).bindPopup("I am a green leaf.");


    data.forEach(function(m) {
        if(+m.lat &&  m.lon) {
                var icon = redIcon;
                if(m.assess_result == 5) {
                    icon = orangeIcon;
                }
                else if(m.assess_result == 6) {
                    icon = greenIcon;
                }
                else {
                    icon = redIcon;
                }
                const marker = L.marker([+m.lat,+m.lon], {
                    title: m.areacode,
                    icon: icon    
                }
            ).bindPopup(m.locality_name+': '+m.result);
            markerCollection.addLayer(marker);
        }
    });

   map.addLayer(markerCollection);

    // ข้อมูลแผนที่ ตำแหน่ง
    var mygeo =
    [{
            "type": "FeatureCollection",
            "features": $geo
    }];

    //ระบายสีลงบนแผนที่
   var geo_json = L.geoJSON(mygeo, {
       onEachFeature: onEachFeature
   }).addTo(map);


    geo_json.eachLayer(function (layer) {
        //layer.bindPopup(layer.feature.properties.name);
    });

    function pointIcon(icon){
        var pointIcon = L.icon({
            iconUrl:icon,
            iconSize: [32, 37], 
        });
        return pointIcon;
    }

   //Click option
    function onEachFeature(feature, layer) {
        console.log(feature);
        // layer.setIcon(pointIcon('http://yii2-starter-kit.localhost/images/01.png'));
        // layer.on({
        //     mouseover: highlightFeature,
        //     mouseout: resetHighlight,
        //     click: zoomToFeature
        // });
    }

    //click ซูม
    function zoomToFeature(e) {
        // alert(e.target.feature.properties.value)
        // map.fitBounds(e.target.getBounds());
        // $mapType!=='tb'?window.open($mapUrl+'&'+$mapType+'='+e.target.feature.properties.geocode,'_self'):null;
    }

    function highlightFeature(e) {
        var layer = e.target;
        info.update(layer.feature.properties);
        layer.setStyle({
            color: '#848484'
            });
        if (!L.Browser.ie && !L.Browser.opera) {
            layer.bringToFront();
        }
    }

    function resetHighlight(e) {
        info.update();
        geo_json.resetStyle(e.target);
    }

    var legend = L.control({position: 'bottomright'});

	legend.addTo(map);

    map.fitBounds(geo_json.getBounds());


JS;
$this->registerJs($js);
?>

<div style="width:100%; height:650px;" id="map"></div>

<style>
.info {
    padding: 6px 8px;
    font: 14px/16px Arial, Helvetica, sans-serif;
    background: white;
    background: rgba(255,255,255,0.8);
    box-shadow: 0 0 15px rgba(0,0,0,0.2);
    border-radius: 5px;
}
.info h4 {
    margin: 0 0 5px;
    color: #777;
}
.legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }</style>

</style>
