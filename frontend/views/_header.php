<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DomainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
# add fancybox
echo newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => false,
    'mouse' => true,
    'config' => [
//        'maxWidth' => '90%',
//        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
//        'width' => '70%',
//        'height' => '70%',
        'autoSize' => true,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => true,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]);
?>

<!-- Original div class="normalheader transition animated fadeIn small-header"-->
<div class="normalheader transition animated fadeIn small-header">
    <div class="hpanel">
        <div class="panel-body">
            <!--<a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>-->

            <h4 class="font-light m-b-xs">
                <?= Html::encode($this->title) ?>
            </h4>
            <?php if (isset($this->params['subtitle'])): ?>
                <small><?= $this->params['subtitle'] ?></small>
            <?php endif; ?>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10">
        <div class="normalheader small-header custom-breadcrumb">

            <?= $this->render('//layouts/_breadcrumb'); ?>

        </div>        
    </div>
    <?PHP if (isset($templatefile) && $templatefile) { ?>
        <div class="col-md-2">
            <div class="text-center"><?PHP echo Html::a("Template", '@web/kpitemplate/'. $templatefile, ['rel' => 'fancybox',"class"=>"btn btn-primary",'target' => '_blank']); ?></div>
        </div>
    <?PHP } ?>

</div>

