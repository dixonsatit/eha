<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Locality */

$this->title = Yii::t('common', 'Update Locality: {name}', [
    'name' => $model->locality_code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Localities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->locality_code, 'url' => ['view', 'id' => $model->locality_code]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="locality-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
