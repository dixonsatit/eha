<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Locality */

$this->title = $model->locality_code;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Localities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="locality-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->locality_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->locality_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'locality_code',
            'locality_name',
            'locality_shortname',
            'administrator',
            'administrative_title',
            'type_code',
            'address',
            'subdistrict_code',
            'district_code',
            'province_code',
            'postal_code',
            'active_status',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
