<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel frontend\models\search\ArticleSearch */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

//$this->title = Yii::t('frontend', 'หน้าหลัก');
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="article-index">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <?php echo \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'pager' => [
                        'hideOnSinglePage' => true,
                    ],
                    'itemView' => '_item'
                ])?>
            </div>
        </div>
    </div>
</div>
