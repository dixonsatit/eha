<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

?>

<?php $this->beginContent('@flareui/views/layouts/base.php') ?>

    <div class="container-scroller">

        <?= $this->render('_header.php', []) ?>

        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel">

                <div class="content-wrapper">
                    <?=
                    Breadcrumbs::widget([
                        'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n", // template for all links
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'tag' => 'ol', // container tag
                        'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                        'options' => [
                            'class' => 'breadcrumb breadcrumb-custom'
                        ]
                    ])
                    ?>
                    <?php if (Yii::$app->session->hasFlash('alert')): ?>
                            <?php echo \yii\bootstrap\Alert::widget([
                                'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                                'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                            ])?>
                    <?php endif; ?>
                    <?php echo $content ?>
                </div>

                <?= $this->render('_footer.php', []) ?>

            </div>
        </div>
    </div>
<?php $this->endContent() ?>