<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@flareui/dist');
?>
<!-- header &&  horizontal navbar -->
<nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
    <div class="nav-top flex-grow-1">
        <div class="container d-flex flex-row h-100">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top">
                <a style="color: white;" class="navbar-brand brand-logo"
                   href="<?php echo \yii\helpers\Url::base(true); ?>">
                    ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อม</a>
                <a style="color: white;" class="navbar-brand brand-logo-mini"
                   href="<?php echo \yii\helpers\Url::base(true); ?>">EHA</a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

                <ul class="navbar-nav navbar-nav-right mr-0">

                    <li class="nav-item">
                        <i class="icon-user"></i> <a href="<?php echo \yii\helpers\Url::to(["/user/default/index"]) ?>"><span
                                style="color: white"
                                class="nav-profile-text"><?php echo Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->getPublicIdentity(); ?></span></a>
                        </li>

                    <li class="nav-item">
                        <a href="<?php echo \yii\helpers\Url::to(["/user/default/list-user"]) ?>"><span
                                style="color: white"
                                class="nav-profile-text"><?php echo Yii::$app->user->isGuest ? '' : '<i class="icon-people"></i> สมาชิก'; ?></span></a>
                    </li>

                    <?php if (Yii::$app->user->isGuest): ?>
                        <a style="color: white;" href="<?php echo \yii\helpers\Url::to(['/user/default/index']) ?>"
                           class="">
                            <i class="icon-login"></i> เข้าสู่ระบบ
                        </a>
                    <?php endif; ?>

                    <?php if (!Yii::$app->user->isGuest): ?>

                        <li class="nav-item">
                            <a style="color: white;" data-method="POST"
                               href="<?php echo \yii\helpers\Url::to(['/user/sign-in/logout']); ?>" class="">
                                <i class="icon-logout"></i> ออกจากระบบ
                            </a>
                        </li>
                    <?php endif; ?>

                </ul>
                <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu text-white"></span>
                </button>
            </div>
        </div>
    </div>
    <!-- Nav Bar Button -->
    <div class="nav-bottom">
        <div class="container">
            <ul class="nav page-navigation">
                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/site/dashboard']); ?>" class="nav-link"><i
                            class="link-icon icon-home"></i><span class="menu-title">หน้าหลัก</span></a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/page/view', 'slug' => 'about']) ?>" class="nav-link"><i
                            class="link-icon icon-screen-desktop"></i><span class="menu-title">รู้จัก EHA</span></a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/article/index', 'slug' => 'documents']) ?>" class="nav-link"><i
                            class="link-icon icon-list"></i><span class="menu-title">คู่มือ เอกสาร</span></a>
                </li>

                <?php if (Yii::$app->user->can('user') && !Yii::$app->user->can('manager') && !Yii::$app->user->can('director')): ?>

                    <li class="nav-item">
                        <a href="<?php echo \yii\helpers\Url::to(['/application-form/index']) ?>" class="nav-link"><i
                                class="link-icon icon-layers"></i><span class="menu-title">ประเมิน EHA</span></a>
                    </li>
                <?php endif; ?>

                <?php if (Yii::$app->user->can('manager') || Yii::$app->user->can('director') || Yii::$app->user->can('administrator')): ?>
                    <li class="nav-item">
                        <a href="<?php echo \yii\helpers\Url::to(['/assessor/locality-approve']) ?>" class="nav-link"><i
                                class="link-icon icon-docs"></i><span class="menu-title">ประเมินโดยคณะกรรมการ</span></a>
                    </li>
                <?php endif; ?>


                <?php if (!Yii::$app->user->isGuest): ?>

                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/assessment-conclusion/passed'])?>" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">อทป.ที่เข้าร่วมประเมินในเขตพื้นที่</span></a>
                </li>
                <?php endif; ?>


                <li class="nav-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/article/index', 'slug' => 'questions'])?>" class="nav-link"><i class="link-icon icon-docs"></i><span class="menu-title">คำถามที่พบบ่อย</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>