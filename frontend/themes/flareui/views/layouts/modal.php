<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;

?>
<?php $this->beginContent('@frontend/views/layouts/_clear.php'); ?>
    <div class="wrap">
        <?= $content ?>
    </div>
<?php $this->endContent(); ?>