<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use flareui\assets\FlareuiAsset;

/* @var $this \yii\web\View */
/* @var $content string */

FlareuiAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@flareui/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title>ระบบสารสนเทศเพื่อพัฒนาคุณภาพระบบบริการอนามัยสิ่งแวดล้อ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php echo Html::csrfMetaTags() ?>
</head>
<?php $this->beginBody([]) ?>

<?= $content ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
<!-- <div id="yii-debug-toolbar" data-url="/eha/debug/default/toolbar?tag=577e692775759" style="display:none" class="yii-debug-toolbar-bottom"></div>
 -->




