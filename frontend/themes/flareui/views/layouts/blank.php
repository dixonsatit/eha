<?php
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Html;
?>

<?php $this->beginContent('@flareui/views/layouts/base.php')?>

    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <?= $content ?>
        </div>
    </div>

<?php $this->endContent(); ?>