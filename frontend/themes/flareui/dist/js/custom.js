$(document).ready(function() {
    // get current URL path and assign 'active' class
    var pathname = window.location.pathname;
    $('.page-navigation > li > a[href="' + pathname + '"]').parent().addClass('active');
})