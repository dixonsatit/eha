

function getDatatable(id,h=null) {
    //datatables
    var table = $(id).DataTable({
        "pageLength": 80,
        "columnDefs": [
            {
                "targets": [ h ],
                "visible": false,
                "searchable": false
            }
        ],
        //lengthChange: false,
        "info": false,
        responsive: true,
        "scrollX": true,
        buttons: ['copy', 'csv', 'excel', 'print'],
         
        //buttons: [ , 'colvis','copy','csv','excel','pdf','print' ],
        "pagingType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
            "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
            "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
            "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
            "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
            "sSearch": "ค้นหา :"
        }
    });

    table.buttons().container()
        .appendTo($('.col-sm-6:eq(0)', table.table().container()));

    // table.buttons().container()
    //     .appendTo(id + '_wrapper .col-sm-6:eq(0)');

}


function getDatatableTotal(id, c) {
    //datatables
    var table = $(id).DataTable({
        //lengthChange: false,
        responsive: true,
        "scrollX": true,
        buttons: ['colvis', 'copy', 'csv', 'excel', 'print'],
        //buttons: [ , 'colvis','copy','csv','excel','pdf','print' ],
        "pagingType": "full_numbers",
        "oLanguage": {
            "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
            "sZeroRecords": "ไม่มีข้อมูล",
            "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
            "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
            "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
            "sSearch": "ค้นหา :"
        },
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api(),
                data;

            // Remove the formatting to get integer data for summation
            var intVal = function(i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                    i : 0;
            };

            // Total over all pages
            total = api
                .column(c)
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(c, { page: 'current' })
                .data()
                .reduce(function(a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(c).footer()).html(
                pageTotal + '    (จากทั้งหมด ' + total + ')'
            );
        }
    });

    table.buttons().container()
        .appendTo(id + '_wrapper .col-sm-6:eq(0)');

}