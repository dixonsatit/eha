<?php
namespace flareui\assets;

use common\assets\Html5shiv;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class FlareuiAsset extends AssetBundle
{

    public $sourcePath = '@flareui/dist';
    public $css = [
        'vendors/iconfonts/simple-line-icon/css/simple-line-icons.css',
        'vendors/css/vendor.bundle.base.css',
        'vendors/css/vendor.bundle.addons.css',
        'css/style.css',
        'datatables/css/datatables.bootstrap.min.css',
        'datatables/extensions/buttons/css/buttons.bootstrap.min.css',
    ];

    public $js = [

        // 'js/dashboard.js',
        // 'js/todolist.js',
        // 'js/template.js',
        // 'js/datatables.js',
        // 'datatables/js/jquery.dataTables.min.js',
        // 'datatables/js/datatables.bootstrap.min.js',
        // 'datatables/extensions/buttons/js/datatables.buttons.min.js',
        // 'datatables/extensions/buttons/js/buttons.bootstrap.min.js',
        // 'datatables/js/pdfmake.min.js',
        // 'datatables/js/vfs_fonts.js',
        // 'datatables/js/jszip.min.js',
        // 'js/custom.js',
        // 'datatables/extensions/buttons/js/buttons.html5.min.js',
        // 'datatables/extensions/buttons/js/buttons.print.min.js',
        // 'datatables/extensions/buttons/js/buttons.colVis.min.js',

        ['vendors/js/vendor.bundle.base.js', 'position' => \yii\web\View::POS_HEAD],
        ['vendors/js/vendor.bundle.addons.js', 'position' => \yii\web\View::POS_HEAD]

    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        Html5shiv::class,
        JqueryAsset::class,
    ];
}