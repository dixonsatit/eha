<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class HomerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/js';

    public $css = [
        'datatables/css/datatables.bootstrap.min.css',
        'datatables/extensions/buttons/css/buttons.bootstrap.min.css',
    ];

    public $js = [
      //datatables
        'datable/datatables.js',
        'datatables/js/jquery.dataTables.min.js',
        'datatables/js/datatables.bootstrap.min.js',
        'datatables/extensions/buttons/js/datatables.buttons.min.js',
        'datatables/extensions/buttons/js/buttons.bootstrap.min.js',
        'datatables/js/pdfmake.min.js',
        'datatables/js/vfs_fonts.js',
        'datatables/js/jszip.min.js',
        'datatables/extensions/buttons/js/buttons.html5.min.js',
        'datatables/extensions/buttons/js/buttons.print.min.js',
        'datatables/extensions/buttons/js/buttons.colVis.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
