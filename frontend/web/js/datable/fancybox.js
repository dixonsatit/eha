$(document).ready(function() {

    $("[data-fancybox]").fancybox({
        toolbar: true,
        smallBtn: true,
        iframe: {
            preload: false
        }
    });

});