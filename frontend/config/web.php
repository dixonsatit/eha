<?php
use kartik\mpdf\Pdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;

$defaultConfig = (new ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];
$defaultFontConfig = (new FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$config = [
    'homeUrl' => Yii::getAlias('@frontendUrl'),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'site/dashboard',
    'bootstrap' => ['maintenance'],
    'aliases' => [
        '@flareui' => '@frontend/themes/flareui',
    ],

    'modules' => [
        'user' => [
            'class' => frontend\modules\user\Module::class,
            'shouldBeActivated' => false, //true = active email
            'enableLoginByPass' => false,
        ],

        'comment' => [
            'class' => 'yii2mod\comments\Module',
        ],

        'file' => [
            'class' => backend\modules\file\Module::className(),
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],

        'airpoll' => [
            'class' => frontend\modules\airpoll\Module::className(),
        ],

        'dashboard' => [
            'class' => frontend\modules\dashboard\Module::className(),
        ]
    ],
    'components' => [

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@flareui/views',
                ],

            ],
        ],

        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'github' => [
                    'class' => yii\authclient\clients\GitHub::class,
                    'clientId' => env('GITHUB_CLIENT_ID'),
                    'clientSecret' => env('GITHUB_CLIENT_SECRET'),
                ],
                'facebook' => [
                    'class' => yii\authclient\clients\Facebook::class,
                    'clientId' => env('FACEBOOK_CLIENT_ID'),
                    'clientSecret' => env('FACEBOOK_CLIENT_SECRET'),
                    'scope' => 'email,public_profile',
                    'attributeNames' => [
                        'name',
                        'email',
                        'first_name',
                        'last_name',
                    ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'maintenance' => [
            'class' => common\components\maintenance\Maintenance::class,
            'enabled' => function ($app) {
                if (env('APP_MAINTENANCE') === '1') {
                    return true;
                }
                return $app->keyStorage->get('frontend.maintenance') === 'enabled';
            },
        ],
        'request' => [
            'cookieValidationKey' => env('FRONTEND_COOKIE_VALIDATION_KEY'),
        ],
        'user' => [
            'class' => yii\web\User::class,
            'identityClass' => common\models\User::class,
            'loginUrl' => ['/user/sign-in/login'],
            'enableAutoLogin' => true,
            'as afterLogin' => common\behaviors\LoginTimestampBehavior::class,
        ],
        'qr' => [
            'class' => '\Da\QrCode\Component\QrCodeComponent'
        ],

        'pdf' => [
            'class' => Pdf::classname(),
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'options' => [
                'fontDir' => array_merge($fontDirs, [Yii::getAlias('@frontend') . '/web/fonts']),
                'fontdata' => [
                    'sarabun' => [
                        'R' => 'THSarabunNew.ttf',
                        'B' => "THSarabunNewBold.ttf",
                        'I' => "THSarabunNewltalic.ttf",
                        'BI' => "THSarabunNewBoldltalic.ttf",
                    ],
                    'dsnptm' => [
                        'R' => 'DSNPTM.TTF',
                    ],
                    'dsnptme' => [
                        'R' => 'DSNPTME.TTF',
                    ],

                    'niramit' => [
                        'R' => 'Niramit-Regular.ttf',
                        'B' => "Niramit-Bold.ttf",
                        'I' => "Niramit-ltalic.ttf",
                        'BI' => "Niramit-Boldltalic.ttf",
                    ],
                ],
            ],
        ]
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs'=>['*']
    ];

    $config['modules']['gii'] = [
        'class' => yii\gii\Module::class,
        'generators' => [
            'crud' => [
                'class' => yii\gii\generators\crud\Generator::class,
                'messageCategory' => 'frontend',
            ],
        ],
    ];
}

return $config;
