<?php

namespace frontend\modules\dashboard\models;

/**
 * This is the ActiveQuery class for [[Dashboard]].
 *
 * @see Dashboard
 */
class DashboardQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Dashboard[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Dashboard|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
