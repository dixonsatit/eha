<?php

namespace frontend\modules\dashboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\dashboard\models\Dashboard;

/**
 * DashboardSearch represents the model behind the search form of `frontend\modules\dashboard\models\Dashboard`.
 */
class DashboardSearch extends Dashboard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cluster_id', 'created_by', 'updated_by'], 'integer'],
            [['kpi_code', 'kpi_name', 'kpi_category', 'preprocessor', 'unit', 'remark', 'data_source', 'data_url', 'created_ip', 'created_at', 'updated_ip', 'updated_at'], 'safe'],
            [['targer_value', 'target2558', 'target2559', 'target2560', 'target2561', 'target2562', 'target2563', 'target2564', 'result2558', 'result2559', 'result2560', 'result2561', 'result2562', 'result2563', 'result2564', 'target1_2558', 'target1_2559', 'target1_2560', 'target1_2561', 'target1_2562', 'target1_2563', 'target1_2564', 'target2_2558', 'target2_2559', 'target2_2560', 'target2_2561', 'target2_2562', 'target2_2563', 'target2_2564', 'result1_2558', 'result1_2559', 'result1_2560', 'result1_2561', 'result1_2562', 'result1_2563', 'result1_2564', 'result2_2558', 'result2_2559', 'result2_2560', 'result2_2561', 'result2_2562', 'result2_2563', 'result2_2564'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dashboard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'targer_value' => $this->targer_value,
            'cluster_id' => $this->cluster_id,
            'target2558' => $this->target2558,
            'target2559' => $this->target2559,
            'target2560' => $this->target2560,
            'target2561' => $this->target2561,
            'target2562' => $this->target2562,
            'target2563' => $this->target2563,
            'target2564' => $this->target2564,
            'result2558' => $this->result2558,
            'result2559' => $this->result2559,
            'result2560' => $this->result2560,
            'result2561' => $this->result2561,
            'result2562' => $this->result2562,
            'result2563' => $this->result2563,
            'result2564' => $this->result2564,
            'target1_2558' => $this->target1_2558,
            'target1_2559' => $this->target1_2559,
            'target1_2560' => $this->target1_2560,
            'target1_2561' => $this->target1_2561,
            'target1_2562' => $this->target1_2562,
            'target1_2563' => $this->target1_2563,
            'target1_2564' => $this->target1_2564,
            'target2_2558' => $this->target2_2558,
            'target2_2559' => $this->target2_2559,
            'target2_2560' => $this->target2_2560,
            'target2_2561' => $this->target2_2561,
            'target2_2562' => $this->target2_2562,
            'target2_2563' => $this->target2_2563,
            'target2_2564' => $this->target2_2564,
            'result1_2558' => $this->result1_2558,
            'result1_2559' => $this->result1_2559,
            'result1_2560' => $this->result1_2560,
            'result1_2561' => $this->result1_2561,
            'result1_2562' => $this->result1_2562,
            'result1_2563' => $this->result1_2563,
            'result1_2564' => $this->result1_2564,
            'result2_2558' => $this->result2_2558,
            'result2_2559' => $this->result2_2559,
            'result2_2560' => $this->result2_2560,
            'result2_2561' => $this->result2_2561,
            'result2_2562' => $this->result2_2562,
            'result2_2563' => $this->result2_2563,
            'result2_2564' => $this->result2_2564,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'kpi_code', $this->kpi_code])
            ->andFilterWhere(['like', 'kpi_name', $this->kpi_name])
            ->andFilterWhere(['like', 'kpi_category', $this->kpi_category])
            ->andFilterWhere(['like', 'preprocessor', $this->preprocessor])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'data_source', $this->data_source])
            ->andFilterWhere(['like', 'data_url', $this->data_url])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'updated_ip', $this->updated_ip]);

        return $dataProvider;
    }
}
