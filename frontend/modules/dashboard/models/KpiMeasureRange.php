<?php

namespace frontend\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "kpi_measure_range".
 *
 * @property integer $id
 * @property integer $kpi_template_id
 * @property integer $min
 * @property integer $max
 * @property string $color
 * @property string $describe
 */
class KpiMeasureRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_measure_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpi_template_id', 'min', 'max'], 'integer'],
            [['color', 'describe'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kpi_template_id' => 'Kpi Template ID',
            'min' => 'Min',
            'max' => 'Max',
            'color' => 'Color',
            'describe' => 'Describe',
        ];
    }
}
