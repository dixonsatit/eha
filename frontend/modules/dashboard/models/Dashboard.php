<?php

namespace frontend\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "{{%kpi_dashboard_result}}".
 *
 * @property string $id
 * @property string $kpi_code
 * @property string $kpi_name
 * @property string $kpi_category
 * @property string $preprocessor
 * @property string $targer_value
 * @property string $cluster_id
 * @property string $unit
 * @property string $target2558
 * @property string $target2559
 * @property string $target2560
 * @property string $target2561
 * @property string $target2562
 * @property string $target2563
 * @property string $target2564
 * @property string $result2558
 * @property string $result2559
 * @property string $result2560
 * @property string $result2561
 * @property string $result2562
 * @property string $result2563
 * @property string $result2564
 * @property string $target1_2558
 * @property string $target1_2559
 * @property string $target1_2560
 * @property string $target1_2561
 * @property string $target1_2562
 * @property string $target1_2563
 * @property string $target1_2564
 * @property string $target2_2558
 * @property string $target2_2559
 * @property string $target2_2560
 * @property string $target2_2561
 * @property string $target2_2562
 * @property string $target2_2563
 * @property string $target2_2564
 * @property string $result1_2558
 * @property string $result1_2559
 * @property string $result1_2560
 * @property string $result1_2561
 * @property string $result1_2562
 * @property string $result1_2563
 * @property string $result1_2564
 * @property string $result2_2558
 * @property string $result2_2559
 * @property string $result2_2560
 * @property string $result2_2561
 * @property string $result2_2562
 * @property string $result2_2563
 * @property string $result2_2564
 * @property string $remark
 * @property string $data_source
 * @property string $data_url
 * @property string $created_ip
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_ip
 * @property int $updated_by
 * @property string $updated_at
 */
class Dashboard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%kpi_dashboard_result}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kpi_code'], 'required'],
            [['targer_value', 'target2558', 'target2559', 'target2560', 'target2561', 'target2562', 'target2563', 'target2564', 'result2558', 'result2559', 'result2560', 'result2561', 'result2562', 'result2563', 'result2564', 'target1_2558', 'target1_2559', 'target1_2560', 'target1_2561', 'target1_2562', 'target1_2563', 'target1_2564', 'target2_2558', 'target2_2559', 'target2_2560', 'target2_2561', 'target2_2562', 'target2_2563', 'target2_2564', 'result1_2558', 'result1_2559', 'result1_2560', 'result1_2561', 'result1_2562', 'result1_2563', 'result1_2564', 'result2_2558', 'result2_2559', 'result2_2560', 'result2_2561', 'result2_2562', 'result2_2563', 'result2_2564'], 'number'],
            [['cluster_id', 'created_by', 'updated_by'], 'integer'],
            [['remark'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['kpi_code'], 'string', 'max' => 20],
            [['kpi_name'], 'string', 'max' => 255],
            [['kpi_category'], 'string', 'max' => 5],
            [['preprocessor'], 'string', 'max' => 30],
            [['unit'], 'string', 'max' => 100],
            [['data_source', 'data_url', 'updated_ip'], 'string', 'max' => 512],
            [['created_ip'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kpi_code' => Yii::t('app', 'Kpi Code'),
            'kpi_name' => Yii::t('app', 'Kpi Name'),
            'kpi_category' => Yii::t('app', 'Kpi Category'),
            'preprocessor' => Yii::t('app', 'Preprocessor'),
            'targer_value' => Yii::t('app', 'Targer Value'),
            'cluster_id' => Yii::t('app', 'Cluster ID'),
            'unit' => Yii::t('app', 'Unit'),
            'target2558' => Yii::t('app', 'Target2558'),
            'target2559' => Yii::t('app', 'Target2559'),
            'target2560' => Yii::t('app', 'Target2560'),
            'target2561' => Yii::t('app', 'Target2561'),
            'target2562' => Yii::t('app', 'Target2562'),
            'target2563' => Yii::t('app', 'Target2563'),
            'target2564' => Yii::t('app', 'Target2564'),
            'result2558' => Yii::t('app', 'Result2558'),
            'result2559' => Yii::t('app', 'Result2559'),
            'result2560' => Yii::t('app', 'Result2560'),
            'result2561' => Yii::t('app', 'Result2561'),
            'result2562' => Yii::t('app', 'Result2562'),
            'result2563' => Yii::t('app', 'Result2563'),
            'result2564' => Yii::t('app', 'Result2564'),
            'target1_2558' => Yii::t('app', 'Target1 2558'),
            'target1_2559' => Yii::t('app', 'Target1 2559'),
            'target1_2560' => Yii::t('app', 'Target1 2560'),
            'target1_2561' => Yii::t('app', 'Target1 2561'),
            'target1_2562' => Yii::t('app', 'Target1 2562'),
            'target1_2563' => Yii::t('app', 'Target1 2563'),
            'target1_2564' => Yii::t('app', 'Target1 2564'),
            'target2_2558' => Yii::t('app', 'Target2 2558'),
            'target2_2559' => Yii::t('app', 'Target2 2559'),
            'target2_2560' => Yii::t('app', 'Target2 2560'),
            'target2_2561' => Yii::t('app', 'Target2 2561'),
            'target2_2562' => Yii::t('app', 'Target2 2562'),
            'target2_2563' => Yii::t('app', 'Target2 2563'),
            'target2_2564' => Yii::t('app', 'Target2 2564'),
            'result1_2558' => Yii::t('app', 'Result1 2558'),
            'result1_2559' => Yii::t('app', 'Result1 2559'),
            'result1_2560' => Yii::t('app', 'Result1 2560'),
            'result1_2561' => Yii::t('app', 'Result1 2561'),
            'result1_2562' => Yii::t('app', 'Result1 2562'),
            'result1_2563' => Yii::t('app', 'Result1 2563'),
            'result1_2564' => Yii::t('app', 'Result1 2564'),
            'result2_2558' => Yii::t('app', 'Result2 2558'),
            'result2_2559' => Yii::t('app', 'Result2 2559'),
            'result2_2560' => Yii::t('app', 'Result2 2560'),
            'result2_2561' => Yii::t('app', 'Result2 2561'),
            'result2_2562' => Yii::t('app', 'Result2 2562'),
            'result2_2563' => Yii::t('app', 'Result2 2563'),
            'result2_2564' => Yii::t('app', 'Result2 2564'),
            'remark' => Yii::t('app', 'Remark'),
            'data_source' => Yii::t('app', 'Data Source'),
            'data_url' => Yii::t('app', 'Data Url'),
            'created_ip' => Yii::t('app', 'สร้างโดย Ip'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเมื่อ'),
            'updated_ip' => Yii::t('app', 'แก้ไขโดย Ip'),
            'updated_by' => Yii::t('app', 'แก้ไขโดย'),
            'updated_at' => Yii::t('app', 'แก้ไขเมื่อ'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DashboardQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DashboardQuery(get_called_class());
    }
}
