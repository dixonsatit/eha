<?php
namespace frontend\modules\dashboard\models;

use Yii;

/**
 * This is the model class for data function services.
 */
class DashboardServices  
{
    public function getReportdate($strDate) {
        // Validate & Correct year number
        $strYear = (int)substr($strDate,0,4)>2003 ? (int)substr($strDate,0,4)+543 : "";
        // Validate & Correct month number
        $strMonth = (int)substr($strDate,4,2)>0 ? (int)substr($strDate,4,2) : "";
        // Validate & Correct day number
        $strDay = (int)substr($strDate,6,2)>0 ? (int)substr($strDate,6,2) : "-";
        // $strHour= date("H",strtotime($strDate));
        // $strMinute= date("i",strtotime($strDate));
        // $strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
        $strMonthThai = $strMonthCut["$strMonth"];
        return "<br><b>วันที่ประมวลผล: </b> $strDay $strMonthThai $strYear";
    } //end actionReportdate

    public function getReportPeriod($ad_year) {
        $data_year = date('Y');
        $data_quarter = 4;
        if($ad_year==date('Y')) {
            $data_year = $ad_year;
            if($month == '1' || $month == '2' || $month == '3') {
                $data_quarter = 1;
            } else if($month == '4' || $month == '5' || $month == '6') {
                $data_quarter = 2;
            } else if($month == '7' || $month == '8' || $month == '9') {
                $data_quarter = 3;
            } else {
                $data_quarter = 4;
            }
        } else {
            if(($ad_year==(date('Y')+1)) && ($month == '10' || $month == '11' || $month == '12')) {
                $data_year = $ad_year-1;
            } else {
                $data_year = $ad_year;
            }
            $data_quarter = 4;
        }
        // print "'data_year'=>$data_year,'data_quarter'=>$data_quarter"; exit;
        return ['data_year'=>$data_year,'data_quarter'=>$data_quarter];
    } //end getReportPeriod

}