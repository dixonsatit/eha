<?php

namespace frontend\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "kpi_range".
 *
 * @property integer $id
 * @property string $year
 * @property integer $kpi_template_id
 * @property integer $range_no
 * @property integer $range_min
 * @property integer $range_max
 * @property string $range_color
 * @property string $range_desc
 */
class KpiRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year','year_en'], 'string', 'max' => 4],
            [['kpi_template_id','range_no'], 'integer'],
            [['range_min', 'range_min'], 'float'],
            [['range_max', 'range_max'], 'float'],
            [['color'], 'string', 'max' => 20],
			[['desc'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัส (ID)',
            'kpi_template_id' => 'รหัสข้อมูล/ตัวชี้วัด (KPI ID)',
            'year' => 'ปี',
            'year' => 'ปี ค.ศ.',
            'range_no' => 'หมายเลขช่วงข้อมูล',
			'range_min' => 'ค่าต่ำสุด',
			'range_max' => 'ค่าสูงสุด',
			'range_color' => 'สีช่วงข้อมูล',
			'range_desc' => 'รายละเอียดช่วงข้อมูล',            
        ];
    }
}
