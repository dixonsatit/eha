<?php

namespace frontend\modules\dashboard\controllers;

//use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;
//use yii\helpers\Json;

class PopulationpyramidController extends \yii\web\Controller
{
    
    public function actionIndex()
    {
        $request = \yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        $data=null;
        $pyramidX=null;
        $pyramidM=null;
        $pyramidF=null;
        
        $sql = 'SELECT groupcode, groupname, '
                . 'SUM(IFNULL(male,0)) AS male, SUM(IFNULL(female,0)) AS female, SUM(IFNULL(total,0)) AS total '
                . 'FROM s_person_pyramid '
                . 'WHERE b_year=:year '
                . 'GROUP BY groupcode '
                . 'ORDER BY groupcode';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        $data = $cmd->queryAll();

        if($data){
            $sql_total = 'SELECT SUM(male) AS male, SUM(female) AS female, SUM(total) AS total, MAX(date_com) AS report_date '
                . 'FROM s_person_pyramid '
                . 'WHERE b_year=:year ';
            $total = $this->getTotal($sql_total,$bYear);
            foreach ($data as $v) {
            $x[] = "'".$v['groupname']."'";
            $m[] = ($v['male']/$total['total'])*-100;
            $f[] = ($v['female']/$total['total'])*100;
            }
            $pyramidX = implode(',', $x);
            $pyramidM = implode(',', $m);
            $pyramidF = implode(',', $f);
        }
        //$total['update'] = $this->getDateTh($total['udate']);

        return $this->render('index',[
            'year' => $year,
            'data' => $data,
            'pyramidX' => $pyramidX,
            'pyramidM' => $pyramidM,
            'pyramidF' => $pyramidF,
            'total' => $total,
            'viewElement' => $this->getElement($bYear),
            'regionList' => $this->getRegion(),
        ]);

    }
    
    public function actionRegion()
    {
        $request = \yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        $rg = $request->get('rg');
        // Data validation
        if($rg==99){
            return $this->redirect(['index']);
        }
        //
        $data=null;
        $pyramidX=null;
        $pyramidM=null;
        $pyramidF=null;
        
        $sql = 'SELECT p.groupcode,p.groupname,SUM(IFNULL(p.male,0)) as male,SUM(IFNULL(p.female,0)) as female,SUM(IFNULL(p.total,0)) as total '
                . 'FROM s_person_pyramid p LEFT JOIN cchangwat c ON c.changwatcode COLLATE utf8_unicode_ci = LEFT(p.areacode,2) '
                . 'WHERE p.b_year=:year AND c.regioncode=:rg '
                . 'GROUP BY p.groupcode '
                . 'ORDER BY p.groupcode';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        $cmd->bindValue(':rg', $rg);
        $data = $cmd->queryAll();

        if($data){
            $sql_total = 'SELECT SUM(p.male) as male,SUM(p.female) as female,SUM(p.total) as total,MAX(date_com) as report_date '
                . 'FROM s_person_pyramid p LEFT JOIN cchangwat c ON c.changwatcode COLLATE utf8_unicode_ci=LEFT(p.areacode,2) '
                . 'WHERE p.b_year=:year AND c.regioncode=:rg ';
            $total = $this->getTotal($sql_total,$bYear,$rg);
            foreach ($data as $v) {
            $x[] = "'".$v['groupname']."'";
            $m[] = ($v['male']/$total['total'])*-100;
            $f[] = ($v['female']/$total['total'])*100;
            }
            $pyramidX = implode(',', $x);
            $pyramidM = implode(',', $m);
            $pyramidF = implode(',', $f);
        }
        // $total['update'] = $this->getDateTh($total['udate']);

        return $this->render('region',[
            'year' => $year,
            'rg'=>$rg,
            'data' => $data,
            'pyramidX' => $pyramidX,
            'pyramidM' => $pyramidM,
            'pyramidF' => $pyramidF,
            'total' => $total,
            'viewElement' => $this->getElement($bYear,$rg),
            'regionList' => $this->getRegion(),
            'changwatList' => $this->getChangwat($rg),
        ]);

    }
    
    public function actionChangwat()
    {
        $request = \yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        $cw = $request->get('cw');
        //
        $data=null;
        $pyramidX=null;
        $pyramidM=null;
        $pyramidF=null;
        
        $sql = 'SELECT p.groupcode,p.groupname,SUM(IFNULL(p.male,0)) as male,SUM(IFNULL(p.female,0)) as female,SUM(IFNULL(p.total,0)) as total '
                . 'FROM s_person_pyramid p LEFT JOIN cchangwat c ON c.changwatcode COLLATE utf8_unicode_ci = LEFT(p.areacode,2) '
                . 'WHERE p.b_year=:year AND c.changwatcode=:cw '
                . 'GROUP BY p.groupcode '
                . 'ORDER BY p.groupcode';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        $cmd->bindValue(':cw', $cw);
        $data = $cmd->queryAll();

        if($data){
            $sql_total = 'SELECT SUM(p.male) as male,SUM(p.female) as female,SUM(p.total) as total,MAX(date_com) as report_date '
                . 'FROM s_person_pyramid p LEFT JOIN cchangwat c ON c.changwatcode COLLATE utf8_unicode_ci=LEFT(p.areacode,2) '
                . 'WHERE p.b_year=:year AND c.changwatcode=:cw ';
            $total = $this->getTotal($sql_total,$bYear,null,$cw);
            foreach ($data as $v) {
            $x[] = "'".$v['groupname']."'";
            $m[] = ($v['male']/$total['total'])*-100;
            $f[] = ($v['female']/$total['total'])*100;
            }
            $pyramidX = implode(',', $x);
            $pyramidM = implode(',', $m);
            $pyramidF = implode(',', $f);
        }
        $element=$this->getElement($bYear,null,$cw);
        // $total['update'] = $this->getDateTh($total['udate']);

        return $this->render('changwat',[
            'year' => $year,
            'cw'=>$cw,
            'data' => $data,
            'pyramidX' => $pyramidX,
            'pyramidM' => $pyramidM,
            'pyramidF' => $pyramidF,
            'total' => $total,
            'viewElement' => $element,
            'regionList' => $this->getRegion(),
            'changwatList' => $this->getChangwat($element['rg']),
            'ampurList' => $this->getAmpur($cw),
        ]);

    }
    
    public function actionAmpur()
    {
        $request = \yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        $ap = $request->get('ap');
        //
        $data=null;
        $pyramidX=null;
        $pyramidM=null;
        $pyramidF=null;
        
        $sql = 'SELECT p.groupcode,p.groupname,SUM(IFNULL(p.male,0)) as male,SUM(IFNULL(p.female,0)) as female,SUM(IFNULL(p.total,0)) as total '
                . 'FROM s_person_pyramid p LEFT JOIN campur c ON c.ampurcodefull COLLATE utf8_unicode_ci = LEFT(p.areacode,4) '
                . 'WHERE p.b_year=:year AND c.ampurcodefull=:ap '
                . 'GROUP BY p.groupcode '
                . 'ORDER BY p.groupcode';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        $cmd->bindValue(':ap', $ap);
        $data = $cmd->queryAll();

        if($data){
            $sql_total = 'SELECT SUM(p.male) as male,SUM(p.female) as female,SUM(p.total) as total,MAX(date_com) as report_date '
                . 'FROM s_person_pyramid p LEFT JOIN campur c ON c.ampurcodefull COLLATE utf8_unicode_ci=LEFT(p.areacode,4) '
                . 'WHERE p.b_year=:year AND c.ampurcodefull=:ap ';
            $total = $this->getTotal($sql_total,$bYear,null,null,$ap);
            foreach ($data as $v) {
            $x[] = "'".$v['groupname']."'";
            $m[] = ($v['male']/$total['total'])*-100;
            $f[] = ($v['female']/$total['total'])*100;
            }
            $pyramidX = implode(',', $x);
            $pyramidM = implode(',', $m);
            $pyramidF = implode(',', $f);
        }
        $element=$this->getElement($bYear, null, null, $ap);
        // $total['update'] = $this->getDateTh($total['udate']);

        return $this->render('ampur',[
            'year' => $year,
            'ap'=>$ap,
            'data' => $data,
            'pyramidX' => $pyramidX,
            'pyramidM' => $pyramidM,
            'pyramidF' => $pyramidF,
            'total' => $total,
            'viewElement' => $element,
            'regionList' => $this->getRegion(),
            'changwatList' => $this->getChangwat($element['rg']),
            'ampurList' => $this->getAmpur($element['cw']),
            'tambonList' => $this->getTambon($ap),
        ]);

    }
    
    public function actionTambon()
    {
        $request = \yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        $tb = $request->get('tb');
        //
        $data=null;
        $pyramidX=null;
        $pyramidM=null;
        $pyramidF=null;
        
        $sql = 'SELECT p.groupcode,p.groupname,SUM(IFNULL(p.male,0)) as male,SUM(IFNULL(p.female,0)) as female,SUM(IFNULL(p.total,0)) as total '
                . 'FROM s_person_pyramid p LEFT JOIN ctambon c ON c.tamboncodefull COLLATE utf8_unicode_ci = LEFT(p.areacode,6) '
                . 'WHERE p.b_year=:year AND c.tamboncodefull=:tb '
                . 'GROUP BY p.groupcode '
                . 'ORDER BY p.groupcode';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        $cmd->bindValue(':tb', $tb);
        $data = $cmd->queryAll();

        if($data){
            $sql_total = 'SELECT SUM(p.male) as male,SUM(p.female) as female,SUM(p.total) as total,MAX(date_com) as report_date '
                . 'FROM s_person_pyramid p LEFT JOIN ctambon c ON c.tamboncodefull COLLATE utf8_unicode_ci = LEFT(p.areacode,6) '
                . 'WHERE p.b_year=:year AND c.tamboncodefull=:tb ';
            $total = $this->getTotal($sql_total,$bYear,null,null,null,$tb);
            foreach ($data as $v) {
            $x[] = "'".$v['groupname']."'";
            $m[] = ($v['male']/$total['total'])*-100;
            $f[] = ($v['female']/$total['total'])*100;
            }
            $pyramidX = implode(',', $x);
            $pyramidM = implode(',', $m);
            $pyramidF = implode(',', $f);
        }
        $element=$this->getElement($bYear, null, null, null, $tb);
        // $total['update'] = $this->getDateTh($total['udate']);

        return $this->render('tambon',[
            'year' => $year,
            'tb'=>$tb,
            'data' => $data,
            'pyramidX' => $pyramidX,
            'pyramidM' => $pyramidM,
            'pyramidF' => $pyramidF,
            'total' => $total,
            'viewElement' => $element,
            'regionList' => $this->getRegion(),
            'changwatList' => $this->getChangwat($element['rg']),
            'ampurList' => $this->getAmpur($element['cw']),
            'tambonList' => $this->getTambon($element['ap']),
        ]);
    }
    
    protected function getElement($bYear, $rg=null, $cw=null, $ap=null, $tb=null)
    {
        $year=$bYear-543;
        //
        if ($rg !== null) { // Changwat
            $areaName='ศูนย์อนามัยที่ '.(int)$rg;
            $yearName='ปี '. $bYear;
            //$tableAreaText='จังหวัด';
            $breadcrumb[]=['label' => 'รวมทั้งหมด', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }else if($cw !== null){   // Ampur
            $conn = Yii::$app->db;
            $sql = 'SELECT regioncode,changwatname FROM cchangwat WHERE changwatcode=:cw';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':cw', $cw);
            $CW = $cmd->queryOne();
            $areaName=$CW['changwatname'];
            $yearName='ปี '. $bYear;
            $rg=$CW['regioncode'];
            //$tableAreaText='อำเภอ';
            $breadcrumb[]=['label' => 'รวมทั้งหมด', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$CW['regioncode'] ,'url' => ['region?year='.$year.'&rg='.$CW['regioncode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($ap !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM campur a INNER JOIN cchangwat c ON a.changwatcode=c.changwatcode '
                    . 'WHERE a.ampurcodefull=:ap';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':ap', $ap);
            $AP = $cmd->queryOne();
            $areaName = (substr($AP['ampurname'], 0, 3)!=='เขต')? $AP['ampurname']:$AP['ampurname'];
            $yearName='ปี '. $bYear;
            $rg=$AP['regioncode'];
            $cw=$AP['changwatcode'];
            $cwName=$AP['changwatname'];
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'รวมทั้งหมด', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$AP['regioncode'] ,'url' => ['region?year='.$year.'&rg='.$AP['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$AP['changwatname'] ,'url' => ['changwat?year='.$year.'&cw='.$AP['changwatcode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($tb !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM ctambon t INNER JOIN cchangwat c ON t.changwatcode=c.changwatcode '
                    . 'LEFT JOIN campur a ON t.ampurcode=a.ampurcodefull '
                    . 'WHERE t.tamboncodefull=:tb';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':tb', $tb);
            $TB = $cmd->queryOne();
            $areaName = 'ตำบล'.$TB['tambonname'];
            $yearName='ปี '. $bYear;
            $rg=$TB['regioncode'];
            $cw=$TB['changwatcode'];
            $cwName=$TB['changwatname'];
            $ap=$TB['ampurcodefull'];
            $apName=$TB['ampurname'];
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'รวมทั้งหมด', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$TB['regioncode'] ,'url' => ['region?year='.$year.'&rg='.$TB['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$TB['changwatname'] ,'url' => ['changwat?year='.$year.'&cw='.$TB['changwatcode']]];
            $breadcrumb[]=['label' => 'อำเภอ'.$TB['ampurname'] ,'url' => ['ampur?year='.$year.'&ap='.$TB['ampurcodefull']]];
            $breadcrumb[]=['label' => $areaName];
        }else{  // Index
            $areaName='รวมทั้งหมด';
            $yearName='ปี '. $bYear;
            //$tableAreaText='เขตสุขภาพ';
            //$breadcrumb[]=['label' => 'ระดับประเทศ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }
        $element=[
            'rg' => $rg,
            'cw' => $cw,
            'cwName' => $cwName,
            'ap' => $ap,
            'apName' => $apName,
            'areaName' => $areaName,
            'yearName' => $yearName,
            //'tableAreaText' => $tableAreaText,
            'breadcrumb' => $breadcrumb,
        ];
        return $element;
            
    }
    
    protected function getTotal($sql,$bYear,$rg=null,$cw=null,$ap=null,$tb=null) {

        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $bYear);
        if ($rg !== null) {
            $cmd->bindValue(':rg', $rg);
        }
        if ($cw !== null) {
            $cmd->bindValue(':cw', $cw);
        }
        if ($ap !== null) {
            $cmd->bindValue(':ap', $ap);
        }
        if ($tb !== null) {
            $cmd->bindValue(':tb', $tb);
        }

        $data = $cmd->queryOne();
        
        return $data;
    }
    
    protected function getRegion() {
        $sql = 'SELECT region_code,region_name '
                . 'FROM cregion '
                . 'WHERE region_code<>13 ';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $data = $cmd->queryAll();
        
        return $data;
    }
    protected function getChangwat($rg) {
        $sql = 'SELECT changwatcode,changwatname '
                . 'FROM cchangwat '
                . 'WHERE regioncode=:rg '
                . 'ORDER BY changwatname';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':rg', $rg);
        $data = $cmd->queryAll();
        
        return $data;
    }
    
    protected function getAmpur($cw) {
        $sql = 'SELECT ampurcodefull,ampurname '
                . 'FROM campur '
                . 'WHERE changwatcode=:cw '
                . 'ORDER BY ampurname';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':cw', $cw);
        $data = $cmd->queryAll();
        
        return $data;
    }
    
    protected function getTambon($ap) {
        $sql = 'SELECT tamboncodefull,tambonname '
                . 'FROM ctambon '
                . 'WHERE ampurcode=:ap AND flag_status=0 '
                . 'ORDER BY tambonname';
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':ap', $ap);
        $data = $cmd->queryAll();
        
        return $data;
    }
    /*
    protected function getDateTh($date) {
        $_month_name = [
            '01'=>'มกราคม',  
            '02'=>'กุมภาพันธ์',  
            '03'=>'มีนาคม',    
            '04'=>'เมษายน',  
            '05'=>'พฤษภาคม',  
            '06'=>'มิถุนายน',    
            '07'=>'กรกฎาคม',  
            '08'=>'สิงหาคม',  
            '09'=>'กันยายน',    
            '10'=>'ตุลาคม', 
            '11'=>'พฤศจิกายน',  
            '12'=>'ธันวาคม',
            ]; 
	$y = ((int)substr($date,0,4))+543;
        $m = substr($date,4,2);
        $d = (int)substr($date,6,2);
        
        $dateTh = $d.' '.$_month_name[$m].' '. $y;
        
	return $dateTh;
    }
     * 
     */

}
