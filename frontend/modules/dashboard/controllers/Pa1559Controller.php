<?php
namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class Pa1559Controller extends \yii\web\Controller
{
    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }

    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }

    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }

    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();
    }

    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        // Column chart and gauge and map
        foreach ($data as $k => $v) {
            $columnData[] = [$v['rangeage'], (float) $v['ratio']];
        }

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'pieData' => $columnData,
        ];
    }

    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null, $tambon = null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }

    public function getTrendData($sql, $bYearPrev, $bYear)
    {
        $conn = Yii::$app->db;

        $sql1 = 'SELECT S.rage_age as rangeage  FROM s_pa1559 S GROUP BY S.rage_age';
        $cmd1 = $conn->createCommand($sql1);
        $rangeage = $cmd1->queryAll();
        $a = 0;
        foreach ($rangeage as $t1) {
            $a = (int)0;
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':queryYearPrev', $bYearPrev);
            $cmd->bindValue(':queryYear', $bYear);
            $cmd->bindValue(':rAge', $t1['rangeage']);

            $trends = $cmd->queryAll();

            foreach ($trends as $t) {
                if($t1['rangeage'] == $t['rage_age']){
                    $datat[$a]=(float)$t['ratio'];
                    $a+=1;
                }
            }

            $trendData[] = [
                "name" => $t['rangeage'],
                "data" =>  $datat
            ];
            
        }

        return $trendData;
    }


//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $conn = Yii::$app->db;

        $sql_maxyear = "SELECT max(P.b_year) as maxyear FROM `s_pa1559` P GROUP BY P.b_year ORDER BY maxyear DESC";
        $cmd_max = $conn->createCommand($sql_maxyear);
        $maxyear = $cmd_max->queryOne();

        $nowyear = ($maxyear['maxyear']-543);

        if (strlen($request->get('year')) <> 4){            
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$nowyear]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? $nowyear:strlen($request->get('year'))!==4 ? $nowyear : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "87" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)($bYear) - 3;
        
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        $pieData = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $pieData = $q->pieData;
        }

        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Trend Data
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'nowyear'=> $nowyear,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'ช่วงอายุ']);
    }

//------------------------------------------END-Index---------------------------------//    

}

// end class
