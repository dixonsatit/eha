<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class DelaydspmController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }


    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        $request = Yii::$app->request;
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $month=date("m");
        //
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        // Columns, Chart, Gauge and Map
        foreach ($data as $k => $v) {
            //column fix quarter4
            //$columnData[] = [$v['areaname_new'], (float) $v['ratioq4']];
            //$columnData[] = [$v['areaname_new'], (float) $v['total_ratio']];

            if($year==date("y")+1 && $month=='10' || $month=='11' || $month=='12')
            {
                $columnData[] = [$v['areaname_new'], (float) $v['ratioq1']];
                $color[]= $this->getMapColor($bYear, $kid, $v['ratioq1']);
                $columnData=[
                 'column'=>$column,
                 'color'=>$color
                ];
            }
            if($year==date("y") && $month=='01' || $month=='02' || $month=='03')
            {
                $columnData[] = [$v['areaname_new'], (float) $v['ratioq2']];
                $color[]= $this->getMapColor($bYear, $kid, $v['ratioq2']);
                $columnData=[
                 'column'=>$column,
                 'color'=>$color
                ];
            }
            if($year==date("y") && $month=='04' || $month=='05' || $month=='06')
            {
                $columnData[] = [$v['areaname_new'], (float) $v['ratioq3']];
                $color[]= $this->getMapColor($bYear, $kid, $v['ratioq3']);
                $columnData=[
                 'column'=>$column,
                 'color'=>$color
                ];
            }
            if($year==date("Y") && $month=='07' || $month=='08' || $month=='09')
            {
                $column[]=[$v['areaname_new'], (float) $v['ratioq4']];
                $color[]= $this->getMapColor($bYear, $kid, $v['ratioq4']);
                $columnData=[
                 'column'=>$column,
                 'color'=>$color
                ];
                
            }
            else
            {
                $column[]=[$v['areaname_new'], (float) $v['ratioq4']];
                $color[]= $this->getMapColor($bYear, $kid, $v['ratioq4']);
                $columnData=[
                 'column'=>$column,
                 'color'=>$color
                ];
            }


            $target += (float) $v['total_target'];
            $result += (float) $v['total_result'];
            // New calculate (2018-07-08)
            $result1 += (float) $v['total_result1'];
            $result2 += (float) $v['total_result2'];
            
              //Quarter1
              $targetq1 += (float) $v['targetq1'];
              $result1q1 += (float) $v['result1q1'];
               
              //Quarter2
              $targetq2 += (float) $v['targetq2'];
              $result1q2 += (float) $v['result1q2'];
               
              //Quarter3
              $targetq3 += (float) $v['targetq3'];
              $result1q3 += (float) $v['result1q3'];
               
              //Quarter4
              $targetq4 += (float) $v['targetq4'];
              $result1q4 += (float) $v['result1q4'];
             

            if ($v['areacode'] !== 'TOTAL') {
                // Map data
                // $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                //     "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                //     "color" => $this->getMapColor($bYear, $kid, $v['total_ratio'])];
                
                // Map data fix quarter4
                // $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                //     "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                //     "color" => $this->getMapColor($bYear, $kid, $v['ratioq4'])];

                    if($year==date("y")+1 && $month=='10' || $month=='11' || $month=='12')
                    {
                        $propData[$v['areacode']] = ["value" => (float) $v['ratioq1'],
                       "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                       "color" => $this->getMapColor($bYear, $kid, $v['ratioq1'])];	
                    }
                    if($year==date("y") && $month=='01' || $month=='02' || $month=='03')
                    {
                        $propData[$v['areacode']] = ["value" => (float) $v['ratioq2'],
                       "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                       "color" => $this->getMapColor($bYear, $kid, $v['ratioq2'])];
                    }
                    if($year==date("y") && $month=='04' || $month=='05' || $month=='06')
                    {
                        $propData[$v['areacode']] = ["value" => (float) $v['ratioq3'],
                        "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                        "color" => $this->getMapColor($bYear, $kid, $v['ratioq3'])];	
                    }
                    if($year==date("Y") && $month=='07' || $month=='08' || $month=='09')
                    {
                       $propData[$v['areacode']] = ["value" => (float) $v['ratioq4'],
                       "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                       "color" => $this->getMapColor($bYear, $kid, $v['ratioq4'])];
                    }
                    else
                    {
                        $propData[$v['areacode']] = ["value" => (float) $v['ratioq4'],
                       "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                       "color" => $this->getMapColor($bYear, $kid, $v['ratioq4'])];
                    }
            }
        }

        // Gauge chart
        if($year==date("y")+1 && $month=='10' || $month=='11' || $month=='12')
        {
            $gaugeData = ($targetq1 == '' OR $targetq1 == 0) ? 0 : @(float)(($targetq1-$result1q1)/$targetq1) * 100;
            $quarterName='ไตรมาส 1';	
        }
        if($year==date("y") && $month=='01' || $month=='02' || $month=='03')
        {
            $gaugeData = ($targetq2 == '' OR $targetq2 == 0) ? 0 : @(float)(($targetq2-$result1q2)/$targetq2) * 100;
            $quarterName='ไตรมาส 2';	
        }
        if($year==date("y") && $month=='04' || $month=='05' || $month=='06')
        {
            $gaugeData = ($targetq3 == '' OR $targetq3 == 0) ? 0 : @(float)(($targetq3-$result1q3)/$targetq3) * 100;
            $quarterName='ไตรมาส 3';	
        }
        if($year==date("Y") && $month=='07' || $month=='08' || $month=='09')
        {
            $gaugeData = ($targetq4 == '' OR $targetq4 == 0) ? 0 : @(float)(($targetq4-$result1q4)/$targetq4) * 100;
            $quarterName='ไตรมาส 4';	
        }
        else
        {
            $gaugeData = ($targetq4 == '' OR $targetq4 == 0) ? 0 : @(float)(($targetq4-$result1q4)/$targetq4) * 100;
            $quarterName='ไตรมาส 4';	
        }

        //gaugeData fix quarter4
        //$gaugeData = ($targetq4 == '' OR $targetq4 == 0) ? 0 : @(float)(($targetq4-$result1q4)/$targetq4) * 100;	
        
        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName
        ];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }

    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        $request = Yii::$app->request;
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $month=date("m");

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            //$trendData[] = [$t['b_year'], (float) $t['total_ratio4']];
            
            if($year==date("Y")+1 && $month=='10' || $month=='11' || $month=='12')
            {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio1']];	
            }
            if($year==date("Y") && $month=='01' || $month=='02' || $month=='03')
            {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio2']];	
            }
            if($year==date("Y") && $month=='04' || $month=='05' || $month=='06')
            {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio3']];	
            }
            if($year==date("Y") && $month=='07' || $month=='08' || $month=='09')
            {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio4']];
            }
            else
            {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio4']];
            }
        }
        return $trendData;
    }

//-------------------------------------------Index---------------------------------//       

    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "151" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $quarterName=null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName=$q->quarterName;
        } else {	
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        
        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Region Trend
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $trendYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'เขตสุขภาพ']);
    }

//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    

    public function actionChangwat()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "151" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $region = substr("00".($request->get('rg')),-2);
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $quarterName=null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;	
            $quarterName=$q->quarterName;		
		}
		if ($gaugeData == 0) {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        
        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join cchangwat c on c.changwatcode=g.areacode
        //         where areatype=2 and c.regioncode=:region';
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where c.regioncode = :region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $trendYear, $region);
        }    
        
        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');
				
        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'จังหวัด']);
    }

//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    

    public function actionAmpur()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "151" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $changwat = substr("00".($request->get('cw')),-2);		
        //		
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $quarterName=null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName=$q->quarterName;
        }
		if ($gaugeData == 0) {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
		}

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join campur a on a.ampurcodefull=g.areacode
        //         where  areatype=3 and a.changwatcode=:changwat
        //         and a.flag_status=0';
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null,$changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {            
            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $trendYear, null, $changwat);
        }	
        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'อำเภอ']);
    }

//------------------------------------------END Ampur ----------------------------------//    


//------------------------------------------Tambon ------------------------------------//

    public function actionTambon()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "151" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $ampur = substr("0000".($request->get('ap')),-4);
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $quarterName=null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {
            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, null, null, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName=$q->quarterName;
        }
		if ($gaugeData == 0) {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
		}		
        // MAP Layout
        $sql = 'select * from geojson_tambon g 
        inner join ctambon t on t.tamboncodefull=g.areacode
        where ampurcode=:ampur';
        $geo = $this->getMapLayout($sql, $propData, null,null,$ampur);

        // Trend
		if ($kpi['sql_query_trend_tambon']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $trendYear, null, null, $ampur);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['mooban', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName,
            'trendData' => $trendData,

            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'ตำบล']);
    }

//------------------------------------------END Tambon ----------------------------------//   

} // end class
