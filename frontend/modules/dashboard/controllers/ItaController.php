<?php
namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use frontend\modules\ita\models\CpublicHealth;
use Yii;
use yii\helpers\Url;

class ItaController extends \yii\web\Controller
{

    public function getpublichealth($year)
    {
        $depment = CpublicHealth::find()->select(['departmentcode', 'departmentname'])->orderBy('departmentname ASC')->all();
        foreach ($depment as $n) {
            // $ndepartment[$n['departmentcode']] = $n['departmentname'];

            $ndepartment[] = ['label'=>$n['departmentname'],
            'url'=>Url::to(['','depm'=>$n['departmentcode'],'year'=>$year])];

            // $department[$n['departmentcode']] = $n['departmentname'];
        }
        return $ndepartment;
    }

    public function getnamepublichealth()
    {
        $depment = CpublicHealth::find()->select(['departmentcode', 'departmentname'])->orderBy('departmentname ASC')->all();
        foreach ($depment as $n) {
            // $ndepartment[$n['departmentcode']] = $n['departmentname'];

            $department[$n['departmentcode']] = $n['departmentname'];
        }
        return $department;
    }

    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }

    public function getSqlQueryData($sql, $bYear, $kid)
    {
        $conn = Yii::$app->db;
        
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        
        $data = $cmd->queryAll();

        // Trend chart column chart and gauge and map
        foreach ($data as $k => $v) {
            $gaugeData[$v['department']] = (float)$v['total_ratio'];;
        }

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }

    public function getTrendData($sql, $bYearPrev, $bYear)
    {
        $conn = Yii::$app->db;
        $request = Yii::$app->request;

        $cmd = $conn->createCommand($sql);
        // $cmd->bindValue(':queryYearPrev', 2553 );
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);
        $cmd->bindValue(':Department',$request->get('depm'));

        $trends = $cmd->queryAll();

        foreach ($trends as $t) {
            $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
            
        }
        return $trendData;
    }



    public function getAllData($sql, $bYearPrev, $bYear)
    {
        $conn = Yii::$app->db;
        $request = Yii::$app->request;

        $cmd = $conn->createCommand($sql);
        // $cmd->bindValue(':queryYearPrev', 2553 );
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);
        $cmd->bindValue(':Department',$request->get('depm'));

        $alls = $cmd->queryAll();

        foreach ($alls as $t) {
            $allData[] = ['name'=> 'คะแนน ปี '.$t['b_year'],'data'=> [(float) $t['total_ratio'],
                    (float) $t['external'],
                    (float) $t['internal'],
                    (float) $t['evidencebased']
                ]];

             $allData2[] = ['name'=> 'คะแนน ปี '.$t['b_year'],'data'=> [(float) $t['total_ratio'],
                    (float) $t['ita1'],
                    (float) $t['ita2'],
                    (float) $t['ita3'],
                    (float) $t['ita4'],
                    (float) $t['ita5']
                ]];                        
            
        }
        return (object) $q2 = [
            'allData2' => $allData2,
            'allData' => $allData];
    }

//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (strlen($request->get('year')) <> 4){            
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "144" : $request->get('kid');    
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');            
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)$bYear - 3;
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        $nameDepmt = $this->getpublichealth($year);
        $department = $this->getnamepublichealth();

        if ($kpi['sql_query_trend']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
            $q2 = $this->getAllData($kpi['sql_query_trend'], $bYearPrev, $bYear);
                $allData = $q2->allData;
                $allData2 = $q2->allData2;
        }

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'nameDepmt' =>  $nameDepmt,
            'colorBar' => $colorBar,
            'department' => $department,
            'allData' => $allData,
            'allData2' => $allData2
            ]);
    }

    
//------------------------------------------END-Index---------------------------------//    

}

// end class
