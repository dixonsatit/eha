<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class ProductchampionController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }

        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();
    }

    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);

        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }

        $data = $cmd->queryAll();
        $a=0;

        // Column chart and gauge and map
        foreach ($data as $k => $v) {
            $a+=1;

            $categoriesData[] = $v['product_name'];

            // $columnData[] = [$v['product_name'], (float) $v['total_ratio']];

            $v['satisfaction_north_ratio'] > 0 ? $Data_N[] = (float)$v['satisfaction_north_ratio'] : $Data_N[] = (float)0;
            $v['satisfaction_central_ratio'] > 0 ? $Data_C[] = (float)$v['satisfaction_central_ratio'] : $Data_C[] = (float)0;
            $v['satisfaction_northeast_ratio'] > 0 ? $Data_NE[] = (float)$v['satisfaction_northeast_ratio'] : $Data_NE[] = (float)0;
            $v['satisfaction_south_ratio'] > 0 ? $Data_S[] = (float)$v['satisfaction_south_ratio'] : $Data_S[] = (float)0;
            

            $v['target_value'] > 0 ? $Data_traget[] = (int)$v['target_value'] : $Data_traget[] = (int)0;
            $v['returned_value'] > 0 ? $Data_returned[] = (int)$v['returned_value'] : $Data_returned[] = (int)0;
            $v['satisfaction_value'] > 0 ? $Data_satisfaction[] = (int)$v['satisfaction_value'] : $Data_satisfaction[] = (int)0;

            $columnDataratio[] = [$v['product_name'], (float) $v['satisfaction_ratio']];

        }

        $columnData[] = ["name" => "ภาคเหนือ","data" => $Data_N];
        $columnData[] = ["name" => "ภาคกลาง","data" => $Data_C];
        $columnData[] = ["name" => "ภาคตะวันออกเฉียงเหนือ","data" => $Data_NE];
        $columnData[] = ["name" => "ภาคใต้","data" => $Data_S];

        $columnData2[] = ["name" => "เป้าหมาย","data" => $Data_traget];
        $columnData2[] = ["name" => "ผู้ตอบ","data" => $Data_returned];
        $columnData2[] = ["name" => "พึงพอใจ","data" => $Data_satisfaction];

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'columnData2' => $columnData2,
            'columnDataratio' => $columnDataratio,
            'categoriesData' => $categoriesData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null, $tambon = null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }


    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
        }

        return $trendData;
    }


//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $conn = Yii::$app->db;

        $sql_maxyear = "SELECT max(P.b_year) as maxyear FROM `s_product_name` P GROUP BY P.b_year ORDER BY maxyear DESC";
        $cmd_max = $conn->createCommand($sql_maxyear);
        $maxyear = $cmd_max->queryOne();
        $nowyear = ($maxyear['maxyear']-543);

        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$nowyear]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? $nowyear:strlen($request->get('year'))!==4 ? $nowyear : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "300" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        //
        $data = null;
        $columnData = null;
        $columnData2 = null;
        $columnDataratio = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
       
        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $columnData2 = $q->columnData2;
            $columnDataratio = $q->columnDataratio;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $categoriesData = $q->categoriesData;
        }

        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Trend Data
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'columnData2' => $columnData2,
            'columnDataratio' => $columnDataratio,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'categoriesData' => $categoriesData,
            'geo' => $geo,
            
            'nowyear'=> $nowyear,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'เขตสุภาพ']);
    }
//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    
    public function actionChangwat()
    {
        $request = Yii::$app->request;
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$nowyear]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? $nowyear:strlen($request->get('year'))!==4 ? $nowyear : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "300" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        //
        $region = $request->get('rg');
        
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid, $region);

        if ($kpi['sql_query_province']) {

            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where c.regioncode=:region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $bYear,$region);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');

        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'nowyear'=> $nowyear,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'จังหวัด']);
    }
//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    
    public function actionAmpur()
    {
        $request = Yii::$app->request;
        if ($request->get('cw') == null  | $request->get('year') == null ){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "300" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        //
        $changwat = $request->get('cw');
        
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null, $changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $bYear,null,$changwat);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));        
        // $mapUrl = json_encode(Url::toRoute('tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'อำเภอ']);
    }

//------------------------------------------END Ampur ----------------------------------//    


//------------------------------------------Tambon ------------------------------------//
    public function actionTambon()
    {
        $request = Yii::$app->request;
        if ($request->get('ap') == null | strlen($request->get('ap')) !== 4 | strlen($request->get('year')) !== 4 | $request->get('year') == null ){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "300" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        //
        $ampur = $request->get('ap');

        $bYear = intval($year) + 543;
        $bYearPrev = $bYear - 3;
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {

            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, null, null, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;

        }

        // MAP Layout
        $sql = 'select * from geojson_tambon g
                inner join ctambon a on a.tamboncodefull=g.areacode
                WHERE a.ampurcode =:ampur 
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null, null, $ampur);

        // Trend
        if ($kpi['sql_query_trend_tambon']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $bYear, null, null, $ampur);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute('mooban', true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'ap' => $ampur,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
           
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'ตำบล']);
    }
    
//------------------------------------------END Tambon ----------------------------------//   

}

// end class
