<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class ElderlybehavController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }

    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        // if ($region !== null) {
        //     $cmd->bindValue(':region', $region);
        // }
        // if ($changwat !== null) {
        //     $cmd->bindValue(':changwat', $changwat);
        // }
        // if ($ampur !== null) {
        //     $cmd->bindValue(':ampur', $ampur);
        // }

        

        $data = $cmd->queryAll();
// print_r($data); die();
        // Column chart and gauge and map
        // foreach ($data as $k => $v) {
        //     // $columnData[] = [$v['areaname_new'], (float) $v['total_ratio']];
        //     $target += (float) $v['target_ratio'];
        //     $result += (float) $v['result'];

        //     // if ($v['areacode'] !== 'TOTAL') {
        //     //     // Map data
        //     //     $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
        //     //         "geocode" => $v['areacode'], "name" => $v['areaname_new'],
        //     //         "color" => $this->getMapColor($bYear, $kid, $v['total_ratio'])];
        //     // }
        // }

        // Gauge chart
        // $gaugeData = @(float) ($result / $target) * 100;
        return (object) $q = [
            'data' => $data];


        // return (object) $q = [
        //     'data' => $data,
        //     'columnData' => $columnData,
        //     'propData' => $propData,
        //     'gaugeData' => $gaugeData];
    }


    // public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null, $tambon = null)
    // {
    //     $conn = Yii::$app->db;

    //     // $sql = 'select * from geojson  where areatype=1';
    //     $cmd = $conn->createCommand($sql);
    //     if ($region !== null) {
    //         $cmd->bindValue(':region', $region);
    //     }
    //     if ($changwat !== null) {
    //         $cmd->bindValue(':changwat', $changwat);
    //     }
    //     if ($ampur !== null) {
    //         $cmd->bindValue(':ampur', $ampur);
    //     }
    //     $mdata = $cmd->queryAll();

    //     foreach ($mdata as $i => $m) {
    //         $gd[] = [
    //             "type" => "Feature",
    //             "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
    //             ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
    //             "geometry" => json_decode($m['geojson']),
    //         ];
    //     }

    //     return $geo = json_encode($gd);
    // }


    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        // print_r($sql); die();
        
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();
        $a = 0;

        // print_r($trends); die();
        // Trend chart
        foreach ($trends as $t) {
            // print_r($t); die();
            // $categoriesData[] = $t['b_year'];


            $datat[$a]=(float)$t['target_ratio'];
            $datar[$a]=(float)$t['result_ratio'];
            $a+=1;
            // print_r($t['b_year']); die();
            // $categoriesData[] = ['เป้าหมาย','ผลการดำเนินงาน'];
            // $datat[$a]=(float)$t['ratio'];
            // $trendData[] = [
            //     "name" => $t['b_year'],
            //     "data" =>  $datat
    
            // ];
            // $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
        }
        $trendData[] = ["name" => 'เป้าหมาย',"data" => $datat];
        $trendData[] = ["name" => 'ผลการดำเนินงาน',"data" => $datar];

        // print_r($categoriesData); die();
        return $trendData;
        // return $datat;
        // return $categoriesData;
        
    }


//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "90" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)$bYear - 3;
        //
        
        $data = null;
        // $columnData = null;
        $trendData = null;
        $categoriesData = null;
        $kpi = null;
        // $gaugeData = null;
        // $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

       
        // hart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;

            // $columnData = $q->columnData;
            // $propData = $q->propData;
            // $gaugeData = $q->gaugeData;
        }

        foreach ($data as $k => $b) {
                $categoriesData[]=$b['b_year'];
        }        

        // Map Layout
        // $sql = 'select * from geojson where areatype=1';
        // $geo = $this->getMapLayout($sql, $propData);

        // Trend Data
        if ($kpi['sql_query']) {
            $trendData = $this->getTrendData($kpi['sql_query'], $bYearPrev, $bYear);
            // $categoriesData = $this->getTrendData($kpi['sql_query'], $bYearPrev, $bYear);
        }

        // Gen URL
        // $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
        //     'kid' => $kid], true));
        // $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'categoriesData' => $categoriesData,
            'trendData' => $trendData]);
    }

//------------------------------------------END-Index---------------------------------//    

}

// end class
