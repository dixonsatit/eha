<?php
namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

use frontend\modules\dashboard\models\DashboardServices;

class DspmController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }

    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }

    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }

    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }

    public function getSqlQueryData($sql, $bYear, $quarter, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        // Chart title
        $quarterName = 'ไตรมาสที่ ' . $quarter . ' ปีงบประมาณ ' . $bYear;

        // Define variables to get quarter's values
        $target = null;
        $result = null;
        //
        $targetq = 'targetq'.$quarter;
        $resultq = 'resultq'.$quarter;
        $ratioq = 'ratioq'.$quarter;
        
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();
        
        // Column chart and gauge and map
        foreach ($data as $k => $v) {

            $target += (float) $v[$targetq];
            $result += (float) $v[$resultq];

            $columnData[] = [$v['areaname_new'], (float)$v[$ratioq]];
            $color[] = $this->getMapColor($bYear, $kid, $v[$ratioq]);

            if ($v['areacode'] !== 'TOTAL') {
                // Map data
                $propData[$v['areacode']] = ["value" => (float) $v[$ratioq],
                    "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                    "color" => $this->getMapColor($bYear, $kid, $v[$ratioq])];
            }
        }

        // Map data out
        //$propData[13] = ["value" => 0, "geocode" => "13", "name" => "เขต 13"];
        // Gauge chart
        $gaugeData = ($target) ? (float)($result / $target) * 100:0;

        // Packs column data and color into a 2D array
        $columnData = ['column' => $columnData, 'color' => $color];
        
        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData,
            'quarterName'=>$quarterName
        ];
    }

    public function getMapLayout($sql, $propData, $region = null, $changwat = null,$ampur=null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }

    public function getTrendData($sql, $bYearPrev, $bYear, $region = null, $changwat = null, $ampur = null)
    {
        $year = $bYear - 543;
        $cond = $this->getTiming($year);
        
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        //จัดรูปแบบ trend chart
        foreach ($trends as $t) {
            if($t['b_year']==$bYear){
                $ratioq = 'ratioq'.$cond['data_quarter'];
            }else{
                $ratioq = 'ratioq4';
            }
            $trendData[] = [$t['b_year'], (float) $t[$ratioq]];
        }
        return $trendData;
    }
    
    protected function getTiming($year){
        $month = date('m');
        
        if($year==date('Y')){
            $data_year = $year;
            if($month == '1' || $month == '2' || $month == '3'){
                $data_quarter = 1;
            }else if($month == '4' || $month == '5' || $month == '6'){
                $data_quarter = 2;
            }else if($month == '7' || $month == '8' || $month == '9'){
                $data_quarter = 3;
            }else {
                $data_quarter = 4;
            }
        }else {
            if(($year==(date('Y')+1)) && ($month == '10' || $month == '11' || $month == '12')){
                $data_year = $year-1;
            }else{
                $data_year = $year;
            }
            $data_quarter = 4;
        }
        
        return ['data_year'=>$data_year,'data_quarter'=>$data_quarter];
        
    }

//-------------------------------------------Index---------------------------------//       

    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "56" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');      
//        $bYear = $year+543;
        $bYearPrev = (int) ($bYear) - 10;
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // กำหนดค่า data_year, data_quarter ที่จะนำข้อมูลมาแสดงผล
        $cond = $this->getTiming($year);
        $bYear = (int)$cond['data_year']+543;
        $data_quarter = $cond['data_quarter'];

        //Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $data_quarter, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName = $q->quarterName;
        } else {
			return $this->redirect(['index']);		
        }
        
        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Region Trend
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'quarterName'=>$quarterName,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'เขตสุขภาพ']);
    }

//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    

    public function actionChangwat()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "56" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');      
        $bYearPrev = (int) ($bYear) - 3;
        //
        $region = substr("00".($request->get('rg')),-2);
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // กำหนดค่า data_year, data_quarter ที่จะนำข้อมูลมาแสดงผล
        $cond = $this->getTiming($year);
        $bYear = (int)$cond['data_year']+543;
        $data_quarter = $cond['data_quarter'];

        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $data_quarter, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;	
            $quarterName=$q->quarterName;	
		}
		if ($gaugeData == 0) {
			return $this->redirect(['index']);
		}
        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join cchangwat c on c.changwatcode=g.areacode
        //         where areatype=2 and c.regioncode=:region';
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where c.regioncode = :region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $bYear, $region);
        }        

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');
				
        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'quarterName'=>$quarterName,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'จังหวัด']);
    }

//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    

    public function actionAmpur()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "56" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');      
//        $bYear = $year==null ? (date("m")>=10 ? date("Y")+543+1 : date("Y")+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYearPrev = (int) ($bYear) - 3;
        //
        $changwat = substr("00".($request->get('cw')),-2);				
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        //Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // กำหนดค่า data_year, data_quarter ที่จะนำข้อมูลมาแสดงผล
        $cond = $this->getTiming($year);
        $bYear = (int)$cond['data_year']+543;
        $data_quarter = $cond['data_quarter'];

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear,$data_quarter, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName = $q->quarterName;
        }
		if ($gaugeData == 0) {
			return $this->redirect(['index']);
		}

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join campur a on a.ampurcodefull=g.areacode
        //         where  areatype=3 and a.changwatcode=:changwat
        //         and a.flag_status=0';
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null,$changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {            
            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $bYear, null ,$changwat);
        }	
        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'quarterName'=>$quarterName,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'อำเภอ']);
    }

//------------------------------------------END Ampur ----------------------------------//    


//------------------------------------------Tambon ------------------------------------//

    public function actionTambon()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "56" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');      
//        $bYear = $year==null ? (date("m")>=10 ? date("Y")+543+1 : date("Y")+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYearPrev = (int) ($bYear) - 3;
        //
        $ampur = substr("0000".($request->get('ap')),-4);
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        //Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // กำหนดค่า data_year, data_quarter ที่จะนำข้อมูลมาแสดงผล
        $cond = $this->getTiming($year);
        $bYear = (int)$cond['data_year']+543;
        $data_quarter = $cond['data_quarter'];

        if ($kpi['sql_query_tambon']) {
            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear,$data_quarter, $kid, null, null,$ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
            $quarterName = $q->quarterName;
        }
		if ($gaugeData == 0) {
			return $this->redirect(['index']);
		}		
        // MAP Layout
        $sql = 'select * from geojson_tambon g 
        inner join ctambon t on t.tamboncodefull=g.areacode
        where ampurcode=:ampur';
        $geo = $this->getMapLayout($sql, $propData, null,null,$ampur);

        // Trend
		if ($kpi['sql_query_trend_tambon']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $bYear, null, null,$ampur);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['mooban', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'quarterName'=>$quarterName,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'ตำบล']);
    }

//------------------------------------------END Tambon ----------------------------------//   

} // end class
