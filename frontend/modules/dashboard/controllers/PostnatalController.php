<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;
use yii\web\Cookie;

class PostnatalController extends \yii\web\Controller
{
    public function actionSetcookie($page,$name,$val,$region = null,$chw = null){
        
        $cookie = new Cookie([
            'name' => $name,
            'value' => $val,
            'expire' => time() + 86400 * 365,
        ]);
        \Yii::$app->getResponse()->getCookies()->add($cookie);
        return $this->redirect([$page,'region'=>$region,'chw'=>$chw]);
    }
    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        //$ranges = KpiRange::find()->where('kpi_template_id=:kid',
        //    [':kid' => $i])->all();

        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }

    public function getPlotBands($bYear, $kid)
    {
        //$bands = KpiRange::find()->where('kpi_template_id=:kid',
        //    [':kid' => $i])->all();

        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }

    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;
        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();
    }


    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        // Column chart and gauge and map
        foreach ($data as $k => $v) {
            $columnData[] = [$v['areaname_new'], @(float) $v['total_ratio']];
            $target += @(float) $v['target'];
            $result += @(float) $v['result'];

            if ($v['areacode'] !== 'TOTAL') {
                //map data
                $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                    "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                    "color" => $this->getMapColor($bYear, $kid, $v['total_ratio'])];
            }
        }

        // Gauge chart
        $gaugeData = ($target == '' OR $target == 0) ? 0 : @(float)($result / $target) * 100;	

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }
        return $geo = json_encode($gd);
    }

    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
        }

        return $trendData;
    }

//-------------------------------------------Index---------------------------------//       
    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "6" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? (date("m")<10 ? date("Y")-1 : date("Y")) : $request->get('year');
		$year = substr("0000".$year,-4);
        // Fiscal Year
        $bYear = (int)(date("m")<10 ? $year-1 : $year)+543;
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2561;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $year = (\Yii::$app->getRequest()->getCookies()->getValue('year')-543);
        if(!\Yii::$app->getRequest()->getCookies()->getValue('year')){
            return $this->redirect(['setcookie','page'=>'process', 'name' => 'year','val'=>($year)+543]);  
        } 
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        //
        if($request->get('year')==null||strlen($request->get('year'))!==4)
        {
          return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $trendYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'year' => $year+543,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'เขตสุขภาพ']);
    }

//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    
    public function actionChangwat()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "6" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? (date("m")<10 ? date("Y")-1 : date("Y")) : $request->get('year');
		$year = substr("0000".$year,-4);
        // Fiscal Year
        $bYear = (int)(date("m")<10 ? $year-1 : $year)+543;
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2561;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $region = $request->get('rg');
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        //
        if($request->get('year')==null||$request->get('rg')==null||$request->get('rg')=='xx'||strlen($request->get('rg'))!==2||strlen($request->get('year'))!==4)
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join cchangwat c on c.changwatcode=g.areacode
        //         where areatype=2 and c.regioncode=:region';
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where c.regioncode = :region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $trendYear, $region);
        }        

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');

        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'จังหวัด']);
    }

//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    
    public function actionAmpur()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "6" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? (date("m")<10 ? date("Y")-1 : date("Y")) : $request->get('year');
		$year = substr("0000".$year,-4);
        // Fiscal Year
        $bYear = (int)(date("m")<10 ? $year-1 : $year)+543;
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2561;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $changwat = $request->get('cw');
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        //
        if($request->get('year')==null||$request->get('cw')==null||$request->get('cw')=='xx'||strlen($request->get('cw'))!==2||strlen($request->get('year'))!==4)
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join campur a on a.ampurcodefull=g.areacode
        //         where  areatype=3 and a.changwatcode=:changwat
        //         and a.flag_status=0';
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null,$changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {            
            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'] ,$bYearPrev, $trendYear, null, $changwat);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'อำเภอ']);
    }

//------------------------------------------END Ampur ----------------------------------//    


//------------------------------------------Tambon ------------------------------------//
    public function actionTambon()
    {
        $request = Yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "6" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? (date("m")<10 ? date("Y")-1 : date("Y")) : $request->get('year');
		$year = substr("0000".$year,-4);
        // Fiscal Year
        $bYear = (int)(date("m")<10 ? $year-1 : $year)+543;
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2561;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $ampur = $request->get('ap');
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        if($request->get('year')==null||$request->get('ap')==null||$request->get('ap')=='xx'||strlen($request->get('ap'))!==4||strlen($request->get('year'))!==4)
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {
            $q = $this->getSqlQueryData($kpi['sql_query_tambon'] ,$bYear, $kid, null, null,$ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        $sql = 'select * from geojson_tambon g 
        inner join ctambon t on t.tamboncodefull=g.areacode
        where ampurcode=:ampur';
        $geo = $this->getMapLayout($sql, $propData, null,null,$ampur);

        //trend
        if ($kpi['sql_query_trend_tambon']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $trendYear, null, null, $ampur);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['mooban', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'ตำบล']);
    }

//------------------------------------------END Tambon ----------------------------------//   

} // end class
