<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class SchoolagebehavController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }


    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();
        // Trend chart column chart and gauge and map
        foreach ($data as $k => $v) {
            $columnData0Q1[]=[$v['areaname_new'],(float) $v['ratio0']];
            $columnData0Q2[]=[$v['areaname_new'],(float) $v['ratio2']];
//            $target+=$v['total_target'];
//            $result+=$v['total_result'];
        }
		 foreach ($data as $k => $v) {
            $columnData1Q1[]=[$v['areaname_new'],(float) $v['ratio1']+$v['ratio3']];
            $columnData1Q2[]=[$v['areaname_new'],(float) $v['ratio2']+$v['ratio1']];
			$columnData1Q3[]=[$v['areaname_new'],(float) $v['ratio3']+$v['ratio2']];
//            $target+=$v['total_target'];
//            $result+=$v['total_result'];
           
        }
		 foreach ($data as $k => $v) {
            $columnData2Q1[]=[$v['areaname_new'],(float) $v['ratio1']];
            $columnData2Q2[]=[$v['areaname_new'],(float) $v['ratio2']];
			$columnData2Q3[]=[$v['areaname_new'],(float) $v['ratio3']];
//            $target+=$v['total_target'];
//            $result+=$v['total_result'];

            if ($v['areacode'] !== 'TOTAL') {
                //map data
                $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                    "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                    "color" => $this->getMapColor($bYear, $kid, $v['total_ratio'])];
            }
        }		

		// Gauge chart   
		$gaugeData = (float) $v['total_ratio'];
		
		//columnData 2 Qt.
        $columnData0=['Q1'=>$columnData0Q1,'Q2'=>$columnData0Q2];
		$columnData1=['Q1'=>$columnData1Q1,'Q2'=>$columnData1Q2,'Q3'=>$columnData1Q3];
		$columnData2=['Q1'=>$columnData2Q1,'Q2'=>$columnData2Q2,'Q3'=>$columnData2Q3];
		
		return (object) $q = [
            'data' => $data,
            'columnData0' => $columnData0,
			'columnData1' => $columnData1,
			'columnData2' => $columnData2,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }



    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            $trendDataQ1[] = [$t['b_year'], (float) $t['ratio1']];
			$trendDataQ2[] = [$t['b_year'], (float) $t['ratio2']];
			$trendDataQ3[] = [$t['b_year'], (float) $t['ratio3']];
        }
		$trendData = ['Q1'=>$trendDataQ1,'Q2'=>$trendDataQ2,'Q3'=>$trendDataQ3];	
				
        return $trendData;
    }

//-------------------------------------------Index---------------------------------//       

    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
		$year = substr("0000".$year,-4);
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "81" : $request->get('kid');
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)$bYear - 3;
        //
        $data = null;
        $columnData0 = null;
		$columnData1 = null;
		$columnData2 = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData0 = $q->columnData0;
			$columnData1 = $q->columnData1;
			$columnData2 = $q->columnData2;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        } else {
			return $this->redirect(['index']);		
		}
        //Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        //Region Trend
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
        }

        //gen URL
//        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
		$mapUrl = json_encode(Url::toRoute(['index', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');
				
        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData0' => $columnData0,
			'columnData1' => $columnData1,
			'columnData2' => $columnData2,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
//            'mapUrl' => $mapUrl,
			'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'scope'=>'เขตสุขภาพ']);
    }

//------------------------------------------END-Index---------------------------------//    

} // end class
