<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class PmqaController extends \yii\web\Controller
{
    public function getSqlQueryData($sql,$bYear)
    {
        $conn = Yii::$app->db;
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        $data = $cmd->queryAll();
        foreach ($data as $k => $v) {
            $pieData[]=[$v['dept_name'],(float) $v['result']];
        }
		return (object) $q = [
            'data' => $data,
            'pieData' => $pieData
		];
    }

//-------------------------------------------Index---------------------------------//       

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $conn = Yii::$app->db;

        $sql_maxyear = "SELECT max(P.b_year) as maxyear FROM s_pmqa P GROUP BY P.b_year ORDER BY maxyear DESC";
        $cmd_max = $conn->createCommand($sql_maxyear);
        $maxyear = $cmd_max->queryOne();

        $year = ($maxyear['maxyear']-543);
		if (strlen($request->get('year')) <> 4) { 
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }        
        //
		$bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;        
        $data = null;
        $pieData = null;
        $kpi = null;
		$kpi['maxyear'] = $bYear;
		$kpi['sql_name'] = 'ตัวชี้วัดที่ 20 การผ่านเกณฑ์คุณภาพการบริหารจัดการภาครัฐ';
		$kpi['sql_template_name'] = 'ตัวชี้วัดที่ 20 การผ่านเกณฑ์คุณภาพการบริหารจัดการภาครัฐ';
		$kpi['sql_query'] = 'SELECT Z.dept_name,K.b_year,K.date_com,K.result 
									FROM s_pmqa K LEFT JOIN s_pmqa_depart Z ON K.area_code = Z.dept_code 
									WHERE b_year = :queryYear 
									GROUP BY K.area_code 
									ORDER BY K.result DESC LIMIT 5';
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear);
            $data = $q->data;
            $pieData = $q->pieData;
        } 
		else {
			return $this->redirect(['index']);		
		}	
        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'pieData' => $pieData]);
    }
}
