<?php

namespace frontend\modules\dataxchange\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Cookie;
/**
 * DataxchangeController implements the CRUD actions for Skpiheight05 model.
 */
class DataxchangeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Skpiheight05 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $conn = Yii::$app->db;
        $data=null;
        // $sql = 'select * from table_listable';
        // $cmd = $conn->createCommand($sql);
        // //$cmd->bindValue(':atype', $assess_type);
        // $data =$cmd->queryAll();

        
        return $this->render('index', [
            'data' => $data,
            
        ]);
    }
    public function actionS_anc()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_anc S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        
        $data =$cmd->queryAll();

        return $this->render('s_anc', [
            'data' => $data,
            
        ]);
    }
    public function actionS_anc_hct()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_anc_hct S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_anc_hct', [
            'data' => $data,
            
        ]);
    }
    public function actionS_anc4()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_anc4 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_anc4', [
            'data' => $data,
        ]);
    }
    public function actionS_anc5()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_anc5 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_anc5', [
            'data' => $data,
        ]);
    }
    public function actionS_child()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_child S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_child', [
            'data' => $data,
        ]);
    }
    public function actionS_childdev()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_childdev S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_childdev', [
            'data' => $data,
        ]);
    }
    public function actionS_childdev_specialpp()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_childdev_specialpp S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_childdev_specialpp', [
            'data' => $data,
        ]);
    }
    public function actionS_childdev_specialpp48()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_childdev_specialpp48 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_childdev_specialpp48', [
            'data' => $data,
        ]);
    }
    public function actionS_dental_22()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_dental_22 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_dental_22', [
            'data' => $data,
        ]);
    }
    public function actionS_dental_65()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_dental_65 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_dental_65', [
            'data' => $data,
        ]);
    }
    public function actionS_dental_66()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_dental_66 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_dental_66', [
            'data' => $data,
            
        ]);
    }
    public function actionS_dental_67()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_dental_67 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_dental_67', [
            'data' => $data,
        ]);
    }
    public function actionS_dental_caries_free()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_dental_caries_free S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_dental_caries_free', [
            'data' => $data,
        ]);
    }
    public function actionS_ferous6_12()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_ferous6_12 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_ferous6_12', [
            'data' => $data,
        ]);
    }
    public function actionS_ferous6_5()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_ferous6_5 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_ferous6_5', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_anc_iodine()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_anc_iodine S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_anc_iodine', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_anc12()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_anc12 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_anc12', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_bmi()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_bmi S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_bmi', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_child_specialpp()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_child_specialpp S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_child_specialpp', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_childdev()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_childdev S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_childdev', [
            'data' => $data,
       
            ]);
    }
    public function actionS_kpi_childdev_pro()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_childdev_pro S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_childdev_pro', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_dental40()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_dental40 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_dental40', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_food()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_food S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_food', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_height05()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_height05 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_height05', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_height614()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_height614 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_height614', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_labor_fp_normal()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_labor_fp_normal S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_labor_fp_normal', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_labor_fp_semi_pm()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_labor_fp__semi_pm S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_labor_fp__semi_pm', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_labor_repeate()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_labor_repeate S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_labor_repeate', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_lborn10_19()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_lborn10_19 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_lborn10_19', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_obes5_14()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_obes5_14 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_obes5_14', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_slender6_14()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_slender6_14 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_slender6_14', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_slender6_14_1()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_slender6_14_1 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_slender6_14_1', [
            'data' => $data,
        ]);
    }
    public function actionS_kpi_labor_fp()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_kpi_labor_fp S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_kpi_labor_fp', [
            'data' => $data,
        ]);
    }
    public function actionS_labor_sex_age()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_labor_sex_age S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_labor_sex_age', [
            'data' => $data,
        ]);
    }
    public function actionS_labor1014()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_labor1014 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_labor1014', [
            'data' => $data,
        ]);
    }
    public function actionS_labor1519()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_labor1519 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_labor1519', [
            'data' => $data,
        ]);
    }
    public function actionS_labor20_repeate()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_labor20_repeate S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_labor20_repeate', [
            'data' => $data,
        ]);
    }
    public function actionS_labor20_repeate_new()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_labor20_repeate_new S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_labor20_repeate_new', [
            'data' => $data,
        ]);
    }
    public function actionS_low_risk05()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_low_risk05 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_low_risk05', [
            'data' => $data,
        ]);
    }
    public function actionS_midyearpopmoph()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_midyearpopmoph S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_midyearpopmoph', [
            'data' => $data,
        ]);
    }
    public function actionS_nutrition05()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_nutrition05 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_nutrition05', [
            'data' => $data,
        ]);
    }
    public function actionS_nutrition15()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_nutrition15 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_nutrition15', [
            'data' => $data,
        ]);
    }
    public function actionS_nutrition6()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_nutrition6 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_nutrition6', [
            'data' => $data,
        ]);
    }
    public function actionS_obesity05()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_obesity05 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_obesity05', [
            'data' => $data,
        ]);
    }
    public function actionS_pop_sex_age_moph()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_pop_sex_age_moph S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_pop_sex_age_moph', [
            'data' => $data,
        ]);
    }
    public function actionS_postnatal()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_postnatal S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

        return $this->render('s_postnatal', [
            'data' => $data,
        ]);
    }
    public function actionS_thin05()
    {
        $conn = Yii::$app->db;
        $request=\yii::$app->request;
        $syear=$request->get('syear');
        $eyear=$request->get('eyear');
        $region=$request->get('region');
        //$data=null;

        $sql = 'SELECT * FROM s_thin05 S
        LEFT JOIN (SELECT * FROM cregion WHERE region_code  = :region) G ON G.region_code=RIGHT(S.areacode,2)
        WHERE b_year BETWEEN :syear AND :eyear';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':syear', $syear);
        $cmd->bindValue(':eyear', $eyear);
        $cmd->bindValue(':region', $region);
        $data =$cmd->queryAll();

// print_r($sql);
// print_r($syear);
// print_r($eyear);
// die();
        return $this->render('s_thin05', [
            'data' => $data,
            
        ]);
    }

   
}
