<?php
namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class PhimsController extends \yii\web\Controller
{

    public function getTypeCaseData($bYear, $region){
        $conn = Yii::$app->db;

        $sqlTypeCase = 'SELECT
        COUNT(kpi_mch_mmr.id) as sumnum,
        kpi_mch_mmr_causetype.cause_type_desc as nametype
        FROM kpi_mch_mmr
        INNER JOIN kpi_mch_mmr_cause ON kpi_mch_mmr.cause_id = kpi_mch_mmr_cause.cause_id
        INNER JOIN kpi_mch_mmr_causetype ON kpi_mch_mmr_cause.cause_type = kpi_mch_mmr_causetype.cause_type_id
        INNER JOIN cchangwat ON cchangwat.changwatcode = kpi_mch_mmr.areacode
        WHERE (CASE WHEN kpi_mch_mmr.month_id>=10 THEN (kpi_mch_mmr.year_id+1)
           ELSE kpi_mch_mmr.year_id END) = :year ';

        if($region==null){
                
            $sqlTypeCase.='GROUP BY kpi_mch_mmr_causetype.cause_type_desc';

        }else{

            $sqlTypeCase.='AND cchangwat.regioncode = :region 
            GROUP BY kpi_mch_mmr_causetype.cause_type_desc';
        }

        $cmd = $conn->createCommand($sqlTypeCase);
        $cmd->bindValue(':year', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        $typecase= $cmd->queryAll();

        foreach ($typecase as $k => $v) {
            $sumnum += (int)$v['sumnum'];
            $Typecase[] = [
                "name" => $v['nametype'],
                "y" => (int)$v['sumnum']
            ];
        }

        return (object) $q1 = [
            'sumnum' => $sumnum,
            'Typecase' => $Typecase];
        }



        public function getCaseData($bYear, $region){
            $conn = Yii::$app->db;
    
            $sqlTypeCase = 'SELECT
            COUNT(kpi_mch_mmr.id) as sumnum,        
            kpi_mch_mmr_cause.cause_desc as namecause,
            kpi_mch_mmr_causetype.cause_type_id as nametype
            FROM kpi_mch_mmr
            INNER JOIN kpi_mch_mmr_cause ON kpi_mch_mmr.cause_id = kpi_mch_mmr_cause.cause_id
            INNER JOIN kpi_mch_mmr_causetype ON kpi_mch_mmr_cause.cause_type = kpi_mch_mmr_causetype.cause_type_id
            INNER JOIN cchangwat ON cchangwat.changwatcode = kpi_mch_mmr.areacode
            WHERE (CASE WHEN kpi_mch_mmr.month_id>=10 THEN (kpi_mch_mmr.year_id+1)
               ELSE kpi_mch_mmr.year_id END) = :year ';
    
            if($region==null){
                    
                $sqlTypeCase.='GROUP BY kpi_mch_mmr_cause.cause_desc';
    
            }else{
    
                $sqlTypeCase.='AND cchangwat.regioncode = :region 
                GROUP BY kpi_mch_mmr_cause.cause_desc';
            }
            $sqlTypeCase.=' ORDER BY sumnum ASC, cause_desc ASC';

            $cmd = $conn->createCommand($sqlTypeCase);
            $cmd->bindValue(':year', $bYear);
            if ($region !== null) {
                $cmd->bindValue(':region', $region);
            }
            $typecase= $cmd->queryAll();
            // $i=0;
            $bar_colr=[
                'DIRECT'=>'#57c466',
                'INDIRECT'=>'#427fe5',
                'UNSPECIFIED'=>'#b05f00'
            ];
            foreach ($typecase as $k => $v) {
                $namecause[] = $v['namecause'];
                $numData[] = (int)$v['sumnum'];
                $colorBar[] =$bar_colr[$v['nametype']];
                // $Typecase2[] = [
                //     // "name" => $v['namecause'],
                //     "data" => (int)$v['sumnum']
                //     // "color" => ""
                // ];
                
            //    $i+1;
            }
    
            return (object) $q2 = [
                'namecause' => $namecause,
                'numData' => $numData,
                'colorBar' => $colorBar];
    
            }

            

    public function getEmptyData($eYear){

        $conn = Yii::$app->db;
        // $emptydatas = null;

        

        $sql = 'select SUM(M.m10+M.m11+M.m12+M.m1+M.m2+M.m3+M.m4+M.m5+M.m6+M.m7+M.m8+M.m9) as sumlive
                FROM moi_livebirths M WHERE M.`Year` = :year ';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $eYear);
        $emptydata = $cmd->queryAll();

        //  print_r($eYear);//die();
        if($emptydata != null && $emptydata <> 0){
            foreach ($emptydata as $k => $v) {
                $emptydatas += $v['sumlive'];
            }
         }
        //  print_r($eYear); die();
        $datayear = ($emptydatas == null || $emptydatas == 0) ?  ($eYear-1) :  $eYear; 
//  print_r($datayear); die();

        return $datayear;
        
    }

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }

    public function getSqlQueryData($sql, $bYear, $lYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        // print_r($region);die();
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        // $cmd->bindValue(':livebirthYear', $lYear);
        
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        //จัดรูปแบบ column chart and gauge and map
        foreach ($data as $k => $v) {

            if($v['areacode'] !== "13"){
                $columnData[] = [$v['areaname_new'], @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2)];
                $columnColor[]= $this->getMapColor($bYear,$kid, @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2));
    
                // $columnData[] = [$v['areaname_new'], @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2)];
                // $target += @(float) $v['target'];
                // $result += @(float) $v['result'];

                if ($v['areacode'] !== 'TOTAL') {
                    //map data
                    $propData[$v['areacode']] = ["value" => @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2),
                        "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                        "color" => $this->getMapColor($bYear, $kid, @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2) )];
                }

            }else if($v['areacode'] == "13"){
                $bb1 += $v['BabyPos'];
                $bb2 += $v['BabyPCR2'];

                $columnData[12] = ["สสม.", @(float) number_format($bb1/$bb2*100,2)];
                if ($v['areacode'] !== 'TOTAL') {
                    //map data
                    $propData[$v['areacode']] = ["value" => @(float) number_format($bb1/$bb2*100,2),
                        "geocode" => $v['areacode'], "name" => "สสม.",
                        "color" => $this->getMapColor($bYear, $kid, @(float) number_format($bb1/$bb2*100,2) )];
                }

            }
            // $n_name = $v['areaname_new'];
            
        }
        $columnData=['Data'=>$columnData, 'Color'=>$columnColor];

        // Gauge chart
        $gaugeData = @(float)  number_format($v['BabyPos']/$v['BabyPCR2']*100,2);
        
        if ($region == '13') {

            $propData['10'] = ["value" => number_format($v['BabyPos']/$v['BabyPCR2']*100,2),
            "geocode" => $v['areacode'], "name" => "กรุงเทพมหานคร",
            "color" => $this->getMapColor($bYear, $kid, @(float) number_format($v['BabyPos']/$v['BabyPCR2']*100,2) )];
        }

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null, $tambon = null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }
        // print_r($sql); die();
        return $geo = json_encode($gd);
    }

    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        // print_r($region);
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();
        // print_r($trends);
        //จัดรูปแบบ trend chart
        foreach ($trends as $t) {
            $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
            
        }
//         print_r($cmd);
// print_r($trendData);
        return $trendData;
    }



    public function getElement($bYear, $kpiName, $kid, $region=null, $changwat=null, $ampur=null)
    {
        // print_r($region); die();
        $year=$bYear-543;
        if ($region !== null) { // Changwat
            $areaName='ศูนย์อนามัยที่ '.(int)$region;
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='จังหวัด';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }else if($changwat !== null){   // Ampur
            $conn = Yii::$app->db;
            $sql = 'SELECT regioncode,changwatname FROM cchangwat WHERE changwatcode=:changwat';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':changwat', $changwat);
            $CW = $cmd->queryOne();
            $areaName='จังหวัด'.$CW['changwatname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='อำเภอ';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$CW['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$CW['regioncode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($ampur !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM campur A INNER JOIN cchangwat C ON A.changwatcode=C.changwatcode '
                    . 'WHERE A.ampurcodefull=:ampur';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':ampur', $ampur);
            $AP = $cmd->queryOne();
            $areaName = (substr($AP['ampurname'], 0, 3)!=='เขต')? 'อำเภอ'.$AP['ampurname']:$AP['ampurname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$AP['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$AP['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$AP['changwatname'] ,'url' => ['ampur?year='.$year.'&cw='.$AP['changwatcode']]];
            $breadcrumb[]=['label' => $areaName];
        }else{  // Index
            $areaName='ระดับเขตสุขภาพ';
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='เขตสุขภาพ';
            //$breadcrumb[]=['label' => 'ระดับประเทศ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ'];
        }
        $element=[
            'areaName' => $areaName,
            //'yearName' => $yearName,
            //'tableAreaText' => $tableAreaText,
            'breadcrumb' => $breadcrumb,
        ];
        return $element;
            
    }


//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;

        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }else{}

        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');

        // $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? (date("m")>=10?(int)date("Y")+1:date("Y")) : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "5" : $request->get('kid');                
        // $bYear = (int) ($year) + 543;
        
        // $bYear = $year==null ? (date("m")>=10 ? (date("m")<10 ? date("Y") : date("Y")+1)+543+1 : (date("m")<10 ? date("Y") : date("Y")+1)+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYear = ($year+543);
        
        // $lYear=$this->getEmptyData($bYear);

        // print_r($bYear); die();
        // $typeCase = $this->getTypeCaseData($bYear, $region=null);

        $sumnum = null;
        $Typecase = null;
        
        $bYearPrev = (int) ($bYear) - 4;
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        $kpi = $this->getKpiData($year, $kid);


        //Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $lYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // Map Layout
        $sql = 'select * from geojson where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid);

        // trendData
        if ($kpi['sql_query_trend']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear);
        }

        //gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'Typecase' => $Typecase,
            'sumnum' => $sumnum,
            'numData' => $numData,
            'namecause' => $namecause,
            'colorBar' => $colorBar,
            
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
            'lYear'=>$lYear]);
    }

    
//------------------------------------------END-Index---------------------------------//    



//----------------------------------------------Changwat----------------------------------//    

    public function actionChangwat()
    {
        
        $request = Yii::$app->request;

        // print_r(strlen($request->get('rg')));
        // die();
        
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }else{}

        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');

        // $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? (date("m")>=10?(int)date("Y")+1:date("Y")) : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "5" : $request->get('kid');                
        // $bYear = (int) ($year) + 543;
        
        // $bYear = $year==null ? (date("m")>=10 ? (date("m")<10 ? date("Y") : date("Y")+1)+543+1 : (date("m")<10 ? date("Y") : date("Y")+1)+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYear = ($year+543);
        
        // $lYear=$this->getEmptyData($bYear);
        // Fix KPI ID                
        $region = $request->get('rg');
        $typeCase = $this->getTypeCaseData($bYear, $region);
        $viewElement= $this->getElement($bYear, $kpi['kpi_name'], $kid ,$region);


        $sumnum = null;
        $Typecase = null;

        // $q1 = $this->getTypeCaseData($bYear, $region);
        // $sumnum = $q1->sumnum;
        // $Typecase = $q1->Typecase;


        // $q2 = $this->getCaseData($bYear, $region);
        // $namecause = $q2->namecause;
        // $numData = $q2->numData;
        // $colorBar = $q2->colorBar;


        // $bYear = intval($year) + 543;
        $bYearPrev = $bYear - 3;
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        //Kpi Data
        $kpi = $this->getKpiData($year, $kid, $region);


        if ($kpi['sql_query_province']) {

            if($region=="13"){
            
            $sqlq = 'SELECT IF(X.areacode!="TOTAL",areaname,"รวมทั้งหมด")as areaname_new, X.*
            FROM (SELECT 
            
            IFNULL(C.changwatcode,"TOTAL") as areacode,
            
            C.changwatname as areaname,
            SUM(S.BabyPos) as BabyPos,
            SUM(BabyPCR2) as BabyPCR2,
            sum(S.DeliANC) as DeliANC,
            sum(S.DeliTotal) as DeliTotal,
            sum(S.DeliANCPositive) as DeliANCPositive,
            SUM(S.DeliANCNegative) as DeliANCNegative,
            SUM(S.DeliNoANCPositive) as DeliNoANCPositive,
            SUM(S.DeliNoANCNegative) as DeliNoANCNegative,
            sum(S.DeliPosTotal) as DeliPosTotal,
            sum(S.DeliANCHAARTTx) as DeliANCHAARTTx,
            SUM(S.DeliANCHAARTPr) as DeliANCHAARTPr,
            SUM(S.DeliANCAZTNVP) as DeliANCAZTNVP,
            SUM(S.DeliANCARV) as DeliANCARV,
            SUM(S.DeliNoANCHAARTTx) as DeliNoANCHAARTTx,
            SUM(S.DeliNoANCHAARTPr) as DeliNoANCHAARTPr,
            SUM(S.DeliNoANCAZTNVP) as DeliNoANCAZTNVP,
            SUM(S.DeliNoANCARV) as DeliNoANCARV,
            SUM(S.VDRLPos) as VDRLPos,
            SUM(S.VDRLNeg) as VDRLNeg,
            SUM(S.SyphilisDx) as SyphilisDx,
            SUM(S.SyphilisTx) as SyphilisTx,
            sum(S.CouplePreCsg) as CouplePreCsg,
            sum(S.CouplePostCsg) as CouplePostCsg
            
            FROM s_phims S
            
            INNER JOIN cchangwat C ON C.changwatcode=LEFT(S.ProvinceId,2) 
            INNER JOIN cregion Z ON Z.region_code=C.regioncode  
            
            WHERE (CASE WHEN S.MonthRpt >= 10 THEN (S.YearRpt+1)
            ELSE S.YearRpt END) = '.$bYear.' 
            AND  C.regioncode in ("13","99")  
            GROUP BY C.changwatcode
            
            WITH ROLLUP) X';
            // print_r($sqlq); die();
            $q = $this->getSqlQueryData($sqlq, $bYear, $lYear, $kid, $region);
            }else{
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $lYear, $kid, $region);
            }

           
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }else{
            print_r('lllllllllllll');
            die();

        }
        // print_r($data);
        //MAP Layout
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where  c.regioncode=:region';
                // print_r($sql);
        $geo = $this->getMapLayout($sql, $propData, $region);



        //trend
        if ($kpi['sql_query_trend_province']) {

            $sqll = 'SELECT 
            (CASE WHEN S.MonthRpt >= 10 THEN (S.YearRpt+1)
            ELSE S.YearRpt END) as b_year,
            
            SUM(S.BabyPos)/ SUM(BabyPCR2) * 100 as total_ratio
            
            FROM s_phims S
            
            INNER JOIN cchangwat C ON C.changwatcode=LEFT(S.ProvinceId,2) 
            INNER JOIN cregion Z ON Z.region_code=C.regioncode  
            
            
            WHERE (CASE WHEN S.MonthRpt >= 10 THEN (S.YearRpt+1)
            ELSE S.YearRpt END) BETWEEN :queryYearPrev and :queryYear
            AND C.regioncode in (13,99) 
            
            GROUP BY (CASE WHEN S.MonthRpt >= 10 THEN (S.YearRpt+1)
            ELSE S.YearRpt END)';

            if($region == "13"){
                $trendData = $this->getTrendData($sqll, $bYearPrev, $bYear);
            }else{
                $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $bYear,$region);
            }
        }
// print_r($trendData); die();
        //gen URL
        // $mapUrl = null;
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], false));
        $mapType = json_encode('cw');

        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,

            // 'Typecase' => $Typecase,
            // 'sumnum' => $sumnum,
            // 'numData' => $numData,
            // 'namecause' => $namecause,
            // 'colorBar' => $colorBar,

            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
            'lYear'=>$lYear]);
    }



//---------------------------------------------END-Changwat----------------------------------//    




//----------------------------------------------Ampur----------------------------------//    
    public function actionAmpur()
    {

        $request = Yii::$app->request;

        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }else{}

        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');

        // $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? (date("m")>=10?(int)date("Y")+1:date("Y")) : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "5" : $request->get('kid');                
        // $bYear = (int) ($year) + 543;
        
        // $bYear = $year==null ? (date("m")>=10 ? (date("m")<10 ? date("Y") : date("Y")+1)+543+1 : (date("m")<10 ? date("Y") : date("Y")+1)+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYear = ($year+543);

        $changwat = $request->get('cw');
        // $bYear = intval($year) + 543;
        $bYearPrev = $bYear - 3;
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        //Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {

            // print_r($kpi['sql_query_ampur']);
            // die();

            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;

        }

        //MAP Layout
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null, $changwat);
        $viewElement= $this->getElement($bYear, $kpi['kpi_name'], $kid, NULL, $changwat);


        //trend
        if ($kpi['sql_query_trend_ampur']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $bYear,null,$changwat);
        }

        //gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));        
        // $mapUrl = json_encode(Url::toRoute('tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }


//------------------------------------------END Ampur----------------------------------//    


//------------------------------------------Tambon------------------------------------//

    public function actionTambon()
    {
        $request = Yii::$app->request;

        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }else{}

        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');

        // $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // $year = $request->get('year')==null ? (date("m")>=10?(int)date("Y")+1:date("Y")) : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "5" : $request->get('kid');                
        // $bYear = (int) ($year) + 543;
        
        // $bYear = $year==null ? (date("m")>=10 ? (date("m")<10 ? date("Y") : date("Y")+1)+543+1 : (date("m")<10 ? date("Y") : date("Y")+1)+543) : (date("m")>=10 ? $year+543+1 : $year+543);
        $bYear = ($year+543);

        $ampur = $request->get('ap');

        // $bYear = intval($year) + 543;
        $bYearPrev = $bYear - 3;
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        //Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {

            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, null, null, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;

        }

        //MAP Layout        
         $sql = 'select * from geojson_tambon g
                    inner join ctambon a on a.tamboncodefull=g.areacode
                    WHERE a.ampurcode =:ampur 
                    and a.flag_status=0';
         $geo = $this->getMapLayout($sql, $propData, null,null,$ampur);
         $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, NULL, $ampur);


        //trend
        if ($kpi['sql_query_trend_tambon']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $bYear, null, null, $ampur);
        }

        //gen URL
        $mapUrl = json_encode(Url::toRoute('mooban', true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'ap' => $ampur,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }


//------------------------------------------END Tambon ----------------------------------//   

}



// end class
