<?php
namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;

class MmrController extends \yii\web\Controller
{

    public function getTypeCaseData($bYear, $region){
        $conn = Yii::$app->db;

        $sqlTypeCase = 'SELECT
        COUNT(kpi_mch_mmr.id) as sumnum,
        kpi_mch_mmr_causetype.cause_type_desc as nametype
        FROM kpi_mch_mmr
        INNER JOIN kpi_mch_mmr_cause ON kpi_mch_mmr.cause_id = kpi_mch_mmr_cause.cause_id
        INNER JOIN kpi_mch_mmr_causetype ON kpi_mch_mmr_cause.cause_type = kpi_mch_mmr_causetype.cause_type_id
        INNER JOIN cchangwat ON cchangwat.changwatcode = kpi_mch_mmr.areacode
        WHERE (CASE WHEN kpi_mch_mmr.month_id>=10 THEN (kpi_mch_mmr.year_id+1)
           ELSE kpi_mch_mmr.year_id END) = :year ';

        if($region==null){
                
            $sqlTypeCase.='GROUP BY kpi_mch_mmr_causetype.cause_type_desc';

        }else{

            $sqlTypeCase.='AND cchangwat.regioncode = :region 
            GROUP BY kpi_mch_mmr_causetype.cause_type_desc';
        }

        $cmd = $conn->createCommand($sqlTypeCase);
        $cmd->bindValue(':year', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        $typecase= $cmd->queryAll();

        foreach ($typecase as $k => $v) {
            $sumnum += (int)$v['sumnum'];
            $Typecase[] = [
                "name" => $v['nametype'],
                "y" => (int)$v['sumnum']
            ];
        }

        return (object) $q1 = [
            'sumnum' => $sumnum,
            'Typecase' => $Typecase];
        }



        public function getCaseData($bYear, $region){
            $conn = Yii::$app->db;
    
            $sqlTypeCase = 'SELECT
            COUNT(kpi_mch_mmr.id) as sumnum,        
            kpi_mch_mmr_cause.cause_desc as namecause,
            kpi_mch_mmr_causetype.cause_type_id as nametype
            FROM kpi_mch_mmr
            INNER JOIN kpi_mch_mmr_cause ON kpi_mch_mmr.cause_id = kpi_mch_mmr_cause.cause_id
            INNER JOIN kpi_mch_mmr_causetype ON kpi_mch_mmr_cause.cause_type = kpi_mch_mmr_causetype.cause_type_id
            INNER JOIN cchangwat ON cchangwat.changwatcode = kpi_mch_mmr.areacode
            WHERE (CASE WHEN kpi_mch_mmr.month_id>=10 THEN (kpi_mch_mmr.year_id+1)
               ELSE kpi_mch_mmr.year_id END) = :year ';
    
            if($region==null){
                    
                $sqlTypeCase.='GROUP BY kpi_mch_mmr_cause.cause_desc';
    
            }else{
    
                $sqlTypeCase.='AND cchangwat.regioncode = :region 
                GROUP BY kpi_mch_mmr_cause.cause_desc';
            }
            $sqlTypeCase.=' ORDER BY sumnum ASC, cause_desc ASC';

            $cmd = $conn->createCommand($sqlTypeCase);
            $cmd->bindValue(':year', $bYear);
            if ($region !== null) {
                $cmd->bindValue(':region', $region);
            }
            //
            $typecase= $cmd->queryAll();
            // 
            $bar_colr=[
                'DIRECT'=>'#57c466',
                'INDIRECT'=>'#427fe5',
                'UNSPECIFIED'=>'#b05f00'
            ];
            //
            foreach ($typecase as $k => $v) {
                $namecause[] = $v['namecause'];
                $numData[] = (int)$v['sumnum'];
                $colorBar[] =$bar_colr[$v['nametype']];
            }
            //
            return (object) $q2 = [
                'namecause' => $namecause,
                'numData' => $numData,
                'colorBar' => $colorBar];
    
        }

            

    public function getEmptyData($eYear){

        $conn = Yii::$app->db;
        // $emptydatas = null;

        

        $sql = 'select SUM(M.m10+M.m11+M.m12+M.m1+M.m2+M.m3+M.m4+M.m5+M.m6+M.m7+M.m8+M.m9) as sumlive
                FROM moi_livebirths M WHERE M.`Year` = :year ';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $eYear);
        $emptydata = $cmd->queryAll();

        //  print_r($eYear);//die();
        if($emptydata != null && $emptydata <> 0){
            foreach ($emptydata as $k => $v) {
                $emptydatas += $v['sumlive'];
            }
         }
        //  print_r($eYear); die();
        // $datayear = ($emptydatas == null || $emptydatas == 0) ?  ($eYear-1) :  $eYear; 

        $emptydatas == 0  ? $datayear = ($eYear-1) <= 2560 ?  $datayear = 2560 : $datayear = $eYear  : $datayear = $eYear; 
//  print_r($datayear); die();

        return $datayear;
        
    }

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();

    }

    public function getSqlQueryData($sql, $bYear, $lYear=null, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        // print_r($lYear);die();
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        // $cmd->bindValue(':livebirthYear', $lYear);
        
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        // Trend chart column chart and gauge and map
        foreach ($data as $k => $v) {
            // $columnData[] = [$v['areaname'], @(float) number_format($v['result'] / $v['target'] * 100000,2)];
            $columnData[] = [$v['areaname'], @(float) number_format($v['result'] / $v['target'] * 100000,2)];
            $columnColor[]= $this->getMapColor($bYear,$kid, @(float) number_format($v['result'] / $v['target'] * 100000,2));

            $target += @(float) $v['target'];
            $result += @(float) $v['result'];

            if ($v['areacode'] !== 'TOTAL') {
                //map data
                $propData[$v['areacode']] = ["value" => @(float) number_format($v['result'] / $v['target'] * 100000,2),
                    "geocode" => $v['areacode'], "name" => $v['areaname'],
                    "color" => $this->getMapColor($bYear, $kid, @(float) number_format($v['result'] / $v['target'] * 100000),2)];
            }
        }

        $columnData=['Data'=>$columnData, 'Color'=>$columnColor];

        // Gauge chart
        $gaugeData = ($target == '' OR $target == 0) ? 0 : @(float)($result / $target) * 100000;	

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null, $tambon = null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }


    public function getAllyear(){
        $conn = Yii::$app->db;
        // $sql = 'select DISTINCT b_year from s_kpi_height614 ORDER BY b_year DESC';
        $sql = 'select DISTINCT year_id from kpi_mch_mmr ORDER BY year_id DESC';
        $cmd = $conn->createCommand($sql);
        return $year = $cmd->queryAll();
    }
    
    public function getThdate($date){
    $y = (substr($date,0,4))+543;
	$m = substr($date,4,2);
	$d = substr($date,6,2);
	
	$_month_name = array("01"=>"มกราคม",  "02"=>"กุมภาพันธ์",  "03"=>"มีนาคม",    
			"04"=>"เมษายน",  "05"=>"พฤษภาคม",  "06"=>"มิถุนายน",    
			"07"=>"กรกฎาคม",  "08"=>"สิงหาคม",  "09"=>"กันยายน",    
			"10"=>"ตุลาคม", "11"=>"พฤศจิกายน",  "12"=>"ธันวาคม");

        return (int)$d . ' ' . $_month_name[$m] . ' ' . $y;;
    }    


    public function getTrendData($sql, $bYearPrev, $bYear, $lYear=null, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', 2559);
        $cmd->bindValue(':queryYear', ((date("m")<10 ? date("Y") : date("Y")+1)+543)); //$bYear);
        // $cmd->bindValue(':livebirthYear', $lYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            $total_ratio =  @(float)($t['result']/$t['target']) * 100000;
            $trendData[] = [$t['b_year'], @(float) $total_ratio];
        }

        return $trendData;
    }


    public function getElement($bYear, $kpiName, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $year=$bYear-543;
        if ($region !== null) { // Changwat
            $areaName='ศูนย์อนามัยที่ '.(int)$region;
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='จังหวัด';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }else if($changwat !== null){   // Ampur
            $conn = Yii::$app->db;
            $sql = 'SELECT regioncode,changwatname FROM cchangwat WHERE changwatcode=:changwat';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':changwat', $changwat);
            $CW = $cmd->queryOne();
            $areaName='จังหวัด'.$CW['changwatname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='อำเภอ';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$CW['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$CW['regioncode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($ampur !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM campur A INNER JOIN cchangwat C ON A.changwatcode=C.changwatcode '
                    . 'WHERE A.ampurcodefull=:ampur';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':ampur', $ampur);
            $AP = $cmd->queryOne();
            $areaName = (substr($AP['ampurname'], 0, 3)!=='เขต')? 'อำเภอ'.$AP['ampurname']:$AP['ampurname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$AP['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$AP['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$AP['changwatname'] ,'url' => ['ampur?year='.$year.'&cw='.$AP['changwatcode']]];
            $breadcrumb[]=['label' => $areaName];
        }else{  // Index
            $areaName='ระดับเขตสุขภาพ';
            //$yearName='';
            //$tableAreaText='เขตสุขภาพ';
            //$breadcrumb[]=['label' => 'ระดับประเทศ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ'];
        }
        $element=[
            'areaName' => $areaName,
            //'yearName' => $yearName,
            //'tableAreaText' => $tableAreaText,
            'breadcrumb' => $breadcrumb,
        ];
        return $element;
            
    }

    


//-------------------------------------------Index---------------------------------//    
    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "76" : $request->get('kid');       
        // Parameter validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }         
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = (int)$bYear - 10;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Beginning year to see data trend
        $bYearPrev = (int)$bYear - 10;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $sumnum = null;
        $Typecase = null;
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        //
        $q1 = $this->getTypeCaseData($bYear, $region=null);
        $sumnum = $q1->sumnum;
        $Typecase = $q1->Typecase;
        //
        $q2 = $this->getCaseData($bYear, $region=null);
        $namecause = $q2->namecause;
        $numData = $q2->numData;
        $colorBar = $q2->colorBar;
        //
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid);
        //
        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $lYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }
        //
        // Map Layout
        $sql = 'select * from geojson where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);
        //
        // Trend Data
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $bYear, $lYear);
        }
        //
        // Gen URL
        // $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
        //     'kid' => $kid], true));
        // $mapType = json_encode('rg');

        $mapUrl = json_encode(Url::toRoute([
            'changwat', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'year' => $year,
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'Typecase' => $Typecase,
            'sumnum' => $sumnum,
            'numData' => $numData,
            'namecause' => $namecause,
            'colorBar' => $colorBar,
            
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
            'allYear' => $this->getAllyear(),
        ]);

    }    
//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    

    public function actionChangwat()
    {
        $request = Yii::$app->request;  
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "76" : $request->get('kid');                
        // Parameter validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }       
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = (int)$bYear - 10;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Beginning year to see data trend
        $bYearPrev = (int)$bYear - 10;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //   
        $region = $request->get('rg');
        //
        // $typeCase = $this->getTypeCaseData($bYear, $region);
        //
        $sumnum = null;
        $Typecase = null;
        //
        $q1 = $this->getTypeCaseData($bYear, $region);
        $sumnum = $q1->sumnum;
        $Typecase = $q1->Typecase;
        //
        $q2 = $this->getCaseData($bYear, $region);
        $namecause = $q2->namecause;
        $numData = $q2->numData;
        $colorBar = $q2->colorBar;
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;
        //
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid, $region);
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid);
        //
        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $lYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }else{
            print('Invalid SQL query.');
        }
        //
        // MAP Layout
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where  c.regioncode=:region';
        $geo = $this->getMapLayout($sql, $propData, $region);
        //
        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $bYear, $lYear=null, $region);
        }

        // Gen URL
        // $mapUrl = null;
        // $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], false));
        // $mapType = json_encode('cw');

        $mapUrl = json_encode(Url::toRoute([
            'ampur', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('cw');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid ,$region);


        return $this->render('changwat', [
            'year' => $year,
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,

            'Typecase' => $Typecase,
            'sumnum' => $sumnum,
            'numData' => $numData,
            'namecause' => $namecause,
            'colorBar' => $colorBar,

            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
            'allYear' => $this->getAllyear(),

            ]);

    }



//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    
    public function actionAmpur()
    {
        $request = Yii::$app->request;
        if ($request->get('cw') == null | strlen($request->get('cw')) !== 2 | strlen($request->get('year')) !== 4 | $request->get('year') == null ){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "76" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)$bYear - 10;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $changwat = $request->get('cw');

        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null, $changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $bYear,null,$changwat);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute([
            'tambon', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('ap');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, $changwat);


        return $this->render('ampur', [
            'year' => $year,
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,            
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear,$kid),
            'viewElement' => $viewElement,
            'allYear' => $this->getAllyear(),
            ]);
    }
//------------------------------------------END Ampur----------------------------------//    


//------------------------------------------Tambon------------------------------------//
    public function actionTambon()
    {
        $request = Yii::$app->request;
        if ($request->get('ap') == null | strlen($request->get('ap')) !== 4 | strlen($request->get('year')) !== 4 | $request->get('year') == null ){    
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }

        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "76" : $request->get('kid');                
        // Fiscal Year
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int)$bYear - 10;
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $ampur = $request->get('ap');

        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {

            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, null, null, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;

        }

        // MAP Layout        
         $sql = 'select * from geojson_tambon g
                    inner join ctambon a on a.tamboncodefull=g.areacode
                    WHERE a.ampurcode =:ampur 
                    and a.flag_status=0';
         $geo = $this->getMapLayout($sql, $propData, null,null,$ampur);

        // Trend
        if ($kpi['sql_query_trend_tambon']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $bYear, null, null, $ampur);
        }

        // Gen URL

        $mapUrl = json_encode(Url::toRoute('tambon', true));
        $mapType = json_encode('tb');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, NULL, $ampur);


        return $this->render('tambon', [
            'year' => $year,
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'ap' => $ampur,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),            
            'viewElement' => $viewElement,
            'allYear' => $this->getAllyear(),
            
            ]);
    }
//------------------------------------------END Tambon ----------------------------------//   

}

// end class
