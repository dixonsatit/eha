<?php

namespace frontend\modules\dashboard\controllers;
use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;
use yii\helpers\Json;

class ImrController extends \yii\web\Controller
{
    public function getMapColor($year, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year_en = :year', [':year' => $year])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }

    public function getMapLegend($year, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year_en = :year', [':year' => $year])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }

    public function getPlotBands($year, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year_en = :year', [':year' => $year])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }

    public function actionIndex()
    {
        //$conn = \Yii::$app->db;
        $request=\yii::$app->request;
        $kid=4;
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');        
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        $pyear=(int)$year-9;
        if($request->get('year')==null)
        {
            $year=$request->get('year');
        }
        if($year<='2016')
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index2', 'year'=>$request->get('year')]));
        }
        else
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index3', 'year'=>$request->get('year')]));
        }
        
        
        return $this->render('index');
    }
    public function actionIndex2()
    {
        $conn = \Yii::$app->db;
        $request=\yii::$app->request;
        $kid=4;
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');        
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        $pyear=(int)$year-9;
        // 
        if($year>='2017')
        {
          return Yii::$app->getResponse()->redirect(Url::to(['index3', 'year'=>$request->get('year')]));
        }

        //1.อัตราตายทารก (ต่ำกว่า 1 ปี) จําแนกตามเพศ
        $target_value=null;
        $gaugeData=null;
        //$plotBands=null;
        $Maternal=null;
        $Neonatal=null;
        $Infant=null;
        $datatrend=null;
        $datamonth=null;
        $trend_nm=null;
        $trend_im=null;
        $trend_mm=null;
        $trend_sex_nm_m=null;
        $trend_sex_nm_f=null;
        $trend_sex_im_m=null;
        $trend_sex_im_f=null;

        //2.อัตราตายทารก (อายุต่ำกว่า 1 ปี) ต่อการเกิดมีชีพ 1,000 คน จำแนกรายเดือน
        $column_month=null;
        $datamonth10y=null;

        //3.อัตราของการตายของทารก (อายุต่ำกว่า 1 ปี) จำแนกรายอายุและเพศ
        $datasexcode=null;
        $column_sex_T=null;
        $column_sex_M=null;
        $column_sex_F=null;

        //Map
        $geo=null;

        //Get Data From kpi_template_to_year
        $sqlkpi='select * from kpi_template_to_year
                  where kpi_template_id=:kid and year=:year';
        $cmd = $conn->createCommand($sqlkpi);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        $datakpi = $cmd->queryAll();
        if($datakpi!="")
        {
            foreach($datakpi as $key=>$value)
            {
                $target_value=$value['target_value'];
            }
        }

        // Query Year and Trend 10 Year
        //1.อัตราตายทารก (ต่ำกว่า 1 ปี) จําแนกตามเพศ
        $sqltrend='select * 
                  from imr_year
                  where b_year between :pyear AND :year
                  GROUP BY b_year
                  ORDER BY b_year DESC';
        $cmd = $conn->createCommand($sqltrend);
        $cmd->bindValue(':pyear', $pyear);
        $cmd->bindValue(':year', $year);
        $datatrend = $cmd->queryAll();
        
        if($datatrend!="")
        {
            foreach($datatrend as $key=>$value)
            {              
                if($value['b_year']==$year)
                {
                    $gaugeData=(float)$value['im_rate_total'];

                    $nm_rate_total=(float)$value['nm_rate_total'];
                    $nm_rate_m=(float)$value['nm_rate_m'];
                    $nm_rate_f=(float)$value['nm_rate_f'];

                    $im_rate_total=(float)$value['im_rate_total'];
                    $im_rate_m=(float)$value['im_rate_m'];
                    $im_rate_f=(float)$value['im_rate_f'];

                    $Maternal_total=(float)$value['mm_rate'];

                    $Neonatal=[$nm_rate_total,$nm_rate_m,$nm_rate_f];
                    $Infant=[$im_rate_total,$im_rate_m,$im_rate_f];
                    $Maternal=[$Maternal_total];
                }

                $thai_year=(int)$value['b_year']+543;
                $trend_nm[]=[$thai_year,(float)$value['nm_rate_total']];
                $trend_im[]=[$thai_year,(float)$value['im_rate_total']];
                $trend_mm[]=[$thai_year,(float)$value['mm_rate']];

                $trend_sex_nm_m[]=[$thai_year,(float)$value['nm_rate_m']];
                $trend_sex_nm_f[]=[$thai_year,(float)$value['nm_rate_f']];
                $trend_sex_im_m[]=[$thai_year,(float)$value['im_rate_m']];
                $trend_sex_im_f[]=[$thai_year,(float)$value['im_rate_f']];
            }
        }

        // Query Month And Trend 10 Year
        //2.อัตราตายทารก (อายุต่ำกว่า 1 ปี) ต่อการเกิดมีชีพ 1,000 คน จำแนกรายเดือน
        $sqlmonth10y='select * from imr_month
                   where b_year between :pyear AND :year
                   GROUP BY b_year
                   ORDER BY b_year DESC';
        $cmd = $conn->createCommand($sqlmonth10y);
        $cmd->bindValue(':pyear', $pyear);
        $cmd->bindValue(':year', $year);
        $datamonth10y = $cmd->queryAll();
        if($datamonth10y!="")
        {
            foreach($datamonth10y as $key=>$value)
            {
                if($value['b_year']==$year)
                {
                    $column_month=[["มกราคม",(float)$value['rate1']],["กุมภาพันธ์",(float)$value['rate2']],["มีนาคม",(float)$value['rate3']],["เมษายน",(float)$value['rate4']],["พฤษภาคม",(float)$value['rate5']],["มิถุนายน",(float)$value['rate6']],["กรกฎาคม",(float)$value['rate7']],["สิงหาคม",(float)$value['rate8']],["กันยายน",(float)$value['rate9']],["ตุลาคม",(float)$value['rate10']],["พฤศจิกายน",(float)$value['rate11']],["ธันวาคม",(float)$value['rate12']],["ทั้งปี",(float)$value['rate_total']]];
                }
            }
        }

        // Query Sex And Code And Trend 10 Year
        //3.อัตราของการตายของทารก (อายุต่ำกว่า 1 ปี) จำแนกรายอายุและเพศ
        $sqlsexcode='SELECT S.b_year,S.sex,
        SUM(CASE WHEN S.age_id=0 THEN S.im_number ELSE "0" END) AS AllAge_number,
        SUM(CASE WHEN S.age_id=0 THEN S.im_rate ELSE "0" END) AS AllAge_rate,
        SUM(CASE WHEN S.age_id=1 THEN S.im_number ELSE "0" END) AS under1day_number,
        SUM(CASE WHEN S.age_id=1 THEN S.im_rate ELSE "0" END) AS under1day_rate,
        SUM(CASE WHEN S.age_id=2 THEN S.im_number ELSE "0" END) AS 1day_number,
        SUM(CASE WHEN S.age_id=2 THEN S.im_rate ELSE "0" END) AS 1day_rate,
        SUM(CASE WHEN S.age_id=3 THEN S.im_number ELSE "0" END) AS 2day_number,
        SUM(CASE WHEN S.age_id=3 THEN S.im_rate ELSE "0" END) AS 2day_rate,
        SUM(CASE WHEN S.age_id=4 THEN S.im_number ELSE "0" END) AS 3day_number,
        SUM(CASE WHEN S.age_id=4 THEN S.im_rate ELSE "0" END) AS 3day_rate,
        SUM(CASE WHEN S.age_id=5 THEN S.im_number ELSE "0" END) AS 4day_number,
        SUM(CASE WHEN S.age_id=5 THEN S.im_rate ELSE "0" END) AS 4day_rate,
        SUM(CASE WHEN S.age_id=6 THEN S.im_number ELSE "0" END) AS 5day_number,
        SUM(CASE WHEN S.age_id=6 THEN S.im_rate ELSE "0" END) AS 5day_rate,
        SUM(CASE WHEN S.age_id=7 THEN S.im_number ELSE "0" END) AS 6day_number,
        SUM(CASE WHEN S.age_id=7 THEN S.im_rate ELSE "0" END) AS 6day_rate,
        SUM(CASE WHEN S.age_id=8 THEN S.im_number ELSE "0" END) AS 7_13day_number,
        SUM(CASE WHEN S.age_id=8 THEN S.im_rate ELSE "0" END) AS 7_13day_rate,
        SUM(CASE WHEN S.age_id=9 THEN S.im_number ELSE "0" END) AS 14_20day_number,
        SUM(CASE WHEN S.age_id=9 THEN S.im_rate ELSE "0" END) AS 14_20day_rate,
        SUM(CASE WHEN S.age_id=10 THEN S.im_number ELSE "0" END) AS 21_27day_number,
        SUM(CASE WHEN S.age_id=10 THEN S.im_rate ELSE "0" END) AS 21_27day_rate,
        SUM(CASE WHEN S.age_id=11 THEN S.im_number ELSE "0" END) AS 28day_2month_number,
        SUM(CASE WHEN S.age_id=11 THEN S.im_rate ELSE "0" END) AS 28day_2month_rate,
        SUM(CASE WHEN S.age_id=12 THEN S.im_number ELSE "0" END) AS 2month_number,
        SUM(CASE WHEN S.age_id=12 THEN S.im_rate ELSE "0" END) AS 2month_rate,
        SUM(CASE WHEN S.age_id=13 THEN S.im_number ELSE "0" END) AS 3month_number,
        SUM(CASE WHEN S.age_id=13 THEN S.im_rate ELSE "0" END) AS 3month_rate,
        SUM(CASE WHEN S.age_id=14 THEN S.im_number ELSE "0" END) AS 4month_number,
        SUM(CASE WHEN S.age_id=14 THEN S.im_rate ELSE "0" END) AS 4month_rate,
        SUM(CASE WHEN S.age_id=15 THEN S.im_number ELSE "0" END) AS 5month_number,
        SUM(CASE WHEN S.age_id=15 THEN S.im_rate ELSE "0" END) AS 5month_rate,
        SUM(CASE WHEN S.age_id=16 THEN S.im_number ELSE "0" END) AS 6month_number,
        SUM(CASE WHEN S.age_id=16 THEN S.im_rate ELSE "0" END) AS 6month_rate,
        SUM(CASE WHEN S.age_id=17 THEN S.im_number ELSE "0" END) AS 7month_number,
        SUM(CASE WHEN S.age_id=17 THEN S.im_rate ELSE "0" END) AS 7month_rate,
        SUM(CASE WHEN S.age_id=18 THEN S.im_number ELSE "0" END) AS 8month_number,
        SUM(CASE WHEN S.age_id=18 THEN S.im_rate ELSE "0" END) AS 8month_rate,
        SUM(CASE WHEN S.age_id=19 THEN S.im_number ELSE "0" END) AS 9month_number,
        SUM(CASE WHEN S.age_id=19 THEN S.im_rate ELSE "0" END) AS 9month_rate,
        SUM(CASE WHEN S.age_id=20 THEN S.im_number ELSE "0" END) AS 10month_number,
        SUM(CASE WHEN S.age_id=20 THEN S.im_rate ELSE "0" END) AS 10month_rate,
        SUM(CASE WHEN S.age_id=21 THEN S.im_number ELSE "0" END) AS 11month_number,
        SUM(CASE WHEN S.age_id=21 THEN S.im_rate ELSE "0" END) AS 11month_rate,
        SUM(CASE WHEN S.age_id=22 THEN S.im_number ELSE "0" END) AS unknow_number,
        SUM(CASE WHEN S.age_id=22 THEN S.im_rate ELSE "0" END) AS unknow_rate
        FROM imr_agesex S
        WHERE S.b_year BETWEEN :pyear AND :year
        GROUP BY S.b_year,S.sex
        ORDER BY S.b_year DESC,S.sex DESC';
        $cmd = $conn->createCommand($sqlsexcode);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':pyear', $pyear);
        $datasexcode = $cmd->queryAll();
        if($datasexcode!="")
        {
            foreach($datasexcode as $key=>$value)
            {
                if($value['b_year']==$year && $value['sex']=="T")
                {
                    $column_sex_T=[['ทุกอายุ',(float)$value['AllAge_rate']],['ต่ำกว่า 1 วัน',(float)$value['under1day_rate']],['1วัน',(float)$value['1day_rate']],['2วัน',(float)$value['2day_rate']],['3วัน',(float)$value['3day_rate']],['4วัน',(float)$value['4day_rate']],['5วัน',(float)$value['5day_rate']],['6วัน',(float)$value['6day_rate']],
                    ['7-13วัน',(float)$value['7_13day_rate']],['14-20วัน',(float)$value['14_20day_rate']],['21-27วัน',(float)$value['21_27day_rate']],['28วัน น้อยกว่า 2เดือน',(float)$value['28day_2month_rate']],
                    ['2เดือน',(float)$value['2month_rate']],['3เดือน',(float)$value['3month_rate']],['4เดือน',(float)$value['4month_rate']],['52เดือน',(float)$value['5month_rate']],['6เดือน',(float)$value['6month_rate']],['7เดือน',(float)$value['7month_rate']],['8เดือน',(float)$value['8month_rate']],['9เดือน',(float)$value['9month_rate']],['10เดือน',(float)$value['10month_rate']],['11เดือน',(float)$value['11month_rate']],
                    ['ไม่ทราบ',(float)$value['unknow_rate']]];
                }
                if($value['b_year']==$year && $value['sex']=="M")
                {
                    $column_sex_M=[['ทุกอายุ',(float)$value['AllAge_rate']],['ต่ำกว่า 1 วัน',(float)$value['under1day_rate']],['1วัน',(float)$value['1day_rate']],['2วัน',(float)$value['2day_rate']],['3วัน',(float)$value['3day_rate']],['4วัน',(float)$value['4day_rate']],['5วัน',(float)$value['5day_rate']],['6วัน',(float)$value['6day_rate']],
                    ['7-13วัน',(float)$value['7_13day_rate']],['14-20วัน',(float)$value['14_20day_rate']],['21-27วัน',(float)$value['21_27day_rate']],['28วัน น้อยกว่า 2เดือน',(float)$value['28day_2month_rate']],
                    ['2เดือน',(float)$value['2month_rate']],['3เดือน',(float)$value['3month_rate']],['4เดือน',(float)$value['4month_rate']],['52เดือน',(float)$value['5month_rate']],['6เดือน',(float)$value['6month_rate']],['7เดือน',(float)$value['7month_rate']],['8เดือน',(float)$value['8month_rate']],['9เดือน',(float)$value['9month_rate']],['10เดือน',(float)$value['10month_rate']],['11เดือน',(float)$value['11month_rate']],
                    ['ไม่ทราบ',(float)$value['unknow_rate']]];
                }
                if($value['b_year']==$year && $value['sex']=="F")
                {
                    $column_sex_F=[['ทุกอายุ',(float)$value['AllAge_rate']],['ต่ำกว่า 1 วัน',(float)$value['under1day_rate']],['1วัน',(float)$value['1day_rate']],['2วัน',(float)$value['2day_rate']],['3วัน',(float)$value['3day_rate']],['4วัน',(float)$value['4day_rate']],['5วัน',(float)$value['5day_rate']],['6วัน',(float)$value['6day_rate']],
                    ['7-13วัน',(float)$value['7_13day_rate']],['14-20วัน',(float)$value['14_20day_rate']],['21-27วัน',(float)$value['21_27day_rate']],['28วัน น้อยกว่า 2เดือน',(float)$value['28day_2month_rate']],
                    ['2เดือน',(float)$value['2month_rate']],['3เดือน',(float)$value['3month_rate']],['4เดือน',(float)$value['4month_rate']],['52เดือน',(float)$value['5month_rate']],['6เดือน',(float)$value['6month_rate']],['7เดือน',(float)$value['7month_rate']],['8เดือน',(float)$value['8month_rate']],['9เดือน',(float)$value['9month_rate']],['10เดือน',(float)$value['10month_rate']],['11เดือน',(float)$value['11month_rate']],
                    ['ไม่ทราบ',(float)$value['unknow_rate']]];
                }
            }
        }

        //Create Map
        $sqlmap='SELECT R.region_thai,R.region,R.sex,R.number,R.rate,b_year,Z.areacode,C.changwatname,Z.geojson 
        FROM imr_region R
        INNER JOIN region_th Z ON Z.region=R.region
        INNER JOIN cchangwat C ON C.changwatcode=Z.areacode
        WHERE R.sex="T" AND R.b_year=:year';
        
        $cmd = $conn->createCommand($sqlmap);
        $cmd->bindValue(':year', $year);
        $datamap = $cmd->queryAll();
        if($datamap!="")
        {
            foreach($datamap as $key=>$value)
            {
                $propData[$value['areacode']] = ["value" => (float) $value['rate'],
                "geocode" => $value['areacode'], "name" => $value['region_thai']."(".$value['changwatname'].")",
                "color" => $this->getMapColor($year, $kid, $value['rate'])];

                $gd[] = [
                    "type" => "Feature",
                    "properties" => $propData[$value['areacode']] !== null ? $propData[$value['areacode']] :
                    ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                    "geometry" => json_decode($value['geojson']),
                ];
            }
            $geo = json_encode($gd);
            // print_r($geo);
            // die();
        }
        
        return $this->render('index2',[
           'target_value'=>$target_value,
           'gaugeData'=>$gaugeData,
           'plotBands' => $this->getPlotBands($year, $kid),
           'datatrend'=>$datatrend,
           'Maternal'=>$Maternal,
           'Neonatal'=>$Neonatal,
           'Infant'=>$Infant,
           'trend_nm'=>$trend_nm,
           'trend_im'=>$trend_im,
           'trend_mm'=>$trend_mm,
           'trend_sex_nm_m'=>$trend_sex_nm_m,
           'trend_sex_nm_f'=>$trend_sex_nm_f,
           'trend_sex_im_m'=>$trend_sex_im_m,
           'trend_sex_im_f'=>$trend_sex_im_f,
           'column_month'=>$column_month,
           'datamonth10y'=>$datamonth10y,
           'datasexcode'=>$datasexcode,
           'column_sex_T'=>$column_sex_T,
           'column_sex_M'=>$column_sex_M,
           'column_sex_F'=>$column_sex_F,
           'geo'=>$geo,
           'mapLegend' => $this->getMapLegend($year, $kid),
        ]
    );
    }
    public function actionIndex3()
    {
        $conn = \Yii::$app->db;
        $request=\yii::$app->request;
        $kid=4;
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');        
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        $pyear=(int)$year-9;
        
        if($year<='2016')
        {
            return Yii::$app->getResponse()->redirect(Url::to(['index2', 'year'=>$request->get('year')]));
        }
        

        //Data
        $data=null;
        $target_value=null;
        $gaugeData=null;
        $columnData=null;
        $trendData=null;
        //Map
        $geo=null;

        //Get Data From kpi_template_to_year
        $sqlkpi='select * from kpi_template_to_year
                  where kpi_template_id=:kid and year=:year';
        $cmd = $conn->createCommand($sqlkpi);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        $datakpi = $cmd->queryAll();
        if($datakpi!="")
        {
            foreach($datakpi as $key=>$value)
            {
                $target_value=(float)$value['target_value'];
            }
        }

        
        //อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน รายเขต
        $sql_region='SELECT IF(X.areacode!="TOTAL",areaname,"รวมทั้งหมด")as areaname_new, X.*
        FROM(SELECT IFNULL(Z.region_code,"TOTAL")as areacode,Z.region_name as areaname, 
        -- maxdate
           MAX(K.date_com) as date_com,
        -- quarter1
        SUM(K.target1) as target1,
        SUM(K.result1) as result1,
        ROUND(
                 SUM(IFNULL(K.result1,0))/
                 SUM(IFNULL(K.target1,0))*1000,2
               ) as total_ratio1,
        -- quarter2
        SUM(K.target2) as target2,
        SUM(K.result2) as result2,
        ROUND(
                 SUM(IFNULL(K.result2,0))/
                 SUM(IFNULL(K.target2,0))*1000,2
               ) as total_ratio2,
        -- quarter3
        SUM(K.target3) as target3,
        SUM(K.result3) as result3,
        ROUND(
                 SUM(IFNULL(K.result3,0))/
                 SUM(IFNULL(K.target3,0))*1000,2
               ) as total_ratio3,
        -- quarter4
        SUM(K.target4) as target4,
        SUM(K.result4) as result4,
        ROUND(
                 SUM(IFNULL(K.result4,0))/
                 SUM(IFNULL(K.target4,0))*1000,2
               ) as total_ratio4,
        -- chart column & gauge & YearData
        SUM(K.result1+K.result2+K.result3+K.result4) as result,
        SUM(K.target1+K.target2+K.target3+K.target4) as target,
        ROUND(
                 SUM(IFNULL(K.result1+K.result2+K.result3+K.result4,0))/
                 SUM(IFNULL(K.target1+K.target2+K.target3+K.target4,0))*1000,2
               ) as total_ratio,
        -- Get Map
        G.geojson
        
        FROM imr_quarter K
        INNER JOIN cchangwat C ON C.changwatcode=K.areacode
        INNER JOIN cregion Z ON Z.region_code=C.regioncode  
        INNER JOIN geojson G ON G.areacode=Z.region_code
        WHERE K.b_year =:year AND G.areatype="1"
        GROUP BY Z.region_code
        WITH ROLLUP) X
        ORDER BY X.areacode';
        $cmd = $conn->createCommand($sql_region);
        $cmd->bindValue(':year', $year);
        $data = $cmd->queryAll();
        
        if($data!="")
        {
            foreach($data as $key=>$value)
            {              
                if($value['areacode']=="TOTAL")
                {
                    // Gauge chart
                    $gaugeData = (float)$value['total_ratio'];

                }
                else
                {
                    $propData[$value['areacode']] = ["value" => (float) $value['total_ratio'],
                    "geocode" => $value['areacode'], "name" => $value['areaname_new'],
                    "color" => $this->getMapColor($year, $kid, $value['total_ratio'])];
    
                    $gd[] = [
                        "type" => "Feature",
                        "properties" => $propData[$value['areacode']] !== null ? $propData[$value['areacode']] :
                        ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                        "geometry" => json_decode($value['geojson']),
                    ];
                }
                $columnData[]=[$value['areaname_new'],(float)$value['total_ratio']];
            }
            $geo = json_encode($gd);

            //gen URL
             $mapUrl = json_encode(Url::toRoute(['changwat3', 'year' => $year,'kid' => $kid], true));
             $mapType = json_encode('rg');
        }
        
        //Trend 10 Year
        //อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน
        $sqltrend='SELECT * FROM (
            SELECT b_year,
                ROUND(
                    SUM(IFNULL(result1+result2+result3+result4,0))/
                    SUM(IFNULL(target1+target2+target3+target4,0))*1000,2
                  ) AS total_ratio
        FROM imr_quarter
            UNION
            SELECT b_year,ROUND(nm_rate_total,2)
        from imr_year
        
        ) trendall
        WHERE b_year BETWEEN :pyear AND :year
        ORDER BY b_year DESC';
        $cmd = $conn->createCommand($sqltrend);
        $cmd->bindValue(':pyear', $pyear);
        $cmd->bindValue(':year', $year);
        $datatrend = $cmd->queryAll();
        if($datatrend!="")
        {
            foreach($datatrend as $key=>$value)
            {              
                $trendData[]=[(int)$value['b_year']+543,(float)$value['total_ratio']];
            }
        }


        return $this->render('index3',[
           'data'=>$data,
           'target_value'=>$target_value,
           'gaugeData'=>$gaugeData,
           'columnData'=>$columnData,
           'plotBands' => $this->getPlotBands($year, $kid),
           'geo'=>$geo,
           'mapUrl' => $mapUrl,
           'mapType' => $mapType,
           'mapLegend' => $this->getMapLegend($year, $kid),
           'trendData'=>$trendData,
           'scope'=>'เขตสุขภาพ',
        ]
    );
    }
    public function actionChangwat3()
    {
        $conn = \Yii::$app->db;
        $request=\yii::$app->request;
        $kid=4;
        $year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year');        
        $bYear = $year==null ? (int)$year+543 : (int)(date("m")<10 ? date("Y") : date("Y")+1)+543;
        $bYearPrev = (int) ($bYear) - 4;
        $pyear=(int)$year-4;
        $region = $request->get('rg');
        if($request->get('year')==null||$request->get('rg')==null||$request->get('rg')=='xx')
        {
          return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$request->get('year')]));
        }

        // Data
        $data=null;
        $target_value=null;
        $gaugeData=null;
        $columnData=null;
        $trendData=null;
        // Map
        $geo=null;

        //Get Data From kpi_template_to_year
        $sqlkpi='select * from kpi_template_to_year
                  where kpi_template_id=:kid and year=:year';
        $cmd = $conn->createCommand($sqlkpi);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        $datakpi = $cmd->queryAll();
        if($datakpi!="")
        {
            foreach($datakpi as $key=>$value)
            {
                $target_value=(float)$value['target_value'];
            }
        }

        
        //อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน รายจังหวัด
        $sql_province='SELECT IF(X.areacode!="TOTAL",areaname,"รวมทั้งหมด")as areaname_new, X.*
        FROM (SELECT IFNULL(C.changwatcode,"TOTAL") as areacode,C.changwatname as areaname,
      -- maxdate
         MAX(K.date_com) as date_com,
      -- quarter1
      SUM(K.target1) as target1,
      SUM(K.result1) as result1,
      ROUND(
               SUM(IFNULL(K.result1,0))/
               SUM(IFNULL(K.target1,0))*1000,2
             ) as total_ratio1,
      -- quarter2
      SUM(K.target2) as target2,
      SUM(K.result2) as result2,
      ROUND(
               SUM(IFNULL(K.result2,0))/
               SUM(IFNULL(K.target2,0))*1000,2
             ) as total_ratio2,
      -- quarter3
      SUM(K.target3) as target3,
      SUM(K.result3) as result3,
      ROUND(
               SUM(IFNULL(K.result3,0))/
               SUM(IFNULL(K.target3,0))*1000,2
             ) as total_ratio3,
      -- quarter4
      SUM(K.target4) as target4,
      SUM(K.result4) as result4,
      ROUND(
               SUM(IFNULL(K.result4,0))/
               SUM(IFNULL(K.target4,0))*1000,2
             ) as total_ratio4,
      -- chart column & gauge & YearData
        SUM(K.result1+K.result2+K.result3+K.result4) as target,
        SUM(K.target1+K.target2+K.target3+K.target4) as result,
        ROUND(
               SUM(IFNULL(K.result1+K.result2+K.result3+K.result4,0))/
               SUM(IFNULL(K.target1+K.target2+K.target3+K.target4,0))*1000,2
             ) as total_ratio,
      -- Get Map
      G.geojson
      
      FROM imr_quarter K
      INNER JOIN cchangwat C ON C.changwatcode=K.areacode
      INNER JOIN cregion Z ON Z.region_code=C.regioncode  
      INNER JOIN geojson G ON G.areacode=C.changwatcode
      WHERE K.b_year  = :year AND Z.region_code=:region AND G.areatype=2
      GROUP BY C.changwatcode
      WITH ROLLUP) X';
        $cmd = $conn->createCommand($sql_province);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':region', $region);
        $data = $cmd->queryAll();
        
        if($data!="")
        {
            foreach($data as $key=>$value)
            {              
                if($value['areacode']=="TOTAL")
                {
                    // Gauge chart
                    $gaugeData = (float)$value['total_ratio'];

                }
                else
                {
                    $propData[$value['areacode']] = ["value" => (float) $value['total_ratio'],
                    "geocode" => $value['areacode'], "name" => $value['areaname_new'],
                    "color" => $this->getMapColor($year, $kid, $value['total_ratio'])];
    
                    $gd[] = [
                        "type" => "Feature",
                        "properties" => $propData[$value['areacode']] !== null ? $propData[$value['areacode']] :
                        ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                        "geometry" => json_decode($value['geojson']),
                    ];
                }
                $columnData[]=[$value['areaname_new'],(float)$value['total_ratio']];
            }
            $geo = json_encode($gd);

           //gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur3', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');
        }
        
        //Trend 5 Year
        //อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน
        $sqltrend='SELECT b_year,
        ROUND(
            SUM(IFNULL(K.result1+K.result2+K.result3+K.result4,0))/
            SUM(IFNULL(K.target1+K.target2+K.target3+K.target4,0))*1000,2
          ) AS total_ratio
   
        FROM imr_quarter K
        WHERE K.b_year BETWEEN :pyear AND :year AND region_code=:region
        GROUP BY K.b_year
        ORDER BY K.b_year DESC';
        $cmd = $conn->createCommand($sqltrend);
        $cmd->bindValue(':pyear', $pyear);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':region', $region);
        $datatrend = $cmd->queryAll();
        if($datatrend!="")
        {
            foreach($datatrend as $key=>$value)
            {              
                $trendData[]=[(int)$value['b_year']+543,(float)$value['total_ratio']];
            }
        }


        return $this->render('changwat3',[
           'data'=>$data,
           'target_value'=>$target_value,
           'gaugeData'=>$gaugeData,
           'columnData'=>$columnData,
           'plotBands' => $this->getPlotBands($year, $kid),
           'geo'=>$geo,
           'mapUrl' => $mapUrl,
           'mapType' => $mapType,
           'mapLegend' => $this->getMapLegend($year, $kid),
           'trendData'=>$trendData,
           'scope'=>'จังหวัด',
        ]
    );
    }
}
