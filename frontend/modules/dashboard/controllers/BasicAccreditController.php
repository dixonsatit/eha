<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use frontend\modules\dashboard\models\DashboardServices;

class BasicAccreditController extends \yii\web\Controller
{

    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }


    public function getMapLegend($bYear, $kid)
    {
        $ranges = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();            

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }


    public function getPlotBands($bYear, $kid)
    {
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from'=>$b->range_min,'to'=>$b->range_max,'color'=>$b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from'=>0,'to'=>100,'color'=>'#848484'];
    }


    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        return $kpi = $cmd->queryOne();
    }


    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();


        // Column chart and gauge and map
        foreach ($data as $k => $v) {
            $columnData[] = [$v['areaname_new'], (float) $v['total_ratio']];
            $columnColor[]= $this->getMapColor($bYear, $kid, $v['total_ratio']);

            $target += (float) @ArrayHelper::getValue($v, 'target', 0);
            $result += (float) @ArrayHelper::getValue($v, 'result', 0);

            if ($v['areacode'] !== 'TOTAL') {
                // Map data
                $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                    "geocode" => $v['areacode'], "name" => $v['areaname_new'],
                    "color" => $this->getMapColor($bYear, $kid, $v['total_ratio'])];
            }
        }

        $columnData=['Data'=>$columnData, 'Color'=>$columnColor];

        // Gauge chart
        $gaugeData = ($target == '' OR $target == 0) ? 0 : @(float)($result / $target) * 100;	
        
        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }


    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $mdata = $cmd->queryAll();

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => $propData[$m['areacode']] !== null ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }



    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);

        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        foreach ($trends as $t) {
            $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
        }

        return $trendData;
    }

//-------------------------------------//


    public function getElement($bYear, $kpiName, $kid, $region=null, $changwat=null, $ampur=null)
    {
        // print_r($region); die();
        $year=$bYear-543;
        if ($region !== null) { // Changwat
            $areaName='ศูนย์อนามัยที่ '.(int)$region;
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='จังหวัด';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }else if($changwat !== null){   // Ampur
            $conn = Yii::$app->db;
            $sql = 'SELECT regioncode,changwatname FROM cchangwat WHERE changwatcode=:changwat';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':changwat', $changwat);
            $CW = $cmd->queryOne();
            $areaName='จังหวัด'.$CW['changwatname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='อำเภอ';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$CW['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$CW['regioncode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($ampur !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM campur A INNER JOIN cchangwat C ON A.changwatcode=C.changwatcode '
                    . 'WHERE A.ampurcodefull=:ampur';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':ampur', $ampur);
            $AP = $cmd->queryOne();
            $areaName = (substr($AP['ampurname'], 0, 3)!=='เขต')? 'อำเภอ'.$AP['ampurname']:$AP['ampurname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$AP['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$AP['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$AP['changwatname'] ,'url' => ['ampur?year='.$year.'&cw='.$AP['changwatcode']]];
            $breadcrumb[]=['label' => $areaName];
        }else{  // Index
            $areaName='ระดับเขตสุขภาพ';
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='เขตสุขภาพ';
            //$breadcrumb[]=['label' => 'ระดับประเทศ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ'];
        }
        $element=[
            'areaName' => $areaName,
            //'yearName' => $yearName,
            //'tableAreaText' => $tableAreaText,
            'breadcrumb' => $breadcrumb,
        ];
        return $element;
            
    }


//-------------------------------------------Index---------------------------------//       
    public function actionIndex()
    {
        $request = Yii::$app->request;
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "2" : $request->get('kid');                
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Year validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        $viewElement= $this->getElement($bYear, $kpi['kpi_name'], $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {

            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            //$q = $this->getSqlQueryData($kpi['sql_query']);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Trend
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $trendYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['changwat', 'year' => $year,
            'kid' => $kid], true));
        $mapType = json_encode('rg');

        return $this->render('index', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }

//------------------------------------------END-Index---------------------------------//    


//----------------------------------------------Changwat----------------------------------//    

    public function actionChangwat()
    {
        $request = Yii::$app->request;
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "2" : $request->get('kid');                
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $region = $request->get('rg');
        //
        // Year validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        $viewElement= $this->getElement($bYear, $kpi['kpi_name'], $kid ,$region);

        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join cchangwat c on c.changwatcode=g.areacode
        //         where areatype=2 and c.regioncode=:region';
        $sql = 'select * from geojson_changwat g
                inner join cchangwat c on c.changwatcode=g.areacode
                where c.regioncode = :region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend
        if ($kpi['sql_query_trend_province']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $trendYear, $region);
        }        

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['ampur', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('cw');

        return $this->render('changwat', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }

//---------------------------------------------END-Changwat----------------------------------//    


//----------------------------------------------Ampur----------------------------------//    

    public function actionAmpur()
    {
        $request = Yii::$app->request;
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "2" : $request->get('kid');                
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $changwat = $request->get('cw');
        //
        // Year validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, null, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        // $sql = 'select * from geojson g
        //         inner join campur a on a.ampurcodefull=g.areacode
        //         where  areatype=3 and a.changwatcode=:changwat
        //         and a.flag_status=0';
        $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, null, $changwat);

        $viewElement= $this->getElement($bYear, $kpi['kpi_name'], $kid, NULL, $changwat);

        // Trend
        if ($kpi['sql_query_trend_ampur']) {            
            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $trendYear, null, $changwat);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['tambon', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('ap');

        return $this->render('ampur', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }

//------------------------------------------END Ampur ----------------------------------//    


//------------------------------------------Tambon ------------------------------------//

    public function actionTambon()
    {
        $request = Yii::$app->request;
        // Fix KPI ID                
        $kid = $request->get('kid')==null ? "2" : $request->get('kid');                
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2558;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $ampur = $request->get('ap');
        //
        // Year validation
        if (strlen($request->get('year')) <> 4){
            return Yii::$app->getResponse()->redirect(Url::to(['index', 'year'=>$year]));
        }
        //
        $data = null;
        $columnData = null;
        $trendData = null;
        $kpi = null;
        $gaugeData = null;
        $geo = null;

        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        if ($kpi['sql_query_tambon']) {
            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, null, null, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // MAP Layout
        $sql = 'select * from geojson_tambon g 
        inner join ctambon t on t.tamboncodefull=g.areacode
        where ampurcode=:ampur';
        $geo = $this->getMapLayout($sql, $propData, null, null, $ampur);

        $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, NULL, $ampur);

        // Trend
        if ($kpi['sql_query_trend_tambon']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $trendYear, null, null, $ampur);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute(['mooban', 'year' => $year, 'kid' => $kid], true));
        $mapType = json_encode('tb');

        return $this->render('tambon', [
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($bYear, $kid),
            'plotBands' => $this->getPlotBands($bYear, $kid),
            'viewElement' => $viewElement,
        ]);
    }

//------------------------------------------END Tambon ----------------------------------//   

} // end class
