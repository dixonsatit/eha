<?php

namespace frontend\modules\dashboard\controllers;

use frontend\modules\dashboard\models\KpiRange;
use Yii;
use yii\helpers\Url;
//use yii\helpers\Json;

class Height1518Controller extends \yii\web\Controller
{
    public function getMapColor($bYear, $kid, $measureValue)
    {
        $rangeColor = KpiRange::find()
            ->where('kpi_template_id=:kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->andWhere(['<=', 'range_min', $measureValue])->andWhere(['>=', 'range_max', $measureValue])->one()->range_color;

        return $rangeColor !== null ? $rangeColor : '#848484';
    }

    public function getMapLegend($i)
    {
        $ranges = KpiRange::find()->where('kpi_template_id=:kid',
            [':kid' => $i])->all();

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc;
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีสี']);
    }

    public function getPlotBands($bYear,$kid)
    {
        // $bands = KpiRange::find()->where('kpi_template_id=:kid',
        //    [':kid' => $i])->all();
        $plotBands=null;
        $bands = KpiRange::find()
            ->where('kpi_template_id = :kid', [':kid' => $kid])
            ->andWhere('year = :year', [':year' => $bYear])
            ->all();

        foreach ($bands as $b) {
            $plotBands[] = ['from' => $b->range_min, 'to' => $b->range_max, 'color' => $b->range_color];
        }
        return $plotBands !== null ? $plotBands : ['from' => 0, 'to' => 100, 'color' => '#848484'];
        
    }

    public function getKpiData($year, $kid)
    {
        $conn = Yii::$app->db;

        $sql = 'select * from kpi_hdc k
        left join kpi_template i on i.id=k.kpi_template_id
        left join kpi_template_to_year y on y.kpi_template_id=k.kpi_template_id
        where y.year=:year and k.kpi_template_id=:kid ';
        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':year', $year);
        $cmd->bindValue(':kid', $kid);
        $kpi = $cmd->queryOne();
        // แก้ไขเบื้องต้น กรณีชื่อตัวชี้วัดไม่แสดง เดี๋ยวกลับมาแก้อีกที
        $kpi['name'] = $kpi['kpi_name'];

        return $kpi;
        //return $kpi = $cmd->queryOne();
    }

    public function getSqlQueryData($sql, $bYear, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        $target = null;
        $result = null;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $data = $cmd->queryAll();

        if($data) {
            // Column chart and gauge and map
            foreach ($data as $k => $v) {
                $columnDataQ1[]=[$v['areaname_new'],(float) $v['ratioq1']];
                $columnDataQ2[]=[$v['areaname_new'],(float) $v['ratioq2']];
                $columnColor1[]=$this->getMapColor($bYear,$kid, $v['ratioq1']);
                $columnColor2[]=$this->getMapColor($bYear,$kid, $v['ratioq2']);
                $target+=$v['targetq1']+$v['targetq2'];
                $result+=$v['resultq1']+$v['resultq2'];

                if ($v['areacode'] !== 'TOTAL') {
                    // Map data
                    $propData[$v['areacode']] = ["value" => (float) $v['total_ratio'],
                        "geocode" => $v['areacode'], "name" => $v['areaname'],
                        "color" => $this->getMapColor($bYear,$kid, (float)$v['total_ratio'])];
                }
            }
                
            // Gauge chart
            $gaugeData=($target>0)? (float)(($result/2)/($target/2))*100:0;
            
            // columnData 2 Qt.
            $columnData=['Q1'=>$columnDataQ1,'Q2'=>$columnDataQ2,'color1'=>$columnColor1,'color2'=>$columnColor2];
        }else{
            $columnData=null;
            $propData=[];
            $gaugeData=0;
        }

        return (object) $q = [
            'data' => $data,
            'columnData' => $columnData,
            'propData' => $propData,
            'gaugeData' => $gaugeData];
    }

    public function getMapLayout($sql, $propData, $region=null, $changwat=null, $ampur=null)
    {
        $conn = Yii::$app->db;
        // $sql = 'select * from geojson  where areatype=1';
        $cmd = $conn->createCommand($sql);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        
        $mdata = $cmd->queryAll();
        $gd=[];

        foreach ($mdata as $i => $m) {
            $gd[] = [
                "type" => "Feature",
                "properties" => array_key_exists($m['areacode'], $propData) ? $propData[$m['areacode']] :
                ["value" => 0, "geocode" => "xx", "name" => "ไม่มีข้อมูล"],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        return $geo = json_encode($gd);
    }
    
    public function getAllyear(){
        $conn = Yii::$app->db;
        // The beginning year HDC data was enough to use 
        $sql = 'select DISTINCT b_year from s_height1518 ORDER BY b_year DESC';
        // $sql = 'select DISTINCT year_buddhism AS b_year from kpi_fiscal_year ORDER BY year_buddhism DESC';
        $cmd = $conn->createCommand($sql);
        return $year = $cmd->queryAll();
    }

    public function getTrendData($sql, $bYearPrev, $bYear, $region=null, $changwat=null, $ampur=null)
    {

        $conn = Yii::$app->db;

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':queryYearPrev', $bYearPrev);
        $cmd->bindValue(':queryYear', $bYear);
        if ($region !== null) {
            $cmd->bindValue(':region', $region);
        }
        if ($changwat !== null) {
            $cmd->bindValue(':changwat', $changwat);
        }
        if ($ampur !== null) {
            $cmd->bindValue(':ampur', $ampur);
        }
        $trends = $cmd->queryAll();

        // Trend chart
        if($trends){
            foreach ($trends as $t) {
                $trendData[] = [$t['b_year'], (float) $t['total_ratio']];
            }
        }else{
            $trendData=null;
        }

        return $trendData;
    }
    
    public function getElement($bYear, $kpiName, $kid, $region=null, $changwat=null, $ampur=null)
    {
        $year=$bYear-543;
        if ($region !== null) { // Changwat
            $areaName='ศูนย์อนามัยที่ '.(int)$region;
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='จังหวัด';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => $areaName];
        }else if($changwat !== null){   // Ampur
            $conn = Yii::$app->db;
            $sql = 'SELECT regioncode,changwatname FROM cchangwat WHERE changwatcode=:changwat';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':changwat', $changwat);
            $CW = $cmd->queryOne();
            $areaName='จังหวัด'.$CW['changwatname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='อำเภอ';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$CW['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$CW['regioncode']]];
            $breadcrumb[]=['label' => $areaName];
        }else if($ampur !== null){   // Tambon
            $conn = Yii::$app->db;
            $sql = 'SELECT * FROM campur A INNER JOIN cchangwat C ON A.changwatcode=C.changwatcode '
                    . 'WHERE A.ampurcodefull=:ampur';
            $cmd = $conn->createCommand($sql);
            $cmd->bindValue(':ampur', $ampur);
            $AP = $cmd->queryOne();
            $areaName = (substr($AP['ampurname'], 0, 3)!=='เขต')? 'อำเภอ'.$AP['ampurname']:$AP['ampurname'];
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='ตำบล';
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ศูนย์อนามัยที่ '.(int)$AP['regioncode'] ,'url' => ['changwat?year='.$year.'&rg='.$AP['regioncode']]];
            $breadcrumb[]=['label' => 'จังหวัด'.$AP['changwatname'] ,'url' => ['ampur?year='.$year.'&cw='.$AP['changwatcode']]];
            $breadcrumb[]=['label' => $areaName];
        }else{  // Index
            $areaName='ระดับเขตสุขภาพ';
            //$yearName='ปีงบประมาณ พ.ศ.'. $bYear;
            //$tableAreaText='เขตสุขภาพ';
            //$breadcrumb[]=['label' => 'ระดับประเทศ', 'url' => ['?year='.$year]];
            $breadcrumb[]=['label' => 'ระดับเขตสุขภาพ'];
        }
        //
        $element=[
            'areaName' => $areaName,
            //'yearName' => $yearName,
            //'tableAreaText' => $tableAreaText,
            'breadcrumb' => $breadcrumb,
        ];
        //
        return $element;
            
    }
    
    public function actionIndex()
    {
        $request = \yii::$app->request;   
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "62" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;
        //
        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2559;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        $kpi=null;
        $data=null;
        $gaugeData=null;
        $trendData=null; 
        $columnData=null;
  
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query']) {
            $q = $this->getSqlQueryData($kpi['sql_query'], $bYear, $kid);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }
        //
        $propData[13] = ["value" => 0, "geocode" => "13", "name" => "เขต 13"];
        
        // Map Layout
        $sql = 'select * from geojson  where areatype=1';
        $geo = $this->getMapLayout($sql, $propData);

        // Trend Data
        if ($kpi['sql_query_trend']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend'], $bYearPrev, $trendYear);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute([
            'changwat', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('rg');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid);
        
        return $this->render('index',[
            'year' => $year,
            'allYear' => $this->getAllyear(),
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear,$kid),
            'viewElement' => $viewElement,
        ]);

    }
    
    public function actionChangwat()
    {
        $request = \yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "62" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2559;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Get Region code
        $region = $request->get('rg');
        //
        // Areacode validation
        if ($request->get('rg')==null | $request->get('rg')=='xx'){ 
            return $this->redirect(['index','year'=>$year]);
        }
        //
        $kpi=null;
        $data=null;
        $gaugeData=null;
        $trendData=null; 
        $columnData=null;
        $propData=[];
  
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);

        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query_province']) {
            $q = $this->getSqlQueryData($kpi['sql_query_province'], $bYear, $kid, $region);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }

        // Map Layout
        //$sql = 'select * from geojson  where areatype=2';
        $sql = 'select * from geojson_changwat g INNER JOIN cchangwat c ON g.areacode=c.changwatcode '
                . 'WHERE c.regioncode=:region';
        $geo = $this->getMapLayout($sql, $propData, $region);

        // Trend Data
        if ($kpi['sql_query_trend_province']) {

            $trendData = $this->getTrendData($kpi['sql_query_trend_province'], $bYearPrev, $trendYear, $region);
        }

        // gen URL
        $mapUrl = json_encode(Url::toRoute([
            'ampur', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('cw');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid ,$region);
        
        return $this->render('changwat',[
            'rg' => $region,
            'year' => $year,
            'allYear' => $this->getAllyear(),
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear,$kid),
            'viewElement' => $viewElement,
        ]);

    }
    
    public function actionAmpur()
    {
        $request = \yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "62" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2559;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Get Changwat code
        $changwat=$request->get('cw');
        //
        // Areacode validation
        if ($request->get('cw')==null | $request->get('cw')=='xx'){return $this->redirect(['index','year'=>$year]);}
        //
        $kpi=null;
        $data=null;
        $gaugeData=null;
        $trendData=null; 
        $columnData=null;
        $propData=[];
  
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query_ampur']) {
            $q = $this->getSqlQueryData($kpi['sql_query_ampur'], $bYear, $kid, NULL, $changwat);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }
        
        // Map Layout
         $sql = 'select * from geojson_ampur g
                inner join campur a on a.ampurcodefull=g.areacode
                where a.changwatcode=:changwat
                and a.flag_status=0';
        $geo = $this->getMapLayout($sql, $propData, NULL, $changwat);
        
        if ($kpi['sql_query_trend_ampur']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_ampur'], $bYearPrev, $trendYear, NULL, $changwat);
        }

        // Gen URL
        $mapUrl = json_encode(Url::toRoute([
            'tambon', 
            'year' => $year,
            /*'kid' => $kid*/], true));
        $mapType = json_encode('ap');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, $changwat);

        return $this->render('ampur',[
            'cw' => $changwat,
            'year' => $year,
            'allYear' => $this->getAllyear(),
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear,$kid),
            'viewElement' => $viewElement,
        ]);

    }
    
    public function actionTambon()
    {
        $request = \yii::$app->request;
        // Fix KPI ID
        $kid = $request->get('kid')==null ? "62" : $request->get('kid');
        // Fix AD. Year
        $year = $request->get('year')==null ? date("Y") : $request->get('year');
        $bYear = (int) ($year) + 543;

        // Beginning year to see data trend
        // $bYearPrev = (int)$bYear - 4;
        $bYearPrev = 2559;
        // If calendar month less than 10, then the latest data's trend year is the previous year
        $trendYear = (date("m")<10 ? date("Y")-1 : date("Y"))+543;
        //
        // Get Ampur code
        $ampur=$request->get('ap');
        //
        // Areacode validation
        if ($request->get('ap')==null | $request->get('ap')=='xx'){return $this->redirect(['index','year'=>$year]);}
        //
        $kpi=null;
        $data=null;
        $gaugeData=null;
        $trendData=null; 
        $columnData=null;
        $propData=[];
  
        // Kpi Data
        $kpi = $this->getKpiData($year, $kid);
        
        // Chart columnData,propData,gaugeData
        if ($kpi['sql_query_tambon']) {
            $q = $this->getSqlQueryData($kpi['sql_query_tambon'], $bYear, $kid, NULL, NULL, $ampur);
            $data = $q->data;
            $columnData = $q->columnData;
            $propData = $q->propData;
            $gaugeData = $q->gaugeData;
        }
        
        $sql = 'select * from geojson_tambon g 
        inner join ctambon t on t.tamboncodefull=g.areacode 
        where t.ampurcode=:ampur and t.flag_status=0'; 
        $geo = $this->getMapLayout($sql, $propData, NULL, NULL,$ampur);

        if ($kpi['sql_query_trend_tambon']) {
            $trendData = $this->getTrendData($kpi['sql_query_trend_tambon'], $bYearPrev, $trendYear, NULL, NULL, $ampur);
        }
        
        // Gen URL
        $mapUrl = json_encode(Url::toRoute('tambon', true));
        $mapType = json_encode('tb');
        
        $viewElement= $this->getElement($bYear, $kpi['name'], $kid, NULL, NULL, $ampur);

        return $this->render('tambon',[
            'ap' => $ampur,
            'year' => $year,
            'allYear' => $this->getAllyear(),
            'kpi' => $kpi,
            'data' => $data,
            'columnData' => $columnData,
            'gaugeData' => $gaugeData,
            'trendData' => $trendData,
            'geo' => $geo,
            'mapUrl' => $mapUrl,
            'mapType' => $mapType,
            'mapLegend' => $this->getMapLegend($kid),
            'plotBands' => $this->getPlotBands($bYear,$kid),
            'viewElement' => $viewElement,
        ]);
    }

}
