<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลช่วงอายุ</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <!--<tr class="info">
    <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
  </tr>-->
  <tr class="info">
  <th class="text-center">#</th>
    <th class="text-center" width="60%"><?=$scope?></th>
    <th class="text-center" width="60%">ร้อยละ</th>
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
    foreach ($data as $k => $v) {
      echo '<tr>';
      echo '<td class="text-left">'.($k+1).'</td>';
      echo '<td class="text-left" width="40%">' . $v['rangeage'] . '</td>';
      echo '<td class="text-right" width="40%">'.number_format($v['ratio'],2).'</td>
        </tr>';

  } //end foreach

} //end if  


?>

</tbody>








<tfoot class="info">
<tr>
<td colspan="3" class="info">
<br/>ที่มา : DoH Dashboard (ข้อมูลจาก การสำรวจกิจกรรมทางกายในสังคมไทย สถาบันวิจัยประชากรและสังคม มหาวิทยาลัยมหิดล)
<br/><br/>
<div class="text-success">
<?=DashboardServices::getReportdate($report_date);?>
</div>
</td>
</tr>
</tfoot>


</div>
<!-- <div class="panel-footer" style="color:orange;">
A หมายถึง ผู้สูงอายุที่มารับบริการตามตัวหาร pteeth-need_pextract >=20<br/>
B หมายถึง ผู้สูงอายุ 60 ปีขึ้นไป(คน) ณ วันที่มารับริการตรวจสุขภาพช่องปาก (ลงแฟ้ม dental) ที่ตรวจโดยทันตบุคลากร และ PTEETH 1 ถึง 32
<br/><br/>ที่มา : ระบบ HDC กระทรวงสาธรณสุข
</div> -->
</div>


<!--///////////////////////////////////////////////////////////////////////////-->




