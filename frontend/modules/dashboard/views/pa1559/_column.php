<?php
use miloschuman\highcharts\Highcharts;
?>


<?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'column',
        ],
        'title' => [
            'text' => 'เปรียบเทียบช่วงอายุ',
            // 'text' => $kpi['kpi_name'],
        ],
        'subtitle' => [
            // 'text'=> 'ปีงบประมาณ'
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
                // 'text' => 'ดัชนีมวลกาย'
            ],
            // 'plotLines' => [[
            //     // 'value' => (int) $kpiTemplateToYear->target_value,
            //     // 'value' => 50,
            //     'value' => (float) $kpi['target_value'],
            //     'color' => 'red',
            //     'width' => 2,
            //     'label' => [
            //         // 'text' => 'โลกร้อน 555',
            //         'align' => 'left',
            //         'style' => [
            //             'color' => 'blue',
            //         ],
            //     ],
            // ]],
        ],
        'legend' => [
            'enabled' => false,
        ],
        'tooltip' => [
            'pointFormat' => '',
        ],
        'series' => [
            [
                'name' => 'Population',
                'colorByPoint' => false,
                // 'color' => '#62cb31',
                // 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData,
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
            ],
        ],
    ],
]);
?>

