<?php
use miloschuman\highcharts\Highcharts;
// print_r($pieData);
// die();

echo Highcharts::widget([
    'options' => [
        
        'chart'=>[
            'plotBackgroundColor'=> null,
            'plotBorderWidth'=> null,
            'plotShadow'=> false,
            'type'=> 'pie',
        ],
        'title' => ['text' => 'กิจกรรมทางกายเพียงพอต่อสุขภาพ : '.$scope],
        'tooltip' => [
            'pointFormat' => 'ร้อยละ {point.y:.2f}',
            // 'pointFormat' => 'ร้อยละ {point.y:.2f} คน',
            'style' => [
                'fontSize' => '18px',
                'fontFamily' => 'Verdana, sans-serif',
            ],
        ],
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
                'colors' => ['#1a1aff','#ff3333','#00b300','#ff6600'],
                'allowPointSelect'=>true,
                'dataLabels'=>[
                    'enable'=>true,
                    'format' => '{point.name}<br>ร้อยละ {point.y:.2f}',
                    // 'format' => '{point.name}<br>{point.y:.0f} คน',
                    'style' => [
                        'fontSize' => '12px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                'showInLegend' => true,
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => ' คน',
                'data' => [
                    $columnData[0],$columnData[1],$columnData[2],$columnData[3]
                ],
            ] // new closing bracket
        ],
    ],
]);
