<?php
use miloschuman\highcharts\Highcharts;
// use yii\web\JsExpression;
// print_r($columnColor);
// die();
?><?php
echo Highcharts::widget([
		'scripts' => [
			'modules/exporting',
			'themes/grid-light',
			],
		'options' => [
				'chart'=> [
						'type'=> 'column'
						],
				'title'=> [
						'text'=> 'เปรียบเทียบรายพื้นที่ ' . $quarterName,
						],
				'subtitle'=> [
						//'text'=> 'ปีงบประมาณ'
						],
				'xAxis'=>[
						'type' => 'category',
						'labels' => [
								'rotation' => -45,
								'style' => [
										'fontSize'=>'13px',
										'fontFamily'=>'Verdana, sans-serif'
										]
								]
						],
				'yAxis' => [
						'min' => 0,
						// 'max' => 25,
						//
						'title' => [
								'text' => ''
								],
						'plotLines' => [[
								'value' => (float) $kpi['target_value'],
								'color' => 'red',
								'width' => 2,
								'label' => [
										//'text' => 'Theoretical mean => 932',
										'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
										'align' => 'center',
										'style' => [
										'color' => 'gray'
												]
										]
								]]
						],
				'legend' => [
						'enabled' => false
						],
				'tooltip' => [
							'headerFormat'=>'<span style="font-size:10px">{point.key}</span><table>',
							'pointFormat'=> '<tr><td style="color:{series.colors};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.2f}%s</b></td></tr>',
							'footerFormat'=>'</table>',
							'shared'=>true,
							'useHTML'=>true
						],
				'series' => [
						[
								'name' => 'ร้อยละ',
								'colorByPoint' => true,
								//'color'=> $columnColor,
								//'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
								'colors'=>$columnData['color'],
								'data'=>$columnData['column'],
								'states'=>[
									'hover'=>[
										'color'=>'#CCFF99'
									],
								],
								'dataLabels'=> [
										'enabled'=> true,
										'rotation'=> -90,
										'color'=> 'gray',
										'align'=> 'right',
										'format'=> '{point.y:.2f}',
										'y' => 10,
										'style' => [
												'fontSize' => '10px',
												'fontFamily' => 'Verdana, sans-serif'
												]
										]
								]
						]
				]
		]);
?>