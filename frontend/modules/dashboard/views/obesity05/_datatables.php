<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive dt-responsive nowrap" cellspacing="0" style="width:100%">
	<thead>
		<tr class="info">
			<th rowspan="2">&nbsp;</th>
			<th rowspan="2" class="text-center"><?=$scope?></th>   
			<!-- <th colspan="3" class="text-center">รวมทั้งปีงบประมาณ</th> -->
			<th colspan="3" class="text-center">ไตรมาส 1</th>
			<th colspan="3" class="text-center">ไตรมาส 2</th>
			<th colspan="3" class="text-center">ไตรมาส 3</th>
			<th colspan="3" class="text-center">ไตรมาส 4</th>
		</tr>
		<tr class="info">
		<!--ภาพรวม-->
			<!-- <th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th> -->
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
		</tr>
	</thead>
	<tfoot>
		<tr class="info">
			<td colspan="14">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	if ($data) {
		foreach ($data as $key => $v) {
			echo '<tr>';
			echo '<td class="text-center">&nbsp;</td>';
			//ภาพรวม
			if ($v['areacode'] !== 'TOTAL') {
				if ($scope == 'ตำบล') {
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} else {
					echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
				}					
			} else {
				//
				$report_date = $v['report_date'];
				//
				echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
			}
			//
			// echo '<td class="text-center">'.number_format($v['total_target']).'</td>';
			// echo '<td class="text-center">'.number_format($v['total_result']).'</td>';
			// echo '<td class="text-center">'.number_format($v['total_ratio'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['targetq1']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq1']).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq1'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['targetq2']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq2']).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq2'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['targetq3']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq3']).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq3'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['targetq4']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq4']).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq4'],2).'</td>';
			echo '</tr>';
			//
		} //end foreach
	} //end if  
	?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
		&nbsp;- B หมายถึง จำนวนเด็กแรกเกิด - 5 ปี วัดความยาวหรือส่วนสูงทั้งหมด	<br>	
		&nbsp;- A หมายถึง จำนวนเด็กแรกเกิด - 5 ปี ที่มีภาวะอ้วน<br>

		<?=DashboardServices::getReportdate($report_date);?>
</div>

