<?php
include ('lib/bc_ampur.php'); 
include ('lib/btn_template.php'); 
?>
<div class="content animate-panel">
	<?php include ('lib/ul_year.php'); ?>
	<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_gauge',['gaugeData'=>$gaugeData,'plotBands'=>$plotBands,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_trend',['kpi'=>$kpi,'targetValue'=>$targetValue,'trendData'=>$trendData])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data,'mapUrl' => $mapUrl,'mapType' => $mapType,])?>
</div>
                </div>
            </div>
            </div>

</div>