<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;

$this->registerJs($script);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <!--<tr class="info">
    <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
  </tr>-->
  <tr class="info">
    <th class="text-center">#</th>
    <th class="text-center"><?=$scope?></th>
    <th class="text-center">เป้าหมาย (B)</th>
    <th class="text-center">ผลงาน (A)</th>
    <th class="text-center">ร้อยละ</th>
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
  foreach ($data as $k => $v) {
    echo '<tr>';
    echo '<td>'. ($k + 1) . '</td>';
    if ($v['areacode'] !== 'TOTAL' && $ap==null ) {
      echo '<td class="text-left">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode'], ['class' => 'success'],'_blank') . '</td>';
    } else {
      //
      $report_date = $v['report_date'];
      //
      echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
    }
    //
    echo '<td class="text-right">'.number_format($v['target'],0).'</td>
        <td class="text-right">'.number_format($v['result'],0).'</td>
        <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
      </tr>';
    //
  } //end foreach
} //end if  


// foreach($data as $k => $v){

//     // $mapType!=='ap'?window.open($mapUrl+'&'+$mapType,'_self'):null;

// $v['areacode'] == 'TOTAL' ? $aa=$v['areaname_new'] : $aa='<a target="_blank" href="changwat/?year='.$_GET['year'].'&kid='.$kpi['kpi_template_id'].'&rg='.$v['areacode'].'">'.$v['areaname_new'].'</a>' ;




// echo '<tr>
//     <td class="text-left">'.($k+1).'</td>
//     <td class="text-left">'.$aa.'</td>
//     <td class="text-right">'.number_format($v['target'],0).'</td>
//     <td class="text-right">'.number_format($v['result'],0).'</td>
//     <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
//   </tr>';
// }

?>

</tbody>
<tfoot class="info">
<tr>
<td colspan="5" class="info">
<p>&nbsp;
<br>
<b>คำอธิบาย:</b>
<br/><br>
A หมายถึง ผู้สูงอายุที่มารับบริการตามตัวหาร pteeth-need_pextract >=20<br/>
B หมายถึง ผู้สูงอายุ 60 ปีขึ้นไป(คน) ณ วันที่มารับริการตรวจสุขภาพช่องปาก (ลงแฟ้ม dental) ที่ตรวจโดยทันตบุคลากร และ PTEETH 1 ถึง 32
<br><br>
*** ข้อมูลทั้งหมดไม่รวมกรุงเทพมหานคร
<br/><br/>ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)
<br/><br/>

<?=DashboardServices::getReportdate($report_date);?>

</td>
</tr>
</tfoot>
</table>


</div>
<!-- <div class="panel-footer" style="color:orange;">
A หมายถึง ผู้สูงอายุที่มารับบริการตามตัวหาร pteeth-need_pextract >=20<br/>
B หมายถึง ผู้สูงอายุ 60 ปีขึ้นไป(คน) ณ วันที่มารับริการตรวจสุขภาพช่องปาก (ลงแฟ้ม dental) ที่ตรวจโดยทันตบุคลากร และ PTEETH 1 ถึง 32
<br/><br/>ที่มา : ระบบ HDC กระทรวงสาธรณสุข
</div> -->
</div>


<!--///////////////////////////////////////////////////////////////////////////-->




