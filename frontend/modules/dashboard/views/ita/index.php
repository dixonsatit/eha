<?php
/* @var $this yii\web\View */
// use yii\helpers\Html;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");

$session['depm'] = $request->get('depm') != null ? $request->get('depm') : 'doh';

$this->title = $kpi['kpi_name']; //. '  ระดับเขตสุขภาพ';
// $this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['changwat']];
$this->params['breadcrumbs'][] = $kpi['kpi_name'];//'ระดับเขตสุขภาพ';

// print_r($data); die();
?>



<?=$this->render('//_header',['templatefile'=>"template_ita.png"])?>



<div class="content animate-panel">



<div class="row">
        <div class="col-lg-12 col-md-6 col-sm-3">
        <div class="hpanel">
        <div class="panel-body">


        <div class="col-md-2">
           <h4> ปีงบประมาณ </h4>
        </div>
        <div class="dropdown col-md-2">
            <a href="#" class="dropdown-toggle btn btn-primary" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"> <?= ($session['year']+543);?> <span class="caret"></span> </a>
            <?php
                for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2016; $i--) {
                    $cyear[] = array('label'=>($i+543),
                    'url'=>Url::to(['','depm'=>$session['depm'],'year'=>$i]));
                }
                echo Dropdown::widget([
                    'items' => $cyear
                ]);
            ?>
        </div>

        <div class="col-md-1">
           <h4> กรม </h4>
        </div>
        <div class="dropdown col-md-3 text-left">
            
        <a href="#" class="dropdown-toggle btn btn-primary" id="depm" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"> <?= $department[$session['depm']];?> <span class="caret"></span> </a>
            <?php
                echo Dropdown::widget([
                    'items' => $nameDepmt
                ]);
            ?>
            
        </div>
        


        </div>
        </div>
        </div>
</div>

<div class="row">

<div class="col-lg-6">

<div class="panel panel-primary">
    <div class="panel-body">
    
                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData[$session['depm']],'department' =>  $department[$session['depm']],'plotBands' =>$plotBands])?>
                     
    </div>
</div>

</div>


<div class="col-lg-6">


<div class="panel panel-primary">

    <div class="panel-body">
                    <!-- trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>
                    
    </div>
</div>

</div>

</div>

<!-- ///////////////////////////////////////////////////////////// -->

<div class="row">

<div class="col-lg-12">

<div class="panel panel-primary">

    <div class="panel-body">
                    <!-- column1 -->
                    <?=$this->render('_column1',['allData' => $allData])?>
                    
    </div>

</div>

</div>

</div>


<!-- ///////////////////////////////////////////////////////////// -->

<div class="row">

<div class="col-lg-12">

<div class="panel panel-primary">

    <div class="panel-body">
                    <!-- column1 -->
                    <?=$this->render('_column2',['allData2' => $allData2])?>
                    
    </div>

</div>

</div>

</div>




<div class="row">
<div class="col-lg-12">
<div class="panel panel-primary">
    <div class="panel-body">
        <?=$this->render('_datatables', ['data' => $data,'yearnow'=>$session['year']])?>
    </div>
</div>
</div>
</div>