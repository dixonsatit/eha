<?php
use miloschuman\highcharts\Highcharts;
?>
<style>
.dot {
  height: 15px;
  width: 15px;
  border-radius: 50%;
  display: inline-block;
}
</style>

<?php
// print_r(($columnData)); die();
// echo json_encode($allData);
// foreach ($columnData as $value) {
//     // echo $value;
// }
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
    'chart'=> [
        'type'=> 'column'
    ],
    'title'=> [
        // 'text'=> 'Monthly Average Rainfall'
        'text'=> 'คะแนน ITA จำแนกตามดัชนี'
    ],
    // 'subtitle'=> [
    //     'text'=> 'Source: WorldClimate.com'
    // ],
    'xAxis'=> [
        'categories'=> [
            'ITA ภาพรวม',
            'ความโปรงใส่',
            'ความพร้อมรับผิด',
            'ความปลอดจากการสุจริตฯ',
            'วัฒนธรรมคุณธรรมในองค์การ',
            'คุณธรรมการทำงานในหน่วยงาน',
        ],
        'crosshair'=> true
    ],
    'yAxis'=> [
        'min'=> 0,
        'max'=> 100,
        'title'=> [
            'text'=> 'ผลคะแนน (ร้อยละ)'
        ]
    ],
    'tooltip'=> [
        'headerFormat'=> '<span style="font-size:16px"><b>คะแนน {point.key} </b></span><table cellpadding="10" bgcolor="#FFFFFF">',
        'pointFormat'=> '<tr><td style="font-size:14px; padding:0"><span class="dot" style="background-color: {series.color};"></span> {series.name}&nbsp;&nbsp;&nbsp;'.
        ' </td><td style="font-size:14px; padding:0; "> ร้อยละ {point.y:.2f} </td></tr>',
        'footerFormat'=> '</table>',
        'shared'=> true,
        'useHTML'=> true
    ],
    'plotOptions'=> [
        'column'=> [
            'pointPadding'=> 0.2,
            'borderWidth'=> 0
        ]
    ],
    'series'=> $allData2
    ]
]);
?>

