<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$request = Yii::$app->request;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);

// print_r($data);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;">
<center>ตารางคะแนนการประเมินคุณธรรมความโปร่งใสในการดำเนินงานของหน่วยงานภาครัฐ
<br/>
ประจำปีงบมาณ พ.ศ.<?=($yearnow+543);?> จำแนกรายดัชนี (ระดับกรมสังกัดกระทรวงสาธารณสุข)
</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <tr class="info">
    <th class="text-center">ลำดับ</th>
    <th class="text-center" width="10%">ส่วนราชการ</th>
    <th class="text-center">คะแนนรวม<br/>ITA</th>
    <th class="text-center">EBIT</th>
    <th class="text-center">IIT</th>
    <th class="text-center">EIT</th>
    <th class="text-center">External</th>
    <th class="text-center">Internal</th>
    <th class="text-center">Evidence-Based</th>
    <th class="text-center">(1)</th>
    <th class="text-center">(2)</th>
    <th class="text-center">(3)</th>
    <th class="text-center">(4)</th>
    <th class="text-center">(5)</th>
  </tr>
</thead>
<tbody>

<?php 
$k =1;
if ($data) {
    foreach ($data as $k => $v) {
// $v['department'] == 'doh' ? $clss = 'class="warning"' : $clss = '';
        echo '<tr>';
        echo '<td>'. ($k + 1) . '</td>';
        echo '<td class="text-left">' .$v['departmentname']. '</td>';
        echo '<td class="text-right">' .$v['total_ratio']. '</td>';
        echo '<td class="text-right">' .$v['ebit']. '</td>';
        echo '<td class="text-right">' .$v['iit']. '</td>';
        echo '<td class="text-right">' .$v['eit']. '</td>';
        echo '<td class="text-right">' .$v['external']. '</td>';
        echo '<td class="text-right">' .$v['internal']. '</td>';
        echo '<td class="text-right">' .$v['evidencebased']. '</td>';
        echo '<td class="text-right">' .$v['ita1']. '</td>';
        echo '<td class="text-right">' .$v['ita2']. '</td>';
        echo '<td class="text-right">' .$v['ita3']. '</td>';
        echo '<td class="text-right">' .$v['ita4']. '</td>';
        echo '<td class="text-right">' .$v['ita5']. '</td>';

        echo '</tr>';

  } //end foreach
  
} //end if  
?>

</tbody>
<tfoot class="info">
<tr>
<td colspan="5" class="info">
ที่มา: DoH Dashboard กรมอนามัย
<br/><br/>

<b>หมายเหตุ:</b><br>
(1) = ความโปรงใส่
<br/>
(2) = ความพร้อมรับผิด
<br/>
(3) = ความปลอดจากการสุจริตฯ
<br/>
(4) = วัฒนธรรมคุณธรรมในองค์การ
<br/>
(5) = คุณธรรมการทำงานในหน่วยงาน
<br/>
<div class="text-success">
<?=DashboardServices::getReportdate($report_date);?>
</div>
</td>
</tr>
</tfoot>
</table>


</div>
</div>