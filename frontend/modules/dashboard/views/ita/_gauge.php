<?php
use miloschuman\highcharts\Highcharts;
?>



<?php

// print_r($department);

echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'ผลการประเมินภาพรวม ITA<br/>'.$department,
            // 'text' => 'สถานการณ์ภาพรวม',
            // 'text' => $kpi['kpi_name'],
        ],
        'chart' => [
            'type' => 'gauge',
            'plotBackgroundColor' => null,
            'plotBackgroundImage' => null,
            'plotBorderWidth' => 0,
            'plotShadow' => false,
        ],
        // 'title' => 'ร้อยละ',

        'pane' => [
            'startAngle' => -100,
            'endAngle' => 100,
            // 'background' => null,
            'background'=> [
                'backgroundColor'=> (Highcharts.theme && Highcharts.theme.background2) || '#FFF',
                'innerRadius'=> '60%',
                'outerRadius'=> '100%',
                'shape'=> 'arc'
            ]
        ],

        // the value axis
        'yAxis' => [
            'min' => 0,
            'max' => 100,

            'minorTickInterval' => 'auto',
            'minorTickWidth' => 1,
            'minorTickLength' => null,
            'minorTickPosition' => 'inside',
            'minorTickColor' => '#666',

            'tickPixelInterval' => 20,
            'tickWidth' => 2,
            'tickPosition' => 'inside',
            'tickLength' => 10,
            // 'tickColor' => '#666',
            'tickColor'=> '#fff',
            'labels' => [
                'step' => 2,
                'rotation' => 'auto',
            ],
            'title' => [
                // 'text' => 'ร้อยละ',
            ],
            'plotBands' => [[
                    'from' =>  81.00,
                    'to' =>100,
                    'color' => '#55BF3B', // red
                ], [
                    'from' => 0,
                    'to' => 80.99,
                    // 'color' => '#55BF3B', // red
                    'color' => '#DF5353', // green
                ]],
                
                //$plotBands
            // 'plotBands' => [[
            //     'from' => 0,
            //     'to' => 50,
            //     'color' => '#DF5353', // green
            // ], [
            //     'from' => 51,
            //     'to' => 80,
            //     'color' => '#DDDF0D', // yellow
            // ], [
            //     'from' => 81,
            //     'to' => 100,
            //     'color' => '#55BF3B', // red
            // ]],
        ],
        'tooltip' => [
            'pointFormat' => 'ร้อยละ {point.y:.2f}',
            'style' => [
                'fontSize' => '18px',
                'fontFamily' => 'Verdana, sans-serif',
            ],
        ],
        'series' => [[
            'name' => 'ร้อยละ',
            // 'data' => [(int)$chartHdc->percentage],
            'data' => [$gaugeData],
            'tooltip' => [
                'valueSuffix' => ' ',
            ],
            // dataLabels:{
            //     enabled:true,
            //     y:-40,
            //     style:{
            //       fontSize:'14px',
            //     },
            //     borderWidth:0,
            //     }
            'dataLabels' => [
                'enabled' => true,
                // 'y'=>-40,
                // 'color' => 'black',
                'format' => '{point.y:.2f}',
                'style' => [
                    'fontSize' => '16px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
                'borderWidth'=>0,
                
            ],
        ]],
    ],
]);
?>

