<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;

$this->registerJs($script);
$request = Yii::$app->request;
$ap=null;
$ap=$request->get('ap');
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางข้อมูลรายพื้นที่</h5></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive dt-responsive nowrap" cellspacing="0" style="width:100%">
    <thead>
    <tr class="info">
      <th rowspan="2" class="text-center align-middle">&nbsp;</th>
      <th rowspan="2" class="text-center align-middle">พื้นที่</th>
      <!-- <th colspan="3" class="text-center">ภาพรวม</th> -->
      <th colspan="3" class="text-center">ไตรมาส 1</th>
      <th colspan="3" class="text-center">ไตรมาส 2</th>
      <th colspan="3" class="text-center">ไตรมาส 3</th>
      <th colspan="3" class="text-center">ไตรมาส 4</th>
    </tr>
    <tr class="info">
      <!--ภาพรวม-->
      <!-- <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th> -->
      <!--ไตรมาส1-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส2-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส3-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส4-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
    </tr>
  </thead>
  <tfoot>
        <tr class="info">
            <td colspan="14">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
        </tr>
    </tfoot>
  <tbody>
<?php
if($data) {
  foreach ($data as $key => $value) {
    echo '<tr>';
    echo '<td class="text-center">&nbsp;</td>';
    // echo '<td class="text-center">'.($key+1).'</td>';
    //echo '<td class="text-center">'.$value['areaname'].'</td>';
    if ($value['areacode'] !== 'TOTAL')
    {
      if($request->get('ap')=='') {
        echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode'], ['class' => 'success'],'_blank') . '</td>';
      }
      else {
        echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
      }
    } 
    else {
    //
    $report_date = $value['report_date'];
    //
    echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
  }
  //
  // echo '<td class="text-center">'.$value['target'].'</td>';
  // echo '<td class="text-center">'.$value['result'].'</td>';
  // echo '<td class="text-center">'.$value['total_ratio'].'</td>';

  echo '<td class="text-center">'.number_format($value['targetq1']).'</td>';
  echo '<td class="text-center">'.number_format($value['resultq1']).'</td>';
  echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['ratioq1'],2).'</td>';

  echo '<td class="text-center">'.number_format($value['targetq2']).'</td>';
  echo '<td class="text-center">'.number_format($value['resultq2']).'</td>';
  echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['ratioq2'],2).'</td>';

  echo '<td class="text-center">'.number_format($value['targetq3']).'</td>';
  echo '<td class="text-center">'.number_format($value['resultq3']).'</td>';
  echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['ratioq3'],2).'</td>';

  echo '<td class="text-center">'.number_format($value['targetq4']).'</td>';
  echo '<td class="text-center">'.number_format($value['resultq4']).'</td>';
  echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['ratioq4'],2).'</td>';
  echo '</tr>';
  }
}
?>
  </tbody>
  </table>
  <div class="well">
                <b>หมายเหตุ:</b> <br> 
B หมายถึง จ่านวนเด็กอายุ 0-5 ปีที่ชั่งน้่าหนักและวัดส่วนสูงทั้งหมด<br>	
A หมายถึง จ่านวนเด็กอายุ 0-5 ปีสูงดีสมส่วน<br>

<?=DashboardServices::getReportdate($report_date);?>

</div>