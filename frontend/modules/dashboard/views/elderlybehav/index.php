<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");

$this->title = $kpi['kpi_name']; //. '  ระดับเขตสุขภาพ';
// $this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['changwat']];
// $this->params['breadcrumbs'][] = 'ระดับเขตสุขภาพ';
?>



<?=$this->render('//_header',['templatefile'=>"template_elderlybehav.png"])?>
<div class="content animate-panel">

<!-- ///////////////////////////////////////////////////////////// -->

<div class="row">

<div class="col-lg-12">

<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    
                    <!-- Trend -->
                    <?//=json_encode($trendData);?>
                    <?=$this->render('_trend', ['kpi' => $kpi, 'categoriesData' => $categoriesData, 'trendData' => $trendData])?>
                    
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>
</div>



<hr>
<div class="panel panel-primary">
<div class="panel-body">
<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data, 'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>
</div>
</div>