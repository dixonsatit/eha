<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>

<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลช่วงอายุ</center></div>
<div class="panel-body">

    <table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
    <thead>
      <!--<tr class="info">
        <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
      </tr>-->
      <tr class="info">
      <th class="text-center">#</th>
        <th class="text-center">ปีงบประมาณ</th>
        <th class="text-center">เป้าหมาย (ร้อยละ)</th>
        <th class="text-center">ผลการดำเนินงาน (ร้อยละ)</th>
      </tr>
    </thead>
    <tbody>

    <?php 
    if ($data) {
        foreach ($data as $k => $v) {
          echo '<tr>';
          echo '<td class="text-left">'.($k+1).'</td>';
          echo '<td class="text-left">' . $v['b_year'] . '</td>';
          echo '<td class="text-right">' . number_format($v['target_ratio'],2) . '</td>';
          echo '<td class="text-right">'.number_format($v['result_ratio'],2).'</td>
            </tr>';

      } //end foreach
    } //end if  
    ?>

    </tbody>
    <tfoot class="info">
    <tr>
    <td colspan="4" class="info">
    <br/>ที่มา : DoH Dashboard (กรมอนามัย)
    </td>
    </tr>
    </tfoot>
    </table>

</div>
<!--///////////////////////////////////////////////////////////////////////////-->




