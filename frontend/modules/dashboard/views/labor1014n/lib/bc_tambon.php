<?php
session_start();
$this->title = $kpi['kpi_name'] ." ระดับ".$scope;
$this->params['breadcrumbs'][] = [ 'label' => $kpi['kpi_name'] , 'url' => [ 'index' , 'year' => $kpi['year'] ]];
$this->params['breadcrumbs'][] = [ 'label' => 'ระดับจังหวัด', 'url' =>[ 'changwat' , 'year'=> $kpi['year'] , 'kid' => $kpi['kpi_template_id'] , 'rg'=> $_SESSION['rg'] ]];
$this->params['breadcrumbs'][] = [ 'label' => 'ระดับอำเภอ', 'url' =>[ 'ampur' , 'year'=> $kpi['year'] , 'kid' => $kpi['kpi_template_id'] , 'cw'=> $_SESSION['cw'] ]];
$this->params['breadcrumbs'][] = $scope
?>