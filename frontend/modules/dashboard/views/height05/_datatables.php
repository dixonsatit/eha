<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);

$request = Yii::$app->request;
$ap=null;
$ap=$request->get('ap');
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางข้อมูลรายพื้นที่</h5></div>
                    <table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive dt-responsive nowrap" cellspacing="0" style="width:100%">
                    <thead>
                      <tr class="info">
                       <th rowspan="3" class="text-center">&nbsp;</th>
                       <th rowspan="3" class="text-center">พื้นที่</th>
                       <th rowspan="3" class="text-center">จำนวนเด็ก 0-5 ปี ทั้งหมด(B1)</th>
                       <!-- <th colspan="10" class="text-center">รวมทั้งปี</th> -->
                       <th colspan="10" class="text-center">ไตรมาส 1</th>
                       <th colspan="10" class="text-center">ไตรมาส 2</th>
                       <th colspan="10" class="text-center">ไตรมาส 3</th>
                       <th colspan="10" class="text-center">ไตรมาส 4</th>
                     </tr>
                     <tr class="info">
                       <!--ภาพรวม-->
                       <!-- <th rowspan="2" class="text-center">จำนวนเด็ก ชั่ง&วัด(B2)</th>
                       <th rowspan="2" class="text-center">ร้อยละชั่ง&วัด(B2/B1)*100</th>
                       <th rowspan="2" class="text-center">สูงดีสมส่วน(A1)</th>
                       <th rowspan="2" class="text-center">ร้อยละสูงดีสมส่วน(A1/B2)*100</th>
                       <th colspan="3" class="text-center">เด็กชายอายุ 5 ปี</th>
                       <th colspan="3" class="text-center">เด็กหญิงอายุ 5 ปี</th> -->
                       <!--ไตรมาส1-->
                       <th rowspan="2" class="text-center">จำนวนเด็ก ชั่ง&วัด(B2)</th>
                       <th rowspan="2" class="text-center">ร้อยละชั่ง&วัด(B2/B1)*100</th>
                       <th rowspan="2" class="text-center">สูงดีสมส่วน(A1)</th>
                       <th rowspan="2" class="text-center">ร้อยละสูงดีสมส่วน(A1/B2)*100</th>
                       <th colspan="3" class="text-center">เด็กชายอายุ 5 ปี</th>
                       <th colspan="3" class="text-center">เด็กหญิงอายุ 5 ปี</th>
                       <!--ไตรมาส2-->
                       <th rowspan="2" class="text-center">จำนวนเด็ก ชั่ง&วัด(B2)</th>
                       <th rowspan="2" class="text-center">ร้อยละชั่ง&วัด(B2/B1)*100</th>
                       <th rowspan="2" class="text-center">สูงดีสมส่วน(A1)</th>
                       <th rowspan="2" class="text-center">ร้อยละสูงดีสมส่วน(A1/B2)*100</th>
                       <th colspan="3" class="text-center">เด็กชายอายุ 5 ปี</th>
                       <th colspan="3" class="text-center">เด็กหญิงอายุ 5 ปี</th>
                       <!--ไตรมาส3-->
                       <th rowspan="2" class="text-center">จำนวนเด็ก ชั่ง&วัด(B2)</th>
                       <th rowspan="2" class="text-center">ร้อยละชั่ง&วัด(B2/B1)*100</th>
                       <th rowspan="2" class="text-center">สูงดีสมส่วน(A1)</th>
                       <th rowspan="2" class="text-center">ร้อยละสูงดีสมส่วน(A1/B2)*100</th>
                       <th colspan="3" class="text-center">เด็กชายอายุ 5 ปี</th>
                       <th colspan="3" class="text-center">เด็กหญิงอายุ 5 ปี</th>
                       <!--ไตรมาส4-->
                       <th rowspan="2" class="text-center">จำนวนเด็ก ชั่ง&วัด(B2)</th>
                       <th rowspan="2" class="text-center">ร้อยละชั่ง&วัด(B2/B1)*100</th>
                       <th rowspan="2" class="text-center">สูงดีสมส่วน(A1)</th>
                       <th rowspan="2" class="text-center">ร้อยละสูงดีสมส่วน(A1/B2)*100</th>
                       <th colspan="3" class="text-center">เด็กชายอายุ 5 ปี</th>
                       <th colspan="3" class="text-center">เด็กหญิงอายุ 5 ปี</th>
                     </tr>
                     <tr class="info">
                     <!--ภาพรวม-->
                       <!-- <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th> -->
                       <!--ไตรมาส1-->
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <!--ไตรมาส2-->
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <!--ไตรมาส3-->
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <!--ไตรมาส4-->
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                       <th class="text-center">วัดส่วนสูง</th>
                       <th class="text-center">ผลรวมส่วนสูง</th>
                       <th class="text-center">ส่วนสูงเฉลี่ย</th>
                     </tr>
                   </thead>
                   <tfoot>
		<tr class="info"  style="padding:0px;" role="row">
      <td colspan="25">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
  </tfoot>
  <tbody>
    <?php
    if($data) {
      foreach ($data as $key => $value) {
        echo '<tr>';
        // Total
        echo '<td class="text-center">&nbsp;</td>';
        if ($value['areacode'] !== 'TOTAL') {
          if($request->get('ap')=='') {
            echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode'], ['class' => 'success'],'_blank') . '</td>';
          }
          else {
            echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
          }
        } 
        else {
          //
          $report_date = $value['report_date'];
          //
          echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
        }
        // 
        //echo '<td class="text-center">'.$value['areaname'].'</td>';
        echo '<td class="text-center">'.number_format($value['targetAll_B1HDC']).'</td>';
        //  echo '<td class="text-center">'.number_format($value['result1_B2HDC']).'</td>';
        //  echo '<td class="text-center">'.$value['B2B1HDC_ratio'].'</td>';
        //  echo '<td class="text-center">'.number_format($value['result2_A1HDC']).'</td>';
        //  echo '<td class="text-center">'.$value['A1B2HDC_ratio'].'</td>';
        //  echo '<td class="text-center">'.number_format($value['result3_height_male']).'</td>';
        //  echo '<td class="text-center">'.number_format($value['result5_sumheight_male']).'</td>';
        //  echo '<td class="text-center">'.$value['heightmale_ratio'].'</td>';
        //  echo '<td class="text-center">'.number_format($value['result4_height_female']).'</td>';
        //  echo '<td class="text-center">'.number_format($value['result6_sumheight_female']).'</td>';
        //  echo '<td class="text-center">'.$value['heightfemale_ratio'].'</td>';
        
        //ไตรมาส1
        echo '<td class="text-center">'.number_format($value['targetq1']).'</td>';
        echo '<td class="text-center">'.number_format($value['target_ratio_q1'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['resultq1']).'</td>';
        echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['result_ratio_q1'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['male_height_q1']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_male_height_q1']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightmale_ratio_q1'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['female_height_q1']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_female_height_q1']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightfemale_ratio_q1'],2).'</td>';

        //ไตรมาส2
        echo '<td class="text-center">'.number_format($value['targetq2']).'</td>';
        echo '<td class="text-center">'.number_format($value['target_ratio_q2'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['resultq2']).'</td>';
        echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['result_ratio_q2'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['male_height_q2']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_male_height_q2']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightmale_ratio_q2'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['female_height_q2']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_female_height_q2']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightfemale_ratio_q2'],2).'</td>';

        //ไตรมาส3
        echo '<td class="text-center">'.number_format($value['targetq3']).'</td>';
        echo '<td class="text-center">'.number_format($value['target_ratio_q3'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['resultq3']).'</td>';
        echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['result_ratio_q3'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['male_height_q3']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_male_height_q3']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightmale_ratio_q3'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['female_height_q3']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_female_height_q3']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightfemale_ratio_q3'],2).'</td>';

        //ไตรมาส4
        echo '<td class="text-center">'.number_format($value['targetq4']).'</td>';
        echo '<td class="text-center">'.number_format($value['target_ratio_q4'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['resultq4']).'</td>';
        echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($value['result_ratio_q4'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['male_height_q4']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_male_height_q4']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightmale_ratio_q4'],2).'</td>';
        echo '<td class="text-center">'.number_format($value['female_height_q4']).'</td>';
        echo '<td class="text-center">'.number_format($value['sum_female_height_q4']).'</td>';
        echo '<td class="text-center">'.number_format($value['heightfemale_ratio_q4'],2).'</td>';
        echo '</tr>';
      }
    }
    ?>
    </tbody>
  </table> 
  <div class="well">
<b>หมายเหตุ:</b> <br> 
ประเมินจากแฟ้ม NUTRITION และแฟ้ม PERSON <br>
<u>เป้าหมาย</u> <br>
เด็กไทยในเขตรับผิดชอบ อายุ 0-5 ปีในปีงบประมาณทุกคน <br>

<u>ผลงาน</u> <br>
- 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูงเกิน 1 ครั้งต่อไตรมาส จะยึดค่าน้ำหนักและส่วนสูงครั้งสุดท้ายของไตรมาส <br>
- 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูง ในหลายไตรมาสจะนับให้ทุกไตรมาสๆมาสละ 1 ครั้ง <br>
- <font color="red">คอลัมน์ รวมทั้งปีงบประมาณ ประมวลผลจากผลการชั่งน้ำหนักวัดส่วนสูงครั้งสุดท้ายที่อายุอยู่ในช่วง</font> ไม่ว่าจะชั่งและวัดในไตรมาสใดๆ จำนวนกี่ครั้งก็ตาม จึงไม่สามารถนำแต่ละไตรมาสมารวมเป็นทั้งปีงบประมาณได้ <br>
- ตัดความซ้ำซ้อนของข้อมูลด้วยเลขบัตรประชาชน<br>

<?=DashboardServices::getReportdate($report_date);?>

</div>
