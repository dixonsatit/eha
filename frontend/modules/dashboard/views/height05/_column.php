<?php
use miloschuman\highcharts\Highcharts;
?>

                    <?php 

    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'column'
                ],
                'title'=> [
                    'text'=> 'เปรียบเทียบรายพื้นที่ '.$quarterName
                ],
                'subtitle'=> [
                    //'text'=> 'ปีงบประมาณ'
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ]
                    ]
                ],
                'yAxis' => [
                    'min' => 0,
                    // 'max' => 25,
                    // 
                    'title' => [
                     'text' => ''
                    ],
                    'plotLines' => [[
                        // 'value' => (int) $kpiTemplateToYear->target_value,
                        'value' => $targetValue,
                        'color' => 'red',
                        'width' => 2,
                        'label' => [
                            //'text' => 'Theoretical mean => 932',
                            'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
                            'align' => 'center',
                            'style' => [
                                'color' => 'gray'
                            ]
                        ]
                    ]]
                ],
                'legend' => [
                    'enabled' => false
                ],
                'tooltip' => [
                    'headerFormat'=>'<span style="font-size:10px">{point.key}</span><table>',
                    'pointFormat'=> '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y}</b></td></tr>',
                    'footerFormat'=>'</table>',
                    'shared'=>true,
                    'useHTML'=>true,
                ],
                'series' => [
                    [
                        'name' => 'สูงดีสมส่วน',
                        'colorByPoint' => true,
                        // 'color'=> '#62cb31',
                        //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                        'colors'=>$columnData['color'],
						'data'=>$columnData['column'],
                        'dataLabels'=> [
                            'enabled'=> true,
                            'rotation'=> -90,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            'y' => 10,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                            ],
                            [
                                'name' => 'สูงเฉลี่ยชาย',
                                'colorByPoint' => false,
                                 'color'=> '#000099',
                                //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                                'data'=>$columnData2,
                                'dataLabels'=> [
                                    'enabled'=> true,
                                    'rotation'=> -90,
                                    'color'=> 'gray',
                                    'align'=> 'right',
                                    'format'=> '{point.y:.2f}',
                                    'y' => 10,
                                    'style' => [
                                        'fontSize' => '10px',
                                        'fontFamily' => 'Verdana, sans-serif'
                                    ]
                                ]
                                    ],
                                    [
                                        'name' => 'สูงเฉลี่ยหญิง',
                                        'colorByPoint' => false,
                                         'color'=> '#FF0099',
                                        //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                                        'data'=>$columnData3,
                                        'dataLabels'=> [
                                            'enabled'=> true,
                                            'rotation'=> -90,
                                            'color'=> 'gray',
                                            'align'=> 'right',
                                            'format'=> '{point.y:.2f}',
                                            'y' => 10,
                                            'style' => [
                                                'fontSize' => '10px',
                                                'fontFamily' => 'Verdana, sans-serif'
                                            ]
                                        ]
                                    ]
                ]
            ]
        ]);
        ?> 