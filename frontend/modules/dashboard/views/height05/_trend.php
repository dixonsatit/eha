<?php
use miloschuman\highcharts\Highcharts;
?>
                    <?php 

    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'line'
                ],
                'title'=> [
                    'text'=> 'แนวโน้มรายปี'
                ],
                'subtitle'=> [
                    //'text'=> 'ปีงบประมาณ'
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ]
                    ]
                ],
                'yAxis' => [
                    'min' => 0,
                    // 'max' => 25,
                    //
                    'title' => [
                     //'text' => 'ปีงบประมาณ รายปีงบประมาณ'
                    ],
                    'plotLines' => [[
                        // 'value' => (int) $kpiTemplateToYear->target_value,
                        'value' => $targetValue,
                        'color' => 'red',
                        'width' => 2,
                        'label' => [
                            //'text' => 'Theoretical mean => 932',
                            'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
                            'align' => 'center',
                            'style' => [
                                'color' => 'gray'
                            ]
                        ]
                    ]]
                ],
                'legend' => [
                    'enabled' => false
                ],
                'tooltip' => [
                    'pointFormat' => ''
                ],
                'series' => [
                    [
                        'name' => 'Population',
                        'colorByPoint' => false,
                        // 'color'=> '#62cb31',
                        //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                        'data'=>$trendData,
                        'dataLabels'=> [
                            'enabled'=> true,
                            'rotation'=> 0,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            'y' => 0,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        ?>