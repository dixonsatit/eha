<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_nutrition05';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target1</th>
                         <th>result1</th>
                         <th>ws1</th>
                         <th>hs1</th>
                         <th>wh1</th>
                         <th>fit1</th>
                         <th>target2</th>
                         <th>result2</th>
                         <th>ws2</th>
                         <th>hs2</th>
                         <th>wh2</th>
                         <th>fit2</th>
                         <th>target3</th>
                         <th>result3</th>
                         <th>ws3</th>
                         <th>hs3</th>
                         <th>wh3</th>
                         <th>fit3</th>
                         <th>target4</th>
                         <th>result4</th>
                         <th>ws4</th>
                         <th>hs4</th>
                         <th>wh4</th>
                         <th>fit4</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target1'] . '</td>';
    echo '<td>' . $item['result1'] . '</td>';
    echo '<td>' . $item['ws1'] . '</td>';
    echo '<td>' . $item['hs1'] . '</td>';
    echo '<td>' . $item['wh1'] . '</td>';
    echo '<td>' . $item['fit1'] . '</td>';
    echo '<td>' . $item['target2'] . '</td>';
    echo '<td>' . $item['result2'] . '</td>';
    echo '<td>' . $item['ws2'] . '</td>';
    echo '<td>' . $item['hs2'] . '</td>';
    echo '<td>' . $item['wh2'] . '</td>';
    echo '<td>' . $item['fit2'] . '</td>';
    echo '<td>' . $item['target3'] . '</td>';
    echo '<td>' . $item['result3'] . '</td>';
    echo '<td>' . $item['ws3'] . '</td>';
    echo '<td>' . $item['hs3'] . '</td>';
    echo '<td>' . $item['wh3'] . '</td>';
    echo '<td>' . $item['fit3'] . '</td>';
    echo '<td>' . $item['target4'] . '</td>';
    echo '<td>' . $item['result4'] . '</td>';
    echo '<td>' . $item['ws4'] . '</td>';
    echo '<td>' . $item['hs4'] . '</td>';
    echo '<td>' . $item['wh4'] . '</td>';
    echo '<td>' . $item['fit4'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>