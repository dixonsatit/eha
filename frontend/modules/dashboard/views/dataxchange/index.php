<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;
use kartik\date\DatePicker;


?>
<?php
$script = <<< JS
//Datatables
 getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$conn = Yii::$app->db;
$request=\yii::$app->request;
$table=null;
// $startdate=null;
// $enddate=null;
$table=$request->get('table');
$syear=$request->get('syear');
$eyear=$request->get('eyear');
$region=$request->get('region');


if($table==null)
{
    $table='กรุณาเลือก';
}
//
if($syear==null)
{
    return Yii::$app->getResponse()->redirect(Url::to(['index','table'=>$table,'syear'=>(date("m")<10 ? date("Y") : date("Y")+1),'eyear'=>$eyear]));
}
if($eyear==null)
{
    return Yii::$app->getResponse()->redirect(Url::to(['index','table'=>$table,'syear'=>$syear,'eyear'=>(date("m")<10 ? date("Y") : date("Y")+1)]));
}
if($region==null)
{
   $regionname='ทั้งหมด';
}
if($region=='01')
{
   $regionname='เขต 1';
}
if($region=='02')
{
   $regionname='เขต 2';
}
if($region=='03')
{
   $regionname='เขต 3';
}
if($region=='04')
{
   $regionname='เขต 4';
}
if($region=='05')
{
   $regionname='เขต 5';
}
if($region=='06')
{
   $regionname='เขต 6';
}
if($region=='07')
{
   $regionname='เขต 7';
}
if($region=='08')
{
   $regionname='เขต 8';
}
if($region=='09')
{
   $regionname='เขต 9';
}
if($region=='10')
{
   $regionname='เขต 10';
}
if($region=='11')
{
   $regionname='เขต 11';
}
if($region=='12')
{
   $regionname='เขต 12';
}
if($region=='13')
{
   $regionname='เขต 13';
}
?>

<?php
$this->title = 'ประมวลผลข้อมูล';
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>

<div class="content animate-panel">
<div class="row">               
<div class="col-md-2 text-right">
                           <h5> เลือกตาราง </h5>
                        </div>
                        <div class="dropdown col-md-2 text-left">

                            <a href="#" class="dropdown-toggle btn btn-default" id="table" data-toggle="dropdown" role="button" aria-haspopup="true" 
                            aria-expanded="false"> <?= $table?><span class="caret"></span> </a>
                            <?php
                            $sql='select * from table_listtable';
                            $cmd = $conn->createCommand($sql);
                            $data = $cmd->queryAll();
                            foreach ($data as $key => $value) {
                                $littalbe[] = array('label'=>$value['table_s'],
                                'url'=>Url::to(['index', 'table'=>$value['table_s']]));
                            }
                                echo Dropdown::widget([
                                    'items' => $littalbe
                                ]);
                            ?>
                            </div>
                
<div class="col-md-1 text-right">
<h5> ปีเริ่มต้น </h5>
                        </div>
                        <div class="dropdown col-md-1 text-left">

                            <a href="#" class="dropdown-toggle btn btn-default" id="syear" data-toggle="dropdown" role="button" aria-haspopup="true" 
                            aria-expanded="false"> <?= $request->get('syear')+543;?><span class="caret"></span> </a>
                            <?php
                            $sqlyear='select * from kpi_fiscal_year where year>=2015 order by year DESC';
                            $cmd = $conn->createCommand($sqlyear);
                            $datayear = $cmd->queryAll();
                            foreach ($datayear as $key => $value) {
                                $csyear[] = array('label'=>$value['year']+543,
                                'url'=>Url::to(['index','table'=>$table, 'syear'=>$value['year'],'eyear'=>$eyear]));
                            }
                                echo Dropdown::widget([
                                    'items' => $csyear
                                ]);
                            ?>
</div>
<div class="col-md-1 text-right">
<h5> ถึงปี </h5>
                        </div>
                        <div  class="dropdown col-md-1 text-left">

<a href="#" class="dropdown-toggle btn btn-default" id="eyear" data-toggle="dropdown" role="button" aria-haspopup="true" 
aria-expanded="false"> <?= $request->get('eyear')+543;?><span class="caret"></span> </a>
<?php
$sqleyear='select * from kpi_fiscal_year where year>=2015 order by year DESC';
$cmd = $conn->createCommand($sqleyear);
$dataeyear = $cmd->queryAll();
foreach ($dataeyear as $key => $value) {
    $ceyear[] = array('label'=>$value['year']+543,
    'url'=>Url::to(['index','table'=>$table ,'syear'=>$syear,'eyear'=>$value['year']]));
}
    echo Dropdown::widget([
        'items' => $ceyear
    ]);
?>
</div>
<div class="col-md-1 text-right">
                           <h5> เลือกเขต </h5>
                        </div>
                        <div class="dropdown col-md-1 text-left">
                            <a href="#" class="dropdown-toggle btn btn-default" id="region" data-toggle="dropdown" role="button" aria-haspopup="true" 
                            aria-expanded="false"><?=$regionname?><span class="caret"></span> </a>
                            <?php
                          echo Dropdown::widget([
                            'items' => [
                                ['label' => 'ทั้งหมด','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear])],
                                ['label' => 'เขต 1','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'01'])],
                                ['label' => 'เขต 2','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'02'])],
                                ['label' => 'เขต 3','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'03'])],
                                ['label' => 'เขต 4','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'04'])],
                                ['label' => 'เขต 5','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'05'])],
                                ['label' => 'เขต 6','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'06'])],
                                ['label' => 'เขต 7','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'07'])],
                                ['label' => 'เขต 8','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'08'])],
                                ['label' => 'เขต 9','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'09'])],
                                ['label' => 'เขต 10','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'10'])],
                                ['label' => 'เขต 11','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'11'])],
                                ['label' => 'เขต 12','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'12'])],
                                ['label' => 'เขต 13','url'=>Url::to(['index', 'table'=>$table ,'syear'=>$syear,'eyear'=>$eyear,'region'=>'13'])],
                            ],
                    ]);
                            ?>
                        </div>
<hr>
<div class="row">
<div class="col-md-12 text-center">
<?php
 if($table=='กรุณาเลือก')
 {
     $table='index';
 }

?>

<?=Html::a('ประมวลผล', [$table,'syear'=>$syear+543,'eyear'=>$eyear+543,'region'=>$region], ['class' => 'btn btn-success']);?>
</div>

</div>