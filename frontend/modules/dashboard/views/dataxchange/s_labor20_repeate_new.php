<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_labor20_repeate_new';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target</th>
                         <th>target10</th>
                         <th>result10</th>
                         <th>target11</th>
                         <th>result11</th>
                         <th>target12</th>
                         <th>result12</th>
                         <th>target1</th>
                         <th>result1</th>
                         <th>target2</th>
                         <th>result2</th>
                         <th>target3</th>
                         <th>result3</th>
                         <th>target4</th>
                         <th>result4</th>
                         <th>target5</th>
                         <th>result5</th>
                         <th>target6</th>
                         <th>result6</th>
                         <th>target7</th>
                         <th>result7</th>
                         <th>target8</th>
                         <th>result8</th>
                         <th>target9</th>
                         <th>result9</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target'] . '</td>';
    echo '<td>' . $item['target10'] . '</td>';
    echo '<td>' . $item['result10'] . '</td>';
    echo '<td>' . $item['target11'] . '</td>';
    echo '<td>' . $item['result11'] . '</td>';
    echo '<td>' . $item['target12'] . '</td>';
    echo '<td>' . $item['result12'] . '</td>';
    echo '<td>' . $item['target1'] . '</td>';
    echo '<td>' . $item['result1'] . '</td>';
    echo '<td>' . $item['target2'] . '</td>';
    echo '<td>' . $item['result2'] . '</td>';
    echo '<td>' . $item['target3'] . '</td>';
    echo '<td>' . $item['result3'] . '</td>';
    echo '<td>' . $item['target4'] . '</td>';
    echo '<td>' . $item['result4'] . '</td>';
    echo '<td>' . $item['target5'] . '</td>';
    echo '<td>' . $item['result5'] . '</td>';
    echo '<td>' . $item['target6'] . '</td>';
    echo '<td>' . $item['result6'] . '</td>';
    echo '<td>' . $item['target7'] . '</td>';
    echo '<td>' . $item['result7'] . '</td>';
    echo '<td>' . $item['target8'] . '</td>';
    echo '<td>' . $item['result8'] . '</td>';
    echo '<td>' . $item['target9'] . '</td>';
    echo '<td>' . $item['result9'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>