<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_kpi_height614';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>targetq1</th>
                         <th>targetq2</th>
                         <th>resultq1</th>
                         <th>resultq2</th>
                         <th>a2q1</th>
                         <th>a2q2</th>
                         <th>a3q1</th>
                         <th>a3q2</th>
                         <th>a4q1</th>
                         <th>a4q2</th>
                         <th>a5q1</th>
                         <th>a5q2</th>
                         <th>a6q1</th>
                         <th>a6q2</th>
                         <th>b3q1</th>
                         <th>b3q2</th>
                         <th>a4q1</th>
                         <th>a4q2</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['targetq1'] . '</td>';
    echo '<td>' . $item['targetq2'] . '</td>';
    echo '<td>' . $item['resultq1'] . '</td>';
    echo '<td>' . $item['resultq2'] . '</td>';
    echo '<td>' . $item['a2q1'] . '</td>';
    echo '<td>' . $item['a2q2'] . '</td>';
    echo '<td>' . $item['a3q1'] . '</td>';
    echo '<td>' . $item['a3q2'] . '</td>';
    echo '<td>' . $item['a4q1'] . '</td>';
    echo '<td>' . $item['a4q2'] . '</td>';
    echo '<td>' . $item['a5q1'] . '</td>';
    echo '<td>' . $item['a5q2'] . '</td>';
    echo '<td>' . $item['a6q1'] . '</td>';
    echo '<td>' . $item['a6q2'] . '</td>';
    echo '<td>' . $item['b3q1'] . '</td>';
    echo '<td>' . $item['b3q2'] . '</td>';
    echo '<td>' . $item['b4q1'] . '</td>';
    echo '<td>' . $item['b4q2'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>