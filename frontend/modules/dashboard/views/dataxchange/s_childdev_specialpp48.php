<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_childdev_specialpp48';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target9</th>
                         <th>result9_1</th>
                         <th>result9_2</th>
                         <th>result9_3</th>
                         <th>result9_4</th>
                         <th>result9_5</th>
                         <th>result9_6</th>
                         <th>result9_7</th>
                         <th>result9_8</th>
                         <th>result9_9</th>
                         <th>target18</th>
                         <th>result18_1</th>
                         <th>result18_2</th>
                         <th>result18_3</th>
                         <th>result18_4</th>
                         <th>result18_5</th>
                         <th>result18_6</th>
                         <th>result18_7</th>
                         <th>result18_8</th>
                         <th>result18_9</th>
                         <th>target30</th>
                         <th>result30_1</th>
                         <th>result30_2</th>
                         <th>result30_3</th>
                         <th>result30_4</th>
                         <th>result30_5</th>
                         <th>result30_6</th>
                         <th>result30_7</th>
                         <th>result30_8</th>
                         <th>result30_9</th>
                         <th>target42</th>
                         <th>result42_1</th>
                         <th>result42_2</th>
                         <th>result42_3</th>
                         <th>result42_4</th>
                         <th>result42_5</th>
                         <th>result42_6</th>
                         <th>result42_7</th>
                         <th>result42_8</th>
                         <th>result42_9</th>
                         <th>improper9</th>
                         <th>improper18</th>
                         <th>improper30</th>
                         <th>improper42</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target9'] . '</td>';
    echo '<td>' . $item['result9_1'] . '</td>';
    echo '<td>' . $item['result9_2'] . '</td>';
    echo '<td>' . $item['result9_3'] . '</td>';
    echo '<td>' . $item['result9_4'] . '</td>';
    echo '<td>' . $item['result9_5'] . '</td>';
    echo '<td>' . $item['result9_6'] . '</td>';
    echo '<td>' . $item['result9_7'] . '</td>';
    echo '<td>' . $item['result9_8'] . '</td>';
    echo '<td>' . $item['result9_9'] . '</td>';
    echo '<td>' . $item['target18'] . '</td>';
    echo '<td>' . $item['result18_1'] . '</td>';
    echo '<td>' . $item['result18_2'] . '</td>';
    echo '<td>' . $item['result18_3'] . '</td>';
    echo '<td>' . $item['result18_4'] . '</td>';
    echo '<td>' . $item['result18_5'] . '</td>';
    echo '<td>' . $item['result18_6'] . '</td>';
    echo '<td>' . $item['result18_7'] . '</td>';
    echo '<td>' . $item['result18_8'] . '</td>';
    echo '<td>' . $item['result18_9'] . '</td>';
    echo '<td>' . $item['target30'] . '</td>';
    echo '<td>' . $item['result30_1'] . '</td>';
    echo '<td>' . $item['result30_2'] . '</td>';
    echo '<td>' . $item['result30_3'] . '</td>';
    echo '<td>' . $item['result30_4'] . '</td>';
    echo '<td>' . $item['result30_5'] . '</td>';
    echo '<td>' . $item['result30_6'] . '</td>';
    echo '<td>' . $item['result30_7'] . '</td>';
    echo '<td>' . $item['result30_8'] . '</td>';
    echo '<td>' . $item['result30_9'] . '</td>';
    echo '<td>' . $item['target42'] . '</td>';
    echo '<td>' . $item['result42_1'] . '</td>';
    echo '<td>' . $item['result42_2'] . '</td>';
    echo '<td>' . $item['result42_3'] . '</td>';
    echo '<td>' . $item['result42_4'] . '</td>';
    echo '<td>' . $item['result42_5'] . '</td>';
    echo '<td>' . $item['result42_6'] . '</td>';
    echo '<td>' . $item['result42_7'] . '</td>';
    echo '<td>' . $item['result42_8'] . '</td>';
    echo '<td>' . $item['result42_9'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>