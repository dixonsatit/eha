<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_nutrition15';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target</th>
                         <th>result_bmi</th>
                         <th>thin</th>
                         <th>fit</th>
                         <th>fat</th>
                         <th>high_fat</th>
                         <th>waist_male</th>
                         <th>male_more90</th>
                         <th>female_more80</th>
                         <th>male_less90</th>
                         <th>female_less80</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target'] . '</td>';
    echo '<td>' . $item['result_bmi'] . '</td>';
    echo '<td>' . $item['thin'] . '</td>';
    echo '<td>' . $item['fit'] . '</td>';
    echo '<td>' . $item['fat'] . '</td>';
    echo '<td>' . $item['high_fat'] . '</td>';
    echo '<td>' . $item['waist_male'] . '</td>';
    echo '<td>' . $item['male_more90'] . '</td>';
    echo '<td>' . $item['female_more80'] . '</td>';
    echo '<td>' . $item['male_less90'] . '</td>';
    echo '<td>' . $item['female_less80'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>