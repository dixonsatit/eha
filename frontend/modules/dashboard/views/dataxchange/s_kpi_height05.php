<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_kpi_height05';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>targetq1</th>
                         <th>targetq2</th>
                         <th>targetq3</th>
                         <th>targetq4</th>
                         <th>resultq1</th>
                         <th>resultq2</th>
                         <th>resultq3</th>
                         <th>resultq4</th>
                         <th>b2q1</th>
                         <th>b2q2</th>
                         <th>b2q3</th>
                         <th>b2q4</th>
                         <th>a2q1</th>
                         <th>a2q2</th>
                         <th>a2q3</th>
                         <th>a2q4</th>
                         <th>b3q1</th>
                         <th>b3q2</th>
                         <th>b3q3</th>
                         <th>b3q4</th>
                         <th>a3q1</th>
                         <th>a3q2</th>
                         <th>a3q3</th>
                         <th>a3q4</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['targetq1'] . '</td>';
    echo '<td>' . $item['targetq2'] . '</td>';
    echo '<td>' . $item['targetq3'] . '</td>';
    echo '<td>' . $item['targetq4'] . '</td>';
    echo '<td>' . $item['resultq1'] . '</td>';
    echo '<td>' . $item['resultq2'] . '</td>';
    echo '<td>' . $item['resultq3'] . '</td>';
    echo '<td>' . $item['resultq4'] . '</td>';
    echo '<td>' . $item['b2q1'] . '</td>';
    echo '<td>' . $item['b2q2'] . '</td>';
    echo '<td>' . $item['b2q3'] . '</td>';
    echo '<td>' . $item['b2q4'] . '</td>';
    echo '<td>' . $item['a2q1'] . '</td>';
    echo '<td>' . $item['a2q2'] . '</td>';
    echo '<td>' . $item['a2q3'] . '</td>';
    echo '<td>' . $item['a2q4'] . '</td>';
    echo '<td>' . $item['b3q1'] . '</td>';
    echo '<td>' . $item['b3q2'] . '</td>';
    echo '<td>' . $item['b3q3'] . '</td>';
    echo '<td>' . $item['b3q4'] . '</td>';
    echo '<td>' . $item['a3q1'] . '</td>';
    echo '<td>' . $item['a3q2'] . '</td>';
    echo '<td>' . $item['a3q3'] . '</td>';
    echo '<td>' . $item['a3q4'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>