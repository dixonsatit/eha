<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_midyearpopmoph';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>male2</th>
                         <th>female2</th>
                         <th>total2</th>
                         <th>male1</th>
                         <th>frmale1</th>
                         <th>total1</th>
                         <th>male</th>
                         <th>female</th>
                         <th>total</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['male2'] . '</td>';
    echo '<td>' . $item['female2'] . '</td>';
    echo '<td>' . $item['total2'] . '</td>';
    echo '<td>' . $item['male1'] . '</td>';
    echo '<td>' . $item['female1'] . '</td>';
    echo '<td>' . $item['total1'] . '</td>';
    echo '<td>' . $item['male'] . '</td>';
    echo '<td>' . $item['female'] . '</td>';
    echo '<td>' . $item['total'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>