<?php

use yii\helpers\Html;
use yii\AGErid\AGEridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchanAGEe\models\SkpiheiAGEht05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         AGEetDatatable('#datatables');
JS;
$this->reAGEisterJs($script);
?>
<?php
$this->title = 'ตาราง s_pop_sex_age_moph';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>MALE_AGE1</th>
                         <th>FEMALE_AGE1</th>
                         <th>MALE_AGE2</th>
                         <th>FEMALE_AGE2</th>
                         <th>MALE_AGE3</th>
                         <th>FEMALE_AGE3</th>
                         <th>MALE_AGE4</th>
                         <th>FEMALE_AGE4</th>
                         <th>MALE_AGE5</th>
                         <th>FEMALE_AGE5</th>
                         <th>MALE_AGE6</th>
                         <th>FEMALE_AGE6</th>
                         <th>MALE_AGE7</th>
                         <th>FEMALE_AGE7</th>
                         <th>MALE_AGE8</th>
                         <th>FEMALE_AGE8</th>
                         <th>MALE_AGE9</th>
                         <th>FEMALE_AGE9</th>
                         <th>MALE_AGE10</th>
                         <th>FEMALE_AGE10</th>
                         <th>MALE_AGE11</th>
                         <th>FEMALE_AGE11</th>
                         <th>MALE_AGE12</th>
                         <th>FEMALE_AGE12</th>
                         <th>MALE_AGE13</th>
                         <th>FEMALE_AGE13</th>
                         <th>MALE_AGE14</th>
                         <th>FEMALE_AGE14</th>
                         <th>MALE_AGE15</th>
                         <th>FEMALE_AGE15</th>
                         <th>MALE_AGE16</th>
                         <th>FEMALE_AGE16</th>
                         <th>MALE_AGE17</th>
                         <th>FEMALE_AGE17</th>
                         <th>MALE_AGE18</th>
                         <th>FEMALE_AGE18</th>
                         <th>MALE_AGE19</th>
                         <th>FEMALE_AGE19</th>
                         <th>MALE_AGE20</th>
                         <th>FEMALE_AGE20</th>
                         <th>MALE_AGE21</th>
                         <th>FEMALE_AGE21</th>
                         <th>MALE_AGE22</th>
                         <th>FEMALE_AGE22</th>
                         <th>MALE_AGE23</th>
                         <th>FEMALE_AGE23</th>
                         <th>MALE_AGE24</th>
                         <th>FEMALE_AGE24</th>
                         <th>MALE_AGE25</th>
                         <th>FEMALE_AGE25</th>
                         <th>MALE_AGE26</th>
                         <th>FEMALE_AGE26</th>
                         <th>MALE_AGE27</th>
                         <th>FEMALE_AGE27</th>
                         <th>MALE_AGE28</th>
                         <th>FEMALE_AGE28</th>
                         <th>MALE_AGE29</th>
                         <th>FEMALE_AGE29</th>
                         <th>MALE_AGE30</th>
                         <th>FEMALE_AGE30</th>
                         <th>MALE_AGE31</th>
                         <th>FEMALE_AGE31</th>
                         <th>MALE_AGE32</th>
                         <th>feMALE_AGE32</th>
                         <th>MALE_AGE33</th>
                         <th>FEMALE_AGE33</th>
                         <th>MALE_AGE34</th>
                         <th>FEMALE_AGE34</th>
                         <th>MALE_AGE35</th>
                         <th>FEMALE_AGE35</th>
                         <th>MALE_AGE36</th>
                         <th>FEMALE_AGE36</th>
                         <th>MALE_AGE37</th>
                         <th>FEMALE_AGE37</th>
                         <th>MALE_AGE38</th>
                         <th>FEMALE_AGE38</th>
                         <th>MALE_AGE39</th>
                         <th>FEMALE_AGE39</th>
                         <th>MALE_AGE40</th>
                         <th>FEMALE_AGE40</th>
                         <th>MALE_AGE41</th>
                         <th>FEMALE_AGE41</th>
                         <th>MALE_AGE42</th>
                         <th>FEMALE_AGE42</th>
                         <th>MALE_AGE43</th>
                         <th>FEMALE_AGE43</th>
                         <th>MALE_AGE44</th>
                         <th>FEMALE_AGE44</th>
                         <th>MALE_AGE45</th>
                         <th>FEMALE_AGE45</th>
                         <th>MALE_AGE46</th>
                         <th>FEMALE_AGE46</th>
                         <th>MALE_AGE47</th>
                         <th>FEMALE_AGE47</th>
                         <th>MALE_AGE48</th>
                         <th>FEMALE_AGE48</th>
                         <th>MALE_AGE49</th>
                         <th>FEMALE_AGE49</th>
                         <th>MALE_AGE50</th>
                         <th>FEMALE_AGE50</th>
                         <th>MALE_AGE51</th>
                         <th>FEMALE_AGE51</th>
                         <th>MALE_AGE52</th>
                         <th>FEMALE_AGE52</th>
                         <th>MALE_AGE53</th>
                         <th>FEMALE_AGE53</th>
                         <th>MALE_AGE54</th>
                         <th>FEMALE_AGE54</th>
                         <th>MALE_AGE55</th>
                         <th>FEMALE_AGE55</th>
                         <th>MALE_AGE56</th>
                         <th>FEMALE_AGE56</th>
                         <th>MALE_AGE57</th>
                         <th>FEMALE_AGE57</th>
                         <th>MALE_AGE58</th>
                         <th>FEMALE_AGE58</th>
                         <th>MALE_AGE59</th>
                         <th>FEMALE_AGE59</th>
                         <th>MALE_AGE60</th>
                         <th>FEMALE_AGE60</th>
                         <th>MALE_AGE61</th>
                         <th>FEMALE_AGE61</th>
                         <th>MALE_AGE62</th>
                         <th>FEMALE_AGE62</th>
                         <th>MALE_AGE63</th>
                         <th>FEMALE_AGE63</th>
                         <th>MALE_AGE64</th>
                         <th>FEMALE_AGE64</th>
                         <th>MALE_AGE65</th>
                         <th>FEMALE_AGE65</th>
                         <th>MALE_AGE66</th>
                         <th>FEMALE_AGE66</th>
                         <th>MALE_AGE67</th>
                         <th>FEMALE_AGE67</th>
                         <th>MALE_AGE68</th>
                         <th>FEMALE_AGE68</th>
                         <th>MALE_AGE69</th>
                         <th>FEMALE_AGE69</th>
                         <th>MALE_AGE70</th>
                         <th>FEMALE_AGE70</th>
                         <th>MALE_AGE71</th>
                         <th>FEMALE_AGE71</th>
                         <th>MALE_AGE72</th>
                         <th>FEMALE_AGE72</th>
                         <th>MALE_AGE73</th>
                         <th>FEMALE_AGE73</th>
                         <th>MALE_AGE74</th>
                         <th>FEMALE_AGE74</th>
                         <th>MALE_AGE75</th>
                         <th>FEMALE_AGE75</th>
                         <th>MALE_AGE76</th>
                         <th>FEMALE_AGE76</th>
                         <th>MALE_AGE77</th>
                         <th>FEMALE_AGE77</th>
                         <th>MALE_AGE78</th>
                         <th>FEMALE_AGE78</th>
                         <th>MALE_AGE79</th>
                         <th>FEMALE_AGE79</th>
                         <th>MALE_AGE80</th>
                         <th>FEMALE_AGE80</th>
                         <th>MALE_AGE91</th>
                         <th>FEMALE_AGE91</th>
                         <th>MALE_AGE92</th>
                         <th>FEMALE_AGE92</th>
                         <th>MALE_AGE93</th>
                         <th>FEMALE_AGE93</th>
                         <th>MALE_AGE94</th>
                         <th>FEMALE_AGE94</th>
                         <th>MALE_AGE95</th>
                         <th>FEMALE_AGE95</th>
                         <th>MALE_AGE96</th>
                         <th>FEMALE_AGE96</th>
                         <th>MALE_AGE97</th>
                         <th>FEMALE_AGE97</th>
                         <th>MALE_AGE98</th>
                         <th>FEMALE_AGE98</th>
                         <th>MALE_AGE99</th>
                         <th>FEMALE_AGE99</th>
                         <th>MALE_AGE100</th>
                         <th>FEMALE_AGE100</th>
                         <th>MALE_AGEM100</th>
                         <th>FEMALE_AGEM100</th>
                         <th>MALE_TDOB</th>
                         <th>FEMALE_TDOB</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';
    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['MALE_AGE1'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE1'] . '</td>';
    echo '<td>' . $item['MALE_AGE2'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE2'] . '</td>';
    echo '<td>' . $item['MALE_AGE3'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE3'] . '</td>';
    echo '<td>' . $item['MALE_AGE4'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE4'] . '</td>';
    echo '<td>' . $item['MALE_AGE5'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE5'] . '</td>';
    echo '<td>' . $item['MALE_AGE6'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE6'] . '</td>';
    echo '<td>' . $item['MALE_AGE7'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE7'] . '</td>';
    echo '<td>' . $item['MALE_AGE8'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE8'] . '</td>';
    echo '<td>' . $item['MALE_AGE9'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE9'] . '</td>';
    echo '<td>' . $item['MALE_AGE10'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE10'] . '</td>';
    echo '<td>' . $item['MALE_AGE11'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE11'] . '</td>';
    echo '<td>' . $item['MALE_AGE12'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE12'] . '</td>';
    echo '<td>' . $item['MALE_AGE13'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE13'] . '</td>';
    echo '<td>' . $item['MALE_AGE14'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE14'] . '</td>';
    echo '<td>' . $item['MALE_AGE15'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE15'] . '</td>';
    echo '<td>' . $item['MALE_AGE16'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE16'] . '</td>';
    echo '<td>' . $item['MALE_AGE17'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE17'] . '</td>';
    echo '<td>' . $item['MALE_AGE18'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE18'] . '</td>';
    echo '<td>' . $item['MALE_AGE19'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE19'] . '</td>';
    echo '<td>' . $item['MALE_AGE20'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE20'] . '</td>';
    echo '<td>' . $item['MALE_AGE21'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE21'] . '</td>';
    echo '<td>' . $item['MALE_AGE22'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE22'] . '</td>';
    echo '<td>' . $item['MALE_AGE23'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE23'] . '</td>';
    echo '<td>' . $item['MALE_AGE24'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE24'] . '</td>';
    echo '<td>' . $item['MALE_AGE25'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE25'] . '</td>';
    echo '<td>' . $item['MALE_AGE26'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE26'] . '</td>';
    echo '<td>' . $item['MALE_AGE27'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE27'] . '</td>';
    echo '<td>' . $item['MALE_AGE28'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE28'] . '</td>';
    echo '<td>' . $item['MALE_AGE29'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE29'] . '</td>';
    echo '<td>' . $item['MALE_AGE30'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE30'] . '</td>';
    echo '<td>' . $item['MALE_AGE31'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE31'] . '</td>';
    echo '<td>' . $item['MALE_AGE32'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE32'] . '</td>';
    echo '<td>' . $item['MALE_AGE33'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE33'] . '</td>';
    echo '<td>' . $item['MALE_AGE34'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE34'] . '</td>';
    echo '<td>' . $item['MALE_AGE35'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE35'] . '</td>';
    echo '<td>' . $item['MALE_AGE36'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE36'] . '</td>';
    echo '<td>' . $item['MALE_AGE37'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE37'] . '</td>';
    echo '<td>' . $item['MALE_AGE38'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE38'] . '</td>';
    echo '<td>' . $item['MALE_AGE39'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE39'] . '</td>';
    echo '<td>' . $item['MALE_AGE40'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE40'] . '</td>';
    echo '<td>' . $item['MALE_AGE41'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE41'] . '</td>';
    echo '<td>' . $item['MALE_AGE42'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE42'] . '</td>';
    echo '<td>' . $item['MALE_AGE43'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE43'] . '</td>';
    echo '<td>' . $item['MALE_AGE44'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE44'] . '</td>';
    echo '<td>' . $item['MALE_AGE45'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE45'] . '</td>';
    echo '<td>' . $item['MALE_AGE46'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE46'] . '</td>';
    echo '<td>' . $item['MALE_AGE47'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE47'] . '</td>';
    echo '<td>' . $item['MALE_AGE48'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE48'] . '</td>';
    echo '<td>' . $item['MALE_AGE49'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE49'] . '</td>';
    echo '<td>' . $item['MALE_AGE50'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE50'] . '</td>';
    echo '<td>' . $item['MALE_AGE51'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE51'] . '</td>';
    echo '<td>' . $item['MALE_AGE52'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE52'] . '</td>';
    echo '<td>' . $item['MALE_AGE53'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE53'] . '</td>';
    echo '<td>' . $item['MALE_AGE54'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE54'] . '</td>';
    echo '<td>' . $item['MALE_AGE55'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE55'] . '</td>';
    echo '<td>' . $item['MALE_AGE56'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE56'] . '</td>';
    echo '<td>' . $item['MALE_AGE57'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE57'] . '</td>';
    echo '<td>' . $item['MALE_AGE58'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE58'] . '</td>';
    echo '<td>' . $item['MALE_AGE59'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE59'] . '</td>';
    echo '<td>' . $item['MALE_AGE60'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE60'] . '</td>';
    echo '<td>' . $item['MALE_AGE61'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE61'] . '</td>';
    echo '<td>' . $item['MALE_AGE62'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE62'] . '</td>';
    echo '<td>' . $item['MALE_AGE63'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE63'] . '</td>';
    echo '<td>' . $item['MALE_AGE64'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE64'] . '</td>';
    echo '<td>' . $item['MALE_AGE65'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE65'] . '</td>';
    echo '<td>' . $item['MALE_AGE66'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE66'] . '</td>';
    echo '<td>' . $item['MALE_AGE67'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE67'] . '</td>';
    echo '<td>' . $item['MALE_AGE68'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE68'] . '</td>';
    echo '<td>' . $item['MALE_AGE69'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE69'] . '</td>';
    echo '<td>' . $item['MALE_AGE70'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE70'] . '</td>';
    echo '<td>' . $item['MALE_AGE71'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE71'] . '</td>';
    echo '<td>' . $item['MALE_AGE72'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE72'] . '</td>';
    echo '<td>' . $item['MALE_AGE73'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE73'] . '</td>';
    echo '<td>' . $item['MALE_AGE74'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE74'] . '</td>';
    echo '<td>' . $item['MALE_AGE75'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE75'] . '</td>';
    echo '<td>' . $item['MALE_AGE76'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE76'] . '</td>';
    echo '<td>' . $item['MALE_AGE77'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE77'] . '</td>';
    echo '<td>' . $item['MALE_AGE78'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE78'] . '</td>';
    echo '<td>' . $item['MALE_AGE79'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE79'] . '</td>';
    echo '<td>' . $item['MALE_AGE80'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE80'] . '</td>';
    echo '<td>' . $item['MALE_AGE81'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE81'] . '</td>';
    echo '<td>' . $item['MALE_AGE82'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE82'] . '</td>';
    echo '<td>' . $item['MALE_AGE83'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE83'] . '</td>';
    echo '<td>' . $item['MALE_AGE84'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE84'] . '</td>';
    echo '<td>' . $item['MALE_AGE85'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE85'] . '</td>';
    echo '<td>' . $item['MALE_AGE86'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE86'] . '</td>';
    echo '<td>' . $item['MALE_AGE87'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE87'] . '</td>';
    echo '<td>' . $item['MALE_AGE88'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE88'] . '</td>';
    echo '<td>' . $item['MALE_AGE89'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE89'] . '</td>';
    echo '<td>' . $item['MALE_AGE90'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE90'] . '</td>';
    echo '<td>' . $item['MALE_AGE91'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE91'] . '</td>';
    echo '<td>' . $item['MALE_AGE92'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE92'] . '</td>';
    echo '<td>' . $item['MALE_AGE93'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE93'] . '</td>';
    echo '<td>' . $item['MALE_AGE94'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE94'] . '</td>';
    echo '<td>' . $item['MALE_AGE95'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE95'] . '</td>';
    echo '<td>' . $item['MALE_AGE96'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE96'] . '</td>';
    echo '<td>' . $item['MALE_AGE97'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE97'] . '</td>';
    echo '<td>' . $item['MALE_AGE98'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE98'] . '</td>';
    echo '<td>' . $item['MALE_AGE99'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE99'] . '</td>';
    echo '<td>' . $item['MALE_AGE100'] . '</td>';
    echo '<td>' . $item['FEMALE_AGE100'] . '</td>';
    echo '<td>' . $item['MALE_AGEM100'] . '</td>';
    echo '<td>' . $item['FEMALE_AGEM100'] . '</td>';
    echo '<td>' . $item['MALE_TDOB'] . '</td>';
    echo '<td>' . $item['FEMALE_TDOB'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>