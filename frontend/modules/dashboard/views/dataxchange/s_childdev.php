<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_childdev';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>group_age</th>
                         <th>resultall_1</th>
                         <th>resultall_2</th>
                         <th>resultall_3</th>
                         <th>resultall_not123</th>
                         <th>resultin_1</th>
                         <th>resultin_2</th>
                         <th>resultin_3</th>
                         <th>resultin_not123</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['group_age'] . '</td>';
    echo '<td>' . $item['resultall_1'] . '</td>';
    echo '<td>' . $item['resultall_2'] . '</td>';
    echo '<td>' . $item['resultall_3'] . '</td>';
    echo '<td>' . $item['resultall_not123'] . '</td>';
    echo '<td>' . $item['resultin_1'] . '</td>';
    echo '<td>' . $item['resultin_2'] . '</td>';
    echo '<td>' . $item['resultin_3'] . '</td>';
    echo '<td>' . $item['resultin_not123'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>