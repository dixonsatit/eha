<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_postnatal';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target</th>
                         <th>result</th>
                         <th>target10</th>
                         <th>result10</th>
                         <th>target11</th>
                         <th>result11</th>
                         <th>target12</th>
                         <th>result12</th>
                         <th>target01</th>
                         <th>result01</th>
                         <th>target02</th>
                         <th>result02</th>
                         <th>target03</th>
                         <th>result03</th>
                         <th>target04</th>
                         <th>result04</th>
                         <th>target05</th>
                         <th>result05</th>
                         <th>target06</th>
                         <th>result06</th>
                         <th>target07</th>
                         <th>result07</th>
                         <th>target08</th>
                         <th>result08</th>
                         <th>target09</th>
                         <th>result09</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target'] . '</td>';
    echo '<td>' . $item['reslut'] . '</td>';
    echo '<td>' . $item['target10'] . '</td>';
    echo '<td>' . $item['result10'] . '</td>';
    echo '<td>' . $item['target11'] . '</td>';
    echo '<td>' . $item['result11'] . '</td>';
    echo '<td>' . $item['target12'] . '</td>';
    echo '<td>' . $item['result12'] . '</td>';
    echo '<td>' . $item['target01'] . '</td>';
    echo '<td>' . $item['result01'] . '</td>';
    echo '<td>' . $item['target02'] . '</td>';
    echo '<td>' . $item['result02'] . '</td>';
    echo '<td>' . $item['target03'] . '</td>';
    echo '<td>' . $item['result03'] . '</td>';
    echo '<td>' . $item['target04'] . '</td>';
    echo '<td>' . $item['result04'] . '</td>';
    echo '<td>' . $item['target05'] . '</td>';
    echo '<td>' . $item['result05'] . '</td>';
    echo '<td>' . $item['target06'] . '</td>';
    echo '<td>' . $item['result06'] . '</td>';
    echo '<td>' . $item['target07'] . '</td>';
    echo '<td>' . $item['result07'] . '</td>';
    echo '<td>' . $item['target08'] . '</td>';
    echo '<td>' . $item['result08'] . '</td>';
    echo '<td>' . $item['target09'] . '</td>';
    echo '<td>' . $item['result09'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>