<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_anc';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target</th>
                         <th>result</th>
                         <th>result01</th>
                         <th>result02</th>
                         <th>result03</th>
                         <th>result04</th>
                         <th>result05</th>
                         <th>result06</th>
                         <th>result07</th>
                         <th>result08</th>
                         <th>result09</th>
                         <th>result10</th>
                         <th>result11</th>
                         <th>result12</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target'] . '</td>';
    echo '<td>' . $item['result'] . '</td>';
    echo '<td>' . $item['result01'] . '</td>';
    echo '<td>' . $item['result02'] . '</td>';
    echo '<td>' . $item['result03'] . '</td>';
    echo '<td>' . $item['result04'] . '</td>';
    echo '<td>' . $item['result05'] . '</td>';
    echo '<td>' . $item['result06'] . '</td>';
    echo '<td>' . $item['result07'] . '</td>';
    echo '<td>' . $item['result08'] . '</td>';
    echo '<td>' . $item['result09'] . '</td>';
    echo '<td>' . $item['result10'] . '</td>';
    echo '<td>' . $item['result11'] . '</td>';
    echo '<td>' . $item['result12'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>