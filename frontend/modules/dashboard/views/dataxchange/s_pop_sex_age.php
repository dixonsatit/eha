<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_pop_sex_age';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>male_g1</th>
                         <th>female_g1</th>
                         <th>male_g2</th>
                         <th>female_g2</th>
                         <th>male_g3</th>
                         <th>female_g3</th>
                         <th>male_g4</th>
                         <th>female_g4</th>
                         <th>male_g5</th>
                         <th>female_g5</th>
                         <th>male_g6</th>
                         <th>female_g6</th>
                         <th>male_g7</th>
                         <th>female_g7</th>
                         <th>male_g8</th>
                         <th>female_g8</th>
                         <th>male_g9</th>
                         <th>female_g9</th>
                         <th>male_g10</th>
                         <th>female_g10</th>
                         <th>male_g11</th>
                         <th>female_g11</th>
                         <th>male_g12</th>
                         <th>female_g12</th>
                         <th>male_g13</th>
                         <th>female_g13</th>
                         <th>male_g14</th>
                         <th>female_g14</th>
                         <th>male_g15</th>
                         <th>female_g15</th>
                         <th>male_g16</th>
                         <th>female_g16</th>
                         <th>male_g17</th>
                         <th>female_g17</th>
                         <th>male_g18</th>
                         <th>female_g18</th>
                         <th>male_g19</th>
                         <th>female_g19</th>
                         <th>male_g20</th>
                         <th>female_g20</th>
                         <th>male_g21</th>
                         <th>female_g21</th>
                         <th>male_g22</th>
                         <th>female_g22</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';
    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['male_g1'] . '</td>';
    echo '<td>' . $item['female_g1'] . '</td>';
    echo '<td>' . $item['male_g2'] . '</td>';
    echo '<td>' . $item['female_g2'] . '</td>';
    echo '<td>' . $item['male_g3'] . '</td>';
    echo '<td>' . $item['female_g3'] . '</td>';
    echo '<td>' . $item['male_g4'] . '</td>';
    echo '<td>' . $item['female_g4'] . '</td>';
    echo '<td>' . $item['male_g5'] . '</td>';
    echo '<td>' . $item['female_g5'] . '</td>';
    echo '<td>' . $item['male_g6'] . '</td>';
    echo '<td>' . $item['female_g6'] . '</td>';
    echo '<td>' . $item['male_g7'] . '</td>';
    echo '<td>' . $item['female_g7'] . '</td>';
    echo '<td>' . $item['male_g8'] . '</td>';
    echo '<td>' . $item['female_g8'] . '</td>';
    echo '<td>' . $item['male_g9'] . '</td>';
    echo '<td>' . $item['female_g9'] . '</td>';
    echo '<td>' . $item['male_g10'] . '</td>';
    echo '<td>' . $item['female_g10'] . '</td>';
    echo '<td>' . $item['male_g11'] . '</td>';
    echo '<td>' . $item['female_g11'] . '</td>';
    echo '<td>' . $item['male_g12'] . '</td>';
    echo '<td>' . $item['female_g12'] . '</td>';
    echo '<td>' . $item['male_g13'] . '</td>';
    echo '<td>' . $item['female_g13'] . '</td>';
    echo '<td>' . $item['male_g14'] . '</td>';
    echo '<td>' . $item['female_g14'] . '</td>';
    echo '<td>' . $item['male_g15'] . '</td>';
    echo '<td>' . $item['female_g15'] . '</td>';
    echo '<td>' . $item['male_g16'] . '</td>';
    echo '<td>' . $item['female_g16'] . '</td>';
    echo '<td>' . $item['male_g17'] . '</td>';
    echo '<td>' . $item['female_g17'] . '</td>';
    echo '<td>' . $item['male_g18'] . '</td>';
    echo '<td>' . $item['female_g18'] . '</td>';
    echo '<td>' . $item['male_g19'] . '</td>';
    echo '<td>' . $item['female_g19'] . '</td>';
    echo '<td>' . $item['male_g20'] . '</td>';
    echo '<td>' . $item['female_g20'] . '</td>';
    echo '<td>' . $item['male_g21'] . '</td>';
    echo '<td>' . $item['female_g21'] . '</td>';
    echo '<td>' . $item['male_g22'] . '</td>';
    echo '<td>' . $item['female_g22'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>