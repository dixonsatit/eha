<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\dataxchange\models\Skpiheight05Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$script = <<< JS
        //Datatables
         getDatatable('#datatables');
JS;
$this->registerJs($script);
?>
<?php
$this->title = 'ตาราง s_kpi_labor_repeate';
$this->params['breadcrumbs'][] = ['label' => 'ประมวลผลข้อมูล', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
                    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">
                         <th></th>
                         <th>hospcode</th>
                         <th>areacode</th>
                         <th>date_com</th>
                         <th>b_year</th>
                         <th>target</th>
                         <th>result</th>
                         <th>targetq1</th>
                         <th>resultq1</th>
                         <th>targetq2</th>
                         <th>resultq2</th>
                         <th>targetq3</th>
                         <th>resultq3</th>
                         <th>targetq4</th>
                         <th>resultq4</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
foreach ($data as $key => $item) {
    $date = date_create($item['date_com']);
    echo '<tr>';

    echo '<td>' . ($key + 1) . '</td>';
    echo '<td>' . $item['hospcode'] . '</td>';
    echo '<td>' . $item['areacode'] . '</td>';
    echo '<td>' .date_format($date,"Y-m-d") . '</td>';
    echo '<td>' . $item['b_year'] . '</td>';
    echo '<td>' . $item['target'] . '</td>';
    echo '<td>' . $item['result'] . '</td>';
    echo '<td>' . $item['target1'] . '</td>';
    echo '<td>' . $item['result1'] . '</td>';
    echo '<td>' . $item['target2'] . '</td>';
    echo '<td>' . $item['result2'] . '</td>';
    echo '<td>' . $item['target3'] . '</td>';
    echo '<td>' . $item['result3'] . '</td>';
    echo '<td>' . $item['target4'] . '</td>';
    echo '<td>' . $item['result4'] . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                    </table>            

                    </div>
</div>