<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;


$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");
$session['ap'] = $request->get('ap');

$this->title = $kpi['kpi_name']. ' รายตำบล';
foreach ($viewElement['breadcrumb'] as $value) {
    $this->params['breadcrumbs'][] = $value;
}
?>

<?=$this->render('//_header')?>


<div class="content animate-panel">


<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData,'plotBands'=>$plotBands])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Map -->
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl, 'mapType' => $mapType, 'mapLegend' => $mapLegend])?>


                    </div>

                </div>
            </div>

</div>


<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>

                    </div>
                </div>
            </div>

</div>



<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>
                    
                    </div>
                    
                </div>
            </div>


    </div>
</div>