<?php
$region = substr('00' . (string) $region, -2);
$conn = Yii::$app->db;
$sql = "SELECT p.*,a.* 
                FROM data_analyze a
                INNER JOIN data_analyze_period p ON a.period_id = p.period_code
                WHERE a.year_id = :year AND find_in_set(:kpi,kpi_id_related)<>0;";
//            echo $sql;
//            echo $year.' - '.$region.' - '.$kpi;
$rs = $conn->createCommand($sql)
        ->bindValue(':year', $year)
        ->bindValue(':kpi', $kpi);
$row = $rs->queryOne();
//            print_r($row);
?>
<?php
$publish = 0;
$publish = $row["situation_is_published"] + $row["process_is_published"] + $row["problem_is_published"] + $row["ksf_is_published"] + $row["recommend_is_published"];
?>
<?php
if ($publish) {
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
/* Remove the navbar's default margin-bottom and rounded borders */ 
.navbar {
    margin-bottom: 0;
    border-radius: 0;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {height: 100%}

/* Set gray background color and 100% height */
.sidenav {
    padding-top: 10px;
    background-color: #f1f1f1;
    height: 100%;
}

/* Set black background color, white text and some padding */
footer {
    background-color: #555;
    color: white;
    padding: 10px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
    .sidenav {
    height: auto;
    padding: 10px;
    }
    .row.content {height:auto;} 
}
</style>

<div class="container-fluid text-center" style="padding-top: 10px;">   

  <div class="row content">

    <div class="col-sm-2 sidenav text-left">
      <h4>ข้อมูลที่เกี่ยวข้อง</h4>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
    </div>
    <!-- <div class="col-sm-2 sidenav"> -->

    <div class="col-sm-6 text-left"> 
      <h4>วิเคราะห์สถานการณ์จากข้อมูล</h4>
      <p>
        <?php 
        if ($row["situation_is_published"]) { ?>
            <?= $row["situation_desc"] ?>
        <?php 
        } ?>
      </p>
      <hr>
      <h4>ขั้นตอนการดำเนินงาน</h4>
      <?php 
        if ($row["process_is_published"]) { ?>
        <p><?= $row["process_desc"] ?></p>
        <?php 
        } ?>
      <hr>
      <h4>ปัจจัยความสำเร็จ</h4>
      <?php 
        if ($row["ksf_is_published"]) { ?>
        <p><?= $row["ksf_desc"] ?></p>
        <?php 
        } ?>
    </div>
    <!-- <div class="col-sm-6 text-left">  -->

    <div class="col-sm-4 sidenav">
      
        <h4>ปัญหา/อุปสรรค</h4>  
        <div class="well text-left">  
        <?php 
        if ($row["problem_is_published"]) { ?>
            <p><?= $row["problem_desc"] ?></p>
        <?php 
        } ?>
        </div>
      
        <h4>ข้อเสนอแนะ</h4>
        <div class="well text-left">
        <?php 
        if ($row["recommend_is_published"]) { ?>
            <div class="well">
            <p><?= $row["recommend_desc"] ?></p>
            </div>
        <?php 
        } ?>

    </div>
    <!-- <div class="col-sm-4 sidenav"> -->

  </div>
  <!-- <div class="row content"> -->

</div>
<!-- <div class="container-fluid text-center">    -->

<?php
} //end if ($publish)
?>