<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive nowrap" width="100%">
	<thead>        
		<tr class="info"style="padding:0px;" role="row">
			<th class="text-center" rowspan="1" colspan="1">&nbsp;</th>
			<th class="text-center col-lg-3" rowspan="1" colspan="1"><?=$scope?></th>
			<th class="text-center" colspan="1" rowspan="1">ร้อยละด้านการบริโภคอาหาร</th>
			<th class="text-center" colspan="1" rowspan="1">ร้อยละด้านกิจกรรมทางกาย</th>
			<th class="text-center" colspan="1" rowspan="1">ร้อยละด้านทันตสุขภาพ</th>			
			<th class="text-center" rowspan="1" colspan="1">ร้อยละภาพรวมทั้งหมด</th>
		</tr>
    </thead>
	<tfoot>
		<tr class="info" style="padding:0px;" role="row">
			<td colspan="6">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก ผลสำรวจพฤติกรรมที่พึงประสงค์)</td>
		</tr>
	</tfoot>
	<tbody style="padding:0px;" role="row">
		<?php
		if ($data) {
			foreach ($data as $key => $v) {
				echo '<tr>';
					echo '<td class="text-center">&nbsp;</td>';
				//ภาพรวม
//				if ($v['areacode'] !== 'TOTAL') {
////					if ($scope == 'ตำบล') {
//						echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
////					} else {
////						echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
////					}					
//				} else {
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
//				}
					echo '<td class="text-center">'.number_format($v['ratio1'],2).'</td>';
					echo '<td class="text-center">'.number_format($v['ratio2'],2).'</td>';
					echo '<td class="text-center">'.number_format($v['ratio3'],2).'</td>';
					echo '<td class="text-center">'.number_format($v['total_ratio'],2).'</td>';
				echo '</tr>';

			} //end foreach
		} //end if  
		?>
	</tbody>
</table>

<div class="well">
	<b>หมายเหตุ:</b><br>

	<?=DashboardServices::getReportdate($report_date);?>
	
</div>

