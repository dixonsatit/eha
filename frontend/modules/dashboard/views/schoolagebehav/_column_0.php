<?php
use miloschuman\highcharts\Highcharts;
?>


<?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'column',
        ],
        'title'=> [
				'text'=> 'ร้อยละของเด็ก ป.4 และ 12 ปี มีพฤติกรรมสุขภาพที่พึงประสงค์ทั้ง 3 ด้าน'
				],
		'subtitle'=> [
				//'text'=> 'ปีงบประมาณ'
				],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'max' => 100,
            'title' => [
//               'text' => 'ดัชนีมวลกาย'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                // 'value' => 50,
//                'value' => (float) $kpi['target_value'],
				'value' => '',
                'color' => 'red',
                'width' => 2,
                'label' => [
                    //'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
                    'align' => 'left',
                    'style' => [
                        'color' => 'red',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => true,
        ],
        'tooltip' => [
            'pointFormat' => '',
        ],
        'series' => [
            [
                'name' => 'ป.4',
                'colorByPoint' => false,
                 'color' => '#4dc3ff',
                // 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData0['Q1'],
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                'tooltip' => [
                    'pointFormat' => 'ป.4',
                ],
            ],
            [
                'name' => '12 ปี',
                'colorByPoint' => false,
                 'color' => '#ff4d88',
                // 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData0['Q2'],
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                'tooltip' => [
                    'pointFormat' => '12 ปี',
                ],
            ],			
        ],
    ],
]);
?>

