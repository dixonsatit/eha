<?php
use miloschuman\highcharts\Highcharts;
?>

<?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart'=> [
            'type'=> 'line'
        ],
        'title'=> [
            'text'=> 'แนวโน้มรายปี'
        ],
        'subtitle'=> [
            //'text'=> 'ปีงบประมาณ'
        ],
        'xAxis'=>[
            'type' => 'category',
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize'=>'13px',
                    'fontFamily'=>'Verdana, sans-serif'
                ]
            ]
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
             'text' => ''
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'label' => [
                    //'text' => 'Theoretical mean => 932',
                    'align' => 'center',
                    'style' => [
                        'color' => 'gray'
                    ]
                ]
            ]]
        ],
        'legend' => [
            'enabled' => false
        ],
        'tooltip' => [
            'pointFormat' => 'ข้อมูล {point.y:.2f}',
        ],
        'series' => [
            [
                'name' => 'Population',
                'colorByPoint' => false,
                'color'=> '#993366',
                //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                'data'=>$trendData['Q1'],
                'dataLabels'=> [
                    'enabled'=> true,
                    'rotation'=> 0,
                    'color'=> 'gray',
                    'align'=> 'right',
                    'format'=> '{point.y:.2f}',
                    'y' => 0,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif'
                    ]
                ]
            ],[
                'name' => 'Population',
                'colorByPoint' => false,
                 'color'=> '#ffff00',
                //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                'data'=>$trendData['Q2'],
                'dataLabels'=> [
                    'enabled'=> true,
                    'rotation'=> 0,
                    'color'=> 'gray',
                    'align'=> 'right',
                    'format'=> '{point.y:.2f}',
                    'y' => 0,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif'
                    ]
                ]
            ],[
                'name' => 'Population',
                'colorByPoint' => false,
                 'color'=> '#ff33cc',
                //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                'data'=>$trendData['Q3'],
                'dataLabels'=> [
                    'enabled'=> true,
                    'rotation'=> 0,
                    'color'=> 'gray',
                    'align'=> 'right',
                    'format'=> '{point.y:.2f}',
                    'y' => 0,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif'
                    ]
                ]
            ]
        ]
    ]
]);