<?php
$this->title = $kpi['name'].' ระดับอำเภอ';
foreach ($viewElement['breadcrumb'] as $value) {
    $this->params['breadcrumbs'][] = $value;
}

$titleGauge='สถานการณ์ภาพรวม';
$titleMap='สถานการณ์รายพื้นที่';
$titleTrend='แนวโน้มรายปี';
$titleColumn='เปรียบเทียบรายพื้นที่';
$titleDatatable='ตารางข้อมูลรายพื้นที่';
?>

<?=$this->render('//_header',['templatefile'=>'template_22.png'])?>

<div class="content animate-panel">
    <?= $this->render('_select_year', 
        [
           'rount' => 'ampur',
           'year' => $year,
           'allYear' => $allYear,
           'parameter' => 'cw',
           'parameterV' => $cw,
        ]);
    ?>
    <!-- Row for Gauge & Map -->
    <div class="row">
        <!-- Gauge Chart -->
        <div class="col-lg-4 col-md-12">
            <div class="hpanel">
                <div class="panel-body">
                    <!-- Gauge -->
                    <div style="height: 450px;">
                    <?= $this->render('_gauge', 
                        [
                           'gaugeData' => $gaugeData,
                           'plotBands'=>$plotBands,
                           'titleGauge'=>$titleGauge,
                        ]);
                    ?>
                    </div>
                </div>
                <!--<div class="panel-footer"></div> -->
            </div>
        </div>
        <!-- End Gauge Chart -->
        
        <!-- Map -->
        <div class="col-lg-8 col-md-12">
            <div class="hpanel">
                <div class="panel-body h-200">
                <!-- Map -->
                <?= $this->render('_map', 
                    [
                        'geo' => $geo, 
                        'mapUrl' => $mapUrl,
                        'mapType' => $mapType, 
                        'mapLegend' => $mapLegend,
                        'titleMap' => $titleMap,
                    ]);
                ?>
                </div>
            </div>
        </div>
        <!-- End Map -->
    </div>
    <!-- End Row for Gauge & Map -->
    
    <!-- Trend -->
    <div class="row">
        <div class="col-lg-4 col-md-12">
            <div class="hpanel">
                <div class="panel-body h-200">
                    <!-- Code Trend -->
                    <?= $this->render('_trend',
                        [
                            'kpi' => $kpi, 
                            'trendData' => $trendData,
                            'titleTrend'=>$titleTrend,
                        ]);
                    ?>

                </div>
                <!-- <div class="panel-footer"></div> -->
            </div>
        </div>
    <!--</div>-->
    <!-- End Trend -->
    
    <!-- Column Chart -->
    <!--<div class="row">-->
        <div class="col-lg-8 col-md-12">
            <div class="hpanel">
                <div class="panel-body h-200">
                    <!-- Code -->
                    <!-- Column -->
                    <?= $this->render('_column', 
                        [
                            'kpi' => $kpi, 
                            'columnData' => $columnData,
                            'titleColumn' => $titleColumn,
                        ]);
                    ?>

                </div>
                <!-- <div class="panel-footer"></div> -->
            </div>
        </div>
    </div>
    <!-- End Column Chart -->

    <!-- Data Table -->
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body h-200">
                <!-- Data Table -->
                <?= $this->render('_datatables', 
                    [
                        'kpi' => $kpi, 
                        'data' => $data,
                        'mapUrl' => $mapUrl,
                        'mapType' => $mapType,
                        'tableAreaText' => 'อำเภอ',
                    ]);
                ?>
                </div>
            </div>
          <!-- <div class="panel-footer"></div> -->
        </div>
    </div>
    <!-- End Data Table -->
   
</div> <!-- End animate-panel -->