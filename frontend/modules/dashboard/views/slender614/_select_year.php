<?php

use yii\helpers\Html;
?>
<div class="row visible-xs">

    <div class="col-xs-6">
        <div class="btn-group">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ปีงบประมาณ <?= $year + 543; ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php
                foreach ($allYear as $v) {
                    echo '<li>' . Html::a($v['b_year'], [$rount, 'year' => (int) $v['b_year'] - 543, $parameter => $parameterV]) . '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
    <hr class="col-xs-12">
</div>
<nav class="navbar navbar-default hidden-xs">
    <div class="container-fluid">

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        ปีงบประมาณ <?= $year + 543 ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">

                        <?php
                        foreach ($allYear as $v) {
                            echo '<li>' . Html::a($v['b_year'], [$rount, 'year' => (int) $v['b_year'] - 543, $parameter => $parameterV]) . '</li>';
                        }
                        ?>
                    </ul>
                </li>
            </ul>                
        </div>
    </div>
</nav>