<?php
use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        //'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'column',
        ],
        'title' => [
            'text' => $titleColumn,
        ],
        'plotOptions' => [
            'column' => [
                'pointPadding' => 0.05,
            ],
        ],
//        'subtitle' => [
//            'text'=> $kpi['target_value']>0 ? 'เป้าหมาย: ร้อยละ '.(float) $kpi['target_value']:'',
//            'align' => 'left',
//            'style' => [
//                'color' => 'red',
//                'fontFamily' => 'Verdana, sans-serif',
//                'fontSize' => '11pt',
//            ],
//        ],
        'xAxis' => [
            'type' => 'category',
            'gridLineWidth' => 1,
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize' => '14px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'max' => 100,
            'title' => [
//               'text' => 'ดัชนีมวลกาย'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                // 'value' => 50,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'label' => [
                    //'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
                    'align' => 'left',
                    'style' => [
                        'color' => 'red',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => false,
        ],
//        'tooltip' => [
//            'pointFormat' => '',
//        ],
        'tooltip' => [
            'backgroundColor' =>'#ffffff',
            'borderWidth' => 1,
            'headerFormat'=> '<span style="font-size:13px;"><b>{point.key}<b></span><table>',
            'pointFormat'=> '<tr><td style="color:{series.color};padding:0"><b>{series.name}: </b></td>'
                .'<td style="padding:0"> ร้อยละ <b>{point.y:.2f} </b></td></tr>',
            'footerFormat'=> '</table>',
            'shared'=> true,
            'useHTML'=> true,
        ],
        'series' => [
            [
                'name' => 'เทอม 2 ',
                'colorByPoint' => true,
                'colors' => $columnData['color1'],
//                'states'=> [
//                    'hover'=> [
//                        'color'=> '#3babd0'                                                           
//                    ]
//                ],
                //'color' => '#62cb31',
                // 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData['Q1'],
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'black',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
//                'tooltip' => [
//                    'pointFormat' => 'เทอม 1',
//                ],
            ],
            [
                'name' => 'เทอม 1 ',
                'colorByPoint' => true,
                'colors' => $columnData['color2'],
//                'states'=> [
//                    'hover'=> [
//                        'color'=> '#ffbf00'                                                           
//                    ]
//                ],
                // 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData['Q2'],
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'black',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
//                'tooltip' => [
//                    'pointFormat' => 'เทอม 2',
//                ],
            ],
        ],
    ],
]);
?>

