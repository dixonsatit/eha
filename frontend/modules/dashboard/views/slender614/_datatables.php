<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
    // Datatables
    getDatatable('#datatables',0)
JS;

$this->registerJs($script);
?>
<div class="text-center" style="font-size: 16px; font-weight: bold;color: black; padding-bottom: 5px;">ตารางข้อมูลรายพื้นที่</div>
<table id="datatables" class="table table-striped table-hover table-bordered">
    <thead>
        
      <tr class="info" style="padding:0px;">
        <th class="text-center" rowspan="4">ลำดับ</th>
        <th class="text-center" rowspan="4" style="min-width:80px;"><?= $tableAreaText ?></th>
        <th class="text-center" rowspan="4">สูงดีสมส่วน</th>
        <th class="text-center" colspan="15">เทอม 2</th>
        <th class="text-center" colspan="15">เทอม 1</th>
      </tr>
      <tr class="info" style="padding:0px;">
        <th class="text-center" colspan="9">ภาวะโภชนาการเด็กอายุ 6-14 ปี</th>
        <th class="text-center" colspan="6">ส่วนสูงเฉลี่ยของเด็กอายุ 12 ปี</th>
        <th class="text-center" colspan="9">ภาวะโภชนาการเด็กอายุ 6-14 ปี</th>
        <th class="text-center" colspan="6">ส่วนสูงเฉลี่ยของเด็กอายุ 12 ปี</th>
      </tr>
      <tr class="info" style="padding:0px;">
        <th class="text-center" rowspan="2" style="min-width:80px;">ชั่งน้ำหนักวัดส่วนสูง<br>(B1)</th>
        <th class="text-center" rowspan="2" style="min-width:60px;">สูงดีสมส่วน<br>(A1)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">ผอม<br>(A2)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">เริ่มอ้วนและอ้วน<br>(A3)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">เตี้ย<br>(A4)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" colspan="3">ชาย</th>
        <th class="text-center" colspan="3">หญิง</th>
        
        <th class="text-center" rowspan="2" style="min-width:80px;">ชั่งน้ำหนักวัดส่วนสูง<br>(B1)</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">สูงดีสมส่วน<br>(A1)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">ผอม<br>(A2)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">เริ่มอ้วนและอ้วน<br>(A3)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" rowspan="2" style="min-width:50px;">เตี้ย<br>(A4)</th>
        <th class="text-center" rowspan="2">%</th>
        <th class="text-center" colspan="3">ชาย</th>
        <th class="text-center" colspan="3">หญิง</th>
      </tr>
      <tr class="info" style="padding:0px;">
        <th class="text-center" style="min-width:50px;">วัดส่วนสูง<br>(B3)</th>
        <th class="text-center" style="min-width:50px;">ผลรวมส่วนสูง<br>(A5)</th>
        <th class="text-center" style="min-width:50px;">ส่วนสูงเฉลี่ย</th>
        <th class="text-center" style="min-width:50px;">วัดส่วนสูง<br>(B4)</th>
        <th class="text-center" style="min-width:50px;">ผลรวมส่วนสูง<br>(A6)</th>
        <th class="text-center" style="min-width:50px;">ส่วนสูงเฉลี่ย</th>
        
        <th class="text-center" style="min-width:50px;">วัดส่วนสูง<br>(B3)</th>
        <th class="text-center" style="min-width:50px;">ผลรวมส่วนสูง<br>(A5)</th>
        <th class="text-center" style="min-width:50px;">ส่วนสูงเฉลี่ย</th>
        <th class="text-center" style="min-width:50px;">วัดส่วนสูง<br>(B4)</th>
        <th class="text-center" style="min-width:50px;">ผลรวมส่วนสูง<br>(A6)</th>
        <th class="text-center" style="min-width:50px;">ส่วนสูงเฉลี่ย</th>
      </tr>
    </thead>

    <tbody>
    
    <?php
    $report_date = null;
    if($data!=NULL) {
        foreach ($data as $key=>$value) {
            echo '<tr>';
            echo '<td class="text-right">'.($key+1).'</td>';
            
            if ($value['areacode'] !== 'TOTAL') {
                $report_date = ($value['report_date']>$report_date)? $value['report_date']:$report_date;
                if (json_decode($mapType)!=='tb') {
                    echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode'], ['class' => 'success'],'_blank') . '</td>';
                }
                else {
                    echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
                }
            } else {
                echo '<td class="text-left"><b>' . $value['areaname_new'] . '</b></td>';
            }
            
            //echo '<td class="text-left">'.$value['areaname_new'].'</td>';
            echo '<td class="text-right"><b>'.number_format($value['total_ratio'],2).'</b></td>';
            
            echo '<td class="text-right" style="background-color:#ffffcc;">'.number_format($value['b1q1']).'</td>';
            echo '<td class="text-right">'.number_format($value['a1q1']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratioq1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a2q1']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a2q1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a3q1']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a3q1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a4q1']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a4q1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['b3q1']).'</td>';
            echo '<td class="text-right">'.number_format($value['a5q1']).'</td>';
            echo '<td class="text-right" style="background-color:#33ffff;">'.number_format($value['avg_a5q1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['b4q1']).'</td>';
            echo '<td class="text-right">'.number_format($value['a6q1']).'</td>';
            echo '<td class="text-right" style="background-color:#ffccff;">'.number_format($value['avg_a6q1'],2).'</td>';
            
            echo '<td class="text-right" style="background-color:#ffffcc;">'.number_format($value['b1q2']).'</td>';
            echo '<td class="text-right">'.number_format($value['a1q2']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratioq2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a2q2']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a2q2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a3q2']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a3q2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['a4q2']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratio_a4q2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['b3q2']).'</td>';
            echo '<td class="text-right">'.number_format($value['a5q2']).'</td>';
            echo '<td class="text-right" style="background-color:#33ffff;">'.number_format($value['avg_a5q2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['b4q2']).'</td>';
            echo '<td class="text-right">'.number_format($value['a6q2']).'</td>';
            echo '<td class="text-right" style="background-color:#ffccff;">'.number_format($value['avg_a6q2'],2).'</td>';
            
            echo '</tr>';
        } //end foreach
    } //end if($data!=NULL)
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="33" class="info">
                <p>
                    <b>หมายเหตุ:</b><br>
                    ประเมินจากแฟ้ม NUTRITION <br>
                    - <span class="text-danger font-bold">เด็กอายุ 6-14 ปี </span><br>
                    - 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูงเกิน 1 ครั้งต่อเทอม จะยึดค่าน้ำหนักและส่วนสูงครั้งสุดท้ายของเทอม <br>
                    - 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูง ทั้ง 2 เทอมจะนับให้เทอมละ 1 ครั้ง <br>
                    สรุปตัดความซ้ำซ้อนด้วย HOSPCODE,PID,เทอม <br>
                    <br>
                    ค่าน้ำหนักต้องอยู่ในช่วง 0.1 ถึง 300 กก. <br>
                    ค่าส่วนสูงต้องอยู่ในช่วง 40 ถึง 250 ซม. <br>
                    สัญชาติ ไทย <br>
                    <span class="text-danger font-bold">นับตามวันที่ให้บริการ(DATE_SERV) ที่อยู่ในช่วง เดือน พค. ถึง กค.(เทอม 1) , เดือน ตค. ถึง มค.(เทอม 2) เท่านั้น </span>
                </p>
                <p>
                ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)
                </p>
                <?= ($report_date)? DashboardServices::getReportdate($report_date):'' ?>
                
            </td>
        </tr>
    </tfoot>
  </table>
