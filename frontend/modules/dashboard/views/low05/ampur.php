<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;
use yii\widgets\ActiveForm;
?>

<?php
$conn = \Yii::$app->db;
$request=\yii::$app->request;
$year = $request->get('year');
$kid=$request->get('kid');
$changwat=$request->get('cw');

$sqlc='select regioncode from cchangwat where changwatcode=:changwat';
$cmd = $conn->createCommand($sqlc);
$cmd->bindValue(':changwat', $changwat);
$datac = $cmd->queryAll();
foreach($datac as $key=>$value)
{
    $region=$value['regioncode'];
}

$this->title = $kpi['kpi_name'] ." ระดับ". $scope;
$this->params['breadcrumbs'][] = ['label' => 'เขตสุขภาพ', 'url' => ['index','year'=>$request->get('year')]];
$this->params['breadcrumbs'][] = ['label' => 'จังหวัด', 'url' => ['changwat','year'=>$request->get('year'),'kid'=>$kid,'rg'=>$region]];
$this->params['breadcrumbs'][] = $scope;
?>

<?=$this->render('//_header',['templatefile'=>"template_low05_60.png"])?>
<div class="content animate-panel">
<div class="row visible-xs">
	<div class="col-xs-6"><h5 class="pull-right">ปีงบประมาณ<h5></div>
		<div class="col-xs-6">
			<div class="btn-group">
				<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?= $kpi['year'] + 543; ?> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<?php	for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2014; $i--) { ?>
					<li><?= Html::a($i + 543, ['ampur', 'year' => $i,'kpi'=>$kid,'cw'=>$changwat]) ?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	<hr class="col-xs-12">
</div>
<nav class="navbar navbar-default hidden-xs">
	<div class="container-fluid">
		<div class="navbar-header">                
			<a class="navbar-brand" href="#">ปีงบประมาณ</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="<?= $v["kpi_system_routing"] ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<?= $kpi['year'] + 543; ?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2014; $i--) { ?>
						<li><?= Html::a($i + 543, ['ampur', 'year' => $i,'kpi'=>$kid,'cw'=>$changwat]) ?></li>
						<?php } ?>
					</ul>
				</li>
			</ul>                
		</div>
	</div>
</nav>
<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_gauge',['gaugeData'=>$gaugeData,'plotBands'=>$plotBands,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_trend',['kpi'=>$kpi,'targetValue'=>$targetValue,'trendData'=>$trendData])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData,'quarterName'=>$quarterName])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data,'mapUrl' => $mapUrl,'mapType' => $mapType,])?>
</div>
                </div>
            </div>
            </div>

</div>