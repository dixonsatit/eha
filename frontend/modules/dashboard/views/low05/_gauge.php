<?php
use miloschuman\highcharts\Highcharts;
?>
<?php
echo Highcharts::widget([
    'id'=>'gauge',
                 'scripts' => [
                 'highcharts-more',  
                 'modules/exporting',
                 'themes/grid-light',
                 ],
                 'options' => [
                    'credits'=>[
                        'enabled'=>false,
                    ],
                 'chart'=> [
                'type' => 'gauge',
                'plotBackgroundColor' => null,
                'plotBackgroundImage' => null,
                'plotBorderWidth' => 0,
                'plotShadow' => false,
                'height'=> 350,
            ],
        'title' => [
            'text' => 'สถานการณ์ภาพรวม ' . $quarterName,
            ],

            'pane' => [
                'startAngle' => -100,
                'endAngle' => 100,
                // 'background' => null,
                'background'=> [
                    'backgroundColor'=> '#FFF',
                    'innerRadius'=> '60%',
                    'outerRadius'=> '100%',
                    'shape'=> 'arc'
                ]
            ],
        // the value axis
        'yAxis'=>[
            'min' => 0,
            'max' => 15,
            //
            'minorTickInterval' => 'auto',
            'minorTickWidth' => 1,
            'minorTickLength' => null,
            'minorTickPosition' => 'inside',
            'minorTickColor' => '#666',

            'tickPixelInterval' => 10,
            'tickWidth' => 1,
            'tickPosition' => 'inside',
            'tickLength' => 30,
            // 'tickColor' => '#666',
            'tickColor'=> '#fff',
            'labels' => [
                'step' => 2,
                'rotation' => 'auto',
            ],
                'title' => [
                    'text' => 'ร้อยละ'
                ],
                'plotBands' => $plotBands
                // 'plotBands'=> [[
                //     'from'=> 0,
                //     'to'=> 90.00,
                //     'color'=> '#ff0000' // red
                // ], [
                //     'from'=> 90.01,
                //     'to'=> 99.99,
                //     'color'=> '#e6e600' // yellow
                // ], [
                //     'from'=> 100.00,
                //     'to'=> 101.99,
                //     'color'=> '#069e06' // green
                // ]],
            ],
            'tooltip' => [
                'pointFormat' => 'ร้อยละ {point.y:.2f}',
                'style' => [
                    'fontSize' => '18px',
                    'fontFamily' => 'Verdana, sans-serif'
                ]
            ],
            'series' => [[
                'name' => 'ร้อยละ',
                // 'data' => [(int)$chartHdc->percentage],
                'data' => [$gaugeData],
                'tooltip'=> [
                        'valueSuffix' => ' '
                ],
                'dataLabels' => [
                    'enabled' => true,
                    // 'y'=>-40,
                    // 'color' => 'black',
                    'format' => '{point.y:.2f}',
                    'style' => [
                        'fontSize' => '16px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                    'borderWidth'=>0,
                    
                ],
            ]]
        ]
])?>
