<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS

// Datatables
getDatatable('#datatables',0);

JS;
$this->registerJs($script);
?>
<div class ="text-center front-weight-bold text-dark"><h5> ตารางข้อมูลรายพื้นที่ </h5></div>
<table id= "datatables" class="table table-striped table-hover table-bordered"  width="100%">
<thead>
<tr class="info">
<th rowspan="2">&nbsp;</th>
<th rowspan="2" class="text-center"><?=$scope?></th>
<th colspan="3" class="text-center">รวมทั้งปี</th>
<th colspan="3" class="text-center">ไตรมาส 1</th>
<th colspan="3" class="text-center">ไตรมาส 2</th>
<th colspan="3" class="text-center">ไตรมาส 3</th>
<th colspan="3" class="text-center">ไตรมาส 4</th>
</tr>
<tr class="info">
<!--ภาพรวม-->
<th class="text-center">B</th>
<th class="text-center">A</th>
<th class="text-center">ร้อยละ</th>
<!--ไตรมาส1-->
<th class="text-center">B</th>
<th class="text-center">A</th>
<th class="text-center">ร้อยละ</th>
<!--ไตรมาส2-->
<th class="text-center">B</th>
<th class="text-center">A</th>
<th class="text-center">ร้อยละ</th>
<!--ไตรมาส3-->
<th class="text-center">B</th>
<th class="text-center">A</th>
<th class="text-center">ร้อยละ</th>
<!--ไตรมาส4-->
<th class="text-center">B</th>
<th class="text-center">A</th>
<th class="text-center">ร้อยละ</th> 
</tr>
</thead>
<tfoot>
		<tr class="info">
            <td colspan="17">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
<tbody>
<?php
if ($data) {
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td class="text-center">&nbsp;</td>';
        //ภาพรวม
        if ($v['areacode'] !== 'TOTAL') {
            if ($scope == 'ตำบล') {
                echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
            } else {
                echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
            }
        } else {
            //
            $report_date = $v['report_date'];
            //
            echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
        }
        //
        echo '<td class="text-center">'.number_format($v['target']).'</td>';
        echo '<td class="text-center">'.number_format($v['result']).'</td>';
        echo '<td class="text-center">'.$v['total_ratio'].'</td>';
        //ไตรมาส1
        echo '<td class="text-center">'.number_format($v['targetq1']).'</td>';
        echo '<td class="text-center">'.number_format($v['resultq1']).'</td>';
        if(intval($v['targetq1'])>0){
            echo '<td class="text-center">'.round(($v['resultq1']*100)/$v['targetq1'],2).'</td>';
        }else{ 
            echo '<td class="text-center">0</td>';
        }
        //ไตรมาส2
        echo '<td class="text-center">'.number_format($v['targetq2']).'</td>';
        echo '<td class="text-center">'.number_format($v['resultq2']).'</td>';
        if(intval($v['targetq2'])>0){
            echo '<td class="text-center">'.round(($v['resultq2']*100)/$v['targetq2'],2).'</td>';
        }else{ 
            echo '<td class="text-center">0</td>';
        }
        //ไตรมาส3
        echo '<td class="text-center">'.number_format($v['targetq3']).'</td>';
        echo '<td class="text-center">'.number_format($v['resultq3']).'</td>';
        if(intval($v['targetq3'])>0){
            echo '<td class="text-center">'.round(($v['resultq3']*100)/$v['targetq3'],2).'</td>';
        }else{ 
            echo '<td class="text-center">0</td>';
        }
        //ไตรมาส4
        echo '<td class="text-center">'.number_format($v['targetq4']).'</td>';
        echo '<td class="text-center">'.number_format($v['resultq4']).'</td>';
        if(intval($v['targetq4'])>0){
            echo '<td class="text-center">'.round(($v['resultq4']*100)/$v['targetq4'],2).'</td>';
        }else{ 
            echo '<td class="text-center">0</td>';
        }
        echo '</tr>';
        //
    } //end foreach
} //end if  
?>
</tbody>
</table>                     

<p class="font-weight-bold">หมายเหตุ:<br>
A หมายถึง จำนวนหญิงตั้งครรภ์ที่ได้รับยาเม็ดเสริมไอโอดีน<br>	
B หมายถึง จำนวนหญิงตั้งครรภ์ที่มารับบริการ<br>
<?=DashboardServices::getReportdate($report_date);?>
</p>
                  



