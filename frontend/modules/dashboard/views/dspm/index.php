<?php
include ('lib/bc_index.php'); 
include ('lib/btn_template.php'); 
?>
<div class="content animate-panel">
	<?php include ('lib/ul_year.php'); ?>	
	<div class="row">
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Gauge -->
					<?=$this->render('_gauge', ['gaugeData'=>$gaugeData,'quarterName'=>$quarterName,'plotBands'=>$plotBands])?>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Map -->
                    <?=$this->render('_map', ['geo'=>$geo,'mapUrl' =>$mapUrl,'mapType'=>$mapType,'mapLegend'=>$mapLegend,'quarterName'=>$quarterName])?>
                </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi'=>$kpi,'trendData'=>$trendData])?>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Column -->
					<?= $this->render('_column', ['kpi'=>$kpi,'columnData'=>$columnData,'quarterName'=>$quarterName])?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Data Table -->
					<?=$this->render('_datatables', ['kpi'=>$kpi,'data'=>$data,'mapUrl'=>$mapUrl,'mapType'=>$mapType,'scope'=>$scope])?>
				</div>
			</div>
		</div>
	</div>
</div>