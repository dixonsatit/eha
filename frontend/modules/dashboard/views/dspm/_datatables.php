<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive dt-responsive nowrap" cellspacing="0" style="width:100%">
	<thead>
		<tr class="info" style="padding:0px;" role="row">
			<th rowspan="2" >&nbsp;</th>
			<th rowspan="2" class="text-center"><?=$scope?></th>
			<!-- <th colspan="5" class="text-center">รวมทั้งปีงบประมาณ</th> -->
			<th colspan="5" class="text-center">ไตรมาสที่ 1</th>
			<th colspan="5" class="text-center">ไตรมาสที่ 2</th>
			<th colspan="5" class="text-center">ไตรมาสที่ 3</th>
			<th colspan="5" class="text-center">ไตรมาสที่ 4</th>
		</tr>
		<tr class="info" style="padding:0px;" role="row">
			<th class="text-center">B</th>
			<th class="text-center">ครั้งแรก</th>
			<th class="text-center">หลังการติดตาม</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">ครั้งแรก</th>
			<th class="text-center">หลังการติดตาม</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">ครั้งแรก</th>
			<th class="text-center">หลังการติดตาม</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">ครั้งแรก</th>
			<th class="text-center">หลังการติดตาม</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
		</tr>
	</thead>
	<tfoot>
		<tr class="info"  style="padding:0px;" role="row">
			<th colspan="22">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</th>
		</tr>
	</tfoot>
	<tbody>
		<?php
		if ($data) {
			foreach ($data as $key => $v) {
				echo '<tr>';
					echo '<td class="text-center">&nbsp;</td>';
				//ภาพรวม
				if ($v['areacode'] !== 'TOTAL') {
					//
					$report_date = $v['report_date'];
					//
					if ($scope == 'ตำบล') {
						echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
					} else {
						echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
					}					
				} else {
					//
					$report_date = $v['report_date'];
					//
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} //end if ($v['areacode'] !== 'TOTAL')
				//
				// echo '<td class="text-center">' . number_format($v['total_target']) . '</td>';
				// echo '<td class="text-center">' . number_format($v['total_result1']) . '</td>';
				// echo '<td class="text-center">' . number_format($v['total_result2']) . '</td>';
				// echo '<td class="text-center">' . number_format($v['total_result']) . '</td>';
				// echo '<td class="text-center">' . number_format($v['total_ratio'],2) . '</td>';
				//
				echo '<td class="text-center">' . number_format($v['targetq1']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result1q1']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result2q1']) . '</td>';
				echo '<td class="text-center">' . number_format($v['resultq1']) . '</td>';
				echo '<td class="text-center" style="background-color:#CCFF99">' . number_format($v['ratioq1'],2) . '</td>';
				
				echo '<td class="text-center">' . number_format($v['targetq2']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result1q2']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result2q2']) . '</td>';
				echo '<td class="text-center">' . number_format($v['resultq2']) . '</td>';
				echo '<td class="text-center" style="background-color:#CCFF99">' . number_format($v['ratioq2'],2) . '</td>';
				
				echo '<td class="text-center">' . number_format($v['targetq3']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result1q3']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result2q3']) . '</td>';
				echo '<td class="text-center">' . number_format($v['resultq3']) . '</td>';
				echo '<td class="text-center" style="background-color:#CCFF99">' . number_format($v['ratioq3'],2) . '</td>';
				
				echo '<td class="text-center">' . number_format($v['targetq4']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result1q4']) . '</td>';
				echo '<td class="text-center">' . number_format($v['result2q4']) . '</td>';
				echo '<td class="text-center">' . number_format($v['resultq4']) . '</td>';
				echo '<td class="text-center" style="background-color:#CCFF99">' . number_format($v['ratioq4'],2) . '</td>';
				echo '</tr>';
				//
			} //end foreach
		} //end if  
		?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
		&nbsp;- B หมายถึง จ่านวนเด็กไทยอายุ 9, 18, 30, และ 42 เดือน ได้รับการประเมินพัฒนาการตามเกณฑ์<br>	
		&nbsp;- A หมายถึง จ่านวนเด็กไทยอายุ 9, 18, 30, และ 42 เดือน ได้รับการประเมินพัฒนาการตามเกณฑ์ และมีพัฒนาการสมวัย รวมกับสมวัยหลังการติดตาม 30 วัน<br>
		<br>
	<b>&nbsp;ประเมินจากแฟ้ม Specialpp </b><br>
		&nbsp; B เด็กไทย อายุ 9,18,30,42 เดือน <font style="color: #F00">ที่ได้รับการคัดกรองในปีงบบประมาณซึ่งอาจจะไม่เท่ากับ ข้อได้รับการคัดกรองตามกลุ่มอายุ specialpp</font><br> 
		&nbsp; เช่น เด็กเกิด 25 กย 2559 จะถูกคิดเป็นเป้าหมายและผลงานในเดือน กันยายน ถึงแม้จะมาคัดกรองวันที่ 2 ตค.<br> 
		&nbsp; ส่วนข้อนี้นับที่เด็กได้รับการคัดกรองในปีงบประมาณ ดังนั้น เด็กคนนี้ที่ได้รับการคัดกรองในเดือน ตค. จะคิดเป็น B ด้วย<br>

	<?=DashboardServices::getReportdate($report_date);?>
</div>

