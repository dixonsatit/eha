<?php
use miloschuman\highcharts\Highcharts;
?>

<!-- ร้อยละความพึงพอใจ 10 ProductChampion(แยกรายภาค) -->
<?php
// echo json_encode($columnData);
// print_r($categoriesData); die();
// print_r($kpi['target_value']);  die();
echo Highcharts::widget([

    'setupOptions' => [
        'lang' => [
            'decimalPoint' => '.',
            'thousandsSep' => ',',
        ]
    ],    
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'bar',
        ],
        'title' => [
            // 'text' => 'เปรียบเทียบรายพื้นที่',
            'text' => 'ผลสำรวจความพึงพอใจ 10 ProductChampion  ปี '.($_GET['year']+543),
            // 'text' => $kpi['kpi_name'],
        ],
        'subtitle' => [
            // 'text'=> '(แยกรายภาค)',
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                // 'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
            'categories' => $categoriesData,
            'crosshair' => true,
        ],


        'yAxis' => [
            'min' => 0,
            // 'max' => 6000,
            'title' => [
                'text' => 'จำนวน (คน)',
            ],
            // 'plotLines' => [[
            //     // 'value' => (int) $kpiTemplateToYear->target_value,
            //     // 'value' => 50,
            //     'value' => (float) $kpi['target_value'],
            //     'color' => 'red',
            //     'width' => 2,
            //     'label' => [
            //         // 'text' => $kpi['target_value'],
            //         'align' => 'left',
            //         'style' => [
            //             'color' => 'blue',
            //         ],
            //     ],
            // ]],
        ],
        'legend' => [
            'layout' => 'vertical',
            'align' => 'right',
            'verticalAlign' => 'top',
            'x' => -40,
            'y' => 80,
            'floating' => true,
            'borderWidth' => 1,
            'backgroundColor' => ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            'shadow' => true
        ],
        // 'legend' => [
        //     'enabled' => true,
        // ],
        // 'tooltip' => [
        //     'pointFormat' => '',
        // ],
        'labels' => [
            'overflow' => 'justify',
        ],
        'tooltip' => [
            'headerFormat' => '<span style="font-size:15px">{point.key}</span><table bgcolor="#00FF00">',
            'pointFormat' => '<tr><td style="font-size:14px; color:{series.color};padding:0"><b>{series.name}&nbsp;:&nbsp;</b></td>'.
                '<td style="font-size:14px; color:{series.color}; padding:0.1;"> <b>&nbsp;&nbsp;{point.y:,.0f}&nbsp;คน </b></td></tr>',
            'footerFormat' => '</table>',
            'shared' => true,
            'useHTML' => true
        ],
        'plotOptions' => [
            'column' => [
                'pointPadding' => 0.2,
                'borderWidth'=> 0,
            ]
        ],
        'series' => $columnData2,
        'plotOptions' => [
            'series' => [
                'borderWidth' => 0,
                'dataLabels' => [
                    'enabled' => true,
                    // 'y' => 30,
                    // 'rotation' => -90,
                    'format' => '{point.y:,.0f}',
                ]
            ]
        ],

    ],
]);
?>