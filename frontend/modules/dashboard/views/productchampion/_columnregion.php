<?php
use miloschuman\highcharts\Highcharts;
?>

<!-- ร้อยละความพึงพอใจ 10 ProductChampion(แยกรายภาค) -->
<?php
// echo json_encode($columnData);
// print_r($categoriesData); die();
// print_r($kpi['target_value']);  die();
echo Highcharts::widget([

    'setupOptions' => [
        'lang' => [
            'decimalPoint' => '.',
            'thousandsSep' => ',',
        ]
    ],    
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'column',
        ],
        'title' => [
            // 'text' => 'เปรียบเทียบรายพื้นที่',
            'text' => 'ร้อยละความพึงพอใจ 10 ProductChampion (แยกรายภาค) ปี '.($_GET['year']+543),
            // 'text' => $kpi['kpi_name'],
        ],
        'subtitle' => [
            // 'text'=> '(แยกรายภาค)',
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
            'categories' => $categoriesData,
            'crosshair' => true,
        ],


        'yAxis' => [
            'min' => 0,
            'max' => 100,
            'title' => [
                'text' => 'ร้อยละความพึงพอใจ'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                // 'value' => 50,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'label' => [
                    // 'text' => $kpi['target_value'],
                    'align' => 'left',
                    'style' => [
                        'color' => 'blue',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => true,
        ],
        // 'tooltip' => [
        //     'pointFormat' => '',
        // ],

        'tooltip' => [
            'headerFormat' => '<span style="font-size:15px">{point.key}</span><table>',
            'pointFormat' => '<tr><td style="font-size:14px; color:{series.color};padding:0"><b>{series.name}: </b></td>'.
                '<td style="font-size:14px; color:{series.color}; padding:0.1;"> <b> ร้อยละ {point.y:,.2f} </b></td></tr>',
            'footerFormat' => '</table>',
            'shared' => true,
            'useHTML' => true
        ],
        'plotOptions' => [
            'column' => [
                'pointPadding' => 0.2,
                'borderWidth'=> 0,
            ]
        ],
        'series' => $columnData,
        'plotOptions' => [
            'series' => [
                'borderWidth' => 0,
                'dataLabels' => [
                    'enabled' => true,
                    'y' => 30,
                    'rotation' => -90,
                    'format' => '{point.y:.2f}',
                ]
            ]
        ],
        // [
        //     [
        //         'name' => 'Tokyo',
        //         'data' => [106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        
        //     ], [
        //         'name' => 'New York',
        //         'data' => [98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
        
        //     ], [
        //         'name' => 'London',
        //         'data' => [9.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
        
        //     ], [
        //         'name' => 'Berlin',
        //         'data' => [34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
        
        //     ]
        // ],
    ],
]);
?>