<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <tr class="info">
    <th class="text-center" rowspan="2">#</th>
    <th class="text-center" rowspan="2">ผลิตภัณฑ์สำคัญ</th>
    <th class="text-center" rowspan="2">เป้าหมาย</th>
    <th class="text-center" rowspan="2">จำนวนผู้ตอบ</th>
    <th class="text-center" rowspan="2">ความพึงพอใจ</th>
    <th class="text-center" rowspan="2">ร้อยละ</th>
    <th class="text-center" colspan="4">ร้อยละความพึงพอใจ (ภูมิภาค)</th>
  </tr>
  <tr class="info">
    <!-- <th class="text-center" >#</th>
    <th class="text-center" >ผลิตภัณฑ์สำคัญ</th>
    <th class="text-center">เป้าหมาย</th>
    <th class="text-center">จำนวนผู้ตอบ</th>
    <th class="text-center">ความพึงพอใจ</th>
    <th class="text-center">ร้อยละ</th> -->
    
    <th class="text-center">เหนือ</th>
    <th class="text-center">กลาง</th>
    <th class="text-center">ตะวันออกเฉียงเหนือ</th>
    <th class="text-center">ภาคใต้</th>
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
    foreach ($data as $k => $v) {


// $sum_ratio = ($v['satisfaction_north_ratio'] + $v['satisfaction_central_ratio'] + $v['satisfaction_northeast_ratio'] + $v['satisfaction_south_ratio']) / 4;
        echo '<tr>';
        echo '<td>'. ($k + 1) . '</td>';
        echo '<td class="text-left">' . $v['product_name'] . '</td>';
        echo '<td class="text-right">' . number_format($v['target_value'],0) . '</td>';
        echo '<td class="text-right">' . number_format($v['returned_value'],0) . '</td>';
        echo '<td class="text-right">' . number_format($v['satisfaction_value'],0) . '</td>';        
        echo '<td class="text-right">'.number_format($v['satisfaction_ratio'],2).'</td>';
        
        echo '<td class="text-right">' . number_format($v['satisfaction_north_ratio'],2) . '</td>';
        echo '<td class="text-right">' . number_format($v['satisfaction_central_ratio'],2) . '</td>';
        echo '<td class="text-right">' . number_format($v['satisfaction_northeast_ratio'],2) . '</td>';
        echo '<td class="text-right">' . number_format($v['satisfaction_south_ratio'],2) . '</td>';

        echo '</tr>';



        $totaltarget += $v['target_value']; 
        $totalreturned += $v['returned_value'];
        $totalsatisfaction += $v['satisfaction_value'];
        // $totalratio += $totalsatisfaction/$totalreturned*100;
  
        $a[] = $v['satisfaction_north_ratio'];
        $b[] = $v['satisfaction_central_ratio'];
        $c[] = $v['satisfaction_northeast_ratio'];
        $d[] = $v['satisfaction_south_ratio'];
  

  } //end foreach
} //end if  

if($a & $b & $c & $d){
$a = array_filter($a);
$ava = array_sum($a)/count($a);

$b = array_filter($b);
$avb = array_sum($b)/count($b);

$c = array_filter($c);
$avc = array_sum($c)/count($c);

$d = array_filter($d);
$avd = array_sum($d)/count($d);
}

echo '<tr>';
echo '<th>'. ($k + 1) . '</th>';
echo '<th class="text-center">รวมทั้งหมด</th>';
echo '<th class="text-right">' . number_format($totaltarget,0) . '</th>';
echo '<th class="text-right">' . number_format($totalreturned,0) . '</th>';
echo '<th class="text-right">' . number_format($totalsatisfaction,0) . '</th>';        
echo '<th class="text-right">'.@number_format(($totalsatisfaction/$totalreturned)*100,2).'</th>';

echo '<th class="text-right">' . number_format($ava,2) . '</th>';
echo '<th class="text-right">' . number_format($avb,2) . '</th>';
echo '<th class="text-right">' . number_format($avc,2) . '</th>';
echo '<th class="text-right">' . number_format($avd,2) . '</th>';

echo '</tr>';



// foreach($data as $k => $v){

//     // $mapType!=='ap'?window.open($mapUrl+'&'+$mapType,'_self'):null;

// $v['areacode'] == 'TOTAL' ? $aa=$v['areaname_new'] : $aa='<a target="_blank" href="changwat/?year='.$_GET['year'].'&kid='.$kpi['kpi_template_id'].'&rg='.$v['areacode'].'">'.$v['areaname_new'].'</a>' ;




// echo '<tr>
//     <td class="text-left">'.($k+1).'</td>
//     <td class="text-left">'.$aa.'</td>
//     <td class="text-right">'.number_format($v['target'],2).'</td>
//     <td class="text-right">'.number_format($v['result'],2).'</td>
//     <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
//   </tr>';
// }

?>

</tbody>
<tfoot class="info">
<tr>
<td colspan="10" class="info">
<br/>ที่มา: DoH Dashboard กรมอนามัย
<br/><br/>

<div class="text-success">
<?=DashboardServices::getReportdate($report_date);?>
</div>
</td>
</tr>
</tfoot>
</table>

<!--///////////////////////////////////////////////////////////////////////////-->




