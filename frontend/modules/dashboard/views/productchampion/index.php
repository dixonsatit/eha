<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");

$this->title = $kpi['kpi_name']; //. '  ระดับเขตสุขภาพ';
// $this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['changwat']];
// $this->params['breadcrumbs'][] = 'ระดับเขตสุขภาพ';
?>

<?=$this->render('//_header',['templatefile'=>""])?>




<div class="content animate-panel">


<div class="row">
        <div class="col-lg-12 col-md-6 col-sm-3">
        <div class="hpanel">
        <div class="panel-body">
        <div class="col-md-3">
           <h4> เลือกปีงบประมาณ </h4>
        </div>
        <?//=$nowyear;?>
        <div class="dropdown col-md-1">
            <a href="#" class="dropdown-toggle btn btn-primary" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"> <?= ($session['year']+543);?> <span class="caret"></span> </a>
            <?php
                for ($i=$nowyear;$i>=2017;$i--) {
                    $cyear[] = array('label'=>($i+543),
                    'url'=>Url::to(['','year'=>$i]));
                }
                echo Dropdown::widget([
                    'items' => $cyear
                ]);
            ?>
        </div>
        </div>
        </div>
        </div>
</div>




<div class="row">

<div class="col-lg-12">


<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
   <? //print_r($categoriesData); die();?>
                     <!-- Column -->
                     <?=$this->render('_columnintro', ['kpi' => $kpi,'categoriesData' => $categoriesData, 'columnData2' => $columnData2])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>

</div>

<!-- ////////////////////////////////////////////////////////////// -->


<div class="row">

<div class="col-lg-12">


<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
   <? //print_r($categoriesData); die();?>
                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi,'categoriesData' => $categoriesData, 'columnDataratio' => $columnDataratio])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>

</div>

<!-- ////////////////////////////////////////////////////////////// -->




<div class="row">

<div class="col-lg-12">


<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
   <? //print_r($categoriesData); die();?>
                     <!-- Column -->
                     <?=$this->render('_columnregion', ['kpi' => $kpi,'categoriesData' => $categoriesData, 'columnData' => $columnData])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>

</div>

<!-- ////////////////////////////////////////////////////////////// -->


<hr>
<div class="panel panel-primary">
<div class="panel-body">
<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data, 'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>
</div>
</div>