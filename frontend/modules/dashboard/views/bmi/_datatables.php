<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive" width="100%">
	<thead>
		<tr class="info">
			<th >&nbsp;</th>
			<th class="text-center"><?=$scope?></th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
		</tr>		
	</thead>
	<tfoot>
		<tr class="info">
			<th colspan="5">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</th>
		</tr>
	</tfoot>
	<tbody>
	<?php
	if ($data) {
		foreach ($data as $key => $v) {
			echo '<tr>';
			echo '<td class="text-center">&nbsp;</td>';
			//ภาพรวม
			if ($v['areacode'] !== 'TOTAL') {
				if ($scope == 'ตำบล') {
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} else {
					echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
				}					
			} else {
				//
				$report_date = $v['report_date'];
				//
				echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
			}
			//
			echo '<td class="text-center">'.number_format($v['total_target']).'</td>';
			echo '<td class="text-center">'.number_format($v['total_result']).'</td>';
			echo '<td class="text-center">'.number_format($v['total_ratio'],2).'</td>';					
			echo '</tr>';

		} //end foreach
	} //end if  
	?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
		- จำนวนประชากรวัยทำงานอายุ 30 ปี - 44 ปี 11 เดือน 29 วัน ที่ชั่งน้ำหนักวัดส่วนสูงทั้งหมด ประมวลผลจากแฟ้ม NCDSCREEN <br>	
		&nbsp;ซึ่งตามนโยบายของแฟ้มแล้ว จะเก็บข้อมูลคนที่ไม่ป่วยด้วยโรคเบาหวาน/ความดันโลหิต ที่มาคัดกรอง โดยอายุที่ 35 ปีขึ้นไป <br>	
		&nbsp;ดังนั้นจะขาดข้อมูลในส่วนประชากรอายุ 30-34 ปี แต่ก็มีบางจังหวัดที่ยังคัดกรองประชากรที่อายุ 15 ปีขึ้นไปด้วยเช่นกัน <br>	
		&nbsp;****คำนวณอายุ ณ วันที่มารับบริการ***	<br>	<br>	
		- ดัชนีมวลกายปกติ หมายถึง ค่าดัชนีมวลกาย(BMI) อยู่ในช่วง 18.5-22.9 กก./ตรม.<br>
	
		<?=DashboardServices::getReportdate($report_date);?>
</div>

