<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
      //Datatables
      getDatatable('#datatables',0);
    // getDatatableSum('#datatables',2,3);
JS;
$this->registerJs($script);
?>
<h4><center>ตารางข้อมูลรายพื้นที่</center></h4>
<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="auto">
    <thead>

    <tr class="success">
      <th rowspan="2" class="text-center align-middle">ลำดับ</th>
      <th rowspan="2" class="text-center align-middle"><?=$scope;?></th>
      <th colspan="3" class="text-center">รวมทั้งปีงบประมาณ</th>
      <th colspan="3" class="text-center">ไตรมาส 1</th>
      <th colspan="3" class="text-center">ไตรมาส 2</th>
      <th colspan="3" class="text-center">ไตรมาส 3</th>
      <th colspan="3" class="text-center">ไตรมาส 4</th>
    </tr>
    <tr class="success">
      <!--รวมทั้งปีงบประมาณ-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส1-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส2-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส3-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส4-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
    </tr>
  </thead>
  <tfoot>
    </tr>
    <tr class="info">
      <p class="font-weight-bold"> หมายเหตุ: <br>
        B หมายถึง จำนวนหญิง 15 - 19 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR<br>	
        A หมายถึง จำนวนหญิง 15 - 19 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR ได้รับบริการคุมกำเนิดภายใน 42 วัน<br>
      </p>
      <td colspan="17">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
    </tr>
    </tfoot>
  <tbody>
  <?php
  foreach ($data as $key => $value) {
    echo '<tr>';
    echo '<td class="text-center">'.($key+1).'</td>';
    if ($value['areacode'] !== 'TOTAL') {
      echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode']) . '</td>';
    } else {
      //
      $report_date = $value['report_date'];
      //
      echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
    }
    //
    echo '<td class="text-center">'.number_format($value['target']).'</td>';
    echo '<td class="text-center">'.number_format($value['result']).'</td>';
    echo '<td class="text-center">'.number_format($value['total_ratio'],2).'</td>';

    echo '<td class="text-center">'.number_format($value['targetq1']).'</td>';
    echo '<td class="text-center">'.number_format($value['resultq1']).'</td>';
    echo '<td class="text-center">'.number_format($value['ratioq1'],2).'</td>';

    echo '<td class="text-center">'.number_format($value['targetq2']).'</td>';
    echo '<td class="text-center">'.number_format($value['resultq2']).'</td>';
    echo '<td class="text-center">'.number_format($value['ratioq2'],2).'</td>';

    echo '<td class="text-center">'.number_format($value['targetq3']).'</td>';
    echo '<td class="text-center">'.number_format($value['resultq3']).'</td>';
    echo '<td class="text-center">'.number_format($value['ratioq3'],2).'</td>';

    echo '<td class="text-center">'.number_format($value['targetq4']).'</td>';
    echo '<td class="text-center">'.number_format($value['resultq4']).'</td>';
    echo '<td class="text-center">'.number_format($value['ratioq4'],2).'</td>';
    echo '</tr>';
  } //end foreach
  ?>
  </tbody>
  </table>
  <div class="panel-footer">
  <p class="font-weight-bold"> หมายเหตุ: <br>
  B หมายถึง จำนวนหญิง 15 - 19 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR<br>	
  A หมายถึง จำนวนหญิง 15 - 19 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR ได้รับบริการคุมกำเนิดภายใน 42 วัน<br>
  
  <?=DashboardServices::getReportdate($report_date);?>

  </p>
  </div>
