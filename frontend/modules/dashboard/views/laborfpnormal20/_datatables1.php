<?php
use yii\helpers\Html;
?>

<?php
$script = <<< JS

// Datatables
getDatatable('#datatables',0)

JS;
$this->registerJs($script);
?>


    <table id="datatables"  class="table table-striped table-hover table-bordered"  width="100%">
    <thead>
    <tr class="info">
        <th colspan=5><?=$kpi['kpi_name']?></th>

    </tr>
    <tr class="info">
        <th>ลำดับ</th>
        <th ><?=$scope?></th>
        <th class="text-center">เป้าหมาย</th>
        <th class="text-center">ผลงาน</th>
        <th class="text-center">ร้อยละ</th>
    </tr>
    </thead>
    <tfoot>
        </tr>
            <tr class="info">
            <td colspan="5">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
        </tr>
    </tfoot>
    <tbody>

        <?php
if ($data) {
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td>' . ($k + 1) . '</td>';
        if ($v['areacode'] !== 'TOTAL') {
            echo '<td class="text-left">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
        } else {
            echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
        }
        echo '<td class="text-center">' . number_format($v['target']) . '</td>';
        echo '<td class="text-center">' . number_format($v['result']) . '</td>';
        echo '<td class="text-center">' . number_format($v['total_ratio'], 2) . '</td>';
        echo '</tr>';
    }
}

?>

    </tbody>
</table>




