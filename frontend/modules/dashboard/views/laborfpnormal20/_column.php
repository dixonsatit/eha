<?php
use miloschuman\highcharts\Highcharts;
// use yii\web\JsExpression;
?>


<?php


echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'column',
        ],
        'title' => [
            'text' => 'เปรียบเทียบรายพื้นที่ ' . $periodName,
        ],
        'subtitle' => [
            //'text'=> 'ปีงบประมาณ'
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'max' => 100,
            //
            'title' => [
                // 'text' => 'ดัชนีมวลกาย'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                // 'value' => 50,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'label' => [
                    // 'text' => 'โลกร้อน 555',
                    'text' => 'เป้าหมาย '.(float)$kpi['target_value'],
                    'align' => 'left',
                    'style' => [
                        'color' => 'blue',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => false,
        ],
        'tooltip' => [
            'pointFormat' => '{point.y:.2f}',
        ],
        'series' => [
            [
                'name' => 'Population',
                'colorByPoint' => false,
                // 'color' => '#62cb31',
                // 'data' => [["เขต 1",70.17,'z'=>'01'],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                'data' => $columnData,
                'dataLabels' => [
                    'enabled' => true,
                    'rotation' => -90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '7px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                // 'cursor'=> 'pointer',
                // 'point'=> [
                //     'events'=> [
                //         'click'=>  new JsExpression('function(){ alert(this.y) }')
                //     ]
                // ]

            ],

        ],
    ],
]);
?>

