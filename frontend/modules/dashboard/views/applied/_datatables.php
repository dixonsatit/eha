<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\HomerAsset;
use frontend\modules\dashboard\models\DashboardServices;
HomerAsset::register($this);


$script = <<< JS

// Datatables
getDatatable('#datatables',0)

JS;
$this->registerJs($script);

// SQL Query samples
// 
// SELECT IF(X.areacode!="TOTAL",areaname,"รวมทั้งหมด") AS areaname_new, X.*
//   FROM (SELECT IFNULL(Z.region_code,"TOTAL") AS areacode,Z.region_name AS areaname,
//
//  MAX(K.date_com) AS report_date,
//
//  SUM(K.target) AS target, SUM(K.result) AS result,
//  ROUND( SUM(IFNULL(K.result,0))/SUM(IFNULL(K.target,0))*100,2 ) AS total_ratio,
//  ROUND( SUM(IFNULL(K.result,0))/SUM(IFNULL(K.target,0))*100,2 ) AS chart_value, 
//  SUM(K.target1) AS targetq1, SUM(K.result1) AS resultq1, 
//  ROUND( SUM(IFNULL(K.result1,0))/SUM(IFNULL(K.target1,0))*100,2 ) AS ratioq1, 
//  SUM(K.target2) AS targetq2, SUM(K.result2) AS resultq2, 
//  ROUND( SUM(IFNULL(K.result2,0))/SUM(IFNULL(K.target2,0))*100,2 ) AS ratioq2, 
//  SUM(K.target3) AS targetq3, SUM(K.result3) AS resultq3, 
//  ROUND( SUM(IFNULL(K.result3,0))/SUM(IFNULL(K.target3,0))*100,2 ) AS ratioq3,  
//  SUM(K.target4) AS targetq4, SUM(K.result4) AS resultq4, 
//  ROUND( SUM(IFNULL(K.result4,0))/SUM(IFNULL(K.target4,0))*100, 2) AS ratioq4 
//
// FROM s_anc5 K 
// INNER JOIN cchangwat C ON C.changwatcode=LEFT(K.areacode,2) 
// INNER JOIN cregion Z ON Z.region_code=C.regioncode  
// WHERE K.id = 'bd63b8d99f7054560fcf9c3b96f39c13' 
//  AND K.b_year  =:queryYear
// GROUP BY Z.region_code
// WITH ROLLUP) X;
//
//
//
// SELECT IF(X.areacode!="TOTAL",areaname,"รวมทั้งหมด")  AS areaname_new, X.*
//  FROM (SELECT IFNULL(R.region_code,"TOTAL") AS areacode, R.region_name AS areaname,
//
// 	MAX(CAST(D.created_at AS DATE)) AS report_date,
// COUNT( DISTINCT IF(D.issue_code= 'EHA1001' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA1001', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA1002' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA1002', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA1003' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA1003', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA2001' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA2001', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA2002' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA2002', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA2003' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA2003', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA3001' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA3001', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA3002' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA3002', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA4001' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA4001', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA4002' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA4002', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA4003' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA4003', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA5000' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA5000', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA6000' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA6000', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA7000' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA7000', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA8000' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA8000', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA9001' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA9001', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA9002' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA9002', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA9003' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA9003', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA9004' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA9004', 
// COUNT( DISTINCT IF(D.issue_code= 'EHA9005' AND D.component1to5_avgscore > 80 AND D.component6to7_avgscore > 80, 1, 0)) AS 'EHA9005' 
//
// FROM locality AS l 
//  LEFT JOIN assessment_conclusion D ON D.locality_code = l.locality_code
//  INNER JOIN province P ON P.province_code = l.province_code
//  INNER JOIN region R ON R.region_code = P.region_code  
// WHERE 1=1 
//  AND D.be_year = 2562
// GROUP BY R.region_code
// WITH ROLLUP) X;
//
?>

<h4> <center> ตารางข้อมูลรายพื้นที่ </center></h4>
<div id="datatables_wrapper">

</div>
<table id="datatables"  class="table table-striped table-hover table-bordered table-responsive" width="100%">
    <thead>
    <tr class="info">
        <th class="text-center">#</th>
        <th class="text-center">พื้นที่<?php //=$scope?></th>
        <th class="text-center">EHA<br>1001</th>
        <th class="text-center">EHA<br>1002</th>
        <th class="text-center">EHA<br>1003</th>
        <th class="text-center">EHA<br>2001</th>
        <th class="text-center">EHA<br>2002</th>
        <th class="text-center">EHA<br>2003</th>
        <th class="text-center">EHA<br>3001</th>
        <th class="text-center">EHA<br>3002</th>
        <th class="text-center">EHA<br>4001</th>
        <th class="text-center">EHA<br>4002</th>
        <th class="text-center">EHA<br>4003</th>
        <th class="text-center">EHA<br>5000</th>
        <th class="text-center">EHA<br>6000</th>
        <th class="text-center">EHA<br>7000</th>
        <th class="text-center">EHA<br>8000</th>
        <th class="text-center">EHA<br>9001</th>
        <th class="text-center">EHA<br>9002</th>
        <th class="text-center">EHA<br>9003</th>
        <th class="text-center">EHA<br>9004</th>
        <th class="text-center">EHA<br>9005</th>
        <th class="text-center">รวม<br>ทั้งหมด</th>
        <th class="text-center">ร้อยละ</th>
    </tr>
    </thead>
    <tbody>

<?php
$request = Yii::$app->request;
$ap = null;
$ap = $request->get('ap');
//
if ($data) {
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td>' . ($k + 1) . '</td>';
        if ($v['areacode'] !== 'TOTAL') {
            echo '<td class="text-left">' ;
            if ($ap==null) {
                echo Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' .$v['areacode']) ;
            } else {
                echo $v['areaname_new'];
            }
            echo '</td>';
        } else {
            //
            $report_date = $v['report_date'];
            //
            echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
        } 
        //
        echo '<td class="text-center">' . number_format($v['eha1001']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha1002']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha1003']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha2001']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha2002']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha2003']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha3001']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha3002']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha4001']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha4002']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha4003']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha5000']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha6000']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha7000']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha8000']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha9001']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha9002']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha9003']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha9004']) . '</td>';
        echo '<td class="text-center">' . number_format($v['eha9005']) . '</td>';
        echo '<td class="text-center">' . number_format($v['total_number'], 2) . '</td>';
        echo '<td class="text-center">' . number_format($v['total_ratio'], 2) . '</td>';
        echo '</tr>';
        //
    } //end foreach
} //end if  
?>

    </tbody>
    <tfoot>
        </tr>
        <tr >
        <td colspan=24 class="info"><p class="font-weight-normal">
        ที่มา: สำนักสุขาภิบาลอาหารและน้ำ กรมอนามัย

        <?=DashboardServices::getReportdate($report_date);?>
        </td>
        </tr>
    </tfoot>
</table>