<?php
use yii\helpers\Html;
$script = <<< JS
// Datatables
getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<!--<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>-->
<table class= "table table-striped table-hover table-bordered table-responsive nowrap" width="100%">
	<thead>
		<tr class="info" style="padding:0px;" role="row">
			<th rowspan="1" colspan="1">&nbsp;</th>
			<th class="col-lg-2 col-md-2 col-xs-2 text-center" rowspan="1" colspan="1">ปี 2560</th>
			<th class="col-lg-2 col-md-2 col-xs-2 text-center" rowspan="1" colspan="1">ปี 2561</th>
			<th class="col-lg-2 col-md-2 col-xs-2 text-center" rowspan="1" colspan="1">ปี 2562</th>
			<th class="col-lg-2 col-md-2 col-xs-2 text-center" rowspan="1" colspan="1">ปี 2563</th>
			<th class="col-lg-2 col-md-2 col-xs-2 text-center" rowspan="1" colspan="1">ปี 2564</th>
		</tr>	
    </thead>
	<tbody>
		<tr style="padding:0px;" role="row">
			<th rowspan="1" colspan="1">เป้าหมาย</th>
			<td class="text-center" rowspan="1" colspan="1">3 หมวดสะสม</td>
			<td class="text-center" rowspan="1" colspan="1">4 หมวดสะสม</td>
			<td class="text-center" rowspan="1" colspan="1">5 หมวดสะสม</td>
			<td class="text-center" rowspan="1" colspan="1">6 หมวดสะสม</td>
			<td class="text-center" rowspan="1" colspan="1">ได้รับการรับรองทุกหมวด</td>
		</tr>
		<tr style="padding:0px;" role="row">
			<th rowspan="1" colspan="1">ผลงาน</th>
			<td class="text-center" rowspan="1" colspan="1">1 หมวด</td>
			<td class="text-center" rowspan="1" colspan="1">1+1<br>(Site Visit หมวด 4)</td>
			<td class="text-center" rowspan="1" colspan="1">&nbsp;</td>
			<td class="text-center" rowspan="1" colspan="1">&nbsp;</td>
			<td class="text-center" rowspan="1" colspan="1">&nbsp;</td>			
		</tr>
	<tbody>
</table>

