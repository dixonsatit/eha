<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

include ('lib/bc_index.php'); 
include ('lib/btn_template.php'); 
?>
<div class="content animate-panel">
	<?php //include ('lib/ul_year.php'); ?>	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body h-200">
					<!-- Column -->
					<?= $this->render('_pie', ['kpi' => $kpi, 'pieData' => $pieData])?>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
				<?=$this->render('_datatables', ['kpi' => $kpi, 'data' => $data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-body">
					<b>หน่วยงานที่ไม่ได้รับรางวัล :</b><br>
					&nbsp; - กรมสนับสนุนฯ<br>
					&nbsp; - สนง.คณะกรรมการอาหารและยา<br>
					&nbsp; - กรมแพทย์แผนไทยฯ<br>
					&nbsp; - กรมการแพทย์ <br>
					
					<?=DashboardServices::getReportdate($report_date);?>
					
				</div>
			</div>
		</div>
	</div>
</div>