<?php
use miloschuman\highcharts\Highcharts;
// print_r($pieData);
// die();

echo Highcharts::widget([
    'options' => [        
        'chart'=>[
            'plotBackgroundColor'=> null,
            'plotBorderWidth'=> null,
            'plotShadow'=> false,
            'type'=> 'pie',
        ],
        'title' => ['text' => 'รางวัลเลิศรัฐสาขาคุณภาพการบริหารจัดการภาครัฐ'],
        'tooltip' => [
            'pointFormat' => 'ผ่านจำนวน  {point.y:.0f} หมวด',
            // 'pointFormat' => 'ร้อยละ {point.y:.2f} คน',
            'style' => [
                'fontSize' => '14px',
                'fontFamily' => 'Verdana, sans-serif',
            ],
        ],
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
                'colors' => ['#00b300','#1a1aff','#ff3333','#ff4d88','#ff6600'],
                'allowPointSelect'=>true,
                'dataLabels'=>[
                    'enable'=>true,
                    'format' => '{point.name}<br>ผ่านจำนวน  {point.y:.0f} หมวด',
                    // 'format' => '{point.name}<br>{point.y:.0f} คน',
                    'style' => [
                        'fontSize' => '12px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                'showInLegend' => true,
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => ' หมวด',
                'data' => [
                    $pieData[0],$pieData[1],$pieData[2],$pieData[3],$pieData[4]
                ],
            ] // new closing bracket
        ],
    ],
]);
