<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;

$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive nowrap no-footer" width="100%">
	<thead>
		<tr class="info" style="padding:0px;" role="row">
			<th rowspan="2">&nbsp;</th>
			<th rowspan="2" colspan="1"><?=$scope?></th>
			<th colspan="5" rowspan="1" class="text-center">คำนวณค่า Adjust(a) จากสุตรคำนวณ</th>
			<th rowspan="2" colspan="1" class="text-center">จำนวนประชากรหญิง<br>15-19 ปีในเขตรับผิดชอบ<br>(B)</th>
			<th colspan="3" rowspan="1" class="text-center">ไตรมาส 1</th>
			<th colspan="4" rowspan="1" class="text-center">ไตรมาส 2</th>
			<th colspan="4" rowspan="1" class="text-center">ไตรมาส 3</th>
			<th colspan="4" rowspan="1" class="text-center">ไตรมาส 4</th>
		</tr>
		<tr class="info" style="padding:0px;" role="row">
			<th rowspan="1" class="text-center">15-19_MOI<br>(คน)</th>
			<th rowspan="1" class="text-center">Birth_MOI<br>(คน)</th>
			<th rowspan="1" class="text-center">15-19_HDC<br>(คน)</th>
			<th rowspan="1" class="text-center">Birth_HDC<br>(คน)</th>
			<th rowspan="1" class="text-center">Adjust<br>(a)</th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิด<br>จากมารดาตาม B (คน)<br>(A) </th>
			<th rowspan="1" class="text-center">อัตราการคลอด<br>(A/B)x1000</th>
			<th rowspan="1" class="text-center">อัตราการคลอด Adj<br>(a) x ((A/B)x1000) x 4 </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิด<br>จากมารดาตาม B <br>(คน) </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิดสะสม<br>จากมารดาตาม B (คน)<br>(A) </th>
			<th rowspan="1" class="text-center">อัตราการคลอด<br>(A/B)x1000</th>
			<th rowspan="1" class="text-center">อัตราการคลอด Adj<br>(a) x ((A/B)x1000) x 2 </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิด<br>จากมารดาตาม B <br>(คน) </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิดสะสม<br>จากมารดาตาม B (คน)<br>(A) </th>
			<th rowspan="1" class="text-center">อัตราการคลอด<br>(A/B)x1000</th>
			<th rowspan="1" class="text-center">อัตราการคลอด Adj<br>(a) x ((A/B)x1000) x 1.33 </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิด<br>จากมารดาตาม B <br>(คน) </th>
			<th rowspan="1" class="text-center">จำนวนเด็กที่เกิดสะสม<br>จากมารดาตาม B (คน)<br>(A) </th>
			<th rowspan="1" class="text-center">อัตราการคลอด<br>(A/B)x1000</th>
			<th rowspan="1" class="text-center">อัตราการคลอด Adj<br>(a) x ((A/B)x1000) </th>
		</tr>
	</thead>
	<tfoot>
		<tr class="info" style="padding:0px;" role="row">
			<td colspan="23">ที่มา: กรมการปกครอง กระทรวงมหาดไทย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	if ($data) {
		foreach ($data as $key => $v) {
			echo '<tr>';
			echo '<td class="text-center">&nbsp;</td>';
			// Total
			if ($v['areacode'] !== 'TOTAL') {
				if ($scope == 'ตำบล') {
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} else {
					echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
				}					
			} else {
				//
				$report_date = $v['report_date'];
				//
				echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
			} //end if ($v['areacode'] !== 'TOTAL')
			//
			echo '<td class="text-center">'.number_format($v['l_target_moi']).'</td>';
			echo '<td class="text-center">'.number_format($v['l_result_moi']).'</td>';
			echo '<td class="text-center">'.number_format($v['l_target_hdc']).'</td>';
			echo '<td class="text-center">'.number_format($v['l_result_hdc']).'</td>';
			echo '<td class="text-center">'.number_format($v['adjust'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['total_target']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq1']).'</td>';
			echo '<td class="text-center">'.number_format($v['ratioq1'],2).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq1adj'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq2']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq2s']).'</td>';
			echo '<td class="text-center">'.number_format($v['ratioq2'],2).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq2adj'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq3']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq3s']).'</td>';
			echo '<td class="text-center">'.number_format($v['ratioq3'],2).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq3adj'],2).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq4']).'</td>';
			echo '<td class="text-center">'.number_format($v['resultq4s']).'</td>';
			echo '<td class="text-center">'.number_format($v['ratioq4'],2).'</td>';
			echo '<td class="text-center" style="background-color:#CCFF99">'.number_format($v['ratioq4adj'],2).'</td>';
			echo '</tr>';
			//
		} //end foreach
	} //end if  
	?>
	</tbody>
</table>

<div class="well">
	<b>หมายเหตุ:</b><br>
	&nbsp;- จำนวนหญิงอายุ 10 - 14 ปี ทั้งหมด ในเขตรับผิดชอบ(ประชากรจากการสำรวจ TypeArea=1,3) คำนวณอายุ ณ วันที่ 1 มกคราคม ของปีงบประมาณ <br>	
	&nbsp;- จำนวนหญิงอายุ 10 - 14 ปี ที่คลอดมีชีพ คำนวณอายุโดย นับถึงวันที่คลอด<br>
	
	<?=DashboardServices::getReportdate($report_date);?>

</div>

