<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive" width="100%">
	<thead>
		<tr class="info">
			<th rowspan="2">&nbsp;</th>
			<th rowspan="2" class="text-center"><?=$scope?></th>   
			<th rowspan="2" class="text-center">B</th>
			<th rowspan="2" class="text-center">A</th>
			<th rowspan="2" class="text-center">อัตราต่อพัน</th>
			<th colspan="12" class="text-center">จำนวนเด็กเกิดมีชีพจากมารดา 15-19 ปี รายเดือนตามปีงบประมาณ</th>
		</tr>
		<tr class="info">
		<!--ภาพรวม-->			
			<th class="text-center">ตค.</th>
			<th class="text-center">พย.</th>
			<th class="text-center">ธค.</th>
			<th class="text-center">มค.</th>
			<th class="text-center">กพ.</th>
			<th class="text-center">มีค.</th>
			<th class="text-center">เมย.</th>
			<th class="text-center">พค.</th>
			<th class="text-center">มิย.</th>
			<th class="text-center">กค.</th>
			<th class="text-center">สค.</th>
			<th class="text-center">กย.</th>
		</tr>
	</thead>
	<tfoot>
		<tr class="info">
			<td colspan="17">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
	<tbody>
		<?php
		if ($data) {
			foreach ($data as $key => $v) {
				echo '<tr>';
				echo '<td class="text-center">&nbsp;</td>';
				// Total
				if ($v['areacode'] !== 'TOTAL') {
					if ($scope == 'ตำบล') {
						echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
					} else {
						echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
					}					
				} else {
					//
					$report_date = $v['report_date'];
					//
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				}
				//
				echo '<td class="text-center">'.number_format($v['target']).'</td>';
				echo '<td class="text-center">'.number_format($v['result']).'</td>';
				echo '<td class="text-center">'.number_format($v['total_ratio'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['result10']).'</td>';
				echo '<td class="text-center">'.number_format($v['result11']).'</td>';
				echo '<td class="text-center">'.number_format($v['result12']).'</td>';
				echo '<td class="text-center">'.number_format($v['result1']).'</td>';
				echo '<td class="text-center">'.number_format($v['result2']).'</td>';
				echo '<td class="text-center">'.number_format($v['result3']).'</td>';
				echo '<td class="text-center">'.number_format($v['result4']).'</td>';
				echo '<td class="text-center">'.number_format($v['result5']).'</td>';
				echo '<td class="text-center">'.number_format($v['result6']).'</td>';
				echo '<td class="text-center">'.number_format($v['result7']).'</td>';
				echo '<td class="text-center">'.number_format($v['result8']).'</td>';
				echo '<td class="text-center">'.number_format($v['result9']).'</td>';
				echo '</tr>';
				//
			} //end foreach
		} //end if  
		?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
	&nbsp;- จำนวนหญิงอายุ 15 - 19 ปี ทั้งหมด ในเขตรับผิดชอบ(ประชากรจากการสำรวจ TypeArea=1,3) คำนวณอายุ ณ วันที่ 1 มกคราคม ของปีงบประมาณ <br>	
	&nbsp;- จำนวนหญิงอายุ 15 - 19 ปี ที่คลอดมีชีพ คำนวณอายุโดย นับถึงวันที่คลอด<br>
	
	<?=DashboardServices::getReportdate($report_date);?>
	
</div>

