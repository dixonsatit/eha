<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);

?>

<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <!--<tr class="info">
    <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
  </tr>-->
  <tr class="info">
    <th class="text-center">#</th>
    <th class="text-center"><?=$scope?></th>
    <th class="text-center">ได้รับการตรวจสุขภาพช่องปาก (B)</th>
    <th class="text-center">ฟันดีไม่มีผุ (A)</th>
    <th class="text-center">ร้อยละ</th>
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
  foreach ($data as $k => $v) {
    echo '<tr>';
    echo '<td>'. ($k + 1) . '</td>';
    if ($v['areacode'] !== 'TOTAL') {
      //
      $report_date = $v['report_date'];
      //
      if ($ap==null) {
        echo '<td class="text-left">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode'], ['class' => 'success'],'_blank') . '</td>';
      }else{
        echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
      } //end if ($ap==null)
    } 
    else {
      //
      $report_date = $v['report_date'];
      //
      echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
    } //end if ($v['areacode'] !== 'TOTAL')
    //
    echo '<td class="text-right">'.number_format($v['target'],2).'</td>
        <td class="text-right">'.number_format($v['result'],2).'</td>
        <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
      </tr>';
  } //end foreach
} //end if  

// print_r($kpi); die();
// foreach($data as $k => $v){

//     // $mapType!=='ap'?window.open($mapUrl+'&'+$mapType,'_self'):null;

// $v['areacode'] == 'TOTAL' ? $aa=$v['areaname_new'] : $aa='<a target="_blank" href="changwat/?year='.$_GET['year'].'&kid='.$kpi['kpi_template_id'].'&rg='.$v['areacode'].'">'.$v['areaname_new'].'</a>' ;

// echo '<tr>
//     <td class="text-left">'.($k+1).'</td>
//     <td class="text-left">'.$aa.'</td>
//     <td class="text-right">'.number_format($v['target'],2).'</td>
//     <td class="text-right">'.number_format($v['result'],2).'</td>
//     <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
//   </tr>';
// }
?>

</tbody>
<tfoot class="info">
<tr>
<td colspan="5" class="info">
<b>หมายเหตุ: </b>
<br/>- ใช้ข้อมูลล่าสุดของการตรวจฟันในปีงบประมาณ(แฟ้ม Dental) 

<br/>B = จำนวนเด็กที่มีอายุ 12 ปี ถึง 12 ปี 11 เดือน 29 วัน ณ วันที่มารับบริการตรวจช่องปาก 
<br/>    และอาศัยอยู่ในเขตพื้นที่รับผิดชอบ Person Type Area (“1”,“3”) และ Person Discharge = “9” , ที่ปรับปรุงข้อมูลล่าสุด (d_update) , ไม่นับซ้ำ (distinct) hospcode + pid และแฟ้ม dental มี provider type=02,06 เงื่อนไขคุณภาพแฟ้ม dental PFILLING+PEXTRACT+PCARIES <=28 , PTEETH= 1 – 28 และ PCARIES+ PFILLING <=PTEETH 

<br/>A = นับจำนวนเด็กจากรายการข้อมูล 2 (B) ที่มีเงื่อนไข คือ PFILLING>=0 และ PEXTRACT=0 และPCARIES=0

<br/><br/>ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)
<br/><br/>
<?=DashboardServices::getReportdate($report_date);?>
<?
// echo Yii::$app->thaiFormatter->asDate($v['datecom'], 'short')."<br>";
?>
</td>
</tr>
</tfoot>

</table>


</div>
<!-- <div class="panel-footer" style="color:green;">
หมายเหตุ: 
<br/>- ใช้ข้อมูลล่าสุดของการตรวจฟันในปีงบประมาณ(แฟ้ม Dental) 

<br/>B=จำนวนเด็กที่มีอายุ 12 ปี ถึง 12 ปี 11 เดือน 29 วัน ณ วันที่มารับบริการตรวจช่องปาก และอาศัยอยู่ในเขตพื้นที่รับผิดชอบ Person Type Area (“1”,“3”) และ Person Discharge = “9” , ที่ปรับปรุงข้อมูลล่าสุด (d_update) , ไม่นับซ้ำ (distinct) hospcode + pid และแฟ้ม dental มี provider type=02,06 เงื่อนไขคุณภาพแฟ้ม dental PFILLING+PEXTRACT+PCARIES <=28 , PTEETH= 1 – 28 และ PCARIES+ PFILLING <=PTEETH 

<br/><br/>A=นับจำนวนเด็กจากรายการข้อมูล 2 (B) ที่มีเงื่อนไข คือ PFILLING>=0 และ PEXTRACT=0 และPCARIES=0

<br/><br/>ที่มา : ระบบ HDC กระทรวงสาธรณสุข
</div> -->
</div>


<!--///////////////////////////////////////////////////////////////////////////-->




