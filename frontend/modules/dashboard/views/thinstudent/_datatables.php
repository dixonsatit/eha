<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;

$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id= "datatables" class= "table table-striped table-hover table-bordered table-responsive" width="100%">
	<thead>        
		<tr class="info" style="padding:0px;" role="row">
			<th class="text-center" rowspan="2" colspan="1">&nbsp;</th>
			<th class="text-center col-lg-2" rowspan="2" colspan="1"><?=$scope?></th>			
			<th class="text-center col-lg-4" colspan="3" rowspan="1">เทอม 2</th>
			<th class="text-center col-lg-4" colspan="3" rowspan="1">เทอม 1</th>
			<th class="text-center col-lg-2" rowspan="2" colspan="1">ร้อยละของทั้งหมด</th>
		</tr>
		<tr class="info" style="padding:0px;" role="row">
			<th class="text-center" rowspan="1" colspan="1">ชั่งน้ำหนักวัดส่วนสูง<br>(B1)</th>
			<th class="text-center" rowspan="1" colspan="1">ผอม<br>(A2)</th>
			<th class="text-center" rowspan="1" colspan="1">%</th>
			<th class="text-center" rowspan="1" colspan="1">ชั่งน้ำหนักวัดส่วนสูง<br>(B1)</th>
			<th class="text-center" rowspan="1" colspan="1">ผอม<br>(A2)</th>
			<th class="text-center" rowspan="1" colspan="1">%</th>
			
		</tr>
    </thead>
	<tfoot>
		<tr class="info">
			<td colspan="9">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	if ($data) {
		foreach ($data as $key => $v) {
			echo '<tr>';
			echo '<td class="text-center">&nbsp;</td>';
			//ภาพรวม
			if ($v['areacode'] !== 'TOTAL') {
				if ($scope == 'ตำบล') {
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} else {
					echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
				}					
			} else {
				//
				$report_date = $v['report_date'];
				//
				echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
			}
			//
			echo '<td class="text-right">'.number_format($v['b1q2']).'</td>';
			echo '<td class="text-right">'.number_format($v['a2q2']).'</td>';
			echo '<td class="text-right">'.number_format($v['ratio_a2q2'],2).'</td>';
			echo '<td class="text-right">'.number_format($v['b1q1']).'</td>';
			echo '<td class="text-right">'.number_format($v['a2q1']).'</td>';
			echo '<td class="text-right">'.number_format($v['ratio_a2q1'],2).'</td>';
			echo '<td class="text-right">'.number_format($v['total_ratio'],2).'</td>';
			echo '</tr>';
			//
		} //end foreach
	} //end if  
	?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
		&nbsp;- B1 หมายถึง จำนวนเด็กวัยเรียน 6-14 ปีที่ชั่งนำหนักวัดส่วนสูง<br>
		&nbsp;- A2 หมายถึง จำนวนเด็กวัยเรียน ผอม<br>
	<b>ประเมินจากแฟ้ม NUTRITION</b><br>
		&nbsp;- เด็กอายุ 6-14 ปี <br>
		&nbsp;- 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูงเกิน 1 ครั้งต่อเทอม จะยึดค่าน้ำหนักและส่วนสูงครั้งสุดท้ายของเทอม <br>
		&nbsp;- 1 คน หากมีการชั่งน้ำหนักวัดส่วนสูง ทั้ง 2 เทอมจะนับให้เทอมละ 1 ครั้ง <br>
		&nbsp;สรุปตัดความซ้ำซ้อนด้วย HOSPCODE,PID,เทอม <br>

		&nbsp;ค่าน้ำหนักต้องอยู่ในช่วง 0.1 ถึง 300 กก. <br>
		&nbsp;ค่าส่วนสูงต้องอยู่ในช่วง 40 ถึง 250 ซม. <br>
		&nbsp;สัญชาติ ไทย <br>
		&nbsp;<font style="color: #F00">นับตามวันที่ให้บริการ(DATE_SERV) ที่อยู่ในช่วง เดือน พค ถึง กค.(เทอม 1) , เดือน ตค. ถึง ม	ค.(เทอม 2) เท่านั้น</font><br>
		
		<?=DashboardServices::getReportdate($report_date);?>
</div>

