<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  // Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>

<div class ="text-center front-weight-bold text-dark"><h5> ตารางข้อมูลรายพื้นที่ </h5></div>
<table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
<thead>
<tr class="info">    
   <th rowspan="2">&nbsp;</th>
   <th rowspan="2" class="text-center"><?=$scope?></th>
   <th rowspan="2" class="text-center">B</th>
   <th rowspan="2" class="text-center">A</th>
   <th rowspan="2" class="text-center">ร้อยละ</th>
   <th colspan="3" class="text-center">ไตรมาส 1</th>
   <th colspan="3" class="text-center">ไตรมาส 2</th>
   <th colspan="3" class="text-center">ไตรมาส 3</th>
   <th colspan="3" class="text-center">ไตรมาส 4</th>
 </tr>
 <tr class="info">
 <!--ภาพรวม-->
   <th class="text-center">ตค.</th>
   <th class="text-center">พย.</th>
   <th class="text-center">ธค.</th>
   <th class="text-center">มค.</th>
   <th class="text-center">กพ.</th>
   <th class="text-center">มีค.</th>
   <th class="text-center">เมย.</th>
   <th class="text-center">พค.</th>
   <th class="text-center">มิย.</th>
   <th class="text-center">กค.</th>
   <th class="text-center">สค.</th>
   <th class="text-center">กย.</th>
 </tr>
</thead>

<tfoot>
      <tr class="info">
            <td colspan="17">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
        </tr>
</tfoot>
<tbody>
<?php
if ($data) {
  foreach ($data as $key => $v) {
    echo '<tr>';
    echo '<td class="text-center">&nbsp;</td>';
    //ภาพรวม
    if ($v['areacode'] !== 'TOTAL') {
      if ($scope == 'ตำบล') {
        echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
      } else {
        echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
      }
    } else {
      //
      $report_date = $v['report_date'];
      //
      echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
    }
    //
    echo '<td class="text-center">'.number_format($v['target']).'</td>';
    echo '<td class="text-center">'.number_format($v['result']).'</td>';
    echo '<td class="text-center">'.number_format($v['ratio'],2).'</td>';
    //ไตรมาส 1
    echo '<td class="text-center">'.number_format($v['resultm10']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm11']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm12']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm01']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm02']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm03']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm04']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm05']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm06']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm07']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm08']).'</td>';
    echo '<td class="text-center">'.number_format($v['resultm09']).'</td>';
    echo '</tr>';
    //
  } //end foreach
} //end if  
?>
</tbody>
</table>

<p class="font-weight-bold">หมายเหตุ:<br>
A หมายถึง จำนวนเด็กแรกเกิดน้ำหนักน้อยกว่า 2500 กรัม ในช่วงเวลาที่กำหนด<br>	
B หมายถึง จำนวนเด็กที่คลอดในช่วงเวลาที่กำหนด<br>

<?=DashboardServices::getReportdate($report_date);?>

</p>
