<?php
include ('lib/bc_ampur.php'); 
include ('lib/btn_template.php'); 
?>

<div class="content animate-panel">
	<?php include ('lib/ul_year.php'); ?>
	<div class="row">
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Gauge -->
					<?= $this->render('_gauge',['gaugeData'=>$gaugeData,'plotBands'=>$plotBands])?>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Map -->
					<?= $this->render('_map',['geo'=>$geo,'mapUrl'=>$mapUrl,'mapType'=>$mapType,'mapLegend'=>$mapLegend])?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Trend -->
					<?= $this->render('_trend',['kpi'=>$kpi,'trendData'=>$trendData])?>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Column -->
					<?= $this->render('_column',['kpi'=>$kpi,'columnData'=>$columnData])?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="hpanel">
				<div class="panel-body h-200">
					<!-- Data Table -->
					<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data,'mapUrl'=>$mapUrl,'mapType'=>$mapType,'scope'=>$scope])?>
				</div>
			</div>
		</div>
	</div>
    
=======

<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;
use yii\widgets\Breadcrumbs;
?>

<?php
$this->title = $kpi['kpi_name'] ." :: ". $scope;;
// $this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' => ['changwat']];
$this->params['breadcrumbs'][] = $scope;;
?>
<div class=" animate-panel">
    <div class="panel panel-default">

        <div class="manifest-default-index content">
        <?= Breadcrumbs::widget([
      'homeLink' => [ 
                      'label' => Yii::t('yii', 'หน้าหลัก'),
                      'url' => Yii::$app->homeUrl,
                 ],
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ]) ;
?>


<?=$this->render('//_header')?>


<div class="content animate-panel">


<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData,'plotBands'=>$plotBands])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Map -->
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl, 'mapType' => $mapType, 'mapLegend' => $mapLegend])?>


                    </div>

                </div>
            </div>

     </div>


     <div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>

                    </div>

                </div>
            </div>

    </div>

    <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <?=$this->render('_datatables', ['kpi' => $kpi, 'data' => $data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>

                    </div>

                </div>
            </div>
    </div>

</div>