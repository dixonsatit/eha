<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables_year',0);
JS;
$this->registerJs($script);
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางแสดงข้อมูลจำนวนและอัตราตายของทารกแรกเกิด (ต่ำกว่า 28 วัน) อัตราตายทารก (ต่ำกว่า 1 ปี) จําแนกตามเพศ และอัตราตายมารดา <br>แสดงข้อมูลย้อนหลัง 10 ปี (นับจากปี <?=$thai_year?>)</h5></div>
<table id="datatables_year" class="table table-striped table-hover table-bordered table-responsive" cellspacing="0" width="100%">
<thead>
<tr class="info">
  <th rowspan="3" class="text-center">&nbsp;</th>
  <th rowspan="3" class="text-center align-middle">ปี พ.ศ.</th>
  <th colspan="6" class="text-center">ทารกแรกเกิดตาย</th>
  <th colspan="6" class="text-center">ทารกตาย</th>
  <th colspan="2" class="text-center">มารดาตาย</th>
</tr>
<tr class="info">
  <!--ทารกแรกเกิดตาย-->
  <th colspan="2" class="text-center">รวม</th>
  <th colspan="2" class="text-center">ชาย</th>
  <th colspan="2" class="text-center">หญิง</th>
  <!--ทารกตาย-->
  <th colspan="2" class="text-center">รวม</th>
  <th colspan="2" class="text-center">ชาย</th>
  <th colspan="2" class="text-center">หญิง</th>

  <th class="text-center"></th>
  <th class="text-center"></th>
</tr>
<tr class="info">
  <!--ทารกแรกเกิดตาย-->
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <!--ทารกตาย-->
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <!--มารดาตาย-->
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
</tr>
</thead>
<tfoot>
        <tr class="info">
            <td colspan="16">
            รวบรวมและวิเคราะห์โดย : กลุ่มข้อมูลข่าวสารสุขภาพ  กองยุทธศาสตร์และแผนงาน
            </td>
        </tr>
    </tfoot>
<tbody>
<?php
if($datatrend)
{
 foreach ($datatrend as $key => $value) {
  $thai_year=(int)$value['b_year']+543;
  echo '<tr>';
  echo '<td class="text-center">&nbsp;</td>';
  echo '<td class="text-center">'.$thai_year.'</td>';
  echo '<td class="text-center">'.$value['nm_amount_total'].'</td>';
  echo '<td class="text-center">'.$value['nm_rate_total'].'</td>';
  echo '<td class="text-center">'.$value['nm_amount_m'].'</td>';
  echo '<td class="text-center">'.$value['nm_rate_m'].'</td>';
  echo '<td class="text-center">'.$value['nm_amount_f'].'</td>';
  echo '<td class="text-center">'.$value['nm_rate_f'].'</td>';
  echo '<td class="text-center">'.$value['im_number_total'].'</td>';
  echo '<td class="text-center">'.$value['im_rate_total'].'</td>';
  echo '<td class="text-center">'.$value['im_number_m'].'</td>';
  echo '<td class="text-center">'.$value['im_rate_m'].'</td>';
  echo '<td class="text-center">'.$value['im_number_f'].'</td>';
  echo '<td class="text-center">'.$value['im_rate_f'].'</td>';
  echo '<td class="text-center">'.$value['mm_number'].'</td>';
  echo '<td class="text-center">'.$value['mm_rate'].'</td>';
  echo '</tr>';
 }
}
?>
</tbody>
</table>