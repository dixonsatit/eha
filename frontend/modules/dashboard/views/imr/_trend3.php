<?php
use miloschuman\highcharts\Highcharts;
?>
<?php 

    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'line'
                ],
                'title'=> [
                    'text'=> 'แนวโน้มรายปี'
                ],
                'subtitle'=> [
                    //'text'=> 'ปีงบประมาณ'
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ]
                    ]
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                     'text' => ''
                    ],
                    'plotLines' => [[
                        // 'value' => (int) $kpiTemplateToYear->target_value,
                        'value' => $target_value,
                        'color' => 'red',
                        'width' => 2,
                        'label' => [
                            //'text' => 'Theoretical mean => 932',
                            'align' => 'center',
                            'style' => [
                                'color' => 'gray'
                            ]
                        ]
                    ]]
                ],
                'legend' => [
                    'enabled' => false
                ],
                'tooltip' => [
                    'headerFormat'=>'<span style="font-size:10px">ปี {point.key}</span><table>',
                    'pointFormat'=> '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.2f}%</b></td></tr>',
                    'footerFormat'=>'</table>',
                    'shared'=>true,
                    'useHTML'=>true,
                ],
                'series' => [
                    [
                        'name' => 'ปีงบประมาณ',
                        'colorByPoint' => false,
                        // 'color'=> '#62cb31',
                        //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                        'data'=>$trendData,
                        'dataLabels'=> [
                            'enabled'=> true,
                            'rotation'=> 0,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            'y' => 0,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                    ]
                ]
            ]
        ]);
        ?>