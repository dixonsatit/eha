<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables3',0);
JS;
$this->registerJs($script);
$request = Yii::$app->request;
$rg=null;
$rg=$request->get('rg');
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางข้อมูลรายพื้นที่</h5></div>
<table id="datatables3" class="table table-striped table-hover table-bordered" cellspacing="0" width="auto">
    <thead>
    <tr class="info">
      <th rowspan="2" class="text-center align-middle">ลำดับ</th>
      <th rowspan="2" class="text-center align-middle">พื้นที่</th>
      <th colspan="3" class="text-center">ภาพรวม</th>
      <th colspan="3" class="text-center">ไตรมาส 1</th>
      <th colspan="3" class="text-center">ไตรมาส 2</th>
      <th colspan="3" class="text-center">ไตรมาส 3</th>
      <th colspan="3" class="text-center">ไตรมาส 4</th>
    </tr>
    <tr class="info">
      <!--ภาพรวม-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส1-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส2-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส3-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
      <!--ไตรมาส4-->
      <th class="text-center">B</th>
      <th class="text-center">A</th>
      <th class="text-center">ร้อยละ</th>
    </tr>
  </thead>
  <tfoot>
        <tr class="info">
            <td colspan="17">ที่มา: กรมการปกครอง กระทรวงมหาดไทย (ข้อมูลจาก กรมการแพทย์)</td>
        </tr>
    </tfoot>
  <tbody>
   <?php
   if($data)
   {
     foreach ($data as $key => $value) {
      echo '<tr>';
      echo '<td class="text-center">'.($key+1).'</td>';
      //echo '<td class="text-center">'.$value['areaname'].'</td>';
      if ($value['areacode'] !== 'TOTAL')
      {
        if($request->get('rg')=='')
        {
         echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode'], ['class' => 'success'],'_blank') . '</td>';
        }
        else 
        {
         echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
        }
      } 
     else 
     {
      echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
     }
      echo '<td class="text-center">'.$value['target'].'</td>';
      echo '<td class="text-center">'.$value['result'].'</td>';
      echo '<td class="text-center">'.$value['total_ratio'].'</td>';

      echo '<td class="text-center">'.$value['target1'].'</td>';
      echo '<td class="text-center">'.$value['result1'].'</td>';
      echo '<td class="text-center">'.$value['total_ratio1'].'</td>';

      echo '<td class="text-center">'.$value['target2'].'</td>';
      echo '<td class="text-center">'.$value['result2'].'</td>';
      echo '<td class="text-center">'.$value['total_ratio2'].'</td>';

      echo '<td class="text-center">'.$value['target3'].'</td>';
      echo '<td class="text-center">'.$value['result3'].'</td>';
      echo '<td class="text-center">'.$value['total_ratio3'].'</td>';

      echo '<td class="text-center">'.$value['target4'].'</td>';
      echo '<td class="text-center">'.$value['result4'].'</td>';
      echo '<td class="text-center">'.$value['total_ratio4'].'</td>';
      echo '</tr>';
     }
    }
   ?>
  </tbody>
  </table>
  <div class="well">
                <b>หมายเหตุ:</b> <br> 
ทารกแรกเกิด หมายถึง ทารกที่คลอด รอดออกมา มีชีวิตจนถึง 28 วันในโรงพยาบาลสังกัดสำนักงานปลัดกระทรวงสาธารณสุข (รพศ./รพท./รพช./รพ.สต.)<br> 
B หมายถึง จำนวนทารกแรกเกิดมีชีพ (คน)<br>	
A หมายถึง จำนวนทารกที่เสียชีวิต < 28 วัน (คน) <br>
สูตรคำนวณตัวชี้วัด=(A/B)*1000<br>
เกณฑ์เป้าหมาย: <= ลดอัตราตายของทารกแรกเกิดอายุต่ำกว่าหรือเท่ากับ 28 วัน
</div>