<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables_sex_age',0);
JS;
$this->registerJs($script);
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางแสดงข้อมูลจำนวนและอัตราการตายของทารก (อายุต่ำกว่า 1 ปี) จำแนกรายอายุและเพศ<br>แสดงข้อมูลย้อนหลัง 10 ปี (นับจากปี <?=$thai_year?> หรือเท่าที่มีข้อมูล)</h5></div>
<table id="datatables_sex_age" class="table table-striped table-hover table-bordered table-responsive" cellspacing="0" width="100%">
<thead>
<tr class="info">
  <th rowspan="2" class="text-center">&nbsp;</th>
  <th rowspan="2" class="text-center align-middle">ปี พ.ศ.</th>
  <th rowspan="2" class="text-center align-middle">เพศ</th>
  <th colspan="2" class="text-center">ทุกอายุ</th>
  <th colspan="2" class="text-center">ต่ำกว่า 1 วัน</th>
  <th colspan="2" class="text-center">1 วัน</th>
  <th colspan="2" class="text-center">2 วัน</th>
  <th colspan="2" class="text-center">3 วัน</th>
  <th colspan="2" class="text-center">4 วัน</th>
  <th colspan="2" class="text-center">5 วัน</th>
  <th colspan="2" class="text-center">6 วัน</th>
  <th colspan="2" class="text-center">7 - 13 วัน</th>
  <th colspan="2" class="text-center">14 - 20 วัน</th>
  <th colspan="2" class="text-center">21 - 27 วัน</th>
  <th colspan="2" class="text-center">28 วัน - ต่ำกว่า 2 เดือน</th>
  <th colspan="2" class="text-center">2 เดือน</th>
  <th colspan="2" class="text-center">3 เดือน</th>
  <th colspan="2" class="text-center">4 เดือน</th>
  <th colspan="2" class="text-center">5 เดือน</th>
  <th colspan="2" class="text-center">6 เดือน</th>
  <th colspan="2" class="text-center">7 เดือน</th>
  <th colspan="2" class="text-center">8 เดือน</th>
  <th colspan="2" class="text-center">9 เดือน</th>
  <th colspan="2" class="text-center">10 เดือน</th>
  <th colspan="2" class="text-center">11 เดือน</th>
  <th colspan="2" class="text-center">ไม่ทราบ</th>
</tr>
<tr class="info">
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
</tr>
</thead>
<tfoot>
        <tr class="info">
            <td colspan="49">
            รวบรวมและวิเคราะห์โดย : กลุ่มข้อมูลข่าวสารสุขภาพ  กองยุทธศาสตร์และแผนงาน
            </td>
        </tr>
    </tfoot>
<tbody>
<?php
if($datasexcode)
{
 foreach ($datasexcode as $key => $value) {
  $thai_year=(int)$value['b_year']+543;
  if($value['sex']=="T")
  {
    $sex="รวม";
  }
  if($value['sex']=="M")
  {
    $sex="ชาย";
  }
  if($value['sex']=="F")
  {
    $sex="หญิง";
  }
  echo '<tr>';
  echo '<td class="text-center">&nbsp;</td>';
  if($value['sex']=="T")
  {
    echo '<td class="text-center">'.$thai_year.'</td>';
  }
  else
  {
    echo '<td class="text-center"></td>';
  }
  echo '<td class="text-center">'.$sex.'</td>';
  echo '<td class="text-center">'.$value['AllAge_number'].'</td>';
  echo '<td class="text-center">'.$value['AllAge_rate'].'</td>';
  echo '<td class="text-center">'.$value['under1day_number'].'</td>';
  echo '<td class="text-center">'.$value['under1day_rate'].'</td>';
  echo '<td class="text-center">'.$value['1day_number'].'</td>';
  echo '<td class="text-center">'.$value['1day_rate'].'</td>';
  echo '<td class="text-center">'.$value['2day_number'].'</td>';
  echo '<td class="text-center">'.$value['2day_rate'].'</td>';
  echo '<td class="text-center">'.$value['3day_number'].'</td>';
  echo '<td class="text-center">'.$value['3day_rate'].'</td>';
  echo '<td class="text-center">'.$value['4day_number'].'</td>';
  echo '<td class="text-center">'.$value['4day_rate'].'</td>';
  echo '<td class="text-center">'.$value['5day_number'].'</td>';
  echo '<td class="text-center">'.$value['5day_rate'].'</td>';
  echo '<td class="text-center">'.$value['6day_number'].'</td>';
  echo '<td class="text-center">'.$value['6day_rate'].'</td>';
  echo '<td class="text-center">'.$value['7_13day_number'].'</td>';
  echo '<td class="text-center">'.$value['7_13day_rate'].'</td>';
  echo '<td class="text-center">'.$value['14_20day_number'].'</td>';
  echo '<td class="text-center">'.$value['14_20day_rate'].'</td>';
  echo '<td class="text-center">'.$value['21_27day_number'].'</td>';
  echo '<td class="text-center">'.$value['21_27day_rate'].'</td>';
  echo '<td class="text-center">'.$value['28day_2month_number'].'</td>';
  echo '<td class="text-center">'.$value['28day_2month_rate'].'</td>';
  echo '<td class="text-center">'.$value['2month_number'].'</td>';
  echo '<td class="text-center">'.$value['2month_rate'].'</td>';
  echo '<td class="text-center">'.$value['3month_number'].'</td>';
  echo '<td class="text-center">'.$value['3month_rate'].'</td>';
  echo '<td class="text-center">'.$value['4month_number'].'</td>';
  echo '<td class="text-center">'.$value['4month_rate'].'</td>';
  echo '<td class="text-center">'.$value['5month_number'].'</td>';
  echo '<td class="text-center">'.$value['5month_rate'].'</td>';
  echo '<td class="text-center">'.$value['6month_number'].'</td>';
  echo '<td class="text-center">'.$value['6month_rate'].'</td>';
  echo '<td class="text-center">'.$value['7month_number'].'</td>';
  echo '<td class="text-center">'.$value['7month_rate'].'</td>';
  echo '<td class="text-center">'.$value['8month_number'].'</td>';
  echo '<td class="text-center">'.$value['8month_rate'].'</td>';
  echo '<td class="text-center">'.$value['9month_number'].'</td>';
  echo '<td class="text-center">'.$value['9month_rate'].'</td>';
  echo '<td class="text-center">'.$value['10month_number'].'</td>';
  echo '<td class="text-center">'.$value['10month_rate'].'</td>';
  echo '<td class="text-center">'.$value['11month_number'].'</td>';
  echo '<td class="text-center">'.$value['11month_rate'].'</td>';
  echo '<td class="text-center">'.$value['unknow_number'].'</td>';
  echo '<td class="text-center">'.$value['unknow_rate'].'</td>';
  echo '</tr>';
 }
}
?>
</tbody>
</table>