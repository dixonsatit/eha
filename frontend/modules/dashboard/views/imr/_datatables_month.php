<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables_month',0);
JS;
$this->registerJs($script);
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางแสดงข้อมูลจำนวนและอัตราตายทารก (อายุต่ำกว่า 1 ปี) ต่อการเกิดมีชีพ 1,000 คน จำแนกรายเดือน<br>แสดงข้อมูลย้อนหลัง 10 ปี (นับจากปี <?=$thai_year?> หรือเท่าที่มีข้อมูล)</h5></div>
<table id="datatables_month" class="table table-striped table-hover table-bordered table-responsive" cellspacing="0" width="100%">
<thead>
<tr class="info">
  <th rowspan="2" class="text-center">&nbsp;</th>
  <th rowspan="2" class="text-center align-middle">ปี พ.ศ.</th>
  <th colspan="2" class="text-center">ทั้งปี</th>
  <th colspan="2" class="text-center">มกราคม</th>
  <th colspan="2" class="text-center">กุมภาพันธ์</th>
  <th colspan="2" class="text-center">มีนาคม</th>
  <th colspan="2" class="text-center">เมษายน</th>
  <th colspan="2" class="text-center">พฤษภาคม</th>
  <th colspan="2" class="text-center">มิถุนายน</th>
  <th colspan="2" class="text-center">กรกฎาคม</th>
  <th colspan="2" class="text-center">สิงหาคม</th>
  <th colspan="2" class="text-center">กันยายน</th>
  <th colspan="2" class="text-center">ตุลาคม</th>
  <th colspan="2" class="text-center">พฤศจิกายน</th>
  <th colspan="2" class="text-center">ธันวาคม</th>
</tr>
<tr class="info">
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  <th class="text-center">จำนวน</th>
  <th class="text-center">อัตรา</th>
  
</tr>
</thead>
<tfoot>
        <tr class="info">
            <td colspan="28">
            รวบรวมและวิเคราะห์โดย : กลุ่มข้อมูลข่าวสารสุขภาพ  กองยุทธศาสตร์และแผนงาน
            </td>
        </tr>
    </tfoot>
<tbody>
<?php
if($datamonth10y)
{
 foreach ($datamonth10y as $key => $value) {
  $thai_year=(int)$value['b_year']+543;
  echo '<tr>';
  echo '<td class="text-center">&nbsp;</td>';
  echo '<td class="text-center">'.$thai_year.'</td>';
  echo '<td class="text-center">'.$value['result_total'].'</td>';
  echo '<td class="text-center">'.$value['rate_total'].'</td>';
  echo '<td class="text-center">'.$value['result1'].'</td>';
  echo '<td class="text-center">'.$value['rate1'].'</td>';
  echo '<td class="text-center">'.$value['result2'].'</td>';
  echo '<td class="text-center">'.$value['rate2'].'</td>';
  echo '<td class="text-center">'.$value['result3'].'</td>';
  echo '<td class="text-center">'.$value['rate3'].'</td>';
  echo '<td class="text-center">'.$value['result4'].'</td>';
  echo '<td class="text-center">'.$value['rate4'].'</td>';
  echo '<td class="text-center">'.$value['result5'].'</td>';
  echo '<td class="text-center">'.$value['rate5'].'</td>';
  echo '<td class="text-center">'.$value['result6'].'</td>';
  echo '<td class="text-center">'.$value['rate6'].'</td>';
  echo '<td class="text-center">'.$value['result7'].'</td>';
  echo '<td class="text-center">'.$value['rate7'].'</td>';
  echo '<td class="text-center">'.$value['result8'].'</td>';
  echo '<td class="text-center">'.$value['rate8'].'</td>';
  echo '<td class="text-center">'.$value['result9'].'</td>';
  echo '<td class="text-center">'.$value['rate9'].'</td>';
  echo '<td class="text-center">'.$value['result10'].'</td>';
  echo '<td class="text-center">'.$value['rate10'].'</td>';
  echo '<td class="text-center">'.$value['result11'].'</td>';
  echo '<td class="text-center">'.$value['rate11'].'</td>';
  echo '<td class="text-center">'.$value['result12'].'</td>';
  echo '<td class="text-center">'.$value['rate12'].'</td>';
  echo '</tr>';
 }
}
?>
</tbody>
</table>