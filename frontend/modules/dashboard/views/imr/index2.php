<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$this->title = 'อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน';
$this->params['breadcrumbs'][] = ['label'=>'ภาพรวมประเทศ(รายปี)'];
?>
<?php
$script = <<< JS
        //Datatables
         getDatatable('#datatables',);
JS;
$this->registerJs($script);
$conn = \Yii::$app->db;
$request=\yii::$app->request;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
<div class="col-md-2 text-right">
                           <h5> เลือกปี </h5>
                        </div>
                        <div class="dropdown col-md-1">

                            <a href="#" class="dropdown-toggle btn btn-default" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
                            aria-expanded="false"> <?= $request->get('year')+543;?><span class="caret"></span> </a>
                            <?php
                            $sqlyear='select * from kpi_fiscal_year where year>=2001 order by year DESC';
                            $cmd = $conn->createCommand($sqlyear);
                            $datayear = $cmd->queryAll();
                            foreach ($datayear as $key => $value) {
                                $cyear[] = array('label'=>$value['year']+543,
                                'url'=>Url::to(['index', 'year'=>$value['year']]));
                            }
                                echo Dropdown::widget([
                                    'items' => $cyear
                                ]);
                            ?>
                        </div>
                       
                        
</div>
<hr>
<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_gauge',['gaugeData'=>$gaugeData,'plotBands'=>$plotBands])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <?=$this->render('_map', ['geo' => $geo, 'mapLegend' => $mapLegend])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
<h3>1.อัตราตายทารก (ต่ำกว่า 1 ปี) จําแนกตามเพศ</h3>
<div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_column_year',['target_value'=>$target_value,'Neonatal'=>$Neonatal,'Infant'=>$Infant,'Maternal'=>$Maternal])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_trend',['target_value'=>$target_value,'trend_nm'=>$trend_nm,'trend_im'=>$trend_im,'trend_mm'=>$trend_mm])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_trend_sex',['target_value'=>$target_value,'trend_sex_nm_m'=>$trend_sex_nm_m,'trend_sex_nm_f'=>$trend_sex_nm_f,'trend_sex_im_m'=>$trend_sex_im_m,'trend_sex_im_f'=>$trend_sex_im_f])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_datatables_year',['datatrend'=>$datatrend])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <hr>
<h3>2.อัตราตายทารก (อายุต่ำกว่า 1 ปี) ต่อการเกิดมีชีพ 1,000 คน จำแนกรายเดือน</h3>
<div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_column_month',['target_value'=>$target_value,'column_month'=>$column_month])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_datatables_month',['datamonth10y'=>$datamonth10y])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <hr>
    <h3>3.อัตราของการตายของทารก (อายุต่ำกว่า 1 ปี) จำแนกรายอายุและเพศ</h3>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_column_sex_code',['column_sex_T'=>$column_sex_T,'column_sex_M'=>$column_sex_M,'column_sex_F'=>$column_sex_F])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_datatables_sex_age',['datasexcode'=>$datasexcode])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
    </div>
