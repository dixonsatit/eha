<?php
use miloschuman\highcharts\Highcharts;
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>
<?php 

    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'line'
                ],
                'title'=> [
                    'text'=> 'แนวโน้มอัตราตายของทารกแรกเกิด (ต่ำกว่า 28 วัน) อัตราตายทารก (ต่ำกว่า 1 ปี) จําแนกตามเพศ(รายปี)'
                ],
                'subtitle'=> [
                    'text'=> 'ข้อมูลย้อนหลังไม่เกิน ่10 ปี(นับจากปี'.$thai_year.')'
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ]
                    ]
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                     'text' => ''
                    ],
                    'plotLines' => [[
                        // 'value' => (int) $kpiTemplateToYear->target_value,
                        'value' => $target_value,
                        'color' => 'red',
                        'width' => 2,
                        'label' => [
                            //'text' => 'Theoretical mean => 932',
                            // 'align' => 'center',
                            // 'style' => [
                            //     'color' => 'gray'
                            // ]
                        ]
                    ]]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'headerFormat'=>'<span style="font-size:10px">ปี {point.key}</span><table>',
                    'pointFormat'=> '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.2f}%</b></td></tr>',
                    'footerFormat'=>'</table>',
                    'shared'=>true,
                    'useHTML'=>true,
                ],
                'series' => [
                    [
                        'name' => 'ทารกแรกเกิดตาย(ชาย)',
                        'colorByPoint' => false,
                        // 'color'=> '#62cb31',
                        //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                        'data'=>$trend_sex_nm_m,
                        'dataLabels'=> [
                            'enabled'=> true,
                            'rotation'=> 0,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            'y' => 0,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                            ],
                            [
                                'name' => 'ทารกแรกเกิดตาย(หญิง)',
                                'colorByPoint' => false,
                                // 'color'=> '#62cb31',
                                //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                                'data'=>$trend_sex_nm_f,
                                'dataLabels'=> [
                                    'enabled'=> true,
                                    'rotation'=> 0,
                                    'color'=> 'gray',
                                    'align'=> 'right',
                                    'format'=> '{point.y:.2f}',
                                    'y' => 0,
                                    'style' => [
                                        'fontSize' => '10px',
                                        'fontFamily' => 'Verdana, sans-serif'
                                    ]
                                ]
                                    ],
                                    [
                                        'name' => 'ทารกตาย(ชาย)',
                                        'colorByPoint' => false,
                                        // 'color'=> '#62cb31',
                                        //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                                        'data'=>$trend_sex_im_m,
                                        'dataLabels'=> [
                                            'enabled'=> true,
                                            'rotation'=> 0,
                                            'color'=> 'gray',
                                            'align'=> 'right',
                                            'format'=> '{point.y:.2f}',
                                            'y' => 0,
                                            'style' => [
                                                'fontSize' => '10px',
                                                'fontFamily' => 'Verdana, sans-serif'
                                            ]
                                        ]
                                            ],
                                            [
                                                'name' => 'ทารกตาย(หญิง)',
                                                'colorByPoint' => false,
                                                // 'color'=> '#62cb31',
                                                //'data' => [["2558",70.17],["2559",45.86],["2560",63.42]],
                                                'data'=>$trend_sex_im_f,
                                                'dataLabels'=> [
                                                    'enabled'=> true,
                                                    'rotation'=> 0,
                                                    'color'=> 'gray',
                                                    'align'=> 'right',
                                                    'format'=> '{point.y:.2f}',
                                                    'y' => 0,
                                                    'style' => [
                                                        'fontSize' => '10px',
                                                        'fontFamily' => 'Verdana, sans-serif'
                                                    ]
                                                ]
                                                    ]
                ]
            ]
        ]);
        ?>