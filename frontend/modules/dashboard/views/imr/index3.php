<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$this->title = "อัตราตายของทารก (อายุต่ํากว่า 1 ปี) ต่อจํานวนการเกิดมีชีพ 1,000 คน ระดับ". $scope;
$this->params['breadcrumbs'][] = $scope;
?>
<?php
$script = <<< JS
        //Datatables
         getDatatable('#datatables',);
JS;
$this->registerJs($script);
$conn = \Yii::$app->db;
$request=\yii::$app->request;
?>
<?=$this->render('//_header')?>
<div class="content animate-panel">
<div class="row">               
<div class="col-md-2 text-right">
                           <h5> เลือกปี </h5>
                        </div>
                        <div class="dropdown col-md-1">

                            <a href="#" class="dropdown-toggle btn btn-default" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
                            aria-expanded="false"> <?= $request->get('year')+543;?><span class="caret"></span> </a>
                            <?php
                            $sqlyear='select * from kpi_fiscal_year where year>=2001 order by year DESC';
                            $cmd = $conn->createCommand($sqlyear);
                            $datayear = $cmd->queryAll();
                            foreach ($datayear as $key => $value) {
                                $cyear[] = array('label'=>$value['year']+543,
                                'url'=>Url::to(['index', 'year'=>$value['year']]));
                            }
                                echo Dropdown::widget([
                                    'items' => $cyear
                                ]);
                            ?>
                        </div>
                       
                        
</div>
<hr>
<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_gauge',['gaugeData'=>$gaugeData,'plotBands'=>$plotBands])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <?=$this->render('_map3', ['geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend])?>
</div>
                    <!-- <div class="panel-footer">

                    </div> -->
                </div>
            </div>
            </div>
<div class="row">
    <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_column3',['target_value'=>$target_value,'columnData'=>$columnData])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
    </div>
<div class="row">
<div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    
    <?= $this->render('_trend3',['target_value'=>$target_value,'trendData'=>$trendData])?>
    </div>
                        <!-- <div class="panel-footer">
    
                        </div> -->
                    </div>
                </div>
</div>
<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
<?= $this->render('_datatables3',['data'=>$data,'mapUrl' => $mapUrl,'mapType' => $mapType,])?>
</div>
                </div>
            </div>
            </div>
    </div>
