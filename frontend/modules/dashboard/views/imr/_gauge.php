<?php
use miloschuman\highcharts\Highcharts;
?>



<?php
//print_r($plotBands);
echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        //'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'สถานการณ์ภาพรวม',
            // 'text' => $kpi['kpi_name'],
        ],
        'credits'=>[
                'enabled'=>false,
            ],
        'chart' => [
            'type' => 'gauge',
            'plotBackgroundColor' => null,
            'plotBackgroundImage' => null,
            'plotBorderWidth' => 0,
            'plotShadow' => false,
            //'height'=> 500,
        ],
        // 'title' => 'ร้อยละ',

        'pane' => [
            'startAngle' => -100,
            'endAngle' => 100,
            // 'background' => null,
            'background'=> [
                'backgroundColor'=> '#FFF',
                'innerRadius'=> '60%',
                'outerRadius'=> '100%',
                'shape'=> 'arc'
            ]
        ],

        // the value axis
        'yAxis' => [
            'min' => 0,
            'max' => 20,

            'minorTickInterval' => 'auto',
            'minorTickWidth' => 1,
            'minorTickLength' => null,
            'minorTickPosition' => 'inside',
            'minorTickColor' => '#666',

            'tickPixelInterval' => 20,
            'tickWidth' => 1,
            'tickPosition' => 'inside',
            'tickLength' => 30,
            // 'tickColor' => '#666',
            'tickColor'=> '#fff',
            'labels' => [
                'step' => 1,
                'rotation' => 'auto',
            ],
            'title' => [
                // 'text' => 'ร้อยละ',
            ],
            'plotBands' => $plotBands
        ],
        'tooltip' => [
            'pointFormat' => 'ร้อยละ {point.y:.2f}',
            'style' => [
                'fontSize' => '18px',
                'fontFamily' => 'Verdana, sans-serif',
            ],
        ],
        'series' => [[
            'name' => 'ร้อยละ',
            // 'data' => [(int)$chartHdc->percentage],
            'data' => [$gaugeData],
            'tooltip' => [
                'valueSuffix' => ' ',
            ],
            // dataLabels:{
            //     enabled:true,
            //     y:-40,
            //     style:{
            //       fontSize:'14px',
            //     },
            //     borderWidth:0,
            //     }
            'dataLabels' => [
                'enabled' => true,
                // 'y'=>-40,
                // 'color' => 'black',
                'format' => '{point.y:.2f}',
                'style' => [
                    'fontSize' => '16px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
                'borderWidth'=>0,
                
            ],
        ]],
    ],
]);
?>

