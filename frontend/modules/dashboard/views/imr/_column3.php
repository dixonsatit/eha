<?php
use miloschuman\highcharts\Highcharts;
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>

                    <?php 
    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'column'
                ],
                'title'=> [
                    'text'=> 'เปรียบเทียบรายพื้นที่'
                ],
                'subtitle'=> [
                  //'text'=> 'ปีปัจจุบัน'
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        //'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ],
                    ],
                    //'categories'=>['รวม','ชาย','หญิง'],
                ],
                'yAxis' => [
                    'min' => 0,
                    'title' => [
                     'text' => ''
                    ],
                    'plotLines' => [[
                        'value' =>  $target_value,
                        'color' => 'red',
                        'width' => 2,
                        // 'label' => [
                        //     'text' => 'ไม่เกินร้อยละ 20',
                        //     'align' => 'center',
                        //     'style' => [
                        //         'color' => 'gray'
                        //     ]
                        // ]
                    ]]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'headerFormat'=>'<span style="font-size:10px">{point.key}</span><table>',
                    'pointFormat'=> '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b> {point.y:.2f}%</b></td></tr>',
                    'footerFormat'=>'</table>',
                    'shared'=>true,
                    'useHTML'=>true,
                ],
                'series' => [
                    [
                        'name' => 'อัตราตายทารกต่อพัน',
                        'colorByPoint' => false,
                        // 'color'=> '#62cb31',
                        //'data' => [["มกราคม",$datamonth['rate1']],["กุมภาพันธ์",65.86],["มีนาคม",63.42],["เมษายน",47.93],["พฤษภาคม",63.98],["มิถุนายน",50.12],["กรกฎาคม",66.1],["สิงหาคม",75.95],["กันยายน",63.13],["ตุลาคม",73.2],["พฤศจิกายน",68.18],["ธันวาคม",75.26],["ทั้งปี",]],
                        'data'=>$columnData,
                        'dataLabels'=> [
                            'enabled'=> true,
                            'rotation'=> -90,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            'y' => 1,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                            ]
                            
                ]
            ]
        ]);
        ?> 