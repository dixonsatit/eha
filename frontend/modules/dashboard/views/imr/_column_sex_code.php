<?php
use miloschuman\highcharts\Highcharts;
$request=\yii::$app->request;
$year = $request->get('year')==null ? date("Y"):strlen($request->get('year'))!==4 ? date("Y") : $request->get('year'); 
$thai_year=(int)$year+543;
?>

                    <?php 
    echo Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart'=> [
                    'type'=> 'bar',
                    'height'=>2000,
                ],
                'title'=> [
                    'text'=> 'อัตราการตายของทารก (อายุต่ำกว่า 1 ปี) จำแนกรายอายุและเพศ'
                ],
                'subtitle'=> [
                  'text'=> 'ปี '.$thai_year
                ],
                'xAxis'=>[
                    'type' => 'category',
                    'labels' => [
                        //'rotation' => -45,
                        'style' => [
                            'fontSize'=>'13px',
                            'fontFamily'=>'Verdana, sans-serif'
                        ],
                      'padding'=>0,
                    ],
                    //'categories'=>['รวม','ชาย','หญิง'],
                ],
                'yAxis' => [
                    'min' => 0,
                    'max'=>100,
                    'title' => [
                     'text' => ''
                    ],
                    'plotLines' => [[
                        //'value' =>  $target_value,
                        'color' => 'red',
                        'width' => 2,
                        // 'label' => [
                        //     'text' => 'ไม่เกินร้อยละ 20',
                        //     'align' => 'center',
                        //     'style' => [
                        //         'color' => 'gray'
                        //     ]
                        // ]
                    ]]
                ],
                'legend' => [
                    'enabled' => true
                ],
                'tooltip' => [
                    'headerFormat'=>'<span style="font-size:10px">{point.key}</span><table>',
                    'pointFormat'=> '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.2f}%</b></td></tr>',
                    'footerFormat'=>'</table>',
                    'shared'=>true,
                    'useHTML'=>true,
                ],
                'series' => [
                    [
                        'name' => 'รวม',
                        'colorByPoint' => false,
                        // 'color'=> '#62cb31',
                        //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                        'data'=>$column_sex_T,
                        'dataLabels'=> [
                            'enabled'=> true,
                            //'rotation'=> -90,
                            'color'=> 'gray',
                            'align'=> 'right',
                            'format'=> '{point.y:.2f}',
                            //'y' => 1,
                            'style' => [
                                'fontSize' => '10px',
                                'fontFamily' => 'Verdana, sans-serif'
                            ]
                        ]
                            ],
                            [
                                'name' => 'ชาย',
                                'colorByPoint' => false,
                                // 'color'=> '#62cb31',
                                //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                                'data'=>$column_sex_M,
                                'dataLabels'=> [
                                    'enabled'=> true,
                                    //'rotation'=> -90,
                                    'color'=> 'gray',
                                    'align'=> 'right',
                                    'format'=> '{point.y:.2f}',
                                    //'y' => 1,
                                    'style' => [
                                        'fontSize' => '10px',
                                        'fontFamily' => 'Verdana, sans-serif'
                                    ]
                                ]
                                    ],
                                    [
                                        'name' => 'หญิง',
                                        'colorByPoint' => false,
                                        // 'color'=> '#62cb31',
                                        //'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
                                       'data'=>$column_sex_F,
                                        'dataLabels'=> [
                                            'enabled'=> true,
                                            //'rotation'=> -90,
                                            'color'=> 'gray',
                                            'align'=> 'right',
                                            'format'=> '{point.y:.2f}',
                                            //'y' => 1,
                                            'style' => [
                                                'fontSize' => '10px',
                                                'fontFamily' => 'Verdana, sans-serif'
                                            ]
                                        ]
                                            ]
                ]
            ]
        ]);
        ?> 