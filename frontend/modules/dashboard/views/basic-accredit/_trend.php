<?php
use miloschuman\highcharts\Highcharts;
?>

<?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            //'text' => $kpi['name'],
            'text'=> 'แนวโน้มรายปี'
        ],
        'subtitle' => [
            //'text'=> 'ปีงบประมาณ'
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                // 'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'max' => 100,
            'title' => [
                // 'text' => 'ดัชนีมวลกาย'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'dashStyle'=> 'Dash',
                'label' => [
                    'text' => 'ผ่านเกณฑ์ = '.(float) $kpi['target_value'],
                    'align' => 'right',
                    'style' => [
                        'color' => 'black',
                        'fontSize'=>'13px',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => false,
        ],
//        'tooltip' => [
//            'pointFormat' => 'ข้อมูล {point.y:.2f}',
//        ],
        'tooltip' => [
            'backgroundColor' =>'#ffffff',
            'borderWidth' => 1,
            'headerFormat'=> '<span style="font-size:13px;"><b>ปี {point.key}<b></span><table>',
            'pointFormat'=> '<tr><td style="color:{series.color};padding:0"><b>{series.name}: </b></td>'
                .'<td style="padding:0"><b>{point.y:.2f} </b></td></tr>',
            'footerFormat'=> '</table>',
            'shared'=> true,
            'useHTML'=> true,
        ],
        'series' => [
            [
                'name' => 'ร้อยละ ',
                'colorByPoint' => false,
                // 'color'=> '#62cb31',
                // 'data' => [["2558",50.17],["2559",65.86],["2560",70.42]],
                'data' => $trendData,
                'dataLabels' => [
                    'enabled' => true,
                    //  'rotation'=> 90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
            ],
        ],
    ],
]);
?>
