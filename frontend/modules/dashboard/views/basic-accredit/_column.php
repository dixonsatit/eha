<?php
use miloschuman\highcharts\Highcharts;
// use yii\web\JsExpression;

// print_r($columnData); die();
?>
<script src = "https://code.highcharts.com/highcharts-3d.js"></script>
<?php
// print_r($kpi['target_value']);
echo Highcharts::widget([
	'scripts' => [
		'modules/exporting',
		'themes/grid-light',
		],
	'options' => [
			'chart'=> [
					'type'=> 'column',
					
					'options3d'=> [
						'enabled'=> true,
						'alpha'=> 15,
						'beta'=> 15,
						'depth'=> 50,
						'viewDistance'=> 25
					]
					],
			'title'=> [
					'text'=> 'เปรียบเทียบรายพื้นที่'
					],
			'subtitle'=> [
					//'text'=> 'ปีงบประมาณ'
					],
			'xAxis'=>[
					'type' => 'category',
					'labels' => [
							'rotation' => -45,
							'style' => [
									'fontSize'=>'13px',
									'fontFamily'=>'Verdana, sans-serif'
									]
							]
					],
			'yAxis' => [
				'min' => 0,
				'max' => 100,
					'title' => [
						// 'text' => ''
							],
							'plotLines' => [[
								// 'value' => (int) $kpiTemplateToYear->target_value,
								// 'value' => (float) $kpi['target_value'],
								'value' => (float) 5,
								'color' => 'red',
								'width' => 2,
								'dashStyle'=> 'Dash',
								'label' => [
									'text' => 'เป้าหมาย = '.(float) $kpi['target_value'],
									'align' => 'right',
									'style' => [
										'color' => 'black',
										'fontSize' => '14px',
									],
								],
										]]
					],
			'legend' => [
					'enabled' => false
					],
			'tooltip' => [
					'pointFormat' => 'ร้อยละ {point.y:.2f}',
					],
			'series' => [
					[
							'name' => 'อปท.',
							'colorByPoint' => true,
							'colors'=>$columnData['Color'],
							// 'data' => [["ศอ.1",70.17],["ศอ.2",65.86],["ศอ.3",63.42],["ศอ.4",47.93],["ศอ.5",63.98],["ศอ.6",50.12],["ศอ.7",66.1],["ศอ.8",75.95],["ศอ.9",63.13],["ศอ.10",73.2],["ศอ.11",68.18],["ศอ.12",75.26]],
							'data'=>$columnData['Data'],
							'dataLabels'=> [
									'enabled'=> true,
									'rotation'=> -90,
									'color'=> 'gray',
									'align'=> 'right',
									'format'=> '{point.y:.2f}',
									'y' => 10,
									'style' => [
											'fontSize' => '10px',
											'fontFamily' => 'Verdana, sans-serif'
											]
									]
							]
					]
			]
	]);
?>
