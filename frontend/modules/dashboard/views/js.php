<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->registerJsFile(Url::base().'/js/datatables/js/jquery.js');
$this->registerJsFile(Url::base().'/js/datatables/js/jquery.dataTables.min.js');
$this->registerJsFile(Url::base().'/js/datatables/js/buttons.html5.min.js');
$this->registerJsFile('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js');
$this->registerJsFile(Url::base().'/js/datatables/js/buttons.print.min.js');
$this->registerJsFile('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js');
$this->registerJsFile(Url::base().'/js/datatables/js/jszip.min.js');
$this->registerJsFile(Url::base().'/js/datatables/js/pdfmake.min.js');
$this->registerJsFile(Url::base().'/js/datatables/js/vfs_fonts.js');
$this->registerJsFile(Url::base().'/js/datatables/js/datatables.bootstrap4.js');
$this->registerCssFile(Url::base().'/js/datatables/css/jquery.datatables.min.css');
$this->registerCssFile(Url::base().'/js/datatables/css/dataTables.uikit.min.css');
$this->registerJsFile(Url::base().'/js/datable/datatables.js');
?>