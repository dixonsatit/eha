<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");

$this->title = $kpi['kpi_name'].' ระดับเขตสุขภาพ';
foreach ($viewElement['breadcrumb'] as $value) {
    $this->params['breadcrumbs'][] = $value;
}
?>



<?=$this->render('//_header',['templatefile'=>"template_dashboard_mmr.png"])?>
<div class="content animate-panel">


<div class="row">
        <div class="col-lg-12">
        <div class="hpanel">
        <div class="panel-body">
        <div class="col-md-3">
           <h4> ปีงบประมาณ </h4>
        </div>
        <div class="dropdown col-md-1">
            <a href="#" class="dropdown-toggle btn btn-primary" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"> <?= ($session['year']+543);?> <span class="caret"></span> </a>
            <?php
                for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2016; $i--) {
                    $cyear[] = array('label'=>($i+543),
                    'url'=>Url::to(['','year'=>$i]));
                }
                echo Dropdown::widget([
                    'items' => $cyear
                ]);
            ?>
        </div>
        </div>
        </div>
        </div>
</div>




<div class="row">

<div class="col-lg-6">

<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    
                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData,'plotBands' =>$plotBands])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>


<div class="col-lg-6">


<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    
                    <!-- Map -->
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend])?>
                    
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>

</div>



<!-- ///////////////////////////////////////////////////////////// -->





<div class="row">

<div class="col-lg-12">

<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    <?//print_r($columnData);die();?>
                     <!-- pie -->
                     <?=$this->render('_pie', ['Typecase' => $Typecase, 'sumnum' => $sumnum])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>
</div>


<div class="row">

<div class="col-lg-12">

<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    <?//print_r($columnData);die();?>
                     <!-- pie -->
                     <?=$this->render('_columncause', ['namecause' => $namecause,
                                                        'Typecase' => $Typecase,
                                                        'numData' => $numData,
                                                        'colorBar' => $colorBar,
                                                        'sumnum' => $sumnum
                                                        ])?>
     <div class="col-md-12 text-center">
                        <span style="color: #57c466;padding-right: 10px; cursor:pointer;"> <i class="fa fa-circle"></i> สาเหตุมารดาตายโดยตรง</span> 
                        <span style="color: #427fe5;padding-right: 10px; cursor:pointer;"> <i class="fa fa-circle"></i> สาเหตุมารดาตายโดยอ้อม</span> 
                        <span style="color: #b05f00;padding-right: 10px; cursor:pointer;"> <i class="fa fa-circle"></i> ไม่ระบุสาเหตุ</span> 
    </div>        

    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>
</div>


<!-- ///////////////////////////////////////////////////////////// -->

<div class="row">

<div class="col-lg-6">

<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    
                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>
                    
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>


<div class="col-lg-6">


<div class="panel panel-primary">
    <!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
    <div class="panel-body">
    
                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>
                     
    </div>
    <!-- <div class="panel-footer">&nbsp;</div> -->
</div>

</div>

</div>

<hr>
<div class="panel panel-primary">
<div class="panel-body">
<?= $this->render('_datatables',['kpi'=>$kpi,'data'=>$data, 'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope
                                 ,'bYear'=>$bYear])?>
</div>
</div>