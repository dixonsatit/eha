<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
  //Datatables
  getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <!--<tr class="info">
    <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
  </tr>-->
  <tr class="info">
    <th class="text-center">#</th>
    <th class="text-center"><?=$scope?></th>
    <th class="text-center">จำนวนเด็กเกิด (A)</th>
    <th class="text-center">มารดาตาย (B)</th>
    <th class="text-center">อัตรามารดาตายต่อการเกิดมีชีพแสนคน (C)</th>
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
    foreach ($data as $k => $v) {
      $target += $v['target'];
      $result += $v['result'];
      echo '<tr>';
      echo '<td>'. ($k + 1) . '</td>';
      
      if($mapType!=='"cw"') {
        echo '<td class="text-left">' .Html::a($v['areaname'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode'], ['class' => 'success'],'_blank') . '</td>';
      }
      else {
        echo '<td class="text-left">' .$v['areaname']. '</td>';
      }
      //
      echo '<td class="text-right">'.number_format($v['target'],0).'</td>
          <td class="text-right">'.number_format($v['result'],0).'</td>
          <td class="text-right">'.number_format($v['result']/$v['target']*100000,2).'</td>
        </tr>';
      //
    } //end foreach
    //
    echo '<tr>';
    echo '<td>'. ($k + 1) . '</td>';
    echo '<td class="text-left">รวมทั้งหมด</td>';
    echo '<td class="text-right">'.number_format($target,0).'</td>
      <td class="text-right">'.number_format($result,0).'</td>
      <td class="text-right">'.number_format($result/$target*100000,2).'</td>
    </tr>';
    //
} //end if  


// foreach($data as $k => $v){

//     // $mapType!=='ap'?window.open($mapUrl+'&'+$mapType,'_self'):null;

// $v['areacode'] == 'TOTAL' ? $aa=$v['areaname_new'] : $aa='<a target="_blank" href="changwat/?year='.$_GET['year'].'&kid='.$kpi['kpi_template_id'].'&rg='.$v['areacode'].'">'.$v['areaname_new'].'</a>' ;




// echo '<tr>
//     <td class="text-left">'.($k+1).'</td>
//     <td class="text-left">'.$aa.'</td>
//     <td class="text-right">'.number_format($v['target'],2).'</td>
//     <td class="text-right">'.number_format($v['result'],2).'</td>
//     <td class="text-right">'.number_format($v['total_ratio'],2).'</td>
//   </tr>';
// }
?>

</tbody>
</table>


</div>
</div>

<div class="well">

<b>วิธีคำนวณ C = (B / A) X 100,000</b><br/><br/>
C = อัตรามารดาตายต่อการเกิดมีชีพแสนคน<br/>
B = การตายมารดาในแต่ปีงบประมาณ (คน)<br/>
A = จำนวนการคลอดมีชีพ (คน) 

<br/><br/>ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจากสำนักส่งเสริมสุขภาพ)


</div>