<?php
use miloschuman\highcharts\Highcharts;
// print_r($pieData);
// die();
// echo json_encode($typeCase);
echo Highcharts::widget([
    'options' => [
        
        'chart'=>[
            'plotBackgroundColor'=> null,
            'plotBorderWidth'=> null,
            'plotShadow'=> false,
            'type'=> 'pie',
        ],
        'title' => ['text' => 'จำนวนมารดาตาย '.$sumnum.' ราย (ประเภทสาเหตุ)'],
        'tooltip' => [
            'pointFormat' => 'จำนวน {point.y:.0f} ราย',
            // 'pointFormat' => 'ร้อยละ {point.y:.2f} ราย',
            'style' => [
                'fontSize' => '16px',
                'fontFamily' => 'Verdana, sans-serif',
            ],
        ],
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
                // 'colors' => ['#1a1aff','#ff3333','#00b300','#ff6600'],
                'allowPointSelect'=>true,
                'dataLabels'=>[
                    'enable'=>true,
                    'format' => '{point.name}<br>จำนวน {point.y:.0f} ราย',
                    // 'format' => '{point.name}<br>{point.y:.0f} ราย',
                    'style' => [
                        'fontSize' => '12px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
                'showInLegend' => true,
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => ' ราย',
                'data' => $Typecase,
            ] // new closing bracket
        ],
        
    ],
]);
