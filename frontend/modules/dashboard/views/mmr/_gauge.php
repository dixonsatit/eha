<?php
use miloschuman\highcharts\Highcharts;
if($gaugeData >50){

    $scale = number_format($gaugeData+10,0);
}else{

    $scale = 50;
}
echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        //'themes/grid-light',
    ],
    'options' => [
        'title' => [
          'text'=> 'สถานการณ์ภาพรวม',
        ],
        'credits'=>[
                'enabled'=>false,
            ],
        'chart' => [
            'type' => 'gauge',
            'plotBackgroundColor' => null,
            'plotBackgroundImage' => null,
            'plotBorderWidth' => 0,
            'plotShadow' => false,
            //'height'=> 230,
        ],
        // 'title' => 'ร้อยละ',

        'pane' => [
            'startAngle' => -100,
            'endAngle' => 100,
            // 'background' => null,
            'background'=> [
                'backgroundColor'=> '#ddd',
                'innerRadius'=> '20%',
                'outerRadius'=> '60%',
                'shape'=> 'arc',
                'borderWidth' => 0,
            ]
        ],

        // the value axis
        'yAxis' => [
            'min' => 0,
            'max' => $scale,

            'minorTickInterval' => 10,
            'minorTickWidth' => 1,
            'minorTickLength' => 13,
            'minorTickPosition' => 'inside',
            'minorTickColor' => '#666',

            'tickPixelInterval' => 20,
            'tickWidth' => 0,
            'tickPosition' => 'inside',
            'tickLength' => 10,
            // 'tickColor' => '#666',
            'tickColor'=> '#fff',
            'labels' => [
                'step' => 4,
                'rotation' => 'auto',
                'style' => [
                    'fontSize' => '14px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
            'title' => [
                // 'text' => 'ร้อยละ',
            ],
            'plotBands' => $plotBands
        ],
        'tooltip' => [
            'enabled' => false,
        ],
//        'tooltip' => [
//            'pointFormat' => 'ร้อยละ {point.y:.2f}',
//            'style' => [
//                'fontSize' => '20px',
//                'fontFamily' => 'Verdana, sans-serif',
//            ],
//        ],
        'series' => [[
            'name' => 'ร้อยละ',
            // 'data' => [(int)$chartHdc->percentage],
            'data' => [$gaugeData],

            'dataLabels' => [
                'enabled' => true,
                // 'y'=>-40,
                'color' => '#444',
                'format' => '{point.y:.2f}',
                'style' => [
                    'fontSize' => '32px',
                    'fontFamily' => 'Verdana, sans-serif',
                    'textShadow' => false,
                ],
                'borderWidth'=>0,
                
            ],
        ]],
    ],
]);
?>


