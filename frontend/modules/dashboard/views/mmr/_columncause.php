<?php
use miloschuman\highcharts\Highcharts;
?>

<?php
// echo json_encode($numData);
// print_r($nametype2); die();
// print_r($kpi['target_value']);  die();
echo Highcharts::widget([

    'setupOptions' => [
        'lang' => [
            'decimalPoint' => '.',
            'thousandsSep' => ',',
        ]
    ],    
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'chart' => [
            'type' => 'bar',
        ],
        'title' => ['text' => 'จำนวนมารดาตาย '.$sumnum.' ราย (สาเหตุ)'],
        'xAxis'=> [
            'categories'=> $namecause
        ],
        'colors'=> $colorBar,
        'legend'=> [
            'enabled'=> false,
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
                'text' => 'จำนวน (คน)'
            ],
        ],
        'tooltip' => [
            'headerFormat' => '<span style="font-size:13px;"><b>ตายด้วยสาเหตุ : {point.key}<b></span><table>',
            'pointFormat' => '<tr><td style="color:{series.color};padding:0"></td>'
            . '<td style="padding:0; font-size:14px;"><b>จำนวน {point.y:,.f} ราย</b></td></tr>',
            'footerFormat' => '</table>',
            'shared' => true,
            'useHTML' => true,
        ],
        'plotOptions'=> [
            'series'=> [
            'colorByPoint'=> true,
            ]
        ],
        
        'series'=> [[
            'data'=> $numData,
            'dataLabels' => [
                'enabled' => true,
                // 'rotation' => 0,
                'color' => 'gray',
                'align' => 'right',
                'format' => '{point.y:,.0f}',
                // 'y' => 10,
                // 'x' => -20,
                'style' => [
                    'fontSize' => '10px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ]]
    ]
]);
?>