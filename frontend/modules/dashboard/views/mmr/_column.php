<?php
use miloschuman\highcharts\Highcharts;
// use yii\web\JsExpression;

if($gaugeData >50){

    $scale = number_format($columnData['Data']+10,0);
}else{

    $scale = 50;
}
?>
<script src = "https://code.highcharts.com/highcharts-3d.js"></script>
<?php
// print_r($kpi['target_value']);
echo Highcharts::widget([
		'scripts' => [
			'modules/exporting',
			'themes/grid-light',
			],
		'options' => [
				'chart'=> [
						'type'=> 'column',
						
						'options3d'=> [
							'enabled'=> true,
							'alpha'=> 15,
							'beta'=> 15,
							'depth'=> 50,
							'viewDistance'=> 25
						]
						],
				'title'=> [
						'text'=> 'เปรียบเทียบรายพื้นที่'
						],
				'subtitle'=> [
						//'text'=> 'ปีงบประมาณ'
						],
				'xAxis'=>[
						'type' => 'category',
						'labels' => [
								'rotation' => -45,
								'style' => [
										'fontSize'=>'13px',
										'fontFamily'=>'Verdana, sans-serif'
										]
								]
						],
				'yAxis' => [
					'min' => 0,
					'max' => $scale,
						'title' => [
							// 'text' => ''
								],
								'plotLines' => [[
									// 'value' => (int) $kpiTemplateToYear->target_value,
									'value' => (float) $kpi['target_value'],
									'color' => 'red',
									'width' => 2,
									'dashStyle'=> 'Dash',
									'label' => [
										'text' => 'ผ่านเกณฑ์ = '.(float) $kpi['target_value'],
										'align' => 'right',
										'style' => [
											'color' => 'black',
											'fontSize' => '14px',
										],
									],
											]]
						],
				'legend' => [
						'enabled' => false
						],
				'tooltip' => [
						'pointFormat' => 'ร้อยละ {point.y:.2f}',
						],
				'series' => [
						[
								// 'name' => 'Population',
								'colorByPoint' => true,
								'colors'=>$columnData['Color'],
								// 'data' => [["เขต 1",70.17],["เขต 2",65.86],["เขต 3",63.42],["เขต 4",47.93],["เขต 5",63.98],["เขต 6",50.12],["เขต 7",66.1],["เขต 8",75.95],["เขต 9",63.13],["เขต 10",73.2],["เขต 11",68.18],["เขต 12",75.26]],
								'data'=>$columnData['Data'],
								'dataLabels'=> [
										'enabled'=> true,
										'rotation'=> -90,
										'color'=> 'gray',
										'align'=> 'right',
										'format'=> '{point.y:.2f}',
										'y' => 10,
										'style' => [
												'fontSize' => '10px',
												'fontFamily' => 'Verdana, sans-serif'
												]
										]
								]
						]
				]
		]);
?>
