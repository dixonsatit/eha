<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
	// Datatables
	getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>
<div class="col-lg-12 text-center" style="font-size: 16px; color: #333"><b>ตารางข้อมูลรายพื้นที่</b></div>
<table id="datatables" class= "table table-striped table-hover table-bordered table-responsive" width="100%">
	<thead>
		<tr class="info">
			<th rowspan="2">&nbsp;</th>
			<th rowspan="2" class="text-center"><?=$scope?></th>   
			<th rowspan="2" class="text-center">B</th>
			<th rowspan="2" class="text-center">A</th>
			<th rowspan="2" class="text-center">ร้อยละ</th>
			<th colspan="3" class="text-center">ตค.</th>
			<th colspan="3" class="text-center">พย.</th>
			<th colspan="3" class="text-center">ธค.</th>
			<th colspan="3" class="text-center">มค.</th>
			<th colspan="3" class="text-center">กพ.</th>
			<th colspan="3" class="text-center">มีค.</th>
			<th colspan="3" class="text-center">เมย.</th>
			<th colspan="3" class="text-center">พค.</th>
			<th colspan="3" class="text-center">มิย.</th>
			<th colspan="3" class="text-center">กค.</th>
			<th colspan="3" class="text-center">สค.</th>
			<th colspan="3" class="text-center">กย.</th>
		</tr>
		<tr class="info">
		<!--ภาพรวม-->			
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
			<th class="text-center">B</th>
			<th class="text-center">A</th>
			<th class="text-center">ร้อยละ</th>
		</tr>
	</thead>
	<tfoot>
		<tr class="info">
			<td colspan="9">ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)</td>
		</tr>
	</tfoot>
	<tbody>
		<?php
		if ($data) {
			foreach ($data as $key => $v) {
				echo '<tr>';
				echo '<td class="text-center">&nbsp;</td>';
				//ภาพรวม
				if ($v['areacode'] !== 'TOTAL') {
					if ($scope == 'ตำบล') {
						echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
					} else {
						echo '<td class="text-center">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode']) . '</td>';
					}					
				} else {
					//
					$report_date = $v['report_date'];
					//
					echo '<td class="text-center">' . $v['areaname_new'] . '</td>';
				} //end if ($v['areacode'] !== 'TOTAL') 
				//
				echo '<td class="text-center">'.number_format($v['target']).'</td>';
				echo '<td class="text-center">'.number_format($v['result']).'</td>';
				echo '<td class="text-center">'.number_format($v['total_ratio'],2).'</td>';					
				echo '<td class="text-center">'.number_format($v['targetm1']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm1']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom1'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm2']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm2']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom2'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm3']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm3']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom3'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm4']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm4']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom4'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm5']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm5']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom5'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm6']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm6']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom6'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm7']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm7']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom7'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm8']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm8']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom8'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm9']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm9']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom9'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm10']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm10']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom10'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm11']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm11']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom11'],2).'</td>';
				echo '<td class="text-center">'.number_format($v['targetm12']).'</td>';
				echo '<td class="text-center">'.number_format($v['resultm12']).'</td>';
				echo '<td class="text-center">'.number_format($v['ratiom12'],2).'</td>';
				echo '</tr>';

			} //end foreach
		} //end if  
		?>
	</tbody>
</table>
<div class="well">
	<b>หมายเหตุ:</b><br>
		B = จำนวนหญิงอายุน้อยกว่า 20 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR โดยมีเงื่อนไขดังต่อไปนี้ <br>	
		&nbsp;- การตั้งครรภ์สิ้นสุดลง(คลอด/แท้งบุตร)ในช่วงเวลาที่กำหนด<br>
		&nbsp;- อายุน้อยกว่า 20 ปี ณ วันที่การตั้งครรภ์สิ้นสุด BDATE(LABOR) - BIRTH(PERSON) ปัดเศษลง < 20 ปี นับเป็นจำนวนผู้มารับบริการ ( ตัดความซ้ำซ้อนด้วย CID + BDATE)<br>
		<br>
		A = จำนวนหญิงอายุน้อยกว่า 20 ปี ที่มารับบริการด้วยการคลอด/แท้งบุตร จากแฟ้ม LABOR โดยมีเงื่อนไขดังต่อไปนี้ <br>	
		&nbsp;- การตั้งครรภ์สิ้นสุดลง(คลอด/แท้งบุตร)ในช่วงเวลาที่กำหนด<br>
		&nbsp;- อายุน้อยกว่า 20 ปี ณ วันที่การตั้งครรภ์สิ้นสุด BDATE(LABOR) - BIRTH(PERSON) ปัดเศษลง < 20 ปี<br>
		&nbsp;- เป็นการตั้งครรภ์ครั้งที่ 2 ขึ้นไป GRAVIDA(LABOR) >= 2 นับเป็นจำนวนผู้มารับบริการ ( ตัดความซ้ำซ้อนด้วย CID + BDATE)<br>
	
		<?=DashboardServices::getReportdate($report_date);?>
	</p>
</div>

