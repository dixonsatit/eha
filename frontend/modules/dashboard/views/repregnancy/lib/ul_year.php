<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\themes\homer\FlotAsset;
use frontend\modules\kpis\models\KpiFiscalYear;
use yii\bootstrap\Dropdown;
?>
<div class="row visible-xs">
	<div class="col-xs-6"><h5 class="pull-right">ปีงบประมาณ<h5></div>
		<div class="col-xs-6">
			<div class="btn-group">
				<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?= $kpi['year'] + 543; ?> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<?php for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2016; $i--) { ?>
					<li><?= Html::a($i + 543, ['index', 'year' => $i]) ?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	<hr class="col-xs-12">
</div>
<nav class="navbar navbar-default hidden-xs">
	<div class="container-fluid">
		<div class="navbar-header">                
			<a class="navbar-brand" href="#">ปีงบประมาณ</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="<?= $v["kpi_system_routing"] ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<?= $kpi['year'] + 543; ?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<?php for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2016; $i--) { ?>
						<li><?= Html::a($i + 543, ['index', 'year' => $i]) ?></li>
						<?php } ?>
					</ul>
				</li>
			</ul>                
		</div>
	</div>
</nav>