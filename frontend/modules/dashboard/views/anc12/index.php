<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

$session = Yii::$app->session;
$request = Yii::$app->request;

$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");

$this->title = $kpi['kpi_name'].' ระดับเขตสุขภาพ';
foreach ($viewElement['breadcrumb'] as $value) {
    $this->params['breadcrumbs'][] = $value;
}
?>


<?=$this->render('//_header',['templatefile'=>""])?>

<div class="content animate-panel">


<div class="row">
        <div class="col-lg-12">
        <div class="hpanel">
        <div class="panel-body">
        <div class="col-md-3">
           <h4> ปีงบประมาณ </h4>
        </div>
        <div class="dropdown col-md-1">
            <a href="#" class="dropdown-toggle btn btn-primary" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
            aria-expanded="false"> <?= ($session['year']+543);?> <span class="caret"></span> </a>
            <?php
                for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2015; $i--) {
                    $cyear[] = array('label'=>($i+543),
                    'url'=>Url::to(['','year'=>$i]));
                }
                echo Dropdown::widget([
                    'items' => $cyear
                ]);
            ?>
        </div>
        </div>
        </div>
        </div>
</div>



<div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData,'plotBands'=>$plotBands])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Map -->
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend])?>


                    </div>

                </div>
            </div>

</div>


    <div class="row">
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">
                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>

                    </div>

                </div>
            </div>

    </div>

    <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <?=$this->render('_datatables', ['kpi' => $kpi, 'data' => $data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>

                    </div>
                    
                </div>
            </div>
    </div>

    <!-- <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">
                        <?php //echo $this->render('_intervention', ['year' => '2018','region'=>'05','kpi'=>(int)$kpi['kpi_code']])?>
                    </div>
                </div>
            </div>
    </div> -->


</div>
