<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS

// Datatables
getDatatable('#datatables',0)

JS;
$this->registerJs($script);
?>

<h4><center>ตารางข้อมูลรายพื้นที่</center></h4>
    <table id="datatables"  class="table table-striped table-hover table-bordered"  width="100%">
    <thead>

    <tr class="info">
        <th>#</th>
        <th ><?=$scope?></th>
        <th class="text-center">เป้าหมาย</th>
        <th class="text-center">ผลงาน</th>
        <th class="text-center">ร้อยละ</th>
    </tr>
    </thead>
    <tbody>

<?php
$request = Yii::$app->request;
$ap = null;
$ap = $request->get('ap');
//
if ($data) {
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td>' . ($k + 1) . '</td>';
        if ($v['areacode'] !== 'TOTAL') {
            echo '<td class="text-left">' ;
            if ($ap==null) {
                echo Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' .$v['areacode']) ;
            } else {
                echo $v['areaname_new'];
            }
            echo  '</td>';
        } else {
            //
            $report_date = $v['report_date'];
            //
            echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
        } 
        //
        echo '<td class="text-center">' . number_format($v['target']) . '</td>';
        echo '<td class="text-center">' . number_format($v['result']) . '</td>';
        echo '<td class="text-center">' . number_format($v['total_ratio'], 2) . '</td>';
        echo '</tr>';
        //
    } //end foreach
} //end if  
?>

    </tbody>
    <tfoot>
        </tr>
        <tr >
        <td colspan=5 class="info"><p class="font-weight-bold"> หมายเหตุ:  </br>
        B หมายถึง จำนวนหญิงไทยในเขตรับผิดชอบที่สิ้นสุดการตั้งครรภ์ทั้งหมด (ฐานข้อมูล 43 แฟ้ม) LABOR	</br>
        A หมายถึง จำนวนหญิงตาม B ที่ฝากครรภ์ครั้งแรกเมื่ออายุครรภ์ <= 12 สัปดาห์ (ข้อมูลจากสมุดสีชมพูบันทึกลงใน 43 แฟ้ม) ANC
        </p>
        ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)

        <?=DashboardServices::getReportdate($report_date);?>

        </td>
        </tr>
    </tfoot>
</table>




