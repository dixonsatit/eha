<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
    // Datatables
    getDatatable('#datatables',0)
JS;
$this->registerJs($script);
?>

<div class="text-center" style="font-size: 16px; font-weight: bold;color: black; padding-bottom: 5px;">ตารางข้อมูลรายพื้นที่</div>
<table id="datatables" class="table table-striped table-hover table-bordered" width="100%">
    <thead>
      <tr class="info" style="padding:0px;">
        <th class="text-center" rowspan="2">ลำดับ</th>
        <th class="text-center" rowspan="2"><?= $tableAreaText ?></th>
        <th class="text-center" colspan="3">เทอม 2</th>
        <th class="text-center" colspan="3">เทอม 1</th>
        <th class="text-center" colspan="3">รวม</th>
      </tr>
      <tr class="info">
        <!--<th>No.</th>-->
        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
      </tr>
    </thead>
    <tbody>       
    <?php
    $report_date = null;
    if($data!=NULL){
        foreach ($data as $key=>$value) {
            echo '<tr>';
            echo '<td class="text-right">'.($key+1).'</td>';
            // 
            if ($value['areacode'] !== 'TOTAL') {
                $report_date = ($value['report_date']>$report_date)? $value['report_date']:$report_date;
                if (json_decode($mapType)!=='tb') {
                    echo '<td class="text-left">' . Html::a($value['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $value['areacode'], ['class' => 'success'],'_blank') . '</td>';
                }
                else {
                    echo '<td class="text-left">' . $value['areaname_new'] . '</td>';
                }
            } else {
                echo '<td class="text-left"><b>' . $value['areaname_new'] . '</b></td>';
            }
            // 
            //echo '<td class="text-left">'.$value['areaname'].'</td>';
            echo '<td class="text-right">'.number_format($value['targetq1']).'</td>';
            echo '<td class="text-right">'.number_format($value['resultq1']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratioq1'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['targetq2']).'</td>';
            echo '<td class="text-right">'.number_format($value['resultq2']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['ratioq2'],2).'</td>';
            echo '<td class="text-right">'.number_format($value['total_target']).'</td>';
            echo '<td class="text-right">'.number_format($value['total_result']).'</td>';
            echo '<td class="text-right" style="background-color:#ccffcc;">'.number_format($value['total_ratio'],2).'</td>';
            echo '</tr>';
        }
    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="11" class="info">
                <p>
                    <b>ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)
                </p>

                <p>
                    <b>หมายเหตุ:</b><br>
                    B หมายถึง จำนวนวัยรุ่นอายุ 15-18 ปีที่ชั่งนำหนักวัดส่วนสูง<br>
                    A หมายถึง จำนวนวัยรุ่นอายุ 15-18 ปี สูงดีสมส่วน
                </p>
                
                <?= ($report_date)? DashboardServices::getReportdate($report_date):'' ?>
                
            </td>
        </tr>
    </tfoot>
  </table>
<!--<p>
    <b>ที่มา: DOH Dashboard กรมอนามัย </b>(ข้อมูลจาก HDC)
</p>
<p>
    <b>
    B หมายถึง จำนวนเด็กวัยเรียน 6-14 ปีที่ชั่งนำหนักวัดส่วนสูง<br>
    A หมายถึง จำนวนเด็กวัยเรียน สูงดีสมส่วน
    </b>
</p>-->

