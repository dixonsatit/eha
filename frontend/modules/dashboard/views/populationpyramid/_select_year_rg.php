<?php

use yii\helpers\Html;

//use yii\helpers\Url;
//use common\themes\homer\FlotAsset;
//use frontend\modules\kpis\models\KpiFiscalYear;
//use yii\bootstrap\Dropdown;
?>
<div class="row visible-xs visible-sm">

    <div class="col-sm-3 col-xs-4">
        <div class="btn-group">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ปีงบประมาณ <?= $year + 543; ?> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2015; $i--) { ?>
                    <li><?= Html::a($i + 543, ['index', 'year' => $i]) ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    
    <div class="col-xs-2">
        <div class="btn-group">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $rgName ?><span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php foreach ($regionList as $v) { ?>เ
                    <li><?= Html::a($v['region_name'],['region', 'year' => $year,'rg'=>$v['region_code']]) ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    
    <div class="col-xs-3">
        <div class="btn-group">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            รวมทั้งหมด<span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <?php foreach ($changwatList as $v) { ?>
                    <li><?= Html::a($v['changwatname'],['changwat', 'year' => $year,'cw'=>$v['changwatcode']]) ?></li>
                <?php } ?>
                <li><?= Html::a('รวมทั้งหมด',['region','year' => $year,'rg'=>$rg]) ?></li>
            </ul>
        </div>
    </div>
    
    <hr>
</div>

<nav class="navbar navbar-default hidden-sm hidden-xs">
    <div class="container-fluid">

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        ปีงบประมาณ <?= $year + 543 ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2015; $i--) { ?>
                            <li><?= Html::a($i + 543, ['index', 'year' => $i]) ?></li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <?= $rgName ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach ($regionList as $v) { ?>
                            <li><?= Html::a($v['region_name'],['region', 'year' => $year,'rg'=>$v['region_code']]) ?></li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    รวมทั้งหมด<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach ($changwatList as $v) { ?>
                            <li><?= Html::a($v['changwatname'],['changwat', 'year' => $year,'cw'=>$v['changwatcode']]) ?></li>
                        <?php } ?>
                        <li><?= Html::a('รวมทั้งหมด',['changwat', 'year' => $year,'rg'=>$rg]) ?></li>
                    </ul>
                </li>
            </ul>
            
        </div>
        

    </div>

</nav>