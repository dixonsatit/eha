<?php
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS

$(document).ready(function() {
    var table = $('#datatables').DataTable( {
        ordering: false,
        scrollX:        false,
        scrollCollapse: false,
        paging:         false,
        searching:      false,
        info: false,
        dom: 'Bfrtip',
        buttons: [
            { extend: 'excelHtml5',text: 'ส่งออก Excel', footer: true },
            { extend: 'csvHtml5',text: 'ส่งออก CSV', footer: true },
            
        ]
    } );
        
} );
        
JS;
$this->registerJs($script);
?>
<div class="text-center" style="font-size: 16px; font-weight: bold;color: black; padding-bottom: 5px;"><?= $title ?></div>
<table id="datatables" class="table table-striped table-hover table-bordered" width="100%">
    <thead>
        <tr class="info" style="padding:0px;">
            <!--<th class="text-center">ลำดับ</th>-->
            <th class="text-center">ช่วงอายุ</th>
            <th class="text-center">ชาย</th>
            <th class="text-center">หญิง</th>
            <th class="text-center">รวม</th>
        </tr>
    </thead>
    <tbody>       
    <?php
    //print_r($data);
    if($data!=NULL){
    foreach ($data as $v) {
        echo '<tr>';
        echo '<td class="text-center">'.$v['groupname'].'</td>';
        echo '<td class="text-right">'.number_format($v['male']).'</td>';
        echo '<td class="text-right">'.number_format($v['female']).'</td>';
        echo '<td class="text-right">'.number_format($v['total']).'</td>';
        echo '</tr>';
    }
    echo '<tr>';
    echo '<td class="text-center font-bold">รวม</td>';
    echo '<td class="text-right font-bold">'.number_format($total['male']).'</td>';
    echo '<td class="text-right font-bold">'.number_format($total['female']).'</td>';
    echo '<td class="text-right font-bold">'.number_format($total['total']).'</td>';
    echo '</tr>';

    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="11" class="info">
                
                    <b>ที่มา: DOH Dashboard กรมอนามัย </b>(ข้อมูลจาก HDC)
                
                <?= ($total['report_date'])? DashboardServices::getReportdate($total['report_date']):'' ?>
            </td>
        </tr>
    </tfoot>
  </table>
<!--<p>
    <b>ที่มา: DOH Dashboard กรมอนามัย </b>(ข้อมูลจาก HDC)
</p>
<p>
    <b>
    B หมายถึง จำนวนเด็กวัยเรียน 6-14 ปีที่ชั่งนำหนักวัดส่วนสูง<br>
    A หมายถึง จำนวนเด็กวัยเรียน สูงดีสมส่วน
    </b>
</p>-->

