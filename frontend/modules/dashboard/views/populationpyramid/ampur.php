<?php

$this->title = 'ปิรามิดประชากร '.$viewElement['areaName'].' '.$viewElement['yearName'];
foreach ($viewElement['breadcrumb'] as $value) {
    $this->params['breadcrumbs'][] = $value;
}

?>

<?= $this->render('//_header',['templatefile'=>'']) ?>

<div class="content animate-panel">
    <?= $this->render('_select_year_ap', 
        [
           'year' => $year,
           'rg' => $viewElement['rg'],
           'cw' => $viewElement['cw'],
           'ap' => $ap,
           'regionList' => $regionList,
           'changwatList' => $changwatList,
           'ampurList' => $ampurList,
           'tambonList' => $tambonList,
           'rgName' => 'ศูนย์อนามัยที่ '.(int)$viewElement['rg'],
           'cwName' => $viewElement['cwName'],
           'apName' => $viewElement['areaName'],
        ]);
    ?>

    <div class="row">
        <!-- Gauge Chart -->
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
<?php if($data){ ?>
                    <!-- Chart -->
                    <div class="col-lg-6 col-md-12">
                    
                    <?= $this->render('_pyramid', 
                    [
                        'pyramidX' => $pyramidX,
                        'pyramidM' => $pyramidM,
                        'pyramidF' => $pyramidF,
                    ]);
                    ?>
                    </div>
                    <!-- End Chart -->
                    
                    <!-- Data Table -->
                    <div class="col-lg-5 col-md-12 col-lg-offset-1">
                    
                    <?= $this->render('_datatables', 
                        [
                            'data' => $data,
                            'total' => $total,
                            'title' => $this->title,
                        ]);
                    ?>
                    </div>
                    <!-- End Data Table -->
<?php }else{ ?>
                    <div class="alert alert-danger">
                        <strong>ผลการค้นหาข้อมูล!</strong> ไม่พบรายการข้อมูลที่ตรงกับเงื่อนที่ค้นหา
                    </div>
<?php } ?>
                </div>
            </div>

        </div>

    </div>    <!-- End row -->
    
</div> <!-- End animate-panel -->