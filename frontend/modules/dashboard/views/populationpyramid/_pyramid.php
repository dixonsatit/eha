<?php

$this->registerJsFile('https://code.highcharts.com/highcharts.js');
$this->registerJsFile('https://code.highcharts.com/modules/exporting.js');
$bg='http://localhost/dohdashboard/frontend/web/images/bg_pyramid.jpg';

$js = <<<JS

var categories = [$pyramidX];
        
Highcharts.chart('pyramid', {
  chart: {
    type: 'bar',
    plotBackgroundImage:'$bg',
  },
  title: {
    text: 'ปิรามิดประชากร'
  },
  
  xAxis: [{
    categories: categories,
    reversed: false,
    labels: {
      step: 1
    }
  }, { // mirror axis on right side
    opposite: true,
    reversed: false,
    categories: categories,
    linkedTo: 0,
    labels: {
      step: 1
    }
  }],
  yAxis: {
    title: {
      text: 'ร้อยละของประชากร'
    },
    labels: {
      formatter: function () {
        return Math.abs(this.value);
      }
    }
  },

  plotOptions: {
    series: {
      stacking: 'normal'
    }
  },

  tooltip: {
    formatter: function () {
      return '<b>' + this.series.name + ', อายุ ' + this.point.category + '</b><br/>' +
        'ร้อยละ: ' + Highcharts.numberFormat(Math.abs(this.point.y), 2);
    }
  },

  series: [{
    name: 'ชาย',
    color: '#7CB5EC',
    data: [$pyramidM]
  }, {
    name: 'หญิง',
    color: '#AA4643',
    data: [$pyramidF]
  }]
});
JS;
$this->registerJs($js);


?>

<div width="100%" style="height:500px;" id="pyramid"></div>
