<?php
use miloschuman\highcharts\Highcharts;
?>

<?php
echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'แนวโน้มการถ่ายทอดเชื้อเอชไอวีแม่สู่ลูก รายปี',
            // 'text' => $kpi['kpi_name'],
        ],
        'subtitle' => [
            //'text'=> 'ปีงบประมาณ'
        ],
        'xAxis' => [
            'type' => 'category',
            'labels' => [
                // 'rotation' => -45,
                'style' => [
                    'fontSize' => '13px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
        ],
        'yAxis' => [
            'min' => 0,
            'title' => [
                // 'text' => 'ดัชนีมวลกาย'
            ],
            'plotLines' => [[
                // 'value' => (int) $kpiTemplateToYear->target_value,
                'value' => (float) $kpi['target_value'],
                'color' => 'red',
                'width' => 2,
                'label' => [
                    // 'text' => 'โลกร้อน 555',
                    'align' => 'right',
                    'style' => [
                        'color' => 'blue',
                    ],
                ],
            ]],
        ],
        'legend' => [
            'enabled' => false,
        ],
        'tooltip' => [
            'pointFormat' => 'ข้อมูล {point.y:.2f}',
        ],
        'series' => [
            [
                'name' => 'Population',
                'colorByPoint' => false,
                // 'color'=> '#62cb31',
                // 'data' => [["2558",50.17],["2559",65.86],["2560",70.42]],
                'data' => $trendData,
                'dataLabels' => [
                    'enabled' => true,
                    //  'rotation'=> 90,
                    'color' => 'gray',
                    'align' => 'right',
                    'format' => '{point.y:.2f}',
                    'y' => 10,
                    'style' => [
                        'fontSize' => '12px',
                        'fontFamily' => 'Verdana, sans-serif',
                    ],
                ],
            ],
        ],
    ],
]);
?>
