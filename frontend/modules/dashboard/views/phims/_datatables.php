<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$request = Yii::$app->request;

$script = <<< JS
        //Datatables
        getDatatable('#datatables',0);
JS;
$this->registerJs($script);
?>


<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

<table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
<thead>
  <!--<tr class="info">
    <th colspan="5"><h5><?=$kpi['kpi_name'];?></h5></th>
  </tr>-->
  <tr class="info">
    <th class="text-center" rowspan="3">#</th>
    <th class="text-center" rowspan="3"><?=$scope?></th>
    <th class="text-center" rowspan="3">อัตราการถ่ายทอดเชื้อ HIV จากแม่สู่ลูก</th>
    <!-- <th class="text-center" rowspan="3">อัตราการติดเชื้อ HIV ต่อแสนการเกิดมีชีพ</th> -->
    <th class="text-center" colspan="7">ร้อยละของหญิงตั้งครรภ์</th>
   
  </tr>
  <tr class="info">
    <th class="text-center" rowspan="2">ฝากครรภ์ (อย่างน้อย 1 ครั้ง)</th>
    <th class="text-center" colspan="3">เอชไอวี HIV</th>
    <th class="text-center" colspan="3">ซิฟิลิส Syphilis</th>
    <!-- <th class="text-center" colspan="2">การตรวจเลือดพร้อมคู่</th> -->
   
  </tr>
  <tr class="info">
    <th class="text-center">รับรู้สถานะการติดเชื้อ</th>
    <th class="text-center">ได้รับการวินิจฉัยว่าติดเชื้อ</th>
    <th class="text-center">ได้รับยาต้าน</th>
    <th class="text-center">ได้รับการตรวจ</th>
    <th class="text-center">ได้รับการวินิจฉัยว่าติดเชื้อ</th>
    <th class="text-center">ได้รับการรักษา</th>
    <!-- <th class="text-center">ก่อน</th>
    <th class="text-center">หลัง</th> -->
  </tr>
</thead>
<tbody>

<?php 
if ($data) {
  // print_r($data); die();
    foreach ($data as $k => $v) {
      // $target += $v['target'];
      // $result += $v['result'];

      $report_date=$v['report_date'];
      $per_BabyPos = @(float)($v['BabyPos'] / $v['BabyPCR2']) * 100;
      $per_Deli = @(float)($v['DeliANC']/$v['DeliTotal']) *100;

      $per_DeliKnow = @(float)(($v['DeliANCPositive']+$v['DeliANCNegative']+$v['DeliNoANCPositive']+$v['DeliNoANCNegative']) / $v['DeliTotal']) *100;
      $per_DeliHIV = @(float)($v['DeliPosTotal'] / ($v['DeliANCPositive']+$v['DeliANCNegative']+$v['DeliNoANCPositive']+$v['DeliNoANCNegative'])) *100;
      $per_AntiHIV = @(float)($v['DeliPosTotal'] / ($v['DeliANCHAARTTx']+$v['DeliANCHAARTPr']+$v['DeliANCAZTNVP']+$v['DeliANCARV']+$v['DeliNoANCHAARTTx']+$v['DeliNoANCHAARTPr']+$v['DeliNoANCAZTNVP']+$v['DeliNoANCARV'])) *100;
      $per_ExamSyphilis = @(float)(($v['VDRLPos']+$v['VDRLNeg']) / $v['DeliTotal']) *100;
      $per_ANCSyphilis = @(float)($v['SyphilisDx'] / ($v['VDRLPos']+$v['VDRLNeg'])) *100;
      $per_AntiSyphilis = @(float)($v['SyphilisDx'] / $v['SyphilisTx']) *100;


      if($v['areacode'] == "13"){

        $BabyPos += $v['BabyPos'];
        $BabyPCR2 += $v['BabyPCR2'];
        $DeliANC += $v['DeliANC'];
        $DeliANCPositive += $v['DeliANCPositive'];
        $DeliANCNegative += $v['DeliANCNegative'];
        $DeliNoANCPositive += $v['DeliNoANCPositive'];
        $DeliNoANCNegative += $v['DeliNoANCNegative'];
        $DeliPosTotal += $v['DeliPosTotal'];
        $DeliTotal += $v['DeliTotal'];
        $DeliANCHAARTTx += $v['DeliANCHAARTTx'];
        $DeliANCHAARTPr += $v['DeliANCHAARTPr'];
        $DeliANCAZTNVP += $v['DeliANCAZTNVP'];
        $DeliANCARV += $v['DeliANCARV'];
        $DeliNoANCHAARTTx +=$v['DeliNoANCHAARTTx'];
        $DeliNoANCHAARTPr += $v['DeliNoANCHAARTPr'];
        $DeliNoANCAZTNVP += $v['DeliNoANCAZTNVP'];
        $DeliNoANCARV += $v['DeliNoANCARV'];

        $VDRLPos += $v['VDRLPos'];
        $VDRLNeg += $v['VDRLNeg'];
        $SyphilisDx += $v['SyphilisDx'];
        $SyphilisTx += $v['SyphilisTx'];
  


      }else if($v['areacode'] !== "13" && $v['areacode'] !== 'TOTAL'){
        echo '<tr>';
        echo '<td>'. ($k + 1) . '</td>';
        
        if($mapType!=='"cw"'){
          // print_r($v); die();

        echo '<td class="text-left">' .Html::a( $v['areaname_new'] , json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode'], ['class' => 'success'],'_blank') . '</td>';
        }else{
          echo '<td class="text-left">' .$v['areaname_new']. '</td>';
        }
        echo '<td class="text-right">' .@(float)number_format($per_BabyPos,2). '</td>';
            // <td class="text-right">' .$v['']. '</td>
        echo '<td class="text-right">' .@(float)number_format($per_Deli,2). '</td>
            <td class="text-right">' .@(float)number_format($per_DeliKnow,2). '</td>
            <td class="text-right">' .@(float)number_format($per_DeliHIV,2). '</td>
            <td class="text-right">' .@(float)number_format($per_AntiHIV,2). '</td>

            <td class="text-right">' .@(float)number_format($per_ExamSyphilis,2). '</td>
            <td class="text-right">' .@(float)number_format($per_ANCSyphilis,2). '</td>
            <td class="text-right">'.@(float)number_format($per_AntiSyphilis,2). '</td>
            </tr>';
          //   <td class="text-right">' .number_format($v['perPreCsg'],2). '</td>
          //   <td class="text-right">' .number_format($v['perPostCsg'],2). '</td>
          // </tr>';

        }

        // print_r($value['report_date']);

  } //end foreach

  if($request->get('rg')=="" || $request->get('rg') == null){
  echo '<tr>';
  echo '<td>'. ($k + 1) . '</td>';
  echo '<td class="text-left">' .Html::a( 'สสม.' , json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . '13', ['class' => 'success'],'_blank') . '</td>';
  echo '<td class="text-right">' .@number_format($BabyPos/$BabyPCR2*100,2). '</td>';
  // <td class="text-right">' .$v['']. '</td>
  echo '<td class="text-right">' .@number_format($DeliANC/$DeliTotal *100,2). '</td>
  <td class="text-right">' .@number_format(($DeliANCPositive+$DeliANCNegative+$DeliNoANCPositive+$DeliNoANCNegative) / $DeliTotal *100,2). '</td>
  <td class="text-right">' .@number_format($DeliPosTotal / ($DeliANCPositive+$DeliANCNegative+$DeliNoANCPositive+$DeliNoANCNegative) *100,2). '</td>
  <td class="text-right">' .@number_format($DeliPosTotal / ($DeliANCHAARTTx+$DeliANCHAARTPr+$DeliANCAZTNVP+$DeliANCARV+$DeliNoANCHAARTTx+$DeliNoANCHAARTPr+$DeliNoANCAZTNVP+$DeliNoANCARV) *100,2). '</td>
  
  <td class="text-right">' .@number_format(($VDRLPos+$VDRLNeg) / $DeliTotal *100,2). '</td>
  <td class="text-right">' .@number_format($SyphilisDx / ($VDRLPos+$VDRLNeg) *100,2). '</td>
  <td class="text-right">' .@number_format($SyphilisDx / $SyphilisTx *100,2). '</td>';
  echo '</tr>';
  }

  echo '<tr>';
  echo '<td>'. ($k + 1) . '</td>';
  echo '<td class="text-left">' .$v['areaname_new']. '</td>';
  echo '<td class="text-right">' .@(float)number_format($per_BabyPos,2). '</td>';
  // <td class="text-right">' .$v['']. '</td>
  echo '<td class="text-right">' .@(float)number_format($per_Deli,2). '</td>
  <td class="text-right">' .@(float)number_format($per_DeliKnow,2). '</td>
  <td class="text-right">' .@(float)number_format($per_DeliHIV,2). '</td>
  <td class="text-right">' .@(float)number_format($per_AntiHIV,2). '</td>
  
  <td class="text-right">' .@(float)number_format($per_ExamSyphilis,2). '</td>
  <td class="text-right">' .@(float)number_format($per_ANCSyphilis,2). '</td>
  <td class="text-right">' .@(float)number_format($per_AntiSyphilis,2). '</td>';
  echo '</tr>';


  // echo $v['areacode'];
  
} //end if  


?>

</tbody>
</table>

<div class="well">
<b>ที่มา</b> : ระบบ DoH Dashboard กรมอนามัย
<br/><br/>
<b>วันที่ประมวลผล :: </b>
<?
echo DateThai(gmdate("Y-m-d", $report_date));


function getTimeToMicroseconds() {
  $t = microtime(true);
  $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
  $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));

  return $d->format("Y-m-d H:i:s.u"); 
}
// echo getTimeToMicroseconds('1510989016');
// $dda = date("Y-m-d","1510989016"); // ปี เดือน วัน
// echo date("d-m-Y H:i:s",$v['date_com']); // วัน เดือน ปี
// echo date("d/m/Y H:i:s","1257267600"); // วัน เดือน ปี เวลา

function DateThai($strDate){

    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));	
    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม",
    "กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";

}
  
?>
</div>
