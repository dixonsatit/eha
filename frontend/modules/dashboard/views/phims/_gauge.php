<?php
use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
    'scripts' => [
        'highcharts-more',
        'modules/exporting',
        //'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'สถานการณ์ภาพรวมการถ่ายทอดเชื้อเอชไอวีแม่สู่ลูก',
        ],
        'credits'=>[
                'enabled'=>false,
            ],
        'chart' => [
            'type' => 'gauge',
            'plotBackgroundColor' => null,
            'plotBackgroundImage' => null,
            'plotBorderWidth' => 0,
            'plotShadow' => false,
            //'height'=> 230,
        ],
        // 'title' => 'ร้อยละ',

        'pane' => [
            'startAngle' => -100,
            'endAngle' => 100,
            // 'background' => null,
            'background'=> [
                'backgroundColor'=> '#ddd',
                'innerRadius'=> '20%',
                'outerRadius'=> '60%',
                'shape'=> 'arc',
                'borderWidth' => 0,
            ]
        ],

        // the value axis
        'yAxis' => [
            'min' => 0,
            'max' => 10,

            'minorTickInterval' => 10,
            'minorTickWidth' => 1,
            'minorTickLength' => 13,
            'minorTickPosition' => 'inside',
            'minorTickColor' => '#666',

            'tickPixelInterval' => 20,
            'tickWidth' => 0,
            'tickPosition' => 'inside',
            'tickLength' => 10,
            // 'tickColor' => '#666',
            'tickColor'=> '#fff',
            'labels' => [
                'step' => 4,
                'rotation' => 'auto',
                'style' => [
                    'fontSize' => '14px',
                    'fontFamily' => 'Verdana, sans-serif',
                ],
            ],
            'title' => [
                // 'text' => 'ร้อยละ',
            ],
            'plotBands' => $plotBands
            // 'plotBands' => [[
            //     'from' => 0,
            //     'to' => 50,
            //     'color' => '#DF5353', // green
            // ], [
            //     'from' => 51,
            //     'to' => 80,
            //     'color' => '#DDDF0D', // yellow
            // ], [
            //     'from' => 81,
            //     'to' => 100,
            //     'color' => '#55BF3B', // red
            // ]],
        ],
        'tooltip' => [
            'enabled' => false,
        ],
//        'tooltip' => [
//            'pointFormat' => 'ร้อยละ {point.y:.2f}',
//            'style' => [
//                'fontSize' => '20px',
//                'fontFamily' => 'Verdana, sans-serif',
//            ],
//        ],
        'series' => [[
            'name' => 'ร้อยละ',
            // 'data' => [(int)$chartHdc->percentage],
            'data' => [$gaugeData],

            'dataLabels' => [
                'enabled' => true,
                // 'y'=>-40,
                'color' => '#444',
                'format' => '{point.y:.2f}',
                'style' => [
                    'fontSize' => '32px',
                    'fontFamily' => 'Verdana, sans-serif',
                    'textShadow' => false,
                ],
                'borderWidth'=>0,
                
            ],
        ]],
    ],
]);
?>

