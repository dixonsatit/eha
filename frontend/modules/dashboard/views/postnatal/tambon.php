<?php
/* @var $this yii\web\View */
?>

<?php
$session = Yii::$app->session;
$request = Yii::$app->request;
$session['kid'] = $request->get('kid');
$session['year'] = $request->get('year') != null ? $request->get('year') : date("Y");
$session['cw'] = $request->get('cw');
$session['ap'] = $request->get('ap');

$this->title = $kpi['kpi_name'] ." ระดับ". $scope;
$this->params['breadcrumbs'][] = ['label' => 'ระดับเขตสุขภาพ', 'url' => ['index?year='.$session['year'].'&kid='.$session['kid']]];
$this->params['breadcrumbs'][] = ['label' => 'ระดับจังหวัด', 'url' =>  ['changwat?year='.$session['year'].'&kid='.$session['kid'].'&rg='.$session['rg']]];
$this->params['breadcrumbs'][] = ['label' => 'ระดับอำเภอ', 'url' =>  ['changwat?year='.$session['year'].'&kid='.$session['kid'].'&rg='.$session['rg'].'&ap='.$session['ap']]];
$this->params['breadcrumbs'][] = $scope;;


?>


<?=$this->render('//_header')?>


<div class="content animate-panel">


<div class="row">
            <div class="col-lg-4">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Gauge -->
                     <?=$this->render('_gauge', ['gaugeData' => $gaugeData,'plotBands'=>$plotBands])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Map -->
                    <?=$this->render('_map', ['geo' => $geo, 'mapUrl' => $mapUrl, 'mapType' => $mapType, 'mapLegend' => $mapLegend])?>


                    </div>

                </div>
            </div>

</div>


<div class="row">
            <div class="col-lg-4">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <!-- Trend -->
                    <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>

                    </div>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="hpanel">
                    <div class="panel-body h-200">

                     <!-- Column -->
                     <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>

                    </div>

                </div>
            </div>

</div>



<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">

                    <?=$this->render('_datatables', ['kpi' => $kpi, 'data' => $data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>

                    </div>

                </div>
            </div>
    </div>




</div>