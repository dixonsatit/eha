<?php
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;
?>

<?php
$this->title = $kpi['kpi_name'] ." ระดับ". $scope;
$this->params['breadcrumbs'][] = " ภาพรวมระดับ".$scope;
?>

<?=$this->render('//_header')?>

<div class="content animate-panel">

    <div class=" animate-panel">
        <div class="panel panel-default">
            <div class="manifest-data-index content">
                <div class="row">
                    <div class="col-md-1">
                        <h4> เลือกปี  </h4>
                    </div>
                    <div class="dropdown col-md-2">
                        <a href="#" class="dropdown-toggle btn btn-primary" id="year" data-toggle="dropdown" role="button" aria-haspopup="true" 
                        aria-expanded="false"> <?= $year;?> <span class="caret"></span> </a>
                        <?php
                        for ($i=(date("m")>=10 ? date("Y")+1 : date("Y")); $i>=2018; $i--) {
                            $cyear[] = array('label'=>($i+543),
                            'url'=>Url::to(['setcookie', 'page'=>'index','name'=>'year','val'=>($i+543)]));
                        }
                        echo Dropdown::widget([
                            'items' => $cyear
                        ]);
                        ?>
                    </div>
                    <div class="col-md-9">
                    </div>
                </div>
                <!-- <div class="row"> -->
            </div>
            <!-- <div class="manifest-data-index content"> -->
        </div>
        <!-- <div class="panel panel-default"> -->
    </div>
    <!-- <div class=" animate-panel"> -->

    <div class="row">
        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body h-200">
                    <!-- Gauge -->
                    <?=$this->render('_gauge', ['kpi' => $kpi,'gaugeData' => $gaugeData,'plotBands'=>$plotBands])?>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="hpanel">
                <div class="panel-body h-200">
                <!-- Map -->
                <?=$this->render('_map', ['kpi' => $kpi,'geo' => $geo, 'mapUrl' => $mapUrl,'mapType' => $mapType, 'mapLegend' => $mapLegend])?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body h-200">
                <!-- Trend -->
                <?=$this->render('_trend', ['kpi' => $kpi, 'trendData' => $trendData])?>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="hpanel">
                <div class="panel-body h-200">
                    <!-- Column -->
                    <?=$this->render('_column', ['kpi' => $kpi, 'columnData' => $columnData])?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body h-200">
                <?=$this->render('_datatables', ['kpi' => $kpi, 'data' => $data,'mapUrl' => $mapUrl,'mapType' => $mapType,'scope'=>$scope])?>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- <div class="content animate-panel"> -->