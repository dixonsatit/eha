<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
    // Datatables
    getDatatable('#datatables',0)
JS;

$this->registerJs($script);
$request = Yii::$app->request;
$ap = null;
$ap = $request->get('ap');
?>
<div class="text-center font-weight-bold text-dark"><h5>ตารางข้อมูลรายพื้นที่</h5></div>
<div class="table-responsive">
<table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
<thead>
    <tr class="info">
        <th></th>
        <th rowspan="2" class="text-center align-middle">พื้นที่</th>
        <th colspan="2" class="text-center align-middle">รวมทั้งปี</th>
        <th colspan="3" class="text-center">ต.ค.</th>
        <th colspan="3" class="text-center">พ.ย.</th>
        <th colspan="3" class="text-center">ธ.ค.</th>
        <th colspan="3" class="text-center">ม.ค.</th>
        <th colspan="3" class="text-center">ก.พ.</th>
        <th colspan="3" class="text-center">มี.ค.</th>
        <th colspan="3" class="text-center">เม.ย.</th>
        <th colspan="3" class="text-center">พ.ค.</th>
        <th colspan="3" class="text-center">มิ.ย.</th>
        <th colspan="3" class="text-center">ก.ค.</th>
        <th colspan="3" class="text-center">ส.ค.</th>
        <th colspan="3" class="text-center">ก.ย.</th>
    </tr>
    <tr class="info">
        <th></th>
        <th class="text-center">เป้าหมาย</th>
        <th class="text-center">ผลลัพธ์</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>                         
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>

        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
        
        <th class="text-center">B</th>
        <th class="text-center">A</th>
        <th class="text-center">ร้อยละ</th>
    </tr>
</thead>        
<tbody>
<?php
if($data){
    foreach ($data as $key => $item) {
        echo '<tr>';
        echo '<td></td>';
        if ($item['areacode'] !== 'TOTAL') {
            if($request->get('ap')=='') {
                echo '<td class="text-left">' . Html::a($item['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $item['areacode'], ['class' => 'success'],'_blank') . '</td>';
            }
            else {
                echo '<td class="text-left">' . $item['areaname_new'] . '</td>';
            }
        } 
        else {
            //
            $report_date = $item['report_date'];
            //
            echo '<td class="text-left">' . $item['areaname_new'] . '</td>';
        }
        //
        echo '<td class="text-center">' . number_format($item['target']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result']) . '</td>';

        echo '<td class="text-center">' . number_format($item['target10']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result10']) . '</td>';
        echo '<td class="text-center">' . $item['ratio10'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target11']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result11']) . '</td>';
        echo '<td class="text-center">' . $item['ratio11'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target12']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result12']) . '</td>';
        echo '<td class="text-center">' . $item['ratio12'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target01']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result01']) . '</td>';
        echo '<td class="text-center">' . $item['ratio01'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target02']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result02']) . '</td>';
        echo '<td class="text-center">' . $item['ratio02'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target03']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result03']) . '</td>';
        echo '<td class="text-center">' . $item['ratio03'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target04']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result04']) . '</td>';
        echo '<td class="text-center">' . $item['ratio04'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target05']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result05']) . '</td>';
        echo '<td class="text-center">' . $item['ratio05'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target06']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result06']) . '</td>';
        echo '<td class="text-center">' . $item['ratio06'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target07']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result07']) . '</td>';
        echo '<td class="text-center">' . $item['ratio07'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target08']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result08']) . '</td>';
        echo '<td class="text-center">' . $item['ratio08'] . '</td>';

        echo '<td class="text-center">' . number_format($item['target09']) . '</td>';
        echo '<td class="text-center">' . number_format($item['result09']) . '</td>';
        echo '<td class="text-center">' . $item['ratio09'] . '</td>';
        echo '</tr>';
    }
}
?>

</tbody>
<tfoot>
    <tr class="info">
    <td colspan="39">
    <br>
    B หมายถึง จำนวนหญิงไทยในเขตรับผิดชอบ สิ้นสุดการตั้งครรภ์ด้วยการคลอดครบ 42 วันทั้งหมดในปีงบประมาณ (ฐานข้อมูล 43 แฟ้ม) LABOR (BTYPE ไม่เท่ากับ 6) 	
    <br>
    A หมายถึง จำนวนหญิงตาม B ที่ได้รับการดูแลครบ 3 ครั้งตามเกณฑ์
    <br>
    หมายเหตุ: 
    คลอดครบ 42 วันในไตรมาสใดเป็นเป้าหมายในไตรมาสนั้นๆ  
    <br>
    <br>
    ที่มา: DoH Dashboard กรมอนามัย (ข้อมูลจาก HDC กระทรวงสาธารณสุข)
    <br>
    <?=DashboardServices::getReportdate($report_date);?>
    <br>
    </td>
    </tr>
</tfoot>  
</table>         

</div>
