<?php
use yii\helpers\Html;
use frontend\modules\dashboard\models\DashboardServices;

$script = <<< JS
        //Datatables
        getDatatable('#datatables',0);
        
JS;
$this->registerJs($script);

// print_r($ap);
// die();
?>

<div class="panel">
<!-- <div class="panel-heading" style="font-Size:18px;"><?echo $kpi['kpi_name'];?></div> -->
<div class="panel-heading" style="font-Size:18px; color:black;"><center>ตารางข้อมูลรายพื้นที่</center></div>
<div class="panel-body">

            <table id="datatables" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                <thead>

                <tr class="info">
                    <th rowspan=3 class="text-center">ลำดับ</th>
                    <th rowspan=3 class="text-center"><?=$scope;?></th>
                    <th colspan=13 class="text-center">จำนวนนักเรียนชั้นประถมศึกษาปีที่ 1 (คน)</th>
                </tr>
                <tr class="info">
                    <!-- <th>ลำดับ</th> -->
                    <th colspan=3 class="text-center">จำนวนทั้งหมด</th>
                    <th colspan=3 class="text-center">ผลการคัดกรองเบื้องต้นโดยครู</th>
                    <th colspan=4 class="text-center">ผลการตรวจสายตาโดย รพสต./รพช./PCU</th>
                    <th colspan=3 class="text-center">การได้รับการรักษา/แก้ไข</th>
                </tr>  
                <tr class="info">
                    <!-- <th>ลำดับ</th> -->
                    <th class="text-center">รวม</th>
                    <th class="text-center">ชาย</th>
                    <th class="text-center">หญิง</th>
                    <th class="text-center">ไม่ได้ทดสอบ</th>
                    <th class="text-center">ปกติ</th>
                    <th class="text-center">ผิดปกติ(A)</th>
                    <th class="text-center">ตรวจซ้ำที่ผิดปกติจากครู(B)</th>
                    <th class="text-center">% ตรวจซ้ำ(B/A)</th>
                    <th class="text-center">ปกติ</th>
                    <th class="text-center">ผิดปกติ</th>
                    <th class="text-center">ได้รับแว่นสายตา</th>
                    <th class="text-center">รับแว่นแล้ว</th>
                    <th class="text-center">ส่งต่อเพื่อรักษา</th>
                </tr>
  
                </thead>

                <tfoot>
                <tr>
                <td colspan=3 class="info">ที่มา: National Eye Health DataCenter</td>
            </tr>
    </tfoot>
                <tbody>

<?php
if($data){
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td class="text-center">'.($k+1).'</td>';
        if ($v['areacode'] !== 'TOTAL') {
            if ($ap==null) {
                echo '<td class="text-left">' . Html::a($v['areaname_new'], json_decode($mapUrl) . '&' . json_decode($mapType) . '=' . $v['areacode'], ['class' => 'success'],'_blank') . '</td>';
            } else {
                //
                $report_date = $v['report_date'];
                //
                echo '<td class="text-left">' . $v['areaname_new'] . '</td>';
            } //end if ($ap==null)
        } //end if ($v['areacode'] !== 'TOTAL')
        //
        echo '<td class="text-center">'.number_format($v['result']).'</td>';
        echo '<td class="text-center">'.number_format($v['male']).'</td>';
        echo '<td class="text-center">'.number_format($v['female']).'</td>';
        echo '<td class="text-center">'.number_format($v['no_test']).'</td>';
        echo '<td class="text-center">'.number_format($v['normal_test']).'</td>';
        echo '<td class="text-center">'.number_format($v['abnormal_test']).'</td>';
        echo '<td class="text-center">'.number_format($v['repeat_teacher']).'</td>';
        echo '<td class="text-center">'.number_format($v['percent'],2).'</td>';
        echo '<td class="text-center">'.number_format($v['repeat_normal']).'</td>';
        echo '<td class="text-center">'.number_format($v['repeat_abnormal']).'</td>';
        echo '<td class="text-center">'.number_format($v['cure1']).'</td>';
        echo '<td class="text-center">'.number_format($v['cure2']).'</td>';
        echo '<td class="text-center">'.number_format($v['cure3']).'</td>';
        echo '</tr>';
        //
    } //end foreach
} //end if         
?>
        </div>
            

                </tbody>
            </table>
        </div>

    </div>
</div>


<!--///////////////////////////////////////////////////////////////////////////-->




