<?php
use miloschuman\highcharts\Highcharts;
?>


                    <?php
                 echo Highcharts::widget([
                 'id'=>'gauge',
                 'scripts' => [
                 'highcharts-more',  
                 'modules/exporting',
                 'themes/grid-light',
                 ],
                 'options' => [
                 'chart'=> [
                'type' => 'gauge',
                'plotBackgroundColor' => null,
                'plotBackgroundImage' => null,
                'plotBorderWidth' => 0,
                'plotShadow' => false
            ],
            'title' => [
                'text'=>'สถานการณ์ภาพรวม'
            ],
            
            'pane' => [
            'startAngle' => -150,
            'endAngle' => 150,
            'background' => [[
                'backgroundColor' => [
                'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                'stops' => [
                [0, '#FFF'],
                [1, '#333']
                ]
            ],
            'borderWidth' => 0,
            'outerRadius' => '109%'
            ], [
            'backgroundColor' => [
            'linearGradient' => [ 'x1' => 0, 'y1' => 0, 'x2' => 0, 'y2' => 1 ],
                'stops' => [
                [0, '#333'],
                [1, '#FFF']
                ]
            ],
                'borderWidth' => 1,
                'outerRadius' => '107%'
            ], [
            // default background
            ], [
            'backgroundColor' => '#DDD',
            'borderWidth' => 0,
            'outerRadius' => '105%',
            'innerRadius' => '103%'
            ]]
            ],
            // the value axis
            'yAxis'=>[
                'min' => 0,
                'max' => 100,

                'minorTickInterval' => 'auto',
                'minorTickWidth' => 1,
                'minorTickLength' => 10,
                'minorTickPosition' => 'inside',
                'minorTickColor' => '#666',

                'tickPixelInterval' => 30,
                'tickWidth' => 2,
                'tickPosition' => 'inside',
                'tickLength' => 10,
                'tickColor' => '#666',
                'labels' => [
                    'step' => 2,
                    'rotation' => 'auto'
                ],
                'title' => [
                    'text' => 'ร้อยละ'
                ],
                 'plotBands' => $plotBands
                // 'plotBands'=> [[
                //     'from'=> 0,
                //     'to'=> 40.0,
                //     'color'=> '#DF5353' // red
                // ], [
                //     'from'=> 40.1,
                //     'to'=> 50,
                //     'color'=> '#DDDF0D' // yellow
                // ], [
                //     'from'=> 50.1,
                //     'to'=> 100,
                //     'color'=> '#55BF3B' // green
                // ]],
            ],
            'tooltip' => [
                'pointFormat' => 'ร้อยละ {point.y:.2f}',
                'style' => [
                    'fontSize' => '18px',
                    'fontFamily' => 'Verdana, sans-serif'
                ]
            ],
            'series' => [[
                'name' => 'ร้อยละ',
                // 'data' => [(int)$chartHdc->percentage],
                'data' => [$gaugeData],
                'tooltip'=> [
                        'valueSuffix' => ' '
                ],
                'dataLabels'=> [
                    'enabled'=> true,
                    'color'=> 'blue',
                    'format'=> '{point.y:.2f}',
                    'style' => [
                        'fontSize' => '10px',
                        'fontFamily' => 'Verdana, sans-serif'
                    ]
                ]
            ]]
        ]
])?>
