<?php
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
$this->registerJsFile('https://unpkg.com/leaflet@1.0.3/dist/leaflet.js');
$this->registerCssFile('https://unpkg.com/leaflet@1.0.3/dist/leaflet.css');

$js = <<<JS


    //Token //je1
       var mapboxAccessToken = "pk.eyJ1Ijoicml0dGhpbmV0IiwiYSI6ImNpenNscmEwMjAzbDUzMm80MjdkcGVwYWcifQ.qA2XQ8qf9KnNlEuFIDqshA";

    //เซ็ตตำแหน่งแผนที่(ประเทศไทย)
    var map = L.map('map').setView([13.736717, 100.523186], 7);

    //กำหนดขนาดตัวอักษร
    map.createPane('labels');
    map.getPane('labels').style.zIndex = 650;
    map.getPane('labels').style.pointerEvents = 'none';
    map.touchZoom.disable();
    map.doubleClickZoom.disable();
    map.scrollWheelZoom.disable();
    map.boxZoom.disable();
    map.keyboard.disable();

        //นำเข้าแผนที่
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png?access_token='+ mapboxAccessToken, {
		maxZoom: 18,
		attribution: '',
		id: 'mapbox.light'
	    }).addTo(map);

    //กำหนด Icon
	var baseballIcon = L.icon({
		//iconUrl: 'baseball-marker.png',
		iconSize: [32, 37],
		iconAnchor: [16, 37],
		popupAnchor: [0, -28]
	});
    // ข้อมูลแผนที่ ตำแหน่ง
    var mygeo =
    [{
            "type": "FeatureCollection",
            "features": $geo
    }];

    //ระบายสีลงบนแผนที่
//    var geo_json = L.geoJSON(mygeo, {
//      style: function(feature) {
//          return getColor(feature.properties.value);
//     },
//     onEachFeature: onEachFeature //เรียกฟังชั่นเสริม
//     }).on('click', function(e){
//             onClick(e);
//     }).addTo(map);

// var myStyle = {
//     "color": "#ff7800",
//     "weight": 5,
//     "opacity": 0.65
// };


var geo_json = L.geoJSON(mygeo, {
    style: function(feature) {
        return getColor(feature.properties.value);
   },
   onEachFeature: onEachFeature 
   }).on('click', function(e){
           onClick(e);
   }).addTo(map);
  


    //highlight จุดที่เลือก
   function highlightFeature(e) {
             var layer = e.target;
             layer.setStyle({
               color: '#117A65'
             });
             if (!L.Browser.ie && !L.Browser.opera) {
               layer.bringToFront();
             }
           }

    //คืนค่า highlight เดิม
    function resetHighlight(e) {
             geo_json.resetStyle(e.target);
           }

    //Hover
    function onEachFeature(feature, layer) {
             layer.on({
               mouseover: highlightFeature,
               mouseout: resetHighlight,
               click: zoomToFeature
             });
           }

    function getColor(d) {

        if(d>51){
           return {color:'#31a354'}; 
        }else{
           return {color:'#ff0000'};  
        }
         
    }

    function getColorLabel(d) {
        d > 51 ? '#31a354' : d > 30 ? '#feb24c' : d > 10 ? '#f03b20' : '#7cb5ec';
         
    }

    
    function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
    }

    function onClick(e) {
        
    }

    geo_json.eachLayer(function (layer) {
        layer.bindPopup(layer.feature.properties.name);
    });

    var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			
            grades = {"#31a354":"สูงกว่าเกณฑ์","#feb24c":"ผ่านเกณฑ์","#f03b20":"ต่ำกว่าเกณฑ์"},
			labels = [],
			from, to;

        Object.keys(grades).forEach(function(key) {
            labels.push(
				'<i style="background:' + key + '"></i> ' +
				grades[key]);

        });

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map);


    map.fitBounds(geo_json.getBounds());


JS;
$this->registerJs($js);

?>
                    <div style="width:100%; height:400px;" id="map"></div>
                    <style>
                                        .info {
                                            padding: 6px 8px;
                                            font: 14px/16px Arial, Helvetica, sans-serif;
                                            background: white;
                                            background: rgba(255,255,255,0.8);
                                            box-shadow: 0 0 15px rgba(0,0,0,0.2);
                                            border-radius: 5px;
                                        }
                                        .info h4 {
                                            margin: 0 0 5px;
                                            color: #777;
                                        }
                                        .legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }</style>
                    
                                        </style>