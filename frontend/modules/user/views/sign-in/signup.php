<?php

use common\models\Office;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\JsonExpression;
use yii\web\JsExpression;

$url = \yii\helpers\Url::to(['default/local-list']);
$urlOffice = \yii\helpers\Url::to(['default/office-list']);



/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\SignupForm */

$this->title = Yii::t('frontend', 'ลงทะเบียน');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
    <div class="row w-100 mx-auto">
        <div class="col-lg-6 mx-auto">
            <h2 class="text-center mb-4"><?php echo Html::encode($this->title) ?></h2>

            <div class="auto-form-wrapper">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <?php echo $form->field($model, 'username')->label('ชื่อผู้ใช้ (ภาษาอังกฤษย่างน้อย 4 ตัวอักษร)') ?>
                <?php echo $form->field($model, 'email')->label('อีเมล'); ?>
                <?php echo $form->field($model, 'password')->passwordInput()->label('รหัสผ่าน') ?>

                <?php
                $officeDesc = empty($profile->office_id) ? '' : Office::find()->where(['office_id' => $profile->office_id])->one()->office_name;
                echo $form->field($model, 'office_id')->widget(\kartik\select2\Select2::classname(), [
                    'initValueText' => $officeDesc, // set the initial display text
                    'size' => \kartik\select2\Select2::LARGE,
                    'options' => ['placeholder' => 'ค้นหาสำนักงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $urlOffice,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
                        'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
                    ],
                ])->label('สำนักงาน');
                ?>

                <?php
                $localDesc = empty($model->locality_code) ? '' : \common\models\Locality::findOne($model->locality_code)->locality_name;
                echo $form->field($model, 'locality_code')->widget(\kartik\select2\Select2::classname(), [
                    'initValueText' => $localDesc,
                    'size' => \kartik\select2\Select2::LARGE,
                    'options' => ['placeholder' => 'ค้นหาหน่วยงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
                        'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
                    ],
                ])->label('หน่วยงาน (อปท.)');
                ?>

                <?php echo $form->field($model, 'firstname'); ?>
                <?php echo $form->field($model, 'lastname'); ?>
                <?php echo $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '99-9999-9999'
                ]) ?>

                <div class="form-group">
                    <br>
                    <?php echo Html::submitButton(Yii::t('frontend', 'บันทีกข้อมูล'), ['class' => 'btn btn-primary btn-block btn-lg', 'name' => 'signup-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>