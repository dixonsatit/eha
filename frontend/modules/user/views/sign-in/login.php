<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = Yii::t('frontend', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
    <div class="row w-100 mx-auto">
        <div class="col-lg-4 mx-auto">
            <div class="auto-form-wrapper">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?php echo $form->field($model, 'identity') ?>
                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <div style="color:#999;margin:1em 0">

                </div>
                <div class="form-group">
                    <?php echo Html::submitButton(Yii::t('frontend', 'Login'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>
                <div class="form-group d-flex justify-content-between">
                    <div class="form-check form-check-flat mt-0">
                            <?php echo $form->field($model, 'rememberMe')->checkbox() ?>
                    </div>
                    <?php echo Yii::t('frontend', '<a href="{link}">ลืมรหัสผ่าน</a>', [
                        'link'=>yii\helpers\Url::to(['sign-in/request-password-reset', 'class' => "text-small forgot-password text-black"])
                    ]) ?>
                </div>

                <div class="text-block text-center my-3">
                    <span class="text-small font-weight-semibold">ไม่ใช่สมาชิก ?</span>
                    <?php echo Html::a(Yii::t('frontend', 'ลงทะเบียน'), ['signup'], ['class' => "text-black text-small"]) ?>

                </div>
            <?php ActiveForm::end(); ?>
             </div>
        </div>
    </div>
</div>