<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Locality;
use common\models\Office;
use yii\web\JsExpression;

$url = \yii\helpers\Url::to(['local-list']);
$urlOffice = \yii\helpers\Url::to(['default/office-list']);

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('frontend', 'User Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"><?php echo Html::encode($this->title) ?></h4>

                <p class="card-description">
                </p>

                <div class="">

                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">

                        <div class="col-md-2">
                            <?php echo $form->field($model->getModel('profile'), 'picture')->widget(
                                Upload::class,
                                [
                                    'url' => ['avatar-upload']
                                ]
                            )?>
                        </div>

                        <div class="col-md-10">

                            <div class="row">
                                <div class="col-md-4">
                                    <?php echo $form->field($model->getModel('profile'), 'firstname')->textInput(['maxlength' => 255]) ?>
                                </div>

                                <div class="col-md-4">
                                    <?php echo $form->field($model->getModel('profile'), 'lastname')->textInput(['maxlength' => 255]) ?>
                                </div>
<!--                                <div class="col-md-4">-->
                                    <?php
//                                    echo $form->field($model->getModel('profile'), 'gender')->dropDownlist([
//                                        \common\models\UserProfile::GENDER_FEMALE => Yii::t('frontend', 'Female'),
//                                        \common\models\UserProfile::GENDER_MALE => Yii::t('frontend', 'Male')
//                                    ], ['prompt' => ''])->label(Yii::t('frontend', 'Gender'))
                                    ?>
<!--                                </div>-->
                            </div>

                            <?php
                            // Get the initial local description

                            $officeDesc = empty($model->getModel('profile')->office_id) ? '' : Office::find()->where(['office_id' => $model->getModel('profile')->office_id])->one();

                            echo $form->field($model->getModel('profile'), 'office_id')->widget(Select2::classname(), [
                                'initValueText' => isset($officeDesc->office_name) ? $officeDesc->office_name : '' , // set the initial display text
                                'size' => \kartik\select2\Select2::LARGE,
                                'options' => ['placeholder' => 'ค้นหาหน่วยงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $urlOffice,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
                                    'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
                                ],
                            ])->label('สำนักงาน');

                            ?>

                            <?php

                            // Get the initial local description
                            $localDesc = empty($model->getModel('profile')->locality_code) ? '' : Locality::findOne($model->getModel('profile')->locality_code)->locality_name;

                            echo $form->field($model->getModel('profile'), 'locality_code')->widget(Select2::classname(), [
                                'initValueText' => $localDesc, // set the initial display text
                                'size' => \kartik\select2\Select2::LARGE,
                                'options' => ['placeholder' => 'ค้นหาหน่วยงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
                                    'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
                                ],
                            ])->label('หน่วยงาน (อปท.)');

                            ?>

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('profile'), 'phone')->label('เบอร์โทรหน่วยงาน'); ?>
                                </div>

                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('profile'), 'mobile')->label('เบอร์มือถือ'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('account'), 'username') ?>
                                </div>

                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('account'), 'email') ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('account'), 'password')->passwordInput() ?>
                                </div>

                                <div class="col-md-6">
                                    <?php echo $form->field($model->getModel('account'), 'password_confirm')->passwordInput() ?>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <?php echo Html::submitButton(Yii::t('frontend', 'Update'), ['class' => 'btn btn-primary']) ?>
                            </div>

                        </div>

                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
