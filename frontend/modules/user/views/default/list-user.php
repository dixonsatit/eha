<?php

use common\grid\EnumColumn;
use common\models\User;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สมาชิกในเขตรับผิดชอบ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo $this->title; ?></h4>



            <?php echo GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'options' => [
                    'class' => 'grid-view table-responsive'
                ],
                'columns' => [
//                    [
//                        'attribute' => 'id',
//                        'options' => ['style' => 'width:50px;']
//                    ],
                    [
                        'class' => 'yii\grid\SerialColumn',
                        // you may configure additional properties here
                    ],
                    [
                        'attribute' => 'username',
                        'label' => 'ชื่อผู้ใช้',
                        'format' => 'raw',
//                        'value' => function ($model) {
//                            return '<strong>' . $model->username . '</strong><br><small>' . ($model->userProfile ? $model->userProfile->fullName : '') . '</small>';
//                        }
                    ],
                    [
                        'label' => 'ชื่อ นามสกุล',
                        'value' => function ($data) {
                            return $data->userProfile->firstname . " " . $data->userProfile->lastname;
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'email',
                        'label' => 'อีเมล'
                    ],
                    [
                        'label' => 'เบอร์โทร์',
                        'value' => function ($data) {
                            return $data->userProfile->mobile;
                        }
                    ],
                    [
                        'label' => 'หน่วยงาน',
                        'value' => function ($data) {
                            return isset($data->userProfile->locality->locality_name) ? $data->userProfile->locality->locality_name: '-';
                        }
                    ],
                    [
                        'label' => 'จังหวัด',
                        'value' => function ($data) {
                            return isset($data->userProfile->locality->provinceCode->province_name) ? $data->userProfile->locality->provinceCode->province_name : '-';
                        }
                    ],
//                    [
//                        'class' => EnumColumn::class,
//                        'attribute' => 'status',
//                        'label' => 'สถานะ',
//                        'enum' => User::statuses(),
//                        'filter' => User::statuses()
//                    ],
                    [
                        'class' => '\kartik\grid\CheckboxColumn',
                        'header' => 'อนุมัติ',
                        'rowSelectedClass' => GridView::TYPE_INFO,
                        'checkboxOptions' => function ($data, $key, $index, $column) /*use ($selection, $model)*/ {
                            if (Yii::$app->user->can('director')) {
                                $bool = $data->status == 2 ? true : false;
                                return [
                                    'value' => $data->id,
                                    'checked' => $bool,
                                ];
                            } else {
                                $bool = $data->status == 2 ? true : false;
                                return [
                                    'value' => $data->id,
                                    'checked' => $bool,
                                    'disabled' => 'disabled',

                                ];
                            }
                        },

                    ],
                    // [
                    //     'attribute' => 'created_at',
                    //     'format' => 'datetime',
                    //     'filter' => DateTimeWidget::widget([
                    //         'model' => $searchModel,
                    //         'attribute' => 'created_at',
                    //         'phpDatetimeFormat' => 'dd.MM.yyyy',
                    //         'momentDatetimeFormat' => 'DD.MM.YYYY',
                    //         'clientEvents' => [
                    //             'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")')
                    //         ],
                    //     ])
                    // ],
                    // [
                    //     'attribute' => 'logged_at',
                    //     'format' => 'datetime',
                    //     'filter' => DateTimeWidget::widget([
                    //         'model' => $searchModel,
                    //         'attribute' => 'logged_at',
                    //         'phpDatetimeFormat' => 'dd.MM.yyyy',
                    //         'momentDatetimeFormat' => 'DD.MM.YYYY',
                    //         'clientEvents' => [
                    //             'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")')
                    //         ],
                    //     ])
                    // ],
                    // 'updated_at',
//
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => ' {view} {update} {delete}',
//                'buttons' => [
//                    'login' => function ($url) {
//                        return Html::a(
//                            '<i class="fa fa-sign-in" aria-hidden="true"></i>',
//                            $url,
//                            [
//                                'title' => Yii::t('backend', 'Login')
//                            ]
//                        );
//                    },
//                ],
//                'visibleButtons' => [
//                    'login' => Yii::$app->user->can('administrator')
//                ]
//
//            ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<div id="url-get-active" data-getactive="<?php echo \yii\helpers\Url::to(['default/active']) ?>"></div>
<div id="url-get-unactive" data-ungetactive="<?php echo \yii\helpers\Url::to(['default/un-active']) ?>"></div>


<?php
$js = <<< JS


    $('.kv-row-checkbox').change(function() {
        if(this.checked) {
            var id = $(this).val();
            var url = $('#url-get-active').data('getactive');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {id: id}
            }).done(function (data){
                $(this).prop("checked", true);
            });
        }else{
            var id = $(this).val();
            var url = $('#url-get-unactive').data('ungetactive');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {id: id}
            }).done(function (data){
                $(this).prop("checked", false);
            });
        }
    });

JS;
$this->registerJS($js);
?>
