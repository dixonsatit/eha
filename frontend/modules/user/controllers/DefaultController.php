<?php

namespace frontend\modules\user\controllers;

use backend\models\search\UserSearch;
use common\base\MultiModel;
use common\models\Assessment;
use common\models\District;
use common\models\Locality;
use common\models\Office;
use common\models\User;
use frontend\modules\user\models\AccountForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;

class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::class,
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::class
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],

                    [
                        'actions' => ['local-list', 'office-list'],
                        'allow' => true,
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $this->layout = '@flareui/views/layouts/main.php';

        $accountForm = new AccountForm();
        $accountForm->setUser(Yii::$app->user->identity);

        $model = new MultiModel([
            'models' => [
                'account' => $accountForm,
                'profile' => Yii::$app->user->identity->userProfile,
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $locale = $model->getModel('profile')->locale;
            Yii::$app->session->setFlash('forceUpdateLocale');
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-success'],
                'body' => Yii::t('frontend', 'Your account has been successfully saved', [], $locale)
            ]);
            return $this->refresh();
        }

        return $this->render('index', ['model' => $model]);

    }

    public function actionListUser()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->user->can('director')) {
            $dataProvider->query->andWhere(['locality.province_code' => Yii::$app->user->identity->userProfile->office->province_code])
                ->orWhere(['province.region_code' => Yii::$app->user->identity->userProfile->office->office_region]);
        }
        if (Yii::$app->user->can('manager') && !Yii::$app->user->can('director')) {
            $dataProvider->query->andWhere(['locality.province_code' => Yii::$app->user->identity->userProfile->office->province_code]);
        }
        if (Yii::$app->user->can('user')) {
            $dataProvider->query->andWhere(['locality.locality_code' => Yii::$app->user->identity->userProfile->locality->locality_code]);
        }

        return $this->render('list-user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionLocalList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(['locality.locality_code AS id,  CONCAT("[",locality.locality_code,"] ", locality.locality_name, " (จังหวัด ", province.province_name, ")") AS text'])
                ->from('locality')
                ->join('LEFT JOIN', 'province', 'province.province_code = locality.province_code')
                ->filterWhere([
                    'or',
                    ['like', 'locality.locality_name', $q],
                    ['like', 'province.province_name', $q],
                ])
                ->limit(30);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Locality::find($id)->locality_name];
        }
        return $out;
    }

    public function actionDistrictList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(['district_code AS id', 'district_name AS text'])
                ->from('district')
                ->where(['like', 'district_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => District::find($id)->distric_name];
        }
        return $out;
    }

    public function actionOfficeList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(['office_id AS id', 'office_name AS text'])
                ->from('office')
                ->where(['like', 'office_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Office::find($id)->office_name];
        }
        return $out;
    }

    public function actionActive()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $user = User::findOne($id);
            $user->status = 2;
            $user->save();
        }
    }

    public function actionUnActive()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $user = User::findOne($id);
            $user->status = 1;
            $user->save();
        }
    }
}
