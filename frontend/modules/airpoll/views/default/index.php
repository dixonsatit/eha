<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'ระบบเฝ้าระวังมลพิษทางอากาศ';
$this->params['breadcrumbs'][] = $this->title;
$script = <<< CSS
.clusterlink > div{
        padding:5px 0 3px 5px;
        }
.clusterlink:hover > div{
    background-color: #34495e;
    color: #FFFFFF !important;   
}
CSS;

$this->registerCss($script);
?>

<?= $this->render('//_header') ?>
<div class=" animate-panel content">
    <div class="panel panel-default">

        <div class="manifest-default-index content">
            <div class="row">

                <div class="col-md-3">
                    <div class="hpanel">
                        <a href="<?= Url::to(['/airpoll/report-air-poll']); ?>" class="clusterlink">
                            <div class="panel-body text-center h-200">
                                <i class="pe-7s-graph2 fa-4x"></i>
                                <h4 class="font-extra-bold no-margins text-success">
                                    รายงานมลพิษทางอากาศ
                                </h4>
                                <!-- <h1 class="m-xs">Paramitter</h1> -->
                                <!-- <small>ข้อมูลตัวชี้วัดจาก HDC</small> -->
                            </div>
                        </a>
                        <div class="panel-footer text-left">
<?= Html::a('<i class="fa fa-link" aria-hidden="true"></i> ดูรายงานมลพิษทางอากาศ', ['/airpoll/report-air-poll'])
?>
                        </div>

                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="hpanel">
                        <a href="<?= Url::to(['/airpoll/kpi-air-poll']); ?>" class="clusterlink">
                            <div class="panel-body text-center h-200">
                                <i class="pe-7s-note fa-4x"></i>
                                <h4 class="font-extra-bold no-margins text-success">
                                    บันทึกมลพิษทางอากาศ
                                </h4>
                                <!-- <h1 class="m-xs">Paramitter</h1> -->
                                <!-- <small>ข้อมูลตัวชี้วัดจาก HDC</small> -->
                            </div>
                        </a>
                        <div class="panel-footer text-left">
<?= Html::a('<i class="fa fa-link" aria-hidden="true"></i> เข้าสู่หน้าบันทึกข้อมูล', ['/airpoll/kpi-air-poll'])
?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
/*
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body h-200">

                    <?= Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> บันทึกมลพิษทางอากาศ ', ['/airpoll/kpi-air-poll'], ['class' => 'btn btn-info']) ?>
                    <br>
                    <br>
<?= Html::a('<i class="fa fa-book" aria-hidden="true"></i> รายงานมลพิษทางอากาศ ', ['/airpoll/report-air-poll'], ['class' => 'btn btn-success']) ?>

                </div>
                <!-- <div class="panel-footer"></div> -->
            </div>
        </div>

    </div>
</div>
*/
?>