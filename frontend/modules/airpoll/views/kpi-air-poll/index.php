<?php

use kartik\widgets\Alert;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\airpoll\models\KpiAirPoll */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ระบบข้อมูลมลพิษทางอากาศ';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/airpoll']];
$this->params['breadcrumbs'][] = 'รายการข้อมูล';
?>

<?php
$script = <<< JS

    //Datatables
    getDatatable('#datatables',0);
    // getDatatableSum('#datatables',2,3);

    // The Javascript shown below is used to initialise the table shown in this example:
    $(document).ready(function() {
        var table = $('#datatables').removeAttr('width').DataTable( {
            paging: false,
            searching: false      
            columnDefs: [
                { width: 150, targets: 0 }
            ],      
        } );
    });        

JS;
$this->registerJs($script);

?>

<div class="content animate-panel">

<?=$this->render('//_header')?>

<?php

if (Yii::$app->session->hasFlash('Save')) {

    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'icon' => 'glyphicon glyphicon-info-sign',
        'title' => 'สถานะการบันทึกข้อมูล!',
        'titleOptions' => ['icon' => 'info-sign'],
        'body' => Yii::$app->session->getFlash('Save'),
        'showSeparator' => true,
        'delay' => 3000,
    ]);
}
?>
<div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body h-200">

    <!-- <h4><?php //echo Html::encode($this->title)?></h4> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=Html::a('บันทึกข้อมูล', ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <table id="datatables" class="table table-striped table-hover table-bordered"  width="100%">
                    <thead>
                        <tr class="info">

                            <th class="text-center">ลำดับ</th>
                            <th class="text-center">สถานีตรวจวัด</th>
                            <th class="text-center">วันที่</th>
                            <th class="text-center">ค่าเฉลี่ย 24 ชม.(มคก./ลบ.ม.)</th>
                            <th class="text-center">ค่าต่ำสุด(มคก./ลบ.ม.)</th>
                            <th class="text-center">ค่าสูงสุด(มคก./ลบ.ม.)</th>
                            <th class="text-center">Actions</th>
                         </tr>
                    </thead>
        <!--            <tfoot>
                        <tr>
                            <th></th>
                            <th>รวมทั้งหมด</th>
                            <th></th>
                        </tr>
                    </tfoot>-->
                    <tbody>

                        <?php
foreach ($data as $key => $item) {

    echo '<tr>';
    echo '<td class="text-center">' . ($key + 1) . '</td>';
    echo '<td>' . $item['station_fullname'] . '</td>';
    echo '<td class="text-center">' . $item['regdate'] . '</td>';
    echo '<td class="text-center">' . $item['avg24'] . '</td>';
    echo '<td class="text-center">' . $item['min'] . '</td>';
    echo '<td class="text-center">' . $item['max'] . '</td>';
    echo '<td class="text-center">';
    // echo Html::a('<i class="fa fa-eye fa-lg text-info"></i>', ['view', 'id' => $item['id']]) . ' ';
    echo Html::a('<i class="fa fa-pencil-square-o fa-lg text-primary"></i>', ['update', 'id' => $item['id']]) . ' ';
    echo Html::a('<i class="fa fa-trash fa-lg text-danger"></i>', ['delete', 'id' => $item['id']], ['data-method' => "post", 'data-confirm' => ("ต้องการลบข้อมูลรายการนี้จริงหรือไม่?"."\r\n\r\nสถานีตรวจวัด: ".$item['station_fullname']."\r\nวันที่: ".$item['regdate']."\r\nค่าเฉลี่ย 24 ชม.: ".$item['avg24']."\r\nค่าต่ำสุด: ".$item['min']."\r\nค่าสูงสุด: ".$item['max']) ]);
    echo '</td>';
    // echo '<td>' . Yii::$app->thaidate->ShortDate($item['regdate']) . '</td>';
    echo '</tr>';
}
?>

                    </tbody>
                </table>

                    </div>
                    <!-- <div class="panel-footer"></div> -->
                </div>
            </div>

</div>
</div>
