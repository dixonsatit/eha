<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\airpoll\models\KpiAirPoll */

$this->title = 'ระบบข้อมูลมลพิษทางอากาศ';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="content animate-panel">

    <?=$this->render('//_header')?>

    <!-- <h4><?php //echo Html::encode($this->title) ?></h4> -->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'ต้องการลบข้อมูลรายการนี้จริงหรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'changwatcode',
            'station_id',
            'regdate',
            'avg24',
            'min',
            'max',
            'note:ntext',
            'ip',
            'created_by',
            'created_at',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
