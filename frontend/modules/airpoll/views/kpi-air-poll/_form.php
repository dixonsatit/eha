<?php

use frontend\modules\airpoll\models\KpiAirPollStation;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\modules\airpoll\models\KpiAirPoll */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kpi-air-poll-form">

    <?php $form = ActiveForm::begin();?>

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">  
    <?php
    echo $form->field($model, 'station_id')->widget(Select2::classname(), [
    'data' => KpiAirPollStation::GetList(),
    'options' => ['placeholder' => 'เลือก...'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
    ]);
    ?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">  
    <?php
    echo $form->field($model, 'regdate')->widget(DatePicker::classname(), [
    'language' => 'th',
    'options' => ['placeholder' => 'เลือกวันที่...', 'readonly' => true],
    // 'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true,
        'todayBtn' => true,
        'autoclose' => true,
    ],
    ]);
    ?>  
        </div>
    </div>    

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">  
    <?=$form->field($model, 'avg24')->textInput()?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">   
    <?=$form->field($model, 'min')->textInput()?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">  
    <?=$form->field($model, 'max')->textInput()?>
        </div>  
    </div>    

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">  
    <?php
    echo $form->field($model, 'note')->widget(\dosamigos\ckeditor\CKEditor::className(), [
        'clientOptions' => ['height' => 100],
        'preset' => 'advance',
    ])
    ?>
        </div>  
    </div>    

    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? 'ตกลง' : 'ตกลง', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
