<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\airpoll\models\KpiAirPoll */

$this->title = 'ระบบข้อมูลมลพิษทางอากาศ';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'บันทึกข้อมูล';
?>
<div class="content animate-panel">
    
    <?=$this->render('//_header')?>

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body h-200">

                    <?= $this->render('_form', [
                        'model' => $model
                    ]) ?>
                    
                </div>
            
            <!-- <div class="panel-footer"></div> -->
            </div>
        </div>

    </div>
</div>

