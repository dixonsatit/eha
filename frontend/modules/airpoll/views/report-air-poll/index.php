<?php

use kartik\widgets\Alert;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\airpoll\models\KpiAirPollRange */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ระบบข้อมูลมลพิษทางอากาศ';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/airpoll']];
$this->params['breadcrumbs'][] = 'รายงาน';
?>

<div class="content animate-panel">

    <?=$this->render('//_header')?>

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body h-200">

                <!-- Map -->
                <?=$this->render('_map', ['geo' => $geo,'mapLegend'=>$mapLegend])?>

                </div>
            
            <!-- <div class="panel-footer"></div> -->
            </div>
        </div>
    </div>

</div>
