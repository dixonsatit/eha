<?php

$this->registerJsFile('https://unpkg.com/leaflet@1.0.3/dist/leaflet.js');
$this->registerCssFile('https://unpkg.com/leaflet@1.0.3/dist/leaflet.css');

$js = <<<JS


   //Token
    // var mapboxAccessToken = "pk.eyJ1Ijoicml0dGhpbmV0IiwiYSI6ImNpenNscmEwMjAzbDUzMm80MjdkcGVwYWcifQ.qA2XQ8qf9KnNlEuFIDqshA";
    var mapboxAccessToken = "pk.eyJ1Ijoicml0dGhpbmV0IiwiYSI6ImNpenNscmEwMjAzbDUzMm80MjdkcGVwYWcifQ.qA2XQ8qf9KnNlEuFIDqshA";

    //เซ็ตตำแหน่งแผนที่(ประเทศไทย)
    var map = L.map('map').setView([13.736717, 100.523186], 5);


    //กำหนดขนาดตัวอักษร
    map.createPane('labels');
    map.getPane('labels').style.zIndex = 650;
    map.getPane('labels').style.pointerEvents = 'none';

        //นำเข้าแผนที่
        // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token='+ mapboxAccessToken, {
		// maxZoom: 18,
		// attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		// 	'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		// 	'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		// id: 'mapbox.light'
        // }).addTo(map);

        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png?access_token='+ mapboxAccessToken, {
		maxZoom: 17,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
	    }).addTo(map);


     // var marker = L.marker([43.64701, -79.39425], {icon: pointIcon}).addTo(map);

    // ข้อมูลแผนที่ ตำแหน่ง
    var mygeo =
    [{
            "type": "FeatureCollection",
            "features": $geo
    }];

    //ระบายสีลงบนแผนที่
   var geo_json = L.geoJSON(mygeo, {
    style: function(feature) {
        //    return {"color":feature.properties.color}
        return {"color":"#2eb82e"}
    },
        
   onEachFeature: onEachFeature //เรียกฟังชั่นเสริม
   }).on('click', function(e){
           onClick(e);
   }).addTo(map);


   var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

    // method that we will use to update the control based on feature properties passed
    info.update = function (props) {
        this._div.innerHTML = '<h5>เลื่อนเม้าส์บนแผนที่</h5>' +  (props ?
            '<b>สถานีตรวจวัด ' + props.sname + '</b><br /> ค่าเฉลี่ย: ' + props.value+' (มคก./ลบ.ม.)' : '');
    };

    info.addTo(map);

    function pointIcon(icon){

        var pointIcon = L.icon({
        iconUrl:icon,
        iconSize: [32, 37], // size of the icon

        });

        return pointIcon;

    }


    function highlightFeature(e) {
        var layer = e.target;
        info.update(layer.feature.properties);
        layer.setStyle({
            color: '#848484'
            });
        if (!L.Browser.ie && !L.Browser.opera) {
            layer.bringToFront();
        }
    }

    function resetHighlight(e) {
        info.update();
        geo_json.resetStyle(e.target);
    }


    //Click option
    function onEachFeature(feature, layer) {

//        console.log(feature.properties.range_color);

        //Custom Marker
        
        pointIcon!==null?layer.setIcon(pointIcon(feature.properties.range_color)):null;
		
        layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
        });
    }

    //click ซูม
    function zoomToFeature(e) {
        // alert(e.target.feature.properties.value)
        // map.fitBounds(e.target.getBounds());
        // $mapType!=='tb'?window.open($mapUrl+'&'+$mapType+'='+e.target.feature.properties.geocode,'_self'):null;
    }

    geo_json.eachLayer(function (layer) {
        layer.bindPopup('สถานีตรวจวัด '+layer.feature.properties.sname+' <br> ค่าเฉลี่ย '+layer.feature.properties.value+' (มคก./ลบ.ม.)');
        
    });

    var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = $mapLegend,
            // grades = { "blue": "0-50มคก./ลบ.ม.", "green": "51-120มคก./ลบ.ม.",
            //             "yellow": "121-350มคก./ลบ.ม.", "orange": "351-420มคก./ลบ.ม.",
            //             "red": "มากกว่ำ 420 มคก./ลบ.ม."
            //             },
			labels = [],
			from, to;
            
        Object.keys(grades).forEach(function(key) {
            labels.push(
				'<i style="background:' + key + '; text-align:center;"></i> ' +
				grades[key]);
        });

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map);

    map.fitBounds(geo_json.getBounds());


JS;
$this->registerJs($js);
?>

<div style="width:100%; height:500px;" id="map"></div>

<style>
.info {
    padding: 6px 8px;
    font: 14px/16px Arial, Helvetica, sans-serif;
    background: white;
    background: rgba(255,255,255,0.8);
    box-shadow: 0 0 15px rgba(0,0,0,0.2);
    border-radius: 5px;
}
.info h4 {
    margin: 0 0 5px;
    color: #777;
}
.legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }</style>

</style>
