<?php

namespace frontend\modules\airpoll;

/**
 * airpoll module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\airpoll\controllers';
    // public $defaultRoute='kpi-air-poll';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
