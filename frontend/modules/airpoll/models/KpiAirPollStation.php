<?php

namespace frontend\modules\airpoll\models;

use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use \frontend\modules\airpoll\models\UploadForm;
use yii\filters\AccessControl;

/**
 * This is the model class for table "kpi_air_poll_station".
 *
 * @property integer $station_id
 * @property string $station_code
 * @property string $station_name
 * @property string $changwatcode
 */
class KpiAirPollStation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_air_poll_station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_code'], 'string', 'max' => 10],
            [['station_name'], 'string', 'max' => 255],
            [['changwatcode'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'station_id' => 'รหัส (ID)',
            'station_code' => 'รหัสสถานี',
            'station_name' => 'สถานีวัดคุณภาพอากาศ',
            'changwatcode' => 'รหัสจังหวัด',
        ];
    }

    public function getFullStation(){
        return $this->station_code.' ::'.$this->station_name;
    }

    public static function GetList() {
        return yii\helpers\ArrayHelper::map(self::find()->orderBy('station_code ASC')->all(), 'station_id', 'FullStation');
    }

}
