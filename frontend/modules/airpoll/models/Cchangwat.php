<?php

namespace frontend\modules\airpoll\models;

use Yii;

/**
 * This is the model class for table "cchangwat".
 *
 * @property string $changwatcode
 * @property string $changwatname
 * @property string $zonecode
 * @property string $regioncode
 */
class Cchangwat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cchangwat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['changwatcode'], 'required'],
            [['changwatcode', 'zonecode', 'regioncode'], 'string', 'max' => 2],
            [['changwatname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'changwatcode' => 'Changwatcode',
            'changwatname' => 'Changwatname',
            'zonecode' => 'Zonecode',
            'regioncode' => 'Regioncode',
        ];
    }


    public static function GetList() {
        return yii\helpers\ArrayHelper::map(self::find()->all(), 'changwatcode', 'changwatname');
    }

    
}
