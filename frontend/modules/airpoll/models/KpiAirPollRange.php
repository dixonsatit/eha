<?php

namespace frontend\modules\airpoll\models;

use Yii;

/**
 * This is the model class for table "kpi_air_poll_range".
 *
 * @property integer $id
 * @property integer $range_min
 * @property integer $range_max
 * @property string $range_color
 * @property string $range_desc
 * @property string $range_info
 */
class KpiAirPollRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_air_poll_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['range_min', 'range_max'], 'integer'],
            [['range_color'], 'string', 'max' => 50],
            [['range_desc', 'range_info'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'range_min' => 'Range Min',
            'range_max' => 'Range Max',
            'range_color' => 'Range Color',
            'range_desc' => 'Range Desc',
            'range_info' => 'Range Info',
        ];
    }
}
