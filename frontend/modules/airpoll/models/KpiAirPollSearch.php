<?php

namespace frontend\modules\airpoll\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\airpoll\models\KpiAirPoll;

/**
 * KpiAirPollSearch represents the model behind the search form about `frontend\modules\airpoll\models\KpiAirPoll`.
 */
class KpiAirPollSearch extends KpiAirPoll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'station_id', 'avg24', 'min', 'max'], 'integer'],
            [['changwatcode', 'regdate', 'note', 'ip', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KpiAirPoll::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'station_id' => $this->station_id,
            'regdate' => $this->regdate,
            'avg24' => $this->avg24,
            'min' => $this->min,
            'max' => $this->max,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'changwatcode', $this->changwatcode])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
}
