<?php

namespace frontend\modules\airpoll\models;

use Yii;

// Use to call function stamping DateTime
use yii\behaviors\TimestampBehavior;
// Use to call function stamping User-ID
use yii\behaviors\BlameableBehavior;
// Use to call function Split Select2's combobox into array
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\Request;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Html;

/**
 * This is the model class for table "kpi_air_poll".
 *
 * @property integer $id
 * @property string $changwatcode
 * @property integer $station_id
 * @property string $regdate
 * @property integer $avg24
 * @property integer $min
 * @property integer $max
 * @property string $note
 * @property string $ip
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_by
 * @property string $updated_at
 */
class KpiAirPoll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_air_poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_id','regdate','avg24','min','max'],'required'],
            [['station_id', 'avg24', 'min', 'max'], 'integer'],
            [['regdate', 'created_at', 'updated_at'], 'safe'],
            [['note'], 'string'],
            [['changwatcode'], 'string', 'max' => 2],
            [['ip'], 'string', 'max' => 15],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัส (ID)',
            'changwatcode' => 'จังหวัด',
            'station_id' => 'สถานีตรวจวัด',
            'regdate' => 'วันที่บันทึก',
            'avg24' => 'ค่าเฉลี่ย 24 ชม.(มคก./ลบ.ม.)',
            'min' => 'ค่าต่ำสุด(มคก./ลบ.ม.)',
            'max' => 'ค่าสูงสุด(มคก./ลบ.ม.)',
            'note' => 'หมายเหตุ',
            'ip' => 'สร้างโดย ip',
            'created_by' => 'สร้างโดยผู้ใช้',
            'created_at' => 'สร้างเมื่อ',
            'updated_by' => 'แก้ไขโดยผู้ใช้',
            'updated_at' => 'แก้ไขเมื่อ',       
        ];
    }


    //Dune
    public static function GetList() {
        return yii\helpers\ArrayHelper::map(self::find()->orderBy('id DESC')->all(), 'id', 'id');
    }

    //Dune
    public function stringToArray($value){
        return empty($value) ? null :explode(',', $value);
    }

    //Dune
    public function arrayToString($value){
        return is_array($value) ? implode(',', $value) : null ;
    }

}
