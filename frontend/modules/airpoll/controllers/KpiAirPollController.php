<?php

namespace frontend\modules\airpoll\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\filters\AjaxFilter;

use frontend\modules\airpoll\models\KpiAirPoll;
use frontend\modules\airpoll\models\KpiAirPollSearch;
use frontend\modules\airpoll\models\KpiAirPollRange;

/**
 * KpiAirPollController implements the CRUD actions for KpiAirPoll model.
 */
class KpiAirPollController extends Controller
{
    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }
    public function behaviors()
    {
        // To use AccessControl, declare it in the behaviors() method of your controller class. 
        // For example, the following declarations will allow authenticated users to access the 
        // "create" and "update" actions and deny all other users from accessing these two actions.        
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['view','download-file'],//'index',
                        // Allow all above permissions 
                        'allow' => true,
                        //'roles' => ['?']
                        // Allow authenticated users
                        //'roles' => ['@'],
                        //'roles' => ['ENV-PM','ENV-PM-ADMIN','ENV-PM-USER'],                    
                        'roles' => ['?'],
                        // Everything else is denied
                    ],
                    [
                        'actions' => ['index','view','download-file','create','list','update','delete','upload-file'],
                        // Allow all above permissions                         
                        'allow' => true,
                        //'roles' => ['?']
                        // Allow authenticated users
                        //'roles' => ['@'],
                        // Allow authenticated specified user                        
                        'roles' => ['ENV-PM','ENV-PM-ADMIN','ENV-PM-USER'],
                        // 'matchCallback' => function ($rule, $action) {
                        //     return (date('d-m-Y') > '28-02-2017' || Yii::$app->user->can('Admin'));
                        // }
                        // Everything else is denied
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         // Allow "delete" POST request
            //         'delete' => ['POST'],
            //         'create' => ['POST'],
            //     ],
            // ],
            // 'ajax' => [
            //     'class' => AjaxFilter::className(),
            //     'only' => ['create'],
            // ],            
        ];
    }


    public function getMapLegend()
    {

        $ranges = KpiAirPollRange::find()->all();

        foreach ($ranges as $r) {
            $legend[$r->range_color] = $r->range_desc.'('.$r->range_info.')';
        }
        return json_encode($legend !== null ? $legend : ['#848484' => 'ไม่มีข้อมูล']);
    }
    
    
    /**
     * Lists all KpiAirPoll models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new KpiAirPollSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $conn = Yii::$app->db;
        $sql = 'select  a.*,s.*,concat(s.station_code," ::",s.station_name) as station_fullname from kpi_air_poll a
        inner join kpi_air_poll_station s on s.station_id=a.station_id';

        $cmd = $conn->createCommand($sql);
        $data = $cmd->queryAll();

        // Map point
        $sql = 'select * from kpi_air_poll_station';
        $cmd = $conn->createCommand($sql);
        $mData = $cmd->queryAll();

        // $mapIconUrl = Url::home(true) . '../images/map-icon/';
        $mapIconUrl = '../images/map-icon/';

        foreach ($mData as $m) {

            // Check icon color
            if ($propData[$m['station_id']]['range_color'] !== null) {
                $range_color = $mapIconUrl . 'air-poll-' . $propData[$m['station_id']]['range_color'] . '.png';
            } else {
                $range_color = $mapIconUrl . 'air-poll-grey.png';
            }

            $gd[] = [
                "type" => "Feature",
                "properties" => ["value" => $propData[$m['station_id']] !== null ? $propData[$m['station_id']]['value'] : 0,
                    "range_color" => $range_color,
                    "sid" => $m['station_id'], "scode" => $m['station_code'], "sname" => $m['station_name']],
                "geometry" => json_decode($m['geojson']),
            ];
        }

        $geo = json_encode($gd);
        // Map point:End

        return $this->render('index', [
            'data'=>$data,
            'geo' => $geo,
            'mapLegend'=>$this->getMapLegend(),            
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KpiAirPoll model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KpiAirPoll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KpiAirPoll();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('Save', 'เพิ่มข้อมูลเรียบร้อยแล้ว');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing KpiAirPoll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('Save', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing KpiAirPoll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();


        Yii::$app->session->setFlash('Save', 'ลบข้อมูลเรียบร้อยแล้ว');
        return $this->redirect(['index']);
    }

    /**
     * Finds the KpiAirPoll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KpiAirPoll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KpiAirPoll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
