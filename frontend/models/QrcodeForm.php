<?php

namespace frontend\models;

use yii\base\Model;
use Da\QrCode\QrCode;

/**
 * QrcodeForm is the model behind the contact form.
 */
class QrcodeForm extends Model
{
    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['data'], 'required'],
            // We need to sanitize them
            [['data'], 'filter', 'filter' => 'strip_tags'],
            [['data'], 'string'],

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'data' => 'ข้อความหรือลิ้งเว็บไซต์',
        ];
    }

    public function createQrcode($data)
    {
        if ($data) {
            $qrCode = (new QrCode($data))
                ->setSize(250)
                ->setMargin(5);
            //$qrCode->writeFile(__DIR__ . '/code.png');
            return $qrCode->writeDataUri();
        }
        return null;
    }
}
