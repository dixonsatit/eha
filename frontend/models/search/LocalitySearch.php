<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Locality;

/**
 * LocalitySearch represents the model behind the search form of `common\models\Locality`.
 */
class LocalitySearch extends Locality
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['locality_code', 'locality_name', 'locality_shortname', 'administrator', 'administrative_title', 'type_code', 'address', 'subdistrict_code', 'district_code', 'province_code', 'postal_code', 'active_status', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Locality::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'province_code' => $this->province_code,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'locality_name', $this->locality_name])
            ->andFilterWhere(['like', 'locality_shortname', $this->locality_shortname])
            ->andFilterWhere(['like', 'administrator', $this->administrator])
            ->andFilterWhere(['like', 'administrative_title', $this->administrative_title])
            ->andFilterWhere(['like', 'type_code', $this->type_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'subdistrict_code', $this->subdistrict_code])
            ->andFilterWhere(['like', 'district_code', $this->district_code])
            ->andFilterWhere(['like', 'province_code', $this->province_code])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'active_status', $this->active_status])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
