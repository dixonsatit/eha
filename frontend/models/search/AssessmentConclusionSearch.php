<?php

namespace frontend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AssessmentConclusion;

/**
 * AssessmentConclusionSearch represents the model behind the search form of `common\models\AssessmentConclusion`.
 */
class AssessmentConclusionSearch extends AssessmentConclusion
{
    public $district;
    public $province;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'be_year', 'assessed_by_uid1', 'assessed_by_uid2', 'assessed_by_uid3', 'created_by', 'updated_by', 'application_form_id'], 'integer'],
            [['issue_code', 'locality_code', 'type', 'assess_result', 'assessed_by_fullname1', 'assessed_by_fullname2', 'assessed_by_fullname3', 'created_by_ip', 'created_at', 'updated_by_ip', 'certificate', 'approve', 'district', 'province'], 'safe'],
            [['component1_score', 'component2_score', 'component3_score', 'component4_score', 'component5_score', 'component1to5_avgscore', 'component6_score', 'component7_score', 'component6to7_avgscore', 'component1to7_avgscore', 'b', 'c', 'd', 'percentA', 'percentB', 'percentC', 'percentE'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssessmentConclusion::find();

        $query->joinWith(['localityCode'])->joinWith('localityCode.provinceCode');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['province'] = [
            'asc' => ['province_code' => SORT_ASC],
            'desc' => ['province_code' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['district'] = [
            'asc' => ['district_code' => SORT_ASC],
            'desc' => ['district_code' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'be_year' => $this->be_year,
            'component1_score' => $this->component1_score,
            'component2_score' => $this->component2_score,
            'component3_score' => $this->component3_score,
            'component4_score' => $this->component4_score,
            'component5_score' => $this->component5_score,
            'component1to5_avgscore' => $this->component1to5_avgscore,
            'component6_score' => $this->component6_score,
            'component7_score' => $this->component7_score,
            'component6to7_avgscore' => $this->component6to7_avgscore,
            'component1to7_avgscore' => $this->component1to7_avgscore,
            'b' => $this->b,
            'c' => $this->c,
            'd' => $this->d,
            'percentA' => $this->percentA,
            'percentB' => $this->percentB,
            'percentC' => $this->percentC,
            'percentE' => $this->percentE,
            'assessed_by_uid1' => $this->assessed_by_uid1,
            'assessed_by_uid2' => $this->assessed_by_uid2,
            'assessed_by_uid3' => $this->assessed_by_uid3,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'application_form_id' => $this->application_form_id,
        ]);

        $query->andFilterWhere(['like', 'issue_code', $this->issue_code])
            ->andFilterWhere(['like', 'locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'assess_result', $this->assess_result])
            ->andFilterWhere(['like', 'assessed_by_fullname1', $this->assessed_by_fullname1])
            ->andFilterWhere(['like', 'assessed_by_fullname2', $this->assessed_by_fullname2])
            ->andFilterWhere(['like', 'assessed_by_fullname3', $this->assessed_by_fullname3])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip])
            ->andFilterWhere(['like', 'certificate', $this->certificate])
            ->andFilterWhere(['like', 'approve', $this->approve])
            ->andFilterWhere(['like', 'district_code', $this->district])
            ->andFilterWhere(['like', 'province.province_code', $this->province]);

        return $dataProvider;
    }
}
