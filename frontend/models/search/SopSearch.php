<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Sop;

/**
 * SopSearch represents the model behind the search form of `common\models\Sop`.
 */
class SopSearch extends Sop
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'be_year', 'assess_score', 'created_by', 'updated_by'], 'integer'],
            [['component_code', 'issue_code', 'assess_step', 'assess_step_detail', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sop::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'be_year' => $this->be_year,
            'assess_score' => $this->assess_score,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'component_code', $this->component_code])
            ->andFilterWhere(['like', 'issue_code', $this->issue_code])
            ->andFilterWhere(['like', 'assess_step', $this->assess_step])
            ->andFilterWhere(['like', 'assess_step_detail', $this->assess_step_detail])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
