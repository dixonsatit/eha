<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Assessment;

/**
 * AssessmentSearch represents the model behind the search form about `common\models\Assessment`.
 */
class AssessmentSearch extends Assessment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sop_id', 'selfassess_score', 'assessor_score', 'created_by', 'updated_by'], 'integer'],
            [['locality_code', 'selfassess_date', 'assessor_date', 'assessor_notes', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Assessment::find()->joinWith('sop')->joinWith('sop.issueCode')->joinWith('sop.issueCode.mainIssueCode');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sop_id' => $this->sop_id,
            'selfassess_score' => $this->selfassess_score,
            'selfassess_date' => $this->selfassess_date,
            'assessor_score' => $this->assessor_score,
            'assessor_date' => $this->assessor_date,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'assessor_notes', $this->assessor_notes])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
