<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ApplicationForm;

/**
 * ApplicationFormSearch represents the model behind the search form of `common\models\ApplicationForm`.
 */
class ApplicationFormSearch extends ApplicationForm
{
    public $province;
    public $district;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'be_year', 'created_by', 'updated_by'], 'integer'],
            [['locality_code', 'province', 'district', 'contact_person', 'contact_title', 'contact_phone', 'contact_mobile', 'contact_email', 'apply_person', 'apply_title', 'apply_issues', 'apply_date', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApplicationForm::find()->joinWith('locality')->joinWith('locality.provinceCode')->joinWith('locality.districtCode');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['be_year' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'be_year' => $this->be_year,
            'apply_date' => $this->apply_date,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'locality.locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'district.district_code', $this->district])
            ->andFilterWhere(['like', 'province.province_code', $this->province])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'contact_title', $this->contact_title])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'apply_person', $this->apply_person])
            ->andFilterWhere(['like', 'apply_title', $this->apply_title])
            ->andFilterWhere(['like', 'apply_issues', $this->apply_issues])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
