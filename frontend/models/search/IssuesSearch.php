<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Issues;

/**
 * IssuesSearch represents the model behind the search form of `common\models\Issues`.
 */
class IssuesSearch extends Issues
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['issue_code', 'issue_name', 'issue_desc', 'main_issue_code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Issues::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'issue_code', $this->issue_code])
            ->andFilterWhere(['like', 'issue_name', $this->issue_name])
            ->andFilterWhere(['like', 'issue_desc', $this->issue_desc])
            ->andFilterWhere(['like', 'main_issue_code', $this->main_issue_code]);

        return $dataProvider;
    }
}
