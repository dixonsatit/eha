<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kpi_range".
 *
 * @property integer $id
 * @property string $year
 * @property string $year_en
 * @property integer $kpi_template_id
 * @property string $condition
 * @property string $value
 * @property string $description
 * @property string $range_no
 * @property string $range_min
 * @property string $range_max
 * @property string $range_color
 * @property string $range_desc
 */
class KpiRange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_range';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year_en', 'kpi_template_id'], 'required'],
            [['kpi_template_id'], 'integer'],
            [['range_no', 'range_min', 'range_max'], 'number'],
            [['year', 'year_en'], 'string', 'max' => 4],
            [['condition', 'value', 'description', 'range_desc'], 'string', 'max' => 255],
            [['range_color'], 'string', 'max' => 20],
            [['year', 'kpi_template_id', 'range_no'], 'unique', 'targetAttribute' => ['year', 'kpi_template_id', 'range_no'], 'message' => 'The combination of Year, Kpi Template ID and Range No has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Year',
            'year_en' => 'Year En',
            'kpi_template_id' => 'Kpi Template ID',
            'condition' => 'Condition',
            'value' => 'Value',
            'description' => 'Description',
            'range_no' => 'Range No',
            'range_min' => 'Range Min',
            'range_max' => 'Range Max',
            'range_color' => 'Range Color',
            'range_desc' => 'Range Desc',
        ];
    }
}
