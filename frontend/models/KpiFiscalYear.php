<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "kpi_fiscal_year".
 *
 * @property integer $id
 * @property string $year
 * @property integer $year_buddhism
 * @property string $description
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class KpiFiscalYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_fiscal_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year'], 'safe'],
            [['year_buddhism', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['created_by', 'created_at', 'updated_by', 'updated_at'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'ปี ค.ศ.',
            'year_buddhism' => 'ปี พ.ศ.',
            'description' => 'รายละเอียด',
            'created_by' => 'สร้างโดย',
            'created_at' => 'สร้างเมื่อ',
            'updated_by' => 'แก้ไขโดย',
            'updated_at' => 'แก้ไขเมื่อ',
        ];
    }

    public static function GetList() {
        return yii\helpers\ArrayHelper::map(self::find()->orderBy('year DESC')->all(), 'year_buddhism', 'year_buddhism');
    }

}
