<?php
namespace frontend\models;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use common\models\User;
use Yii;
/**
 * Dashboard
 */
class ChartDashboard extends Component
{

    public function getChartData($sql){
        $data = [];
        $models = Yii::$app->db->createCommand($sql)->queryAll();
        if(is_array($models)){
            $data = ArrayHelper::getColumn($models, function ($m) {
                return [$m['areaname'], (float) $m['result']];
            });
        }
        return $data;
    }

    public function getChart1()
    {
      $sql = "-- Q3-03. ร้อยละของเด็กอายุ 0-5 ปี สูงดีสมส่วน - รายเขต
                SELECT 
                    Z.region_code as areacode, Z.region_name as areaname,
                    SUM(K.targetq1+K.targetq2+K.targetq3+K.targetq4) as target,  
                    SUM(K.resultq1+K.resultq2+K.resultq3+K.resultq4) / SUM(K.targetq1+K.targetq2+K.targetq3+K.targetq4) * 100 as result, 
                    SUM(K.b2q1+K.b2q2+K.b2q3+K.b2q4) as b2, 
                    SUM(K.a2q1+K.a2q2+K.a2q3+K.a2q4) as a2,
                    SUM(K.b3q1+K.b3q2+K.b3q3+K.b3q4) as b3, 
                    SUM(K.a3q1+K.a3q2+K.a3q3+K.a3q4) as a3, 
                    SUM(K.targetq1) as targetq1, SUM(K.targetq2) as targetq2, 
                    SUM(K.targetq3) as targetq3, SUM(K.targetq4) as targetq4, 
                    SUM(K.resultq1) as resultq1, SUM(K.resultq2) as resultq2, 
                    SUM(K.resultq3) as resultq3, SUM(K.resultq4) as resultq4, 
                    SUM(K.b2q1) as b2q1, SUM(K.b2q2) as b2q2, 
                    SUM(K.b2q3) as b2q3, SUM(K.b2q4) as b2q4, 
                    SUM(K.a2q1) as a2q1, SUM(K.a2q2) as a2q2, 
                    SUM(K.a2q3) as a2q3, SUM(K.a2q4) as a2q4, 
                    SUM(K.b3q1) as b3q1, SUM(K.b3q2) as b3q2, 
                    SUM(K.b3q3) as b3q3, SUM(K.b3q4) as b3q4, 
                    SUM(K.a3q1) as a3q1, SUM(K.a3q2) as a3q2, 
                    SUM(K.a3q3) as a3q3, SUM(K.a3q4) as a3q4 
                FROM s_kpi_height05 K 
                INNER JOIN cchangwat C ON C.changwatcode=LEFT(K.areacode,2) 
                INNER JOIN cregion Z ON Z.region_code=C.regioncode  
                WHERE K.id = '67e41dbb1ce5d844d49f6b7b10e30d01' 
                    AND K.b_year = '2559' 
                GROUP BY Z.region_code;";
      return $this->getChartData($sql);
    }
    public function getChart2()
    {
      $sql = "-- Q3-13. อัตราการคลอดในหญิงอายุ 15-19 ปี - รายเขต
        SELECT 
          Z.region_code as areacode, Z.region_name as areaname,
          SUM(K.target) as target, 
		  SUM(K.result) / SUM(K.target) * 1000 as result, 
          SUM(K.result1) as resultq1, SUM(K.result2) as resultq2, 
          SUM(K.result3) as resultq3, SUM(K.result4) as resultq4 
        FROM s_kpi_lborn15_19 K 
        INNER JOIN cchangwat C ON C.changwatcode=LEFT(K.areacode,2) 
        INNER JOIN cregion Z ON Z.region_code=C.regioncode  
        WHERE K.id = 'de7a5a9263961f3a75195680a122e43a' 
          AND K.b_year = '2559' 
        GROUP BY Z.region_code;";
      return $this->getChartData($sql);
    }
    public function getChart3()
    {
      $sql = "-- Q3-14. ร้อยละของการตั้งครรภ์ซ้ำในวัยรุ่นอายุ 15-19 ปี - รายเขต
      SELECT 
        Z.region_code as areacode, Z.region_name as areaname, 
        SUM(K.target) as target, 
		SUM(K.result) / SUM(K.target) * 100 as result, 
        SUM(K.target1) as targetq1, SUM(K.result1) as resultq1, 
        SUM(K.target2) as targetq2, SUM(K.result2) as resultq2, 
        SUM(K.target3) as targetq3, SUM(K.result3) as resultq3  
      FROM s_kpi_labor_repeate K 
      INNER JOIN cchangwat C ON C.changwatcode=LEFT(K.areacode,2) 
      INNER JOIN cregion Z ON Z.region_code=C.regioncode  
      WHERE K.id = '66a5c898c2b8c97a327ff2941f1fde7f' 
        AND K.b_year = '2559' 
      GROUP BY Z.region_code;
      ";
      return $this->getChartData($sql);
    }
    
    public function getChart4()
    {
      $sql = "-- Q3-15. ร้อยละของวัยทำงานอายุ 30-44 ปี มีค่าดัชนีมวลกายปกติ - รายเขต
        SELECT 
          Z.region_code as areacode, Z.region_name as areaname, 
          SUM(K.target) as target, 
          SUM(K.result) / SUM(K.target) * 100 as result 
        FROM s_kpi_bmi K 
        INNER JOIN cchangwat C ON C.changwatcode=LEFT(K.areacode,2) 
        INNER JOIN cregion Z ON Z.region_code=C.regioncode  
        WHERE K.id = 'e2d0b1a802a956529b7d1d0f9516313f' 
          AND K.b_year = '2559' 
        GROUP BY Z.region_code;
        ";
      return $this->getChartData($sql);
    }

}