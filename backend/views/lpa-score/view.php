<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScore */

$this->title = $model->be_year;
$this->params['breadcrumbs'][] = ['label' => 'Lpa Scores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpa-score-view">

    <p>
        <?php echo Html::a('Update', ['update', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'be_year',
            'locality_code',
            'component1_score',
            'component2_score',
            'component3_score',
            'component4_score',
            'component5_score',
            'component1to5_avgscore',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
