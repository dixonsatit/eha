<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScoreSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="lpa-score-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'be_year') ?>

    <?php echo $form->field($model, 'locality_code') ?>

    <?php echo $form->field($model, 'component1_score') ?>

    <?php echo $form->field($model, 'component2_score') ?>

    <?php // echo $form->field($model, 'component3_score') ?>

    <?php // echo $form->field($model, 'component4_score') ?>

    <?php // echo $form->field($model, 'component5_score') ?>

    <?php // echo $form->field($model, 'component1to5_avgscore') ?>

    <?php // echo $form->field($model, 'created_by_ip') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_by_ip') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
