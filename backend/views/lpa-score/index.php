<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LpaScoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lpa Scores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpa-score-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Lpa Score', ['create'], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Import Lpa Score', ['import'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'be_year',
                'filter'=> $year
            ],
            'locality_code',
            'component1_score',
            'component2_score',
            'component3_score',
            'component4_score',
            'component5_score',
            'component1to5_avgscore',
            // 'created_by_ip',
            // 'created_by',
            // 'created_at',
            // 'updated_by_ip',
            // 'updated_by',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
