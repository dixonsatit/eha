<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScore */

$this->title = 'Update Lpa Score: ' . ' ' . $model->be_year;
$this->params['breadcrumbs'][] = ['label' => 'Lpa Scores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->be_year, 'url' => ['view', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpa-score-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
