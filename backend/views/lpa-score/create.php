<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LpaScore */

$this->title = 'Create Lpa Score';
$this->params['breadcrumbs'][] = ['label' => 'Lpa Scores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpa-score-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
