<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScore */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="lpa-score-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'be_year')->textInput() ?>

    <?php echo $form->field($model, 'locality_code')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component1_score')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component2_score')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component3_score')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component4_score')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component5_score')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'component1to5_avgscore')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'created_by_ip')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'created_by')->textInput() ?>

    <?php echo $form->field($model, 'created_at')->textInput() ?>

    <?php echo $form->field($model, 'updated_by_ip')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'updated_by')->textInput() ?>

    <?php echo $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
