<?php

/**
 * @var $this yii\web\View
 * @var $content string
 */

use yii\helpers\ArrayHelper;
use lavrentiev\widgets\toastr\Notification;

?>
<?php $this->beginContent('@backend/views/layouts/common.php'); ?>
<div class="box">
    <div class="box-body">

        <?php if (Yii::$app->session->hasFlash('alert')) : ?>
            <?php echo Notification::widget([
                'type' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'type'),
                'title' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'title'),
                'message' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'message'),
                'options' => [
                    "closeButton" => true,
                    "debug" => false,
                    "newestOnTop" => true,
                    "progressBar" => false,
                    "positionClass" => "toast-bottom-right",
                    "preventDuplicates" => false,
                    "onclick" => null,
                    "showDuration" => "5000",
                    "hideDuration" => "1000",
                    "timeOut" => "5000",
                    "extendedTimeOut" => "1000",
                    "showEasing" => "swing",
                    "hideEasing" => "linear",
                    "showMethod" => "fadeIn",
                    "hideMethod" => "fadeOut"
                ]
            ]) ?>
        <?php endif; ?>
  
        <?php
        // echo \yii\bootstrap\Alert::widget([
        //     'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
        //     'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
        // ]) ;
        ?>
        <?php //endif; 
        ?>
        <?php echo $content ?>
    </div>
</div>
<?php $this->endContent(); ?>