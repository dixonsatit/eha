<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\SopSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="sop-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'parent_id') ?>

    <?php echo $form->field($model, 'component_code') ?>

    <?php echo $form->field($model, 'be_year') ?>

    <?php echo $form->field($model, 'issue_code') ?>

    <?php // echo $form->field($model, 'assess_step') ?>

    <?php // echo $form->field($model, 'assess_step_detail') ?>

    <?php // echo $form->field($model, 'assess_score') ?>

    <?php // echo $form->field($model, 'created_by_ip') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_by_ip') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
