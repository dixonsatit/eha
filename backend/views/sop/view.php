<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sop */

$this->title = $model->be_year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sop-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Update'), ['update', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'be_year' => $model->be_year, 'issue_code' => $model->issue_code, 'assess_step' => $model->assess_step], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_id',
            'component_code',
            'be_year',
            'issue_code',
            'assess_step',
            'assess_step_detail',
            'assess_score',
            'created_by_ip',
            'created_by',
            'created_at',
            'updated_by_ip',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
