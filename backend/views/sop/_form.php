<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\Sop */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="sop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-md-4">
            <?php echo $form->field($model, 'parent_id')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->field($model, 'component_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php echo $form->field($model, 'be_year')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?php echo $form->field($model, 'issue_code')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?php echo $form->field($model, 'assess_step')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-2">
        <?php echo $form->field($model, 'assess_score')->textInput() ?>
        </div>

        <div class="col-md-2">
            <?php echo $form->field($model, 'assess_type')->dropDownList([
                'text' => 'Text',
                'number' => 'Number',
                'radio' => 'Radio',
                'checkbox' => 'Checkbox',
                ]); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'assess_step_detail')->widget(Widget::classname(), [
                'options' => [
                    'toolbar' => true,
                    'css' => 'wym.css',
                    'lang' => 'th',
                    'minHeight' => 300,

                ],
                'plugins' => [
                    'fullscreen',
                    'clips'
                ]
            ])->label('รายละเอียด');
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'assess_step_evidence')->widget(Widget::classname(), [
                'options' => [
                    'toolbar' => true,
                    'css' => 'wym.css',
                    'lang' => 'th',
                    'minHeight' => 300,
                ],
                'plugins' => [
                    'fullscreen',
                    'clips'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo $form->field($model, 'assess_type_value')->widget(Widget::classname(), [
                'options' => [
                    'toolbar' => true,
                    'css' => 'wym.css',
                    'lang' => 'th',
                    'minHeight' => 300,
                ],
                'plugins' => [
                    'fullscreen',
                    'clips'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="form-group"> 
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', '<i class="fa fa-save"></i> บันทึก') : Yii::t('backend', '<i class="fa fa-pencil"></i> แก้ไข'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>




<?php
$js = <<< JS

$(function() {
    $.fn.size = function() {
        return this.length;
    }
})
    
JS;
$this->registerJS($js);
?>