<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sop */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Sop',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sop-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
