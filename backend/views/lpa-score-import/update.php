<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScoreImport */

$this->title = 'Update Lpa Score Import: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lpa Score Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lpa-score-import-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
