<?php

use backend\models\LpaScoreImport;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LpaScoreImportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lpa Score Imports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpa-score-import-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('นำเข้าข้อมูล LPA Scores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'ปี',
            'filter' => ArrayHelper::map(LpaScoreImport::find()->asArray()->all(), 'year', 'year'),
            'attribute' => 'year',
        ],
        'created_at:dateTime',
        'total_record',
        [
            'class' => 'yii\grid\ActionColumn',
            'options' => ['style' => 'width:240px;'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'template' => '<div class="btn-group btn-group-sm text-center" role="group"> {import} {view} {update} {delete} </div>',
            'buttons'=> [
                'import' => function ($url, $model, $key) {
                    return Html::a('<i class="glyphicon glyphicon-transfer"> </i> ดูตัวอย่างข้อมูล ', ['preview', 'id'=>$model->id], ['class'=>'btn btn-default']);
                }
            ]
        ],
    ],
]); ?>

</div>
