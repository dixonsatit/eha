<?php 
use yii\grid\GridView;
use yii\helpers\Html;
?>
<h3>ตัวอย่างข้อมูลก่อนนำเข้า</h3>
<div class="text-right">
<?php echo Html::a('<i class="glyphicon glyphicon-transfer"></i> นำเข้าข้อมูลชุดนี้', ['imports','id'=>$model->id], ['class'=>'btn btn-success']);?>
</div>

<br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
]) ?>