<?php

use yii\helpers\Html;
use kartik\grid\GridView;
?>
<h3>ข้อมูล Lpa Score</h3>

<br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'panel' => [
        'before' => ' '
    ],
    'columns' => [
        [
            'attribute' => 'be_year',
            'label' => 'Be Year'
        ],
        [
            'attribute' => 'locality_code',
            'label' => 'Locality Code'
        ],
        [
            'attribute' => 'component1_score',
            'label' => 'Component1 Score'
        ],
        [
            'attribute' => 'component2_score',
            'label' => 'Component2 Score'
        ],
        [
            'attribute' => 'component3_score',
            'label' => 'Component3 Score'
        ],
        [
            'attribute' => 'component4_score',
            'label' => 'Component4 Score'
        ],
        [
            'attribute' => 'component5_score',
            'label' => 'Component5 Score'
        ],
        [
            'attribute' => 'component1to5_avgscore',
            'label' => 'Avg Score'
        ],
    ]
]) ?>