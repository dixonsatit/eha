<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LpaScoreImport */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="lpa-score-import-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?php echo $form->field($model, 'year')->dropDownList($model->createRangeYear(date('Y', strtotime('+1 year'))+543)) ?>
        </div>
    </div>
    
    <?= $form->field($model, 'file')->widget(\trntv\filekit\widget\Upload::classname(), [
        'url'=>['file-upload'],
        'id'=>'wperson-photo'
    ]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
