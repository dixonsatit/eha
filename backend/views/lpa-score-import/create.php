<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LpaScoreImport */

$this->title = 'Create Lpa Score Import';
$this->params['breadcrumbs'][] = ['label' => 'Lpa Score Imports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpa-score-import-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
