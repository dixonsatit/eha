<?php

use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use common\models\Locality;
use yii\web\JsExpression;
use common\models\Office;

/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin() ?>
    <?php echo $form->field($model, 'username') ?>
    <div class="row">
        <div class="col-md-6"> <?php echo $form->field($profile, 'firstname') ?></div>
        <div class="col-md-6"> <?php echo $form->field($profile, 'lastname') ?></div>
    </div>

    <?php
    $officeDesc = empty($profile->office_id) ? '' : Office::find()->where(['office_id' => $profile->office_id])->one()->office_name;
    echo $form->field($profile, 'office_id')->widget(Select2::classname(), [
        'initValueText' => $officeDesc, // set the initial display text
        'options' => ['placeholder' => 'ค้นหาสำนักงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['office-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
            'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
        ],
    ])->label('สำนักงาน');
    ?>

    <?php
    $localDesc = empty($profile->locality_code) ? '' : Locality::findOne($profile->locality_code)->locality_name;
    echo $form->field($profile, 'locality_code')->widget(Select2::classname(), [
        'initValueText' => $localDesc, // set the initial display text
        'options' => ['placeholder' => 'ค้นหาหน่วยงาน พิมพ์ตัวอักศรอย่างน้อย 2 ตัว'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['local-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(locality) { return locality.text; }'),
            'templateSelection' => new JsExpression('function (locality) { return locality.text; }'),
        ],
    ])->label('หน่วยงาน');
    ?>

    <?php echo $form->field($model, 'email') ?>
    <?php echo $form->field($model, 'password')->passwordInput() ?>
    <?php echo $form->field($model, 'status')->dropDownList(User::statuses()) ?>
    <?php echo $form->field($model, 'roles')->checkboxList($roles) ?>
    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
    </div>
    <?php ActiveForm::end() ?>

</div>
