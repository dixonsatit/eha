<?php

use backend\modules\rbac\models\RbacAuthItem;
use common\grid\EnumColumn;
use common\models\Office;
use common\models\User;
use kartik\select2\Select2;
use trntv\yii\datetime\DateTimeWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\dialog\Dialog;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'ผู้ใช้งาน');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>
    <p>
        <?php echo Html::a(Yii::t('backend', '<i class="fa fa-plus"></i> เพิ่ม{modelClass}', [
            'modelClass' => 'ผู้ใช้',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '-',
        ],
        'options' => [
            'class' => 'grid-view table-responsive'
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['style' => 'width:50px;'],
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'attribute' => 'username',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<strong>' . $model->username . '</strong><br><small>' . ($model->userProfile ? $model->userProfile->fullName : '') . '</small>';
                }
            ],
            [
                'label' => 'อีเมล์',
                'format' => 'raw',
                'attribute' => 'email',
            ],
            [
                'label' => 'สถานะ',
                'class' => EnumColumn::class,
                'attribute' => 'status',
                'enum' => User::statuses(),
                'filter' => User::statuses()
            ],
            [
                'label' => 'สิทธิ์',
                'filter' => ArrayHelper::map(RbacAuthItem::find()->where(['type' => 1])->all(), 'name', 'description'),
                'attribute' => 'role',
                'value' => function ($data) {
                    return isset($data->role->itemName->description) ? $data->role->itemName->description : '';
                }
            ],

            [
                'label' => 'หน่วยงาน',
                'attribute' => 'office',
                'value' => function ($data) {
                    if (!empty($data->userProfile->office->office_id)) {
                        return isset($data->userProfile->office->office_name) ? $data->userProfile->office->office_name : '';
                    } else {
                        return '-';
                    }
                },
                'filter' => Select2::widget([
                    'attribute' => 'office',
                    'model' => $searchModel,
                    'value' => $searchModel->userProfile->office->office_name,
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => '-- หน่วยงาน --',
                        'style' => 'width: 100%;'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'closeOnSelect' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/user/office-list']),
                            'dataType' => 'json',
                            'data'=>new JsExpression('function(params) { return {q:params.term};}')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(prefix) { return prefix.text; }'),
                        'templateSelection' => new JsExpression('function (prefix) { return prefix.text; }'),
                    ],
                ]),
            ],

            [
                'label' => 'อปท.',
                'attribute' => 'locality',
                'value' => function ($data) {
                    if (!empty($data->userProfile->locality)) {
                        return isset($data->userProfile->locality->locality_name) ? $data->userProfile->locality->locality_name : '';
                    } else {
                        return '-';
                    }
                },
                'filter' => Select2::widget([
                    'attribute' => 'locality',
                    'model' => $searchModel,
                    // 'value' => $searchModel->userProfile->locality->locality_name,
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => '-- อปท. --',
                        'style' => 'width: 300px;'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'closeOnSelect' => true,
                        'minimumInputLength' => 3,
                        'ajax' => [
                            'url' => Url::to(['/user/locality-list']),
                            'dataType' => 'json',
                        ],
                    ],
                ]),
            ],
            // [
            //     'attribute' => 'localityCode.province_code',
            //     'label' => 'จังหวัด',
            //     'value' => function ($data) {
            //         if (!empty($data->userProfile->office_id)) {
            //             return $data->userProfile->office->provinceCode->province_name;
            //         }
            //         if (!empty($data->userProfile->locality_code)) {
            //             return $data->userProfile->locality->provinceCode->province_name;
            //         } else {
            //             return '-';
            //         }
            //     },
            //     'filter' => Select2::widget([
            //         'name' => 'userProfile.localityCode.province_code',
            //         'model' => $searchModel,
            //         'value' => $searchModel->userProfile->localityCode->province_code,
            //         'data' =>  \yii\helpers\ArrayHelper::map(\common\models\Province::find()->asArray()->all(), 'province_code', 'province_name'),
            //         'size' => Select2::MEDIUM,
            //         'options' => [
            //             'placeholder' => '-- จังหวัด --',
            //             'style' => 'width: 100%;'
            //         ],
            //         'pluginOptions' => [
            //             'allowClear' => true
            //         ],
            //         // 'addon' => [
            //         //     'prepend' => [
            //         //         'content' => FAS::icon('open')
            //         //     ]
            //         // ],
            //     ]),
            // ],
            [
                'attribute' => 'logged_at',
                'label' => 'เข้าระบบล่าสุด',
                'format' => 'datetime',
                'filter' => DateTimeWidget::widget([
                    'model' => $searchModel,
                    'attribute' => 'logged_at',
                    'phpDatetimeFormat' => 'dd.MM.yyyy',
                    'momentDatetimeFormat' => 'DD.MM.YYYY',
                    'clientEvents' => [
                        'dp.change' => new JsExpression('(e) => $(e.target).find("input").trigger("change.yiiGridView")')
                    ],
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'noWrap' => true
                ],
                'template' => ' {reset-password} {view} {update} {delete} ',
                'buttons' => [
                    'login' => function ($url) {
                        return Html::a(
                            '<i class="fa fa-sign-in" aria-hidden="true"></i>',
                            $url,
                            [
                                'title' => Yii::t('backend', 'Login')
                            ]
                        );
                    },

                    'reset-password' => function ($url, $data) {
                        return Html::a('', Url::to(['/user/reset-password']), [
                            'class'       => 'glyphicon glyphicon-cog btn-reset-password',
                            'data-id'     => $data->id,
                        ]);
                    },
                    // 'urlCreator'     => function ($action, $model, $key, $index) {
                    //     $url = Url::to(['/index/reset-password', 'id' => $model->id]);
                    //     return $url;
                    // },
                ],
                'visibleButtons' => [
                    'login' => Yii::$app->user->can('administrator')
                ]

            ],
        ],
    ]); ?>
</div>
<?php echo Dialog::widget(); ?>
<?php Modal::begin([
    'header' => '<h2 class="modal-title"></h2>',
    'id'     => 'modal-reset-password',
    'footer' => Html::a('ตกลง', '', ['class' => 'btn btn-primary', 'id' => 'reset-confirm']),
]); ?>

<?= 'คุณต้องการ รีเซ็ตรหัสผ่านของผู้ใช้นี้ ใช่หรือไม่ ?'; ?>

<?php Modal::end(); ?>

<?php
Modal::begin([
    'id' => 'modal-confirm',
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ],
    'footer' => Html::a('ตกลง', '', ['class' => 'btn btn-primary', 'id' => 'reset-confirm']),
]);
?>
<?php echo "<div id='modalConfirmContent'>คุณต้องการ รีเซ็ตรหัสผ่านของผู้ใช้นี้ ใช่หรือไม่ ? </div>"; ?>

<?php Modal::end(); ?>

<?php
$js = <<< JS
$('.btn-reset-password').on('click', function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    var data = $(this).data("id");
    console.log(url);
    console.log(data);
    krajeeDialog.confirm("คุณแน่ใจหรือไม่ว่าต้องการรีเซ็ตรหัสผ่านของผู้ใช้งานเป็น P@ssw0rd", function (result) {
    if (result) {
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {id: data}
            })
            .done(function(response) {
                if (response.data.success == true) {
                    console.log("reset password commented");
                }
            })
            .fail(function() {
                console.log("error");
            });

    } else { // confirmation was cancelled
        // execute your code for cancellation
    }
});

});
JS;
$this->registerJS($js);
?>