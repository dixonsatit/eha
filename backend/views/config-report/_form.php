<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigReport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-report-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'config_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'headline1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'headline2')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'headline3')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'headline4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'headline5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'footer5')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
