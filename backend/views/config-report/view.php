<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigReport */

$this->title = $model->config_id;
$this->params['breadcrumbs'][] = ['label' => 'Config Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->config_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->config_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'config_id',
            'config_name',
            'headline1',
            'headline2',
            'headline3',
            'headline4',
            'headline5',
            'footer1',
            'footer2',
            'footer3',
            'footer4',
            'footer5',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>
