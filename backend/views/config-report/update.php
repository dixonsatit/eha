<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ConfigReport */

$this->title = 'Update Config Report: ' . $model->config_id;
$this->params['breadcrumbs'][] = ['label' => 'Config Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->config_id, 'url' => ['view', 'id' => $model->config_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="config-report-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
