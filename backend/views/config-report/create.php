<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ConfigReport */

$this->title = 'Create Config Report';
$this->params['breadcrumbs'][] = ['label' => 'Config Reports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
