<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[LpaScore]].
 *
 * @see LpaScore
 */
class LpaScoreQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return LpaScore[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return LpaScore|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
