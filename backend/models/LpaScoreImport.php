<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "lpa_score_import".
 *
 * @property int $id
 * @property string $file_name
 * @property int $total_record
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class LpaScoreImport extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpa_score_import';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'created_by',
            ],
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'filesStorage' => 'fileStorage',
                'attribute' => 'file',
                'pathAttribute' => 'file_path',
                'baseUrlAttribute' => 'file_base_url',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file', 'year'], 'required'],
            [['file','created_at', 'updated_at'], 'safe'],
            [['total_record', 'created_by'], 'integer'],
            [['file_path', 'file_base_url'], 'string', 'max' => 255],
            [['year'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'ชื่อไฟล์',
            'total_record' => 'จำนวน Record',
            'created_by' => 'ผู้บันทึก',
            'created_at' => 'วันที่บันทึก',
            'updated_at' => 'วันที่แก้ไข',
            'year' => 'ปี'
        ];
    }

    /**
     * {@inheritdoc}
     * @return LpaScoreImportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LpaScoreImportQuery(get_called_class());
    }

    public function createRangeYear($end, $start = 2559)
    {
        $year = [];
        for ($i=$start; $i<=$end; $i++) {
            $year[$i] = $i;
        }
        return $year;
    }

    public function dataProvider($data)
    {
        $provider = new \yii\data\ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 10000,
            ],
        ]);
        return $provider;
    }
}
