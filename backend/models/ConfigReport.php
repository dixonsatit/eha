<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "config_report".
 *
 * @property int $config_id
 * @property string $config_name
 * @property string $headline1
 * @property string $headline2
 * @property string $headline3
 * @property string $headline4
 * @property string $headline5
 * @property string $footer1
 * @property string $footer2
 * @property string $footer3
 * @property string $footer4
 * @property string $footer5
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 */
class ConfigReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['config_name'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['config_name', 'headline1', 'headline2', 'headline3', 'headline4', 'headline5', 'footer1', 'footer2', 'footer3', 'footer4', 'footer5'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'config_id' => 'Config ID',
            'config_name' => 'Config Name',
            'headline1' => 'Headline1',
            'headline2' => 'Headline2',
            'headline3' => 'Headline3',
            'headline4' => 'Headline4',
            'headline5' => 'Headline5',
            'footer1' => 'Footer1',
            'footer2' => 'Footer2',
            'footer3' => 'Footer3',
            'footer4' => 'Footer4',
            'footer5' => 'Footer5',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
