<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Locality;

/**
 * LocalitySearch represents the model behind the search form of `common\models\Locality`.
 */
class LocalitySearch extends Locality
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['locality_code', 'locality_name', 'locality_shortname', 'administrator', 'administrative_title', 'type_code', 'address', 'subdistrict_code', 'district_code', 'province_code', 'postal_code', 'active_status', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //ถ้าเป็นระดับเขตเห็นในเขตของตน office_region
        if (Yii::$app->user->can('director') && !Yii::$app->user->can('administrator')) {
            $office_region = Yii::$app->user->identity->userProfile->office->office_region;
            $query = Locality::find()->joinWith('applicationForm')->joinWith('provinceCode')->andWhere(['province.region_code' => $office_region]);
        }
        //ถ้าเป็นระดับสสจของตนอยู่ในเขตรับผิดชอบ สสจ. province_code
        else if (Yii::$app->user->can('manager') && !Yii::$app->user->can('administrator')) {
            $province_code = Yii::$app->user->identity->userProfile->office->province_code;
            $query = Locality::find()->joinWith('applicationForm')->joinWith('provinceCode')->andWhere(['locality.province_code' => $province_code]);
        }
        //ถ้าเป็นระดับผู้ดูแลระบบ
        else if (Yii::$app->user->can('administrator')) {
            $query = Locality::find()->joinWith('applicationForm');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'locality.locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'locality_name', $this->locality_name])
            ->andFilterWhere(['like', 'locality_shortname', $this->locality_shortname])
            ->andFilterWhere(['like', 'administrator', $this->administrator])
            ->andFilterWhere(['like', 'administrative_title', $this->administrative_title])
            ->andFilterWhere(['like', 'type_code', $this->type_code])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'subdistrict_code', $this->subdistrict_code])
            ->andFilterWhere(['like', 'district_code', $this->district_code])
            ->andFilterWhere(['=', 'locality.province_code', $this->province_code])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'active_status', $this->active_status])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
