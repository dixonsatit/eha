<?php

namespace backend\models\search;

use common\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{

    public $office;
    public $province;
    public $locality;
    public $role;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['created_at', 'updated_at', 'logged_at'], 'filter', 'filter' => 'strtotime', 'skipOnEmpty' => true],
            [['created_at', 'updated_at', 'logged_at'], 'default', 'value' => null],
            [['username', 'auth_key', 'password_hash', 'email', 'office', 'locality', 'province', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['userProfile', 'userProfile.locality', 'userProfile.office', 'userProfile.locality.provinceCode', 'role']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=> ['id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 50
            ]

        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        if ($this->created_at !== null) {
            $query->andFilterWhere(['between', 'created_at', $this->created_at, $this->created_at + 3600 * 24]);
        }

        if ($this->updated_at !== null) {
            $query->andFilterWhere(['between', 'updated_at', $this->updated_at, $this->updated_at + 3600 * 24]);
        }

        if ($this->logged_at !== null) {
            $query->andFilterWhere(['between', 'logged_at', $this->logged_at, $this->logged_at + 3600 * 24]);
        }

        $query->andWhere(' user.username like :username or user_profile.firstname like :username or user_profile.lastname like :username', [
            ':username' => '%'.$this->username.'%'
        ]);
        $query->andFilterWhere(['like', 'email', $this->email])
        ->andFilterWhere(['user_profile.office_id' => $this->office])
        ->andFilterWhere(['user_profile.locality_code' => $this->locality])
        ->andFilterWhere(['locality.province_code' => $this->province])
        ->andFilterWhere(['office.province_code' => $this->province])
        ->andFilterWhere(['rbac_auth_assignment.item_name' => $this->role]);

        return $dataProvider;
    }
}
