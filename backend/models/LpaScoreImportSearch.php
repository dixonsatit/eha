<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LpaScoreImport;

/**
 * LpaScoreImportSearch represents the model behind the search form about `backend\models\LpaScoreImport`.
 */
class LpaScoreImportSearch extends LpaScoreImport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'total_record', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['file_path','file_base_url', 'year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LpaScoreImport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'total_record' => $this->total_record,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name]);

        return $dataProvider;
    }
}
