<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lpa_score".
 *
 * @property int $id เลขที่
 * @property int $be_year ปีงบประมาณ (พ.ศ.)
 * @property string $locality_code รหัส อปท.
 * @property string $component1_score คะแนน LPA องค์ประกอบที่ 1 (ร้อยละ)
 * @property string $component2_score คะแนน LPA องค์ประกอบที่ 2 (ร้อยละ)
 * @property string $component3_score คะแนน LPA องค์ประกอบที่ 3 (ร้อยละ)
 * @property string $component4_score คะแนน LPA องค์ประกอบที่ 4 (ร้อยละ)
 * @property string $component5_score คะแนน LPA องค์ประกอบที่ 5 (ร้อยละ)
 * @property string $component1to5_avgscore คะแนน LPA เฉลี่ยองค์ประกอบที่ 1-5 (ร้อยละ)
 * @property string $created_by_ip สร้างโดย ip
 * @property int $created_by สร้างโดย
 * @property string $created_at สร้างเมื่อ
 * @property string $updated_by_ip แก้ไขโดย ip
 * @property int $updated_by แก้ไขโดย
 * @property string $updated_at แก้ไขเมื่อ
 */
class LpaScore extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lpa_score';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['be_year', 'locality_code'], 'required'],
            [['be_year', 'created_by', 'updated_by'], 'integer'],
            [['component1_score', 'component2_score', 'component3_score', 'component4_score', 'component5_score', 'component1to5_avgscore'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['locality_code'], 'string', 'max' => 8],
            [['created_by_ip', 'updated_by_ip'], 'string', 'max' => 50],
            [['be_year', 'locality_code'], 'unique', 'targetAttribute' => ['be_year', 'locality_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'be_year' => 'Be Year',
            'locality_code' => 'Locality Code',
            'component1_score' => 'Score 1',
            'component2_score' => 'Score 2',
            'component3_score' => 'Score 3',
            'component4_score' => 'Score 4',
            'component5_score' => 'Score 5',
            'component1to5_avgscore' => 'AvgScore',
            'created_by_ip' => 'Created By Ip',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by_ip' => 'Updated By Ip',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LpaScoreQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LpaScoreQuery(get_called_class());
    }
}
