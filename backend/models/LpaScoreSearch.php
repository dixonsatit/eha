<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LpaScore;

/**
 * LpaScoreSearch represents the model behind the search form about `backend\models\LpaScore`.
 */
class LpaScoreSearch extends LpaScore
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'be_year', 'created_by', 'updated_by'], 'integer'],
            [['locality_code', 'created_by_ip', 'created_at', 'updated_by_ip', 'updated_at'], 'safe'],
            [['component1_score', 'component2_score', 'component3_score', 'component4_score', 'component5_score', 'component1to5_avgscore'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LpaScore::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'be_year' => $this->be_year,
            'component1_score' => $this->component1_score,
            'component2_score' => $this->component2_score,
            'component3_score' => $this->component3_score,
            'component4_score' => $this->component4_score,
            'component5_score' => $this->component5_score,
            'component1to5_avgscore' => $this->component1to5_avgscore,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'locality_code', $this->locality_code])
            ->andFilterWhere(['like', 'created_by_ip', $this->created_by_ip])
            ->andFilterWhere(['like', 'updated_by_ip', $this->updated_by_ip]);

        return $dataProvider;
    }
}
