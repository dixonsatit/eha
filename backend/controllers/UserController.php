<?php

namespace backend\controllers;

use backend\models\search\UserSearch;
use backend\models\UserForm;
use common\commands\AddToTimelineCommand;
use common\models\Locality;
use common\models\Office;
use common\models\User;
use common\models\UserProfile;
use common\models\UserToken;
use DateTime;
use yii\db\Query;
use Yii;
use yii\db\conditions\NotCondition;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use lavrentiev\widgets\toastr\Notification;
use yii\db\Expression;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        // \Yii::$app->session->setFlash('alert', [
        //     'type' => 'success',
        //     'title' => 'ผลการทำงาน',
        //     'message'=> 'บันทึกข้อมูลเสร็จเรียบร้อย!',
        // ]);

        // Notification::widget([
        //     'type' => 'success',
        //     'title' => 'ผลการทำงาน',
        //     'message' => 'สำเร็จ'
        // ]);

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\base\Exception
     * @throws NotFoundHttpException
     */
    public function actionLogin($id)
    {
        $model = $this->findModel($id);
        $tokenModel = UserToken::create(
            $model->getId(),
            UserToken::TYPE_LOGIN_PASS,
            60
        );

        return $this->redirect(
            Yii::$app->urlManagerFrontend->createAbsoluteUrl(['user/sign-in/login-by-pass', 'token' => $tokenModel->token])
        );
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();
        $profile = new UserProfile([
            'locale' => 'th',
        ]);
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($model->save($profile->attributes)) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'profile' => $profile,
            'model' => $model,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')
        ]);
    }

    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new UserForm();
        $user = $this->findModel($id);
        $profile = $user->userProfile;
        $model->setModel($user);
        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if ($profile->save(false)) {
                    return $this->redirect(['index']);
                } else {
                    VarDumper::dump($profile->getErrors());
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'profile' => $profile,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')
        ]);
    }

    public function actionLocalList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select(['locality_code AS id,  CONCAT("[",locality_code,"] ", locality_name) AS text'])
                ->from('locality')
                ->where(['like', 'locality_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Locality::find($id)->locality_name];
        }
        return $out;
    }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Yii::$app->authManager->revokeAll($id);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionOfficeList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('office_id AS id, office_name AS text')
                ->from('office')
                ->where(['like', 'office_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' =>  Office::find()->select(['office_name'])->where(['office_code' => $id])];
        }
        return $out;
    }

    public function actionLocalityList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('locality_code AS id, locality_name AS text')
                ->from('locality')
                ->where(['like', 'locality_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' =>  Locality::find()->select(['locality_name'])->where(['locality_code' => $id])];
        }
        return $out;
    }

    public function actionResetPassword()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (Yii::$app->request->post('id')) {
                $id = Yii::$app->request->post('id');
                $model = new UserForm();
                $model['password'] = "P@ssw0rd";
                $user = $this->findModel($id);
                $model->setModel($user);
                if ($user) {
                    if ($model->save()) {
                        $now = new DateTime();
                        Yii::$app->commandBus->handle(new AddToTimelineCommand([
                            'category' => 'user',
                            'event' => 'reset-password',
                            'data' => [
                                'public_identity' => $user->username,
                                'user_id' => $id,
                                'created_at' => $now->getTimestamp()
                            ]
                        ]));

                        \Yii::$app->session->setFlash('alert', [
                            'type' => 'success',
                            'title' => 'ผลการทำงาน',
                            'message' => 'บันทึกข้อมูลเสร็จเรียบร้อย!',
                        ]);

                        return [
                            'data' => [
                                'success' => true,
                                'message' => 'Model has been saved.',
                            ],
                            'code' => 0,
                        ];
                    }
                } else {
                    return [
                        'data' => [
                            'success' => false,
                            'model' => null,
                            'message' => $user->getErrors()
                        ],
                        'code' => 1,
                    ];
                }
            }
        }
    }
}
