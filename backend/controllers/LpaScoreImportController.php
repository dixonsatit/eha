<?php

namespace backend\controllers;

use backend\models\LpaScore;
use backend\models\LpaScoreImport;
use backend\models\LpaScoreImportSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LpaScoreImportController implements the CRUD actions for LpaScoreImport model.
 */
class LpaScoreImportController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'file-upload' => [
                'class' => 'trntv\filekit\actions\UploadAction',
                'deleteRoute' => 'file-delete',
            ],
            'file-delete' => [
                'class' => 'trntv\filekit\actions\DeleteAction',
                'fileStorage' => 'fileStorage',
            ],
            'file-download' => [
                'class' => 'trntv\filekit\actions\ViewAction',
            ],
        ];
    }

    /**
     * Lists all LpaScoreImport models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LpaScoreImportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLpaLists()
    {
        $searchModel = new LpaScoreImportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('lpa-lists', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LpaScoreImport model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LpaScoreImport model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LpaScoreImport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LpaScoreImport model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LpaScoreImport model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LpaScoreImport model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LpaScoreImport the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LpaScoreImport::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPreview($id)
    {
        $model = $this->findModel($id);
        $rawData = $this->readLpaData($model->file_path);
        $dataProvider = $model->dataProvider($rawData);
        return $this->render('preview', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImports($id)
    {
        $columns = [
            'be_year',
            'locality_code',
            'component1_score',
            'component2_score',
            'component3_score',
            'component4_score',
            'component5_score',
            'component1to5_avgscore',
        ];
        $model = $this->findModel($id);
        $rawData = $this->readLpaData($model->file_path, 'insearch');
        if (count($rawData) > 0) {
            Yii::$app->db->createCommand()->batchInsert('lpa_score', $columns, $rawData)->execute();
            $model->total_record = count($rawData);
            $model->save();
        }
        return $this->redirect(['index']);
    }

    public function actionExport($id)
    {
        // $model = $this->findModel($id);
        $rawData = LpaScore::find()->where(['be_year' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $rawData,
        ]);

        $dataProvider->pagination->pageSize=1000;


        return $this->render('export', [
            // 'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function readLpaData($fileName, $type = 'read')
    {
        $items = [];
        $rows = [];
        $i = 0;
        $handle = fopen(Yii::getAlias('@storage/web/source') . '/' . $fileName, "r");
        while (($fileop = fgetcsv($handle, 10000, ",")) !== false) {
            if ($i > 0) {
                $rows[] = [
                    'be_year' => $fileop[0],
                    'locality_code' => $fileop[1],
                    'component1_score' => $fileop[2],
                    'component2_score' => $fileop[3],
                    'component3_score' => $fileop[4],
                    'component4_score' => $fileop[5],
                    'component5_score' => $fileop[6],
                    'component1to5_avgscore' => $fileop[7],
                ];
                $items[] = [
                    $fileop[0],
                    $fileop[1],
                    $fileop[2],
                    $fileop[3],
                    $fileop[4],
                    $fileop[5],
                    $fileop[6],
                    $fileop[7],
                ];
            }
            $i++;
        }
        return $type == 'read' ? $rows : $items;
    }
}
