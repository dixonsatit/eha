<?php

namespace backend\controllers;

use backend\models\LpaScore;
use backend\models\LpaScoreSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LpaScoreController implements the CRUD actions for LpaScore model.
 */
class LpaScoreController extends Controller
{

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LpaScore models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LpaScoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $year = LpaScore::find()->groupBy('be_year')->select('be_year')->indexBy('be_year')->column();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'year' => $year,
        ]);
    }

    /**
     * Displays a single LpaScore model.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     */
    public function actionView($be_year, $locality_code)
    {
        return $this->render('view', [
            'model' => $this->findModel($be_year, $locality_code),
        ]);
    }

    /**
     * Creates a new LpaScore model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LpaScore();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LpaScore model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     */
    public function actionUpdate($be_year, $locality_code)
    {
        $model = $this->findModel($be_year, $locality_code);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'be_year' => $model->be_year, 'locality_code' => $model->locality_code]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LpaScore model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $be_year
     * @param string $locality_code
     * @return mixed
     */
    public function actionDelete($be_year, $locality_code)
    {
        $this->findModel($be_year, $locality_code)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LpaScore model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $be_year
     * @param string $locality_code
     * @return LpaScore the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($be_year, $locality_code)
    {
        if (($model = LpaScore::findOne(['be_year' => $be_year, 'locality_code' => $locality_code])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionHistory(){
        $searchModel = new LpaScoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
